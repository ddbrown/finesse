#ifndef KAT_KNM_BS_H
#define	KAT_KNM_BS_H

#include "kat.h"
#include "kat_optics.h"

/**
 * Enum that states the different coupling coefficients and their 
 * directions between the nodes, NOTE: the values should be not
 * cross over with those in KNM_MIRROR_NODE_DIRECTION
 */
typedef enum KNM_BS_NODE_DIRECTION {
    BS12 = 1,
    BS21 = 2,
    BS34 = 3,
    BS43 = 4,
    BS13 = 5,
    BS31 = 6,
    BS24 = 7,
    BS42 = 8
} KNM_BS_NODE_DIRECTION_t;

typedef enum KNM_BS_FLAG {
    BS12Calc = 1,
    BS21Calc = 2,
    BS34Calc = 4,
    BS43Calc = 8,
    BS13Calc = 16,
    BS31Calc = 32,
    BS24Calc = 64,
    BS42Calc = 128
} KNM_BS_FLAG;

#define CALC_BS_KNM(A, KNM) (((A)->knm_calc_flags & BS##KNM##Calc) == BS##KNM##Calc)
#define FLAG_BS_KNM(A, KNM) (A)->knm_calc_flags |= BS##KNM##Calc

/*** Contains the parameters needed to do mirror map integration using GSL */
typedef struct bs_knm_map_int_params {
    int n1, m1, n2, m2;
    
    u_nm_accel_t *acc_12_nr1_1, *acc_12_nr1_2; //accelerator for calculating k11
    u_nm_accel_t *acc_21_nr1_1, *acc_21_nr1_2; //accelerator for calculating k11
    u_nm_accel_t *acc_34_nr2_1, *acc_34_nr2_2; //accelerator for calculating k11
    u_nm_accel_t *acc_43_nr2_1, *acc_43_nr2_2; //accelerator for calculating k11
    u_nm_accel_t *acc_13_nr1_1, *acc_13_nr2_2; //accelerator for calculating k11
    u_nm_accel_t *acc_31_nr2_1, *acc_31_nr1_2; //accelerator for calculating k11
    u_nm_accel_t *acc_24_nr1_1, *acc_24_nr2_2; //accelerator for calculating k11
    u_nm_accel_t *acc_42_nr2_1, *acc_42_nr1_2; //accelerator for calculating k11

    bs_knm_q_t *knm_q;
    
    surface_merged_map_t *merged_map;
    beamsplitter_t *bs;
    
    double nr1, nr2;
    double A, Cr, Ct, Br, Bt;
    double kr, kt, k0;
    double xmin[2], xmax[2];
    
    // Factors of u_nm that depend on z,n,m,n',m' only, 1c2 part means nr1
    // medium times conjugate of nr2 medium etc.
    complex_t znm1c2;
    complex_t znm2c1;
    complex_t znm3c4;
    complex_t znm4c3;
    complex_t znm1c3;
    complex_t znm3c1;
    complex_t znm2c4;
    complex_t znm4c2;
    
    KNM_BS_NODE_DIRECTION_t knum;
    double J1, J2;
    double aperture_radius; // 0 if infinite radius
    bool polar_limits_used;
    int integrating;
    double betax, betay;
    
    bool usingMap;
    
    unsigned int knm_calc_flags;
    
} bs_knm_map_int_params_t;


inline void CHECK_BS_KNM_T(bs_knm_t *A);
bool IS_BS_KNM_ALLOCD(bs_knm_t *A);

void alloc_knm_accel_bs_mem(long int *bytes);

void bs_knm_matrix_copy(bs_knm_t* src, bs_knm_t* dest);
void bs_knm_matrix_mult(bs_knm_t* A, bs_knm_t* B, bs_knm_t *result);
void bs_knm_alloc(bs_knm_t* knm, long int *bytes);
void bs_knm_free(bs_knm_t* knm);

void compute_bs_knm_integral(beamsplitter_t *mirror, double nr1, double nr2, int integrating, int mismatch, int astigmatism);

void calculate_bs_qt_qt2(KNM_BS_NODE_DIRECTION_t KNM,  bs_knm_q_t *knm_q, complex_t qx1,
        complex_t qy1, complex_t qx2, complex_t qy2, complex_t qx3, complex_t qy3, complex_t qx4, complex_t qy4,
        double nr1, double nr2, int n1idx, int n2idx, int n3idx, int n4idx,  int cidx);

void bs_knm_matrix_ident(bs_knm_t* M);

void calc_bs_limit_ws(double *wx1,double *wx2,double *wy1,double *wy2, int knum, bs_knm_map_int_params_t *p);

bool should_integrate_bs_aperture_knm(beamsplitter_t *bs, int mismatch, int astigmatism);

void check_bs_knm_mismatch_astig(bitflag knm_calc_flags, bs_knm_q_t *knm_q, bitflag *mismatch, bitflag *astigmatism);

void get_bs_int_limit(bs_knm_map_int_params_t *p, int knum);

void get_bs_acc_zc_values(KNM_BS_NODE_DIRECTION_t knum, bs_knm_map_int_params_t *pbs, u_nm_accel_t **a1, u_nm_accel_t **a2, complex_t **zc);
#endif	/* KAT_KNM_BS_H */

