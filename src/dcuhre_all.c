/* dcuhre.f -- translated by f2c (version 20030320). 
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

/* 
 * \file dcuhre_all.c
 * \brief Dunno what this does yet.  Translated by f2c.
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */
#include "f2c.h"

/* Table of constant values */

static integer c__15 = 15;
static integer c__1 = 1;
static integer c__2 = 2;


/* Subroutine */ 
/*! Driver for the integration routine dadhre */
int dcuhre_(integer *ndim, integer *numfun, doublereal *a, 
	doublereal *b, integer *minpts, integer *maxpts, U_fp funsub, 
	doublereal *epsabs, doublereal *epsrel, integer *key, integer *nw, 
	integer *restar, doublereal *result, doublereal *abserr, integer *
	neval, integer *ifail, doublereal *work)
{
    static integer i1, i2, i3, i4, i5, i6, i7, i8, k1, k2, k3, k4, k5, k6, k7,
	     k8, num, keyf, lenw, nsub;
    static doublereal work2[648];

    /*! Integration routine
     
     The routine calculates an approximation to a given
     vector of definite integrals

      B(1) B(2)     B(NDIM)
     I    I    ... I       (F ,F ,...,F      ) DX(NDIM)...DX(2)DX(1),
      A(1) A(2)     A(NDIM)  1  2      NUMFUN

       where F = F (X ,X ,...,X    ), I = 1,2,...,NUMFUN.
              I   I  1  2      NDIM

      hopefully satisfying for each component of I the following
      claim for accuracy:
      ABS(I(K)-RESULT(K)).LE.MAX(EPSABS,EPSREL*ABS(I(K))) 
    */
    extern int dadhre_(integer *, integer *, integer *, 
	    doublereal *, doublereal *, integer *, integer *, U_fp, 
	    doublereal *, doublereal *, integer *, integer *, integer *, 
	    integer *, integer *, doublereal *, doublereal *, integer *, 
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *), dchhre_(integer *, integer *, integer *, integer *,
	     doublereal *, doublereal *, integer *, integer *, doublereal *, 
	    doublereal *, integer *, integer *, integer *, integer *, integer 
	    *, integer *, integer *, integer *, integer *);
    static integer minsub, maxsub, wtleng, wrksub;

/* ***BEGIN PROLOGUE DCUHRE */
/* ***DATE WRITTEN   900116   (YYMMDD) */
/* ***REVISION DATE  900116   (YYMMDD) */
/* ***CATEGORY NO. H2B1A1 */
/* ***AUTHOR */
/*            Jarle Berntsen, The Computing Centre, */
/*            University of Bergen, Thormohlens gt. 55, */
/*            N-5008 Bergen, Norway */
/*            Phone..  47-5-544055 */
/*            Email..  jarle@eik.ii.uib.no */
/*            Terje O. Espelid, Department of Informatics, */
/*            University of Bergen, Thormohlens gt. 55, */
/*            N-5008 Bergen, Norway */
/*            Phone..  47-5-544180 */
/*            Email..  terje@eik.ii.uib.no */
/*            Alan Genz, Computer Science Department, Washington State */
/*            University, Pullman, WA 99163-1210, USA */
/*            Phone.. 509-335-2131 */
/*            Email..  acg@cs2.cs.wsu.edu */
/* ***KEYWORDS automatic multidimensional integrator, */
/*            n-dimensional hyper-rectangles, */
/*            general purpose, global adaptive */
/* ***PURPOSE  The routine calculates an approximation to a given */
/*            vector of definite integrals */

/*      B(1) B(2)     B(NDIM) */
/*     I    I    ... I       (F ,F ,...,F      ) DX(NDIM)...DX(2)DX(1), */
/*      A(1) A(2)     A(NDIM)  1  2      NUMFUN */

/*       where F = F (X ,X ,...,X    ), I = 1,2,...,NUMFUN. */
/*              I   I  1  2      NDIM */

/*            hopefully satisfying for each component of I the following */
/*            claim for accuracy: */
/*            ABS(I(K)-RESULT(K)).LE.MAX(EPSABS,EPSREL*ABS(I(K))) */
/* ***DESCRIPTION Computation of integrals over hyper-rectangular */
/*            regions. */
/*            DCUHRE is a driver for the integration routine */
/*            DADHRE, which repeatedly subdivides the region */
/*            of integration and estimates the integrals and the */
/*            errors over the subregions with greatest */
/*            estimated errors until the error request */
/*            is met or MAXPTS function evaluations have been used. */

/*            For NDIM = 2 the default integration rule is of */
/*            degree 13 and uses 65 evaluation points. */
/*            For NDIM = 3 the default integration rule is of */
/*            degree 11 and uses 127 evaluation points. */
/*            For NDIM greater then 3 the default integration rule */
/*            is of degree 9 and uses NUM evaluation points where */
/*              NUM = 1 + 4*2*NDIM + 2*NDIM*(NDIM-1) + 4*NDIM*(NDIM-1) + */
/*                    4*NDIM*(NDIM-1)*(NDIM-2)/3 + 2**NDIM */
/*            The degree 9 rule may also be applied for NDIM = 2 */
/*            and NDIM = 3. */
/*            A rule of degree 7 is available in all dimensions. */
/*            The number of evaluation */
/*            points used by the degree 7 rule is */
/*              NUM = 1 + 3*2*NDIM + 2*NDIM*(NDIM-1) + 2**NDIM */

/*            When DCUHRE computes estimates to a vector of */
/*            integrals, all components of the vector are given */
/*            the same treatment. That is, I(F ) and I(F ) for */
/*                                            J         K */
/*            J not equal to K, are estimated with the same */
/*            subdivision of the region of integration. */
/*            For integrals with enough similarity, we may save */
/*            time by applying DCUHRE to all integrands in one call. */
/*            For integrals that vary continuously as functions of */
/*            some parameter, the estimates produced by DCUHRE will */
/*            also vary continuously when the same subdivision is */
/*            applied to all components. This will generally not be */
/*            the case when the different components are given */
/*            separate treatment. */

/*            On the other hand this feature should be used with */
/*            caution when the different components of the integrals */
/*            require clearly different subdivisions. */

/*   ON ENTRY */

/*     NDIM   Integer. */
/*            Number of variables. 1 < NDIM <=  15. */
/*     NUMFUN Integer. */
/*            Number of components of the integral. */
/*     A      Real array of dimension NDIM. */
/*            Lower limits of integration. */
/*     B      Real array of dimension NDIM. */
/*            Upper limits of integration. */
/*     MINPTS Integer. */
/*            Minimum number of function evaluations. */
/*     MAXPTS Integer. */
/*            Maximum number of function evaluations. */
/*            The number of function evaluations over each subregion */
/*            is NUM. */
/*            If (KEY = 0 or KEY = 1) and (NDIM = 2) Then */
/*              NUM = 65 */
/*            Elseif (KEY = 0 or KEY = 2) and (NDIM = 3) Then */
/*              NUM = 127 */
/*            Elseif (KEY = 0 and NDIM > 3) or (KEY = 3) Then */
/*              NUM = 1 + 4*2*NDIM + 2*NDIM*(NDIM-1) + 4*NDIM*(NDIM-1) + */
/*                    4*NDIM*(NDIM-1)*(NDIM-2)/3 + 2**NDIM */
/*            Elseif (KEY = 4) Then */
/*              NUM = 1 + 3*2*NDIM + 2*NDIM*(NDIM-1) + 2**NDIM */
/*            MAXPTS >= 3*NUM and MAXPTS >= MINPTS */
/*            For 3 < NDIM < 13 the minimum values for MAXPTS are: */
/*             NDIM =    4   5   6    7    8    9    10   11    12 */
/*            KEY = 3:  459 819 1359 2151 3315 5067 7815 12351 20235 */
/*            KEY = 4:  195 309  483  765 1251 2133 3795  7005 13299 */
/*     FUNSUB Externally declared subroutine for computing */
/*            all components of the integrand at the given */
/*            evaluation point. */
/*            It must have parameters (NDIM,X,NUMFUN,FUNVLS) */
/*            Input parameters: */
/*              NDIM   Integer that defines the dimension of the */
/*                     integral. */
/*              X      Real array of dimension NDIM */
/*                     that defines the evaluation point. */
/*              NUMFUN Integer that defines the number of */
/*                     components of I. */
/*            Output parameter: */
/*              FUNVLS Real array of dimension NUMFUN */
/*                     that defines NUMFUN components of the integrand. */

/*     EPSABS Real. */
/*            Requested absolute error. */
/*     EPSREL Real. */
/*            Requested relative error. */
/*     KEY    Integer. */
/*            Key to selected local integration rule. */
/*            KEY = 0 is the default value. */
/*                  For NDIM = 2 the degree 13 rule is selected. */
/*                  For NDIM = 3 the degree 11 rule is selected. */
/*                  For NDIM > 3 the degree  9 rule is selected. */
/*            KEY = 1 gives the user the 2 dimensional degree 13 */
/*                  integration rule that uses 65 evaluation points. */
/*            KEY = 2 gives the user the 3 dimensional degree 11 */
/*                  integration rule that uses 127 evaluation points. */
/*            KEY = 3 gives the user the degree 9 integration rule. */
/*            KEY = 4 gives the user the degree 7 integration rule. */
/*                  This is the recommended rule for problems that */
/*                  require great adaptivity. */
/*     NW     Integer. */
/*            Defines the length of the working array WORK. */
/*            Let MAXSUB denote the maximum allowed number of subregions */
/*            for the given values of MAXPTS, KEY and NDIM. */
/*            MAXSUB = (MAXPTS-NUM)/(2*NUM) + 1 */
/*            NW should be greater or equal to */
/*            MAXSUB*(2*NDIM+2*NUMFUN+2) + 17*NUMFUN + 1 */
/*            For efficient execution on parallel computers */
/*            NW should be chosen greater or equal to */
/*            MAXSUB*(2*NDIM+2*NUMFUN+2) + 17*NUMFUN*MDIV + 1 */
/*            where MDIV is the number of subregions that are divided in */
/*            each subdivision step. */
/*            MDIV is default set internally in DCUHRE equal to 1. */
/*            For efficient execution on parallel computers */
/*            with NPROC processors MDIV should be set equal to */
/*            the smallest integer such that MOD(2*MDIV,NPROC) = 0. */

/*     RESTAR Integer. */
/*            If RESTAR = 0, this is the first attempt to compute */
/*            the integral. */
/*            If RESTAR = 1, then we restart a previous attempt. */
/*            In this case the only parameters for DCUHRE that may */
/*            be changed (with respect to the previous call of DCUHRE) */
/*            are MINPTS, MAXPTS, EPSABS, EPSREL and RESTAR. */

/*   ON RETURN */

/*     RESULT Real array of dimension NUMFUN. */
/*            Approximations to all components of the integral. */
/*     ABSERR Real array of dimension NUMFUN. */
/*            Estimates of absolute errors. */
/*     NEVAL  Integer. */
/*            Number of function evaluations used by DCUHRE. */
/*     IFAIL  Integer. */
/*            IFAIL = 0 for normal exit, when ABSERR(K) <=  EPSABS or */
/*              ABSERR(K) <=  ABS(RESULT(K))*EPSREL with MAXPTS or less */
/*              function evaluations for all values of K, */
/*              1 <= K <= NUMFUN . */
/*            IFAIL = 1 if MAXPTS was too small for DCUHRE */
/*              to obtain the required accuracy. In this case DCUHRE */
/*              returns values of RESULT with estimated absolute */
/*              errors ABSERR. */
/*            IFAIL = 2 if KEY is less than 0 or KEY greater than 4. */
/*            IFAIL = 3 if NDIM is less than 2 or NDIM greater than 15. */
/*            IFAIL = 4 if KEY = 1 and NDIM not equal to 2. */
/*            IFAIL = 5 if KEY = 2 and NDIM not equal to 3. */
/*            IFAIL = 6 if NUMFUN is less than 1. */
/*            IFAIL = 7 if volume of region of integration is zero. */
/*            IFAIL = 8 if MAXPTS is less than 3*NUM. */
/*            IFAIL = 9 if MAXPTS is less than MINPTS. */
/*            IFAIL = 10 if EPSABS < 0 and EPSREL < 0. */
/*            IFAIL = 11 if NW is too small. */
/*            IFAIL = 12 if unlegal RESTAR. */
/*     WORK   Real array of dimension NW. */
/*            Used as working storage. */
/*            WORK(NW) = NSUB, the number of subregions in the data */
/*            structure. */
/*            Let WRKSUB=(NW-1-17*NUMFUN*MDIV)/(2*NDIM+2*NUMFUN+2) */
/*            WORK(1),...,WORK(NUMFUN*WRKSUB) contain */
/*              the estimated components of the integrals over the */
/*              subregions. */
/*            WORK(NUMFUN*WRKSUB+1),...,WORK(2*NUMFUN*WRKSUB) contain */
/*              the estimated errors over the subregions. */
/*            WORK(2*NUMFUN*WRKSUB+1),...,WORK(2*NUMFUN*WRKSUB+NDIM* */
/*              WRKSUB) contain the centers of the subregions. */
/*            WORK(2*NUMFUN*WRKSUB+NDIM*WRKSUB+1),...,WORK((2*NUMFUN+ */
/*              NDIM)*WRKSUB+NDIM*WRKSUB) contain subregion half widths. */
/*            WORK(2*NUMFUN*WRKSUB+2*NDIM*WRKSUB+1),...,WORK(2*NUMFUN* */
/*              WRKSUB+2*NDIM*WRKSUB+WRKSUB) contain the greatest errors */
/*              in each subregion. */
/*            WORK((2*NUMFUN+2*NDIM+1)*WRKSUB+1),...,WORK((2*NUMFUN+ */
/*              2*NDIM+1)*WRKSUB+WRKSUB) contain the direction of */
/*              subdivision in each subregion. */
/*            WORK(2*(NDIM+NUMFUN+1)*WRKSUB),...,WORK(2*(NDIM+NUMFUN+1)* */
/*              WRKSUB+ 17*MDIV*NUMFUN) is used as temporary */
/*              storage in DADHRE. */


/*        DCUHRE Example Test Program */


/*   DTEST1 is a simple test driver for DCUHRE. */

/*   Output produced on a SUN 3/50. */

/*       DCUHRE TEST RESULTS */

/*    FTEST CALLS = 3549, IFAIL =  0 */
/*   N   ESTIMATED ERROR   INTEGRAL */
/*   1     0.00000010     0.13850818 */
/*   2     0.00000013     0.06369469 */
/*   3     0.00000874     0.05861748 */
/*   4     0.00000021     0.05407034 */
/*   5     0.00000019     0.05005614 */
/*   6     0.00000009     0.04654608 */

/*     PROGRAM DTEST1 */
/*     EXTERNAL FTEST */
/*     INTEGER KEY, N, NF, NDIM, MINCLS, MAXCLS, IFAIL, NEVAL, NW */
/*     PARAMETER (NDIM = 5, NW = 5000, NF = NDIM+1) */
/*     DOUBLE PRECISION A(NDIM), B(NDIM), WRKSTR(NW) */
/*     DOUBLE PRECISION ABSEST(NF), FINEST(NF), ABSREQ, RELREQ */
/*     DO 10 N = 1,NDIM */
/*        A(N) = 0 */
/*        B(N) = 1 */
/*  10 CONTINUE */
/*     MINCLS = 0 */
/*     MAXCLS = 10000 */
/*     KEY = 0 */
/*     ABSREQ = 0 */
/*     RELREQ = 1E-3 */
/*     CALL DCUHRE(NDIM, NF, A, B, MINCLS, MAXCLS, FTEST, ABSREQ, RELREQ, */
/*    * KEY, NW, 0, FINEST, ABSEST, NEVAL, IFAIL, WRKSTR) */
/*     PRINT 9999, NEVAL, IFAIL */
/* 9999 FORMAT (8X, 'DCUHRE TEST RESULTS', //'     FTEST CALLS = ', I4, */
/*    * ', IFAIL = ', I2, /'    N   ESTIMATED ERROR   INTEGRAL') */
/*     DO 20 N = 1,NF */
/*        PRINT 9998, N, ABSEST(N), FINEST(N) */
/* 9998    FORMAT (3X, I2, 2F15.8) */
/*  20 CONTINUE */
/*     END */
/*     SUBROUTINE FTEST(NDIM, Z, NFUN, F) */
/*     INTEGER N, NDIM, NFUN */
/*     DOUBLE PRECISION Z(NDIM), F(NFUN), SUM */
/*     SUM = 0 */
/*     DO 10 N = 1,NDIM */
/*        SUM = SUM + N*Z(N)**2 */
/*  10 CONTINUE */
/*     F(1) = EXP(-SUM/2) */
/*     DO 20 N = 1,NDIM */
/*        F(N+1) = Z(N)*F(1) */
/*  20 CONTINUE */
/*     END */

/* ***LONG DESCRIPTION */

/*   The information for each subregion is contained in the */
/*   data structure WORK. */
/*   When passed on to DADHRE, WORK is split into eight */
/*   arrays VALUES, ERRORS, CENTRS, HWIDTS, GREATE, DIR, */
/*   OLDRES and WORK. */
/*   VALUES contains the estimated values of the integrals. */
/*   ERRORS contains the estimated errors. */
/*   CENTRS contains the centers of the subregions. */
/*   HWIDTS contains the half widths of the subregions. */
/*   GREATE contains the greatest estimated error for each subregion. */
/*   DIR    contains the directions for further subdivision. */
/*   OLDRES and WORK are used as work arrays in DADHRE. */

/*   The data structures for the subregions are in DADHRE organized */
/*   as a heap, and the size of GREATE(I) defines the position of */
/*   region I in the heap. The heap is maintained by the program */
/*   DTRHRE. */

/*   The subroutine DADHRE is written for efficient execution on shared */
/*   memory parallel computer. On a computer with NPROC processors we wil */
/*   in each subdivision step divide MDIV regions, where MDIV is */
/*   chosen such that MOD(2*MDIV,NPROC) = 0, in totally 2*MDIV new region */
/*   Each processor will then compute estimates of the integrals and erro */
/*   over 2*MDIV/NPROC subregions in each subdivision step. */
/*   The subroutine for estimating the integral and the error over */
/*   each subregion, DRLHRE, uses WORK2 as a work array. */
/*   We must make sure that each processor writes its results to */
/*   separate parts of the memory, and therefore the sizes of WORK and */
/*   WORK2 are functions of MDIV. */
/*   In order to achieve parallel processing of subregions, compiler */
/*   directives should be placed in front of the DO 200 */
/*   loop in DADHRE on machines like Alliant and CRAY. */

/* ***REFERENCES */
/*   J.Berntsen, T.O.Espelid and A.Genz, An Adaptive Algorithm */
/*   for the Approximate Calculation of Multiple Integrals, */
/*   To be published. */

/*   J.Berntsen, T.O.Espelid and A.Genz, DCUHRE: An Adaptive */
/*   Multidimensional Integration Routine for a Vector of */
/*   Integrals, To be published. */

/* ***ROUTINES CALLED DCHHRE,DADHRE */
/* ***END PROLOGUE DCUHRE */

/*   Global variables. */


/*   Local variables. */

/*   MDIV   Integer. */
/*          MDIV is the number of subregions that are divided in */
/*          each subdivision step in DADHRE. */
/*          MDIV is chosen default to 1. */
/*          For efficient execution on parallel computers */
/*          with NPROC processors MDIV should be set equal to */
/*          the smallest integer such that MOD(2*MDIV,NPROC) = 0. */
/*   MAXDIM Integer. */
/*          The maximum allowed value of NDIM. */
/*   MAXWT  Integer. The maximum number of weights used by the */
/*          integration rule. */
/*   WTLENG Integer. */
/*          The number of generators used by the selected rule. */
/*   WORK2  Real work space. The length */
/*          depends on the parameters MDIV,MAXDIM and MAXWT. */
/*   MAXSUB Integer. */
/*          The maximum allowed number of subdivisions */
/*          for the given values of KEY, NDIM and MAXPTS. */
/*   MINSUB Integer. */
/*          The minimum allowed number of subregions for the given */
/*          values of MINPTS, KEY and NDIM. */
/*   WRKSUB Integer. */
/*          The maximum allowed number of subregions as a function */
/*          of NW, NUMFUN, NDIM and MDIV. This determines the length */
/*          of the main work arrays. */
/*   NUM    Integer. The number of integrand evaluations needed */
/*          over each subregion. */


/* ***FIRST EXECUTABLE STATEMENT DCUHRE */

/*   Compute NUM, WTLENG, MAXSUB and MINSUB, */
/*   and check the input parameters. */

    /* Parameter adjustments */
    --b;
    --a;
    --abserr;
    --result;
    --work;

    /* Function Body */
    dchhre_(&c__15, ndim, numfun, &c__1, &a[1], &b[1], minpts, maxpts, epsabs,
	     epsrel, key, nw, restar, &num, &maxsub, &minsub, &keyf, ifail, &
	    wtleng);
    wrksub = (*nw - 1 - *numfun * 17) / ((*ndim << 1) + (*numfun << 1) + 2);
    if (*ifail != 0) {
	goto L999;
    }

/*   Split up the work space. */

    i1 = 1;
    i2 = i1 + wrksub * *numfun;
    i3 = i2 + wrksub * *numfun;
    i4 = i3 + wrksub * *ndim;
    i5 = i4 + wrksub * *ndim;
    i6 = i5 + wrksub;
    i7 = i6 + wrksub;
    i8 = i7 + *numfun;
    k1 = 1;
    k2 = k1 + (wtleng << 1) * *ndim;
    k3 = k2 + wtleng * 5;
    k4 = k3 + wtleng;
    k5 = k4 + *ndim;
    k6 = k5 + *ndim;
    k7 = k6 + (*ndim << 1);
    k8 = k7 + wtleng * 3;

/*   On restart runs the number of subregions from the */
/*   previous call is assigned to NSUB. */

    if (*restar == 1) {
	nsub = (integer) work[*nw];
    }

/*   Compute the size of the temporary work space needed in DADHRE. */

    lenw = *numfun << 4;
    dadhre_(ndim, numfun, &c__1, &a[1], &b[1], &minsub, &maxsub, (U_fp)funsub,
	     epsabs, epsrel, &keyf, restar, &num, &lenw, &wtleng, &result[1], 
	    &abserr[1], neval, &nsub, ifail, &work[i1], &work[i2], &work[i3], 
	    &work[i4], &work[i5], &work[i6], &work[i7], &work[i8], &work2[k1 
	    - 1], &work2[k2 - 1], &work2[k3 - 1], &work2[k4 - 1], &work2[k5 - 
	    1], &work2[k6 - 1], &work2[k7 - 1], &work2[k8 - 1]);
    work[*nw] = (doublereal) nsub;
/*      write (stdout,'(IFAIL, ABSERR, NEVAL)') */
L999:
    return 0;

/* ***END DCUHRE */

} /* dcuhre_ */





/*! Check the validity of the input parameters to dcuhre
 *
 * DCHHRE computes NUM, MAXSUB, MINSUB, KEYF, WTLENG and 
 *            IFAIL as functions of the input parameters to DCUHRE, 
 *            and checks the validity of the input parameters to DCUHRE.
 */
int dchhre_(integer *maxdim, integer *ndim, integer *numfun, 
	integer *mdiv, doublereal *a, doublereal *b, integer *minpts, integer 
	*maxpts, doublereal *epsabs, doublereal *epsrel, integer *key, 
	integer *nw, integer *restar, integer *num, integer *maxsub, integer *
	minsub, integer *keyf, integer *ifail, integer *wtleng)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer pow_ii(integer *, integer *);

    /* Local variables */
    static integer j, limit;

/* ***BEGIN PROLOGUE DCHHRE */
/* ***PURPOSE  DCHHRE checks the validity of the */
/*            input parameters to DCUHRE. */
/* ***DESCRIPTION */
/*            DCHHRE computes NUM, MAXSUB, MINSUB, KEYF, WTLENG and */
/*            IFAIL as functions of the input parameters to DCUHRE, */
/*            and checks the validity of the input parameters to DCUHRE. */

/*   ON ENTRY */

/*     MAXDIM Integer. */
/*            The maximum allowed number of dimensions. */
/*     NDIM   Integer. */
/*            Number of variables. 1 < NDIM <= MAXDIM. */
/*     NUMFUN Integer. */
/*            Number of components of the integral. */
/*     MDIV   Integer. */
/*            MDIV is the number of subregions that are divided in */
/*            each subdivision step in DADHRE. */
/*            MDIV is chosen default to 1. */
/*            For efficient execution on parallel computers */
/*            with NPROC processors MDIV should be set equal to */
/*            the smallest integer such that MOD(2*MDIV,NPROC) = 0. */
/*     A      Real array of dimension NDIM. */
/*            Lower limits of integration. */
/*     B      Real array of dimension NDIM. */
/*            Upper limits of integration. */
/*     MINPTS Integer. */
/*            Minimum number of function evaluations. */
/*     MAXPTS Integer. */
/*            Maximum number of function evaluations. */
/*            The number of function evaluations over each subregion */
/*            is NUM. */
/*            If (KEY = 0 or KEY = 1) and (NDIM = 2) Then */
/*              NUM = 65 */
/*            Elseif (KEY = 0 or KEY = 2) and (NDIM = 3) Then */
/*              NUM = 127 */
/*            Elseif (KEY = 0 and NDIM > 3) or (KEY = 3) Then */
/*              NUM = 1 + 4*2*NDIM + 2*NDIM*(NDIM-1) + 4*NDIM*(NDIM-1) + */
/*                    4*NDIM*(NDIM-1)*(NDIM-2)/3 + 2**NDIM */
/*            Elseif (KEY = 4) Then */
/*              NUM = 1 + 3*2*NDIM + 2*NDIM*(NDIM-1) + 2**NDIM */
/*            MAXPTS >= 3*NUM and MAXPTS >= MINPTS */
/*     EPSABS Real. */
/*            Requested absolute error. */
/*     EPSREL Real. */
/*            Requested relative error. */
/*     KEY    Integer. */
/*            Key to selected local integration rule. */
/*            KEY = 0 is the default value. */
/*                  For NDIM = 2 the degree 13 rule is selected. */
/*                  For NDIM = 3 the degree 11 rule is selected. */
/*                  For NDIM > 3 the degree  9 rule is selected. */
/*            KEY = 1 gives the user the 2 dimensional degree 13 */
/*                  integration rule that uses 65 evaluation points. */
/*            KEY = 2 gives the user the 3 dimensional degree 11 */
/*                  integration rule that uses 127 evaluation points. */
/*            KEY = 3 gives the user the degree 9 integration rule. */
/*            KEY = 4 gives the user the degree 7 integration rule. */
/*                  This is the recommended rule for problems that */
/*                  require great adaptivity. */
/*     NW     Integer. */
/*            Defines the length of the working array WORK. */
/*            Let MAXSUB denote the maximum allowed number of subregions */
/*            for the given values of MAXPTS, KEY and NDIM. */
/*            MAXSUB = (MAXPTS-NUM)/(2*NUM) + 1 */
/*            NW should be greater or equal to */
/*            MAXSUB*(2*NDIM+2*NUMFUN+2) + 17*NUMFUN + 1 */
/*            For efficient execution on parallel computers */
/*            NW should be chosen greater or equal to */
/*            MAXSUB*(2*NDIM+2*NUMFUN+2) + 17*NUMFUN*MDIV + 1 */
/*            where MDIV is the number of subregions that are divided in */
/*            each subdivision step. */
/*            MDIV is default set internally in DCUHRE equal to 1. */
/*            For efficient execution on parallel computers */
/*            with NPROC processors MDIV should be set equal to */
/*            the smallest integer such that MOD(2*MDIV,NPROC) = 0. */
/*     RESTAR Integer. */
/*            If RESTAR = 0, this is the first attempt to compute */
/*            the integral. */
/*            If RESTAR = 1, then we restart a previous attempt. */

/*   ON RETURN */

/*     NUM    Integer. */
/*            The number of function evaluations over each subregion. */
/*     MAXSUB Integer. */
/*            The maximum allowed number of subregions for the */
/*            given values of MAXPTS, KEY and NDIM. */
/*     MINSUB Integer. */
/*            The minimum allowed number of subregions for the given */
/*            values of MINPTS, KEY and NDIM. */
/*     IFAIL  Integer. */
/*            IFAIL = 0 for normal exit. */
/*            IFAIL = 2 if KEY is less than 0 or KEY greater than 4. */
/*            IFAIL = 3 if NDIM is less than 2 or NDIM greater than */
/*                      MAXDIM. */
/*            IFAIL = 4 if KEY = 1 and NDIM not equal to 2. */
/*            IFAIL = 5 if KEY = 2 and NDIM not equal to 3. */
/*            IFAIL = 6 if NUMFUN less than 1. */
/*            IFAIL = 7 if volume of region of integration is zero. */
/*            IFAIL = 8 if MAXPTS is less than 3*NUM. */
/*            IFAIL = 9 if MAXPTS is less than MINPTS. */
/*            IFAIL = 10 if EPSABS < 0 and EPSREL < 0. */
/*            IFAIL = 11 if NW is too small. */
/*            IFAIL = 12 if unlegal RESTAR. */
/*     KEYF   Integer. */
/*            Key to selected integration rule. */
/*     WTLENG Integer. */
/*            The number of generators of the chosen integration rule. */

/* ***ROUTINES CALLED-NONE */
/* ***END PROLOGUE DCHHRE */

/*   Global variables. */


/*   Local variables. */


/* ***FIRST EXECUTABLE STATEMENT DCHHRE */

    /* Parameter adjustments */
    --b;
    --a;

    /* Function Body */
    *ifail = 0;

/*   Check on legal KEY. */

    if (*key < 0 || *key > 4) {
	*ifail = 2;
	goto L999;
    }

/*   Check on legal NDIM. */

    if (*ndim < 2 || *ndim > *maxdim) {
	*ifail = 3;
	goto L999;
    }

/*   For KEY = 1, NDIM must be equal to 2. */

    if (*key == 1 && *ndim != 2) {
	*ifail = 4;
	goto L999;
    }

/*   For KEY = 2, NDIM must be equal to 3. */

    if (*key == 2 && *ndim != 3) {
	*ifail = 5;
	goto L999;
    }

/*   For KEY = 0, we point at the selected integration rule. */

    if (*key == 0) {
	if (*ndim == 2) {
	    *keyf = 1;
	} else if (*ndim == 3) {
	    *keyf = 2;
	} else {
	    *keyf = 3;
	}
    } else {
	*keyf = *key;
    }

/*   Compute NUM and WTLENG as a function of KEYF and NDIM. */

    if (*keyf == 1) {
	*num = 65;
	*wtleng = 14;
    } else if (*keyf == 2) {
	*num = 127;
	*wtleng = 13;
    } else if (*keyf == 3) {
	*num = (*ndim << 3) + 1 + (*ndim << 1) * (*ndim - 1) + (*ndim << 2) * 
		(*ndim - 1) + (*ndim << 2) * (*ndim - 1) * (*ndim - 2) / 3 + 
		pow_ii(&c__2, ndim);
	*wtleng = 9;
	if (*ndim == 2) {
	    *wtleng = 8;
	}
    } else if (*keyf == 4) {
	*num = *ndim * 6 + 1 + (*ndim << 1) * (*ndim - 1) + pow_ii(&c__2, 
		ndim);
	*wtleng = 6;
    }

/*   Compute MAXSUB. */

    *maxsub = (*maxpts - *num) / (*num << 1) + 1;

/*   Compute MINSUB. */

    *minsub = (*minpts - *num) / (*num << 1) + 1;
    if ((*minpts - *num) % (*num << 1) != 0) {
	++(*minsub);
    }
    *minsub = max(2,*minsub);

/*   Check on positive NUMFUN. */

    if (*numfun < 1) {
	*ifail = 6;
	goto L999;
    }

/*   Check on legal upper and lower limits of integration. */

    i__1 = *ndim;
    for (j = 1; j <= i__1; ++j) {
	if (a[j] - b[j] == 0.) {
	    *ifail = 7;
	    goto L999;
	}
/* L10: */
    }

/*   Check on MAXPTS < 3*NUM. */

    if (*maxpts < *num * 3) {
	*ifail = 8;
	goto L999;
    }

/*   Check on MAXPTS >= MINPTS. */

    if (*maxpts < *minpts) {
	*ifail = 9;
	goto L999;
    }

/*   Check on legal accuracy requests. */

    if (*epsabs < 0. && *epsrel < 0.) {
	*ifail = 10;
	goto L999;
    }

/*   Check on big enough double precision workspace. */

    limit = *maxsub * ((*ndim << 1) + (*numfun << 1) + 2) + *mdiv * 17 * *
	    numfun + 1;
    if (*nw < limit) {
	*ifail = 11;
	goto L999;
    }

/*    Check on legal RESTAR. */

    if (*restar != 0 && *restar != 1) {
	*ifail = 12;
	goto L999;
    }
L999:
    return 0;

/* ***END DCHHRE */

} /* dchhre_ */


/* Subroutine */ int dadhre_(integer *ndim, integer *numfun, integer *mdiv, 
	doublereal *a, doublereal *b, integer *minsub, integer *maxsub, U_fp 
	funsub, doublereal *epsabs, doublereal *epsrel, integer *key, integer 
	*restar, integer *num, integer *lenw, integer *wtleng, doublereal *
	result, doublereal *abserr, integer *neval, integer *nsub, integer *
	ifail, doublereal *values, doublereal *errors, doublereal *centrs, 
	doublereal *hwidts, doublereal *greate, doublereal *dir, doublereal *
	oldres, doublereal *work, doublereal *g, doublereal *w, doublereal *
	rulpts, doublereal *center, doublereal *hwidth, doublereal *x, 
	doublereal *scales, doublereal *norms)
{
    /* System generated locals */
    integer values_dim1, values_offset, errors_dim1, errors_offset, 
	    centrs_dim1, centrs_offset, hwidts_dim1, hwidts_offset, 
	    oldres_dim1, oldres_offset, g_dim1, g_dim2, g_offset, x_dim1, 
	    x_offset, i__1, i__2, i__3;
    doublereal d__1;

    /* Local variables */
    static integer i__, j, k, l1;
    static doublereal est1, est2;
    static integer ndiv, index;
    static doublereal oldcen;
    extern /* Subroutine */ int dinhre_(integer *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static integer direct;
    static doublereal errcof[6];
    extern /* Subroutine */ int drlhre_(integer *, doublereal *, doublereal *,
	     integer *, doublereal *, doublereal *, doublereal *, integer *, 
	    U_fp, doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), dtrhre_(integer *, 
	    integer *, integer *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static integer intsgn, sbrgns, pointr;

/* ***BEGIN PROLOGUE DADHRE */
/* ***KEYWORDS automatic multidimensional integrator, */
/*            n-dimensional hyper-rectangles, */
/*            general purpose, global adaptive */
/* ***PURPOSE  The routine calculates an approximation to a given */
/*            vector of definite integrals, I, over a hyper-rectangular */
/*            region hopefully satisfying for each component of I the */
/*            following claim for accuracy: */
/*            ABS(I(K)-RESULT(K)).LE.MAX(EPSABS,EPSREL*ABS(I(K))) */
/* ***DESCRIPTION Computation of integrals over hyper-rectangular */
/*            regions. */
/*            DADHRE repeatedly subdivides the region */
/*            of integration and estimates the integrals and the */
/*            errors over the subregions with  greatest */
/*            estimated errors until the error request */
/*            is met or MAXSUB subregions are stored. */
/*            The regions are divided in two equally sized parts along */
/*            the direction with greatest absolute fourth divided */
/*            difference. */

/*   ON ENTRY */

/*     NDIM   Integer. */
/*            Number of variables. 1 < NDIM <= MAXDIM. */
/*     NUMFUN Integer. */
/*            Number of components of the integral. */
/*     MDIV   Integer. */
/*            Defines the number of new subregions that are divided */
/*            in each subdivision step. */
/*     A      Real array of dimension NDIM. */
/*            Lower limits of integration. */
/*     B      Real array of dimension NDIM. */
/*            Upper limits of integration. */
/*     MINSUB Integer. */
/*            The computations proceed until there are at least */
/*            MINSUB subregions in the data structure. */
/*     MAXSUB Integer. */
/*            The computations proceed until there are at most */
/*            MAXSUB subregions in the data structure. */

/*     FUNSUB Externally declared subroutine for computing */
/*            all components of the integrand in the given */
/*            evaluation point. */
/*            It must have parameters (NDIM,X,NUMFUN,FUNVLS) */
/*            Input parameters: */
/*              NDIM   Integer that defines the dimension of the */
/*                     integral. */
/*              X      Real array of dimension NDIM */
/*                     that defines the evaluation point. */
/*              NUMFUN Integer that defines the number of */
/*                     components of I. */
/*            Output parameter: */
/*              FUNVLS Real array of dimension NUMFUN */
/*                     that defines NUMFUN components of the integrand. */

/*     EPSABS Real. */
/*            Requested absolute error. */
/*     EPSREL Real. */
/*            Requested relative error. */
/*     KEY    Integer. */
/*            Key to selected local integration rule. */
/*            KEY = 0 is the default value. */
/*                  For NDIM = 2 the degree 13 rule is selected. */
/*                  For NDIM = 3 the degree 11 rule is selected. */
/*                  For NDIM > 3 the degree  9 rule is selected. */
/*            KEY = 1 gives the user the 2 dimensional degree 13 */
/*                  integration rule that uses 65 evaluation points. */
/*            KEY = 2 gives the user the 3 dimensional degree 11 */
/*                  integration rule that uses 127 evaluation points. */
/*            KEY = 3 gives the user the degree 9 integration rule. */
/*            KEY = 4 gives the user the degree 7 integration rule. */
/*                  This is the recommended rule for problems that */
/*                  require great adaptivity. */
/*     RESTAR Integer. */
/*            If RESTAR = 0, this is the first attempt to compute */
/*            the integral. */
/*            If RESTAR = 1, then we restart a previous attempt. */
/*            (In this case the output parameters from DADHRE */
/*            must not be changed since the last */
/*            exit from DADHRE.) */
/*     NUM    Integer. */
/*            The number of function evaluations over each subregion. */
/*     LENW   Integer. */
/*            Defines the length of the working array WORK. */
/*            LENW should be greater or equal to */
/*            16*MDIV*NUMFUN. */
/*     WTLENG Integer. */
/*            The number of weights in the basic integration rule. */
/*     NSUB   Integer. */
/*            If RESTAR = 1, then NSUB must specify the number */
/*            of subregions stored in the previous call to DADHRE. */

/*   ON RETURN */

/*     RESULT Real array of dimension NUMFUN. */
/*            Approximations to all components of the integral. */
/*     ABSERR Real array of dimension NUMFUN. */
/*            Estimates of absolute accuracies. */
/*     NEVAL  Integer. */
/*            Number of function evaluations used by DADHRE. */
/*     NSUB   Integer. */
/*            Number of stored subregions. */
/*     IFAIL  Integer. */
/*            IFAIL = 0 for normal exit, when ABSERR(K) <=  EPSABS or */
/*              ABSERR(K) <=  ABS(RESULT(K))*EPSREL with MAXSUB or less */
/*              subregions processed for all values of K, */
/*              1 <=  K <=  NUMFUN. */
/*            IFAIL = 1 if MAXSUB was too small for DADHRE */
/*              to obtain the required accuracy. In this case DADHRE */
/*              returns values of RESULT with estimated absolute */
/*              accuracies ABSERR. */
/*     VALUES Real array of dimension (NUMFUN,MAXSUB). */
/*            Used to store estimated values of the integrals */
/*            over the subregions. */
/*     ERRORS Real array of dimension (NUMFUN,MAXSUB). */
/*            Used to store the corresponding estimated errors. */
/*     CENTRS Real array of dimension (NDIM,MAXSUB). */
/*            Used to store the centers of the stored subregions. */
/*     HWIDTS Real array of dimension (NDIM,MAXSUB). */
/*            Used to store the half widths of the stored subregions. */
/*     GREATE Real array of dimension MAXSUB. */
/*            Used to store the greatest estimated errors in */
/*            all subregions. */
/*     DIR    Real array of dimension MAXSUB. */
/*            DIR is used to store the directions for */
/*            further subdivision. */
/*     OLDRES Real array of dimension (NUMFUN,MDIV). */
/*            Used to store old estimates of the integrals over the */
/*            subregions. */
/*     WORK   Real array of dimension LENW. */
/*            Used  in DRLHRE and DTRHRE. */
/*     G      Real array of dimension (NDIM,WTLENG,2*MDIV). */
/*            The fully symmetric sum generators for the rules. */
/*            G(1,J,1),...,G(NDIM,J,1) are the generators for the */
/*            points associated with the Jth weights. */
/*            When MDIV subregions are divided in 2*MDIV */
/*            subregions, the subregions may be processed on different */
/*            processors and we must make a copy of the generators */
/*            for each processor. */
/*     W      Real array of dimension (5,WTLENG). */
/*            The weights for the basic and null rules. */
/*            W(1,1), ..., W(1,WTLENG) are weights for the basic rule. */
/*            W(I,1), ..., W(I,WTLENG) , for I > 1 are null rule weights. */
/*     RULPTS Real array of dimension WTLENG. */
/*            Work array used in DINHRE. */
/*     CENTER Real array of dimension NDIM. */
/*            Work array used in DTRHRE. */
/*     HWIDTH Real array of dimension NDIM. */
/*            Work array used in DTRHRE. */
/*     X      Real array of dimension (NDIM,2*MDIV). */
/*            Work array used in DRLHRE. */
/*     SCALES Real array of dimension (3,WTLENG). */
/*            Work array used by DINHRE and DRLHRE. */
/*     NORMS  Real array of dimension (3,WTLENG). */
/*            Work array used by DINHRE and DRLHRE. */

/* ***REFERENCES */

/*   P. van Dooren and L. de Ridder, Algorithm 6, An adaptive algorithm */
/*   for numerical integration over an n-dimensional cube, J.Comput.Appl. */
/*   Math. 2(1976)207-217. */

/*   A.C.Genz and A.A.Malik, Algorithm 019. Remarks on algorithm 006: */
/*   An adaptive algorithm for numerical integration over an */
/*   N-dimensional rectangular region,J.Comput.Appl.Math. 6(1980)295-302. */

/* ***ROUTINES CALLED DTRHRE,DINHRE,DRLHRE */
/* ***END PROLOGUE DADHRE */

/*   Global variables. */


/*   Local variables. */

/*   INTSGN is used to get correct sign on the integral. */
/*   SBRGNS is the number of stored subregions. */
/*   NDIV   The number of subregions to be divided in each main step. */
/*   POINTR Pointer to the position in the data structure where */
/*          the new subregions are to be stored. */
/*   DIRECT Direction of subdivision. */
/*   ERRCOF Heuristic error coeff. defined in DINHRE and used by DRLHRE */
/*          and DADHRE. */


/* ***FIRST EXECUTABLE STATEMENT DADHRE */

/*   Get the correct sign on the integral. */

    /* Parameter adjustments */
    --hwidth;
    --center;
    --b;
    --a;
    --abserr;
    --result;
    x_dim1 = *ndim;
    x_offset = 1 + x_dim1;
    x -= x_offset;
    oldres_dim1 = *numfun;
    oldres_offset = 1 + oldres_dim1;
    oldres -= oldres_offset;
    --dir;
    --greate;
    hwidts_dim1 = *ndim;
    hwidts_offset = 1 + hwidts_dim1;
    hwidts -= hwidts_offset;
    centrs_dim1 = *ndim;
    centrs_offset = 1 + centrs_dim1;
    centrs -= centrs_offset;
    errors_dim1 = *numfun;
    errors_offset = 1 + errors_dim1;
    errors -= errors_offset;
    values_dim1 = *numfun;
    values_offset = 1 + values_dim1;
    values -= values_offset;
    --work;
    norms -= 4;
    scales -= 4;
    --rulpts;
    w -= 6;
    g_dim1 = *ndim;
    g_dim2 = *wtleng;
    g_offset = 1 + g_dim1 * (1 + g_dim2);
    g -= g_offset;

    /* Function Body */
    intsgn = 1;
    i__1 = *ndim;
    for (j = 1; j <= i__1; ++j) {
	if (b[j] < a[j]) {
	    intsgn = -intsgn;
	}
/* L10: */
    }

/*   Call DINHRE to compute the weights and abscissas of */
/*   the function evaluation points. */

    dinhre_(ndim, key, wtleng, &w[6], &g[g_offset], errcof, &rulpts[1], &
	    scales[4], &norms[4]);

/*   If RESTAR = 1, then this is a restart run. */

    if (*restar == 1) {
	sbrgns = *nsub;
	goto L110;
    }

/*   Initialize the SBRGNS, CENTRS and HWIDTS. */

    sbrgns = 1;
    i__1 = *ndim;
    for (j = 1; j <= i__1; ++j) {
	centrs[j + centrs_dim1] = (a[j] + b[j]) / 2;
	hwidts[j + hwidts_dim1] = (d__1 = b[j] - a[j], abs(d__1)) / 2;
/* L15: */
    }

/*   Initialize RESULT, ABSERR and NEVAL. */

    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {
	result[j] = 0.;
	abserr[j] = 0.;
/* L20: */
    }
    *neval = 0;

/*   Apply DRLHRE over the whole region. */

    drlhre_(ndim, &centrs[centrs_dim1 + 1], &hwidts[hwidts_dim1 + 1], wtleng, 
	    &g[g_offset], &w[6], errcof, numfun, (U_fp)funsub, &scales[4], &
	    norms[4], &x[x_offset], &work[1], &values[values_dim1 + 1], &
	    errors[errors_dim1 + 1], &dir[1]);
    *neval += *num;

/*   Add the computed values to RESULT and ABSERR. */

    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {
	result[j] += values[j + values_dim1];
/* L55: */
    }
    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {
	abserr[j] += errors[j + errors_dim1];
/* L65: */
    }

/*   Store results in heap. */

    index = 1;
    dtrhre_(&c__2, ndim, numfun, &index, &values[values_offset], &errors[
	    errors_offset], &centrs[centrs_offset], &hwidts[hwidts_offset], &
	    greate[1], &work[1], &work[*numfun + 1], &center[1], &hwidth[1], &
	    dir[1]);


/* ***End initialisation. */

/* ***Begin loop while the error is too great, */
/*   and SBRGNS+1 is less than MAXSUB. */

L110:
    if (sbrgns + 1 <= *maxsub) {

/*   If we are allowed to divide further, */
/*   prepare to apply basic rule over each half of the */
/*   NDIV subregions with greatest errors. */
/*   If MAXSUB is great enough, NDIV = MDIV */

	if (*mdiv > 1) {
	    ndiv = *maxsub - sbrgns;
/* Computing MIN */
	    i__1 = min(ndiv,*mdiv);
	    ndiv = min(i__1,sbrgns);
	} else {
	    ndiv = 1;
	}

/*   Divide the NDIV subregions in two halves, and compute */
/*   integral and error over each half. */

	i__1 = ndiv;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    pointr = sbrgns + ndiv + 1 - i__;

/*   Adjust RESULT and ABSERR. */

	    i__2 = *numfun;
	    for (j = 1; j <= i__2; ++j) {
		result[j] -= values[j + values_dim1];
		abserr[j] -= errors[j + errors_dim1];
/* L115: */
	    }

/*   Compute first half region. */

	    i__2 = *ndim;
	    for (j = 1; j <= i__2; ++j) {
		centrs[j + pointr * centrs_dim1] = centrs[j + centrs_dim1];
		hwidts[j + pointr * hwidts_dim1] = hwidts[j + hwidts_dim1];
/* L120: */
	    }
	    direct = (integer) dir[1];
	    dir[pointr] = (doublereal) direct;
	    hwidts[direct + pointr * hwidts_dim1] = hwidts[direct + 
		    hwidts_dim1] / 2;
	    oldcen = centrs[direct + centrs_dim1];
	    centrs[direct + pointr * centrs_dim1] = oldcen - hwidts[direct + 
		    pointr * hwidts_dim1];

/*   Save the computed values of the integrals. */

	    i__2 = *numfun;
	    for (j = 1; j <= i__2; ++j) {
		oldres[j + (ndiv - i__ + 1) * oldres_dim1] = values[j + 
			values_dim1];
/* L125: */
	    }

/*   Adjust the heap. */

	    dtrhre_(&c__1, ndim, numfun, &sbrgns, &values[values_offset], &
		    errors[errors_offset], &centrs[centrs_offset], &hwidts[
		    hwidts_offset], &greate[1], &work[1], &work[*numfun + 1], 
		    &center[1], &hwidth[1], &dir[1]);

/*   Compute second half region. */

	    i__2 = *ndim;
	    for (j = 1; j <= i__2; ++j) {
		centrs[j + (pointr - 1) * centrs_dim1] = centrs[j + pointr * 
			centrs_dim1];
		hwidts[j + (pointr - 1) * hwidts_dim1] = hwidts[j + pointr * 
			hwidts_dim1];
/* L130: */
	    }
	    centrs[direct + (pointr - 1) * centrs_dim1] = oldcen + hwidts[
		    direct + pointr * hwidts_dim1];
	    hwidts[direct + (pointr - 1) * hwidts_dim1] = hwidts[direct + 
		    pointr * hwidts_dim1];
	    dir[pointr - 1] = (doublereal) direct;
/* L150: */
	}

/*   Make copies of the generators for each processor. */

	i__1 = ndiv << 1;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    i__2 = *ndim;
	    for (j = 1; j <= i__2; ++j) {
		i__3 = *wtleng;
		for (k = 1; k <= i__3; ++k) {
		    g[j + (k + i__ * g_dim2) * g_dim1] = g[j + (k + g_dim2) * 
			    g_dim1];
/* L190: */
		}
	    }
	}

/*   Apply basic rule. */

/* vd$l cncall */
	i__3 = ndiv << 1;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    index = sbrgns + i__;
	    l1 = ((i__ - 1) << 3) * *numfun + 1;
	    drlhre_(ndim, &centrs[index * centrs_dim1 + 1], &hwidts[index * 
		    hwidts_dim1 + 1], wtleng, &g[(i__ * g_dim2 + 1) * g_dim1 
		    + 1], &w[6], errcof, numfun, (U_fp)funsub, &scales[4], &
		    norms[4], &x[i__ * x_dim1 + 1], &work[l1], &values[index *
		     values_dim1 + 1], &errors[index * errors_dim1 + 1], &dir[
		    index]);
/* L200: */
	}
	*neval += (ndiv << 1) * *num;

/*   Add new contributions to RESULT. */

	i__3 = ndiv << 1;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    i__2 = *numfun;
	    for (j = 1; j <= i__2; ++j) {
		result[j] += values[j + (sbrgns + i__) * values_dim1];
/* L210: */
	    }
/* L220: */
	}

/*   Check consistency of results and if necessary adjust */
/*   the estimated errors. */

	i__3 = ndiv;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    greate[sbrgns + (i__ << 1) - 1] = 0.;
	    greate[sbrgns + (i__ << 1)] = 0.;
	    i__2 = *numfun;
	    for (j = 1; j <= i__2; ++j) {
		est1 = (d__1 = oldres[j + i__ * oldres_dim1] - (values[j + (
			sbrgns + (i__ << 1) - 1) * values_dim1] + values[j + (
			sbrgns + (i__ << 1)) * values_dim1]), abs(d__1));
		est2 = errors[j + (sbrgns + (i__ << 1) - 1) * errors_dim1] + 
			errors[j + (sbrgns + (i__ << 1)) * errors_dim1];
		if (est2 > 0.) {
		    errors[j + (sbrgns + (i__ << 1) - 1) * errors_dim1] *= 
			    errcof[4] * est1 / est2 + 1;
		    errors[j + (sbrgns + (i__ << 1)) * errors_dim1] *= errcof[
			    4] * est1 / est2 + 1;
		}
		errors[j + (sbrgns + (i__ << 1) - 1) * errors_dim1] += errcof[
			5] * est1;
		errors[j + (sbrgns + (i__ << 1)) * errors_dim1] += errcof[5] *
			 est1;
		if (errors[j + (sbrgns + (i__ << 1) - 1) * errors_dim1] > 
			greate[sbrgns + (i__ << 1) - 1]) {
		    greate[sbrgns + (i__ << 1) - 1] = errors[j + (sbrgns + (
			    i__ << 1) - 1) * errors_dim1];
		}
		if (errors[j + (sbrgns + (i__ << 1)) * errors_dim1] > greate[
			sbrgns + (i__ << 1)]) {
		    greate[sbrgns + (i__ << 1)] = errors[j + (sbrgns + (i__ <<
			     1)) * errors_dim1];
		}
		abserr[j] = abserr[j] + errors[j + (sbrgns + (i__ << 1) - 1) *
			 errors_dim1] + errors[j + (sbrgns + (i__ << 1)) * 
			errors_dim1];
/* L230: */
	    }
/* L240: */
	}

/*   Store results in heap. */

	i__3 = ndiv << 1;
	for (i__ = 1; i__ <= i__3; ++i__) {
	    index = sbrgns + i__;
	    dtrhre_(&c__2, ndim, numfun, &index, &values[values_offset], &
		    errors[errors_offset], &centrs[centrs_offset], &hwidts[
		    hwidts_offset], &greate[1], &work[1], &work[*numfun + 1], 
		    &center[1], &hwidth[1], &dir[1]);
/* L250: */
	}
	sbrgns += ndiv << 1;

/*   Check for termination. */

	if (sbrgns < *minsub) {
	    goto L110;
	}
	i__3 = *numfun;
	for (j = 1; j <= i__3; ++j) {
	    if (abserr[j] > *epsrel * (d__1 = result[j], abs(d__1)) && abserr[
		    j] > *epsabs) {
		goto L110;
	    }
/* L255: */
	}

	*ifail = 0;
	goto L499;

/*   Else we did not succeed with the */
/*   given value of MAXSUB. */

    } else {
	*ifail = 1;
    }

/*   Compute more accurate values of RESULT and ABSERR. */

L499:
    i__3 = *numfun;
    for (j = 1; j <= i__3; ++j) {
	result[j] = 0.;
	abserr[j] = 0.;
/* L500: */
    }		

    i__3 = sbrgns;
    for (i__ = 1; i__ <= i__3; ++i__) {
	i__2 = *numfun;

	for (j = 1; j <= i__2; ++j) {
	    result[j] += values[j + i__ * values_dim1];
	    abserr[j] += errors[j + i__ * errors_dim1];
/* L505: */
	}	
/* L510: */
    }

/*   Compute correct sign on the integral. */

    i__3 = *numfun;
    for (j = 1; j <= i__3; ++j) {
	result[j] *= intsgn;
/* L600: */
    }
    *nsub = sbrgns;

    i__3 = *lenw; // silence compiler warning adf 021003
    return 0;

/* ***END DADHRE */

} /* dadhre_ */


/* Subroutine */ int dfshre_(integer *ndim, doublereal *center, doublereal *
	hwidth, doublereal *x, doublereal *g, integer *numfun, S_fp funsub, 
	doublereal *fulsms, doublereal *funvls)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer i__, j, l;
    static doublereal gi, gl;
    static integer ixchng, lxchng;

/* ***BEGIN PROLOGUE DFSHRE */
/* ***KEYWORDS fully symmetric sum */
/* ***PURPOSE  To compute fully symmetric basic rule sums */
/* ***AUTHOR   Alan Genz, Computer Science Department, Washington */
/*            State University, Pullman, WA 99163-1210 USA */
/* ***LAST MODIFICATION 88-04-08 */
/* ***DESCRIPTION DFSHRE computes a fully symmetric sum for a vector */
/*            of integrand values over a hyper-rectangular region. */
/*            The sum is fully symmetric with respect to the center of */
/*            the region and is taken over all sign changes and */
/*            permutations of the generators for the sum. */

/*   ON ENTRY */

/*   NDIM   Integer. */
/*          Number of variables. */
/*   CENTER Real array of dimension NDIM. */
/*          The coordinates for the center of the region. */
/*   HWIDTH Real Array of dimension NDIM. */
/*          HWIDTH(I) is half of the width of dimension I of the region. */
/*   X      Real Array of dimension NDIM. */
/*          A work array. */
/*   G      Real Array of dimension NDIM. */
/*          The generators for the fully symmetric sum. These MUST BE */
/*          non-negative and non-increasing. */
/*   NUMFUN Integer. */
/*          Number of components for the vector integrand. */
/*   FUNSUB Externally declared subroutine. */
/*          For computing the components of the integrand at a point X. */
/*          It must have parameters (NDIM, X, NUMFUN, FUNVLS). */
/*           Input Parameters: */
/*            X      Real array of dimension NDIM. */
/*                   Defines the evaluation point. */
/*            NDIM   Integer. */
/*                   Number of variables for the integrand. */
/*            NUMFUN Integer. */
/*                   Number of components for the vector integrand. */
/*           Output Parameters: */
/*            FUNVLS Real array of dimension NUMFUN. */
/*                   The components of the integrand at the point X. */
/*   ON RETURN */

/*   FULSMS Real array of dimension NUMFUN. */
/*          The values for the fully symmetric sums for each component */
/*          of the integrand. */
/*   FUNVLS Real array of dimension NUMFUN. */
/*          A work array. */

/* ***ROUTINES CALLED: FUNSUB */

/* ***END PROLOGUE DFSHRE */

/*   Global variables. */


/*   Local variables. */


/* ***FIRST EXECUTABLE STATEMENT DFSHRE */

    /* Parameter adjustments */
    --g;
    --x;
    --hwidth;
    --center;
    --funvls;
    --fulsms;

    /* Function Body */
    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {
	fulsms[j] = 0.;
/* L10: */
    }

/*     Compute centrally symmetric sum for permutation of G */

L20:
    i__1 = *ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	x[i__] = center[i__] + g[i__] * hwidth[i__];
/* L30: */
    }
L40:
    (*funsub)(ndim, &x[1], numfun, &funvls[1]);
    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {
	fulsms[j] += funvls[j];
/* L50: */
    }
    i__1 = *ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	g[i__] = -g[i__];
	x[i__] = center[i__] + g[i__] * hwidth[i__];
	if (g[i__] < 0.) {
	    goto L40;
	}
/* L60: */
    }

/*       Find next distinct permutation of G and loop back for next sum. */
/*       Permutations are generated in reverse lexicographic order. */

    i__1 = *ndim;
    for (i__ = 2; i__ <= i__1; ++i__) {
	if (g[i__ - 1] > g[i__]) {
	    gi = g[i__];
	    ixchng = i__ - 1;
	    i__2 = (i__ - 1) / 2;
	    for (l = 1; l <= i__2; ++l) {
		gl = g[l];
		g[l] = g[i__ - l];
		g[i__ - l] = gl;
		if (gl <= gi) {
		    --ixchng;
		}
		if (g[l] > gi) {
		    lxchng = l;
		}
/* L70: */
	    }
	    if (g[ixchng] <= gi) {
		ixchng = lxchng;
	    }
	    g[i__] = g[ixchng];
	    g[ixchng] = gi;
	    goto L20;
	}
/* L80: */
    }

/*     Restore original order to generators */

    i__1 = *ndim / 2;
    for (i__ = 1; i__ <= i__1; ++i__) {
	gi = g[i__];
	g[i__] = g[*ndim - i__ + 1];
	g[*ndim - i__ + 1] = gi;
/* L90: */
    }

/* ***END DFSHRE */

    return 0;
} /* dfshre_ */




/* Subroutine */ int dinhre_(integer *ndim, integer *key, integer *wtleng, 
	doublereal *w, doublereal *g, doublereal *errcof, doublereal *rulpts, 
	doublereal *scales, doublereal *norms)
{
    /* System generated locals */
    integer g_dim1, g_offset, i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    integer pow_ii(integer *, integer *);

    /* Local variables */
    static integer i__, j, k;
    static doublereal we[14];
    extern /* Subroutine */ int d113re_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *), d132re_(integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *), d07hre_(integer *, 
	    integer *, doublereal *, doublereal *, doublereal *, doublereal *)
	    , d09hre_(integer *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);

/* ***BEGIN PROLOGUE DINHRE */
/* ***PURPOSE DINHRE computes abscissas and weights of the integration */
/*            rule and the null rules to be used in error estimation. */
/*            These are computed as functions of NDIM and KEY. */
/* ***DESCRIPTION DINHRE will for given values of NDIM and KEY compute or */
/*            select the correct values of the abscissas and */
/*            corresponding weights for different */
/*            integration rules and null rules and assign them to */
/*            G and W. */
/*            The heuristic error coefficients ERRCOF */
/*            will be computed as a function of KEY. */
/*            Scaling factors SCALES and normalization factors NORMS */
/*            used in the error estimation are computed. */


/*   ON ENTRY */

/*     NDIM   Integer. */
/*            Number of variables. */
/*     KEY    Integer. */
/*            Key to selected local integration rule. */
/*     WTLENG Integer. */
/*            The number of weights in each of the rules. */

/*   ON RETURN */

/*     W      Real array of dimension (5,WTLENG). */
/*            The weights for the basic and null rules. */
/*            W(1,1), ...,W(1,WTLENG) are weights for the basic rule. */
/*            W(I,1), ...,W(I,WTLENG), for I > 1 are null rule weights. */
/*     G      Real array of dimension (NDIM,WTLENG). */
/*            The fully symmetric sum generators for the rules. */
/*            G(1,J),...,G(NDIM,J) are the generators for the points */
/*            associated with the the Jth weights. */
/*     ERRCOF Real array of dimension 6. */
/*            Heuristic error coefficients that are used in the */
/*            error estimation in BASRUL. */
/*            It is assumed that the error is computed using: */
/*             IF (N1*ERRCOF(1) < N2 and N2*ERRCOF(2) < N3) */
/*               THEN ERROR = ERRCOF(3)*N1 */
/*               ELSE ERROR = ERRCOF(4)*MAX(N1, N2, N3) */
/*             ERROR = ERROR + EP*(ERRCOF(5)*ERROR/(ES+ERROR)+ERRCOF(6)) */
/*            where N1-N3 are the null rules, EP is the error for */
/*            the parent */
/*            subregion and ES is the error for the sibling subregion. */
/*     RULPTS Real array of dimension WTLENG. */
/*            A work array containing the number of points produced by */
/*            each generator of the selected rule. */
/*     SCALES Real array of dimension (3,WTLENG). */
/*            Scaling factors used to construct new null rules, */
/*            N1, N2 and N3, */
/*            based on a linear combination of two successive null rules */
/*            in the sequence of null rules. */
/*     NORMS  Real array of dimension (3,WTLENG). */
/*            2**NDIM/(1-norm of the null rule constructed by each of */
/*            the scaling factors.) */

/* ***ROUTINES CALLED  D132RE,D113RE,D07HRE,D09HRE */
/* ***END PROLOGUE DINHRE */

/*   Global variables. */


/*   Local variables. */


/* ***FIRST EXECUTABLE STATEMENT DINHRE */

/*   Compute W, G and ERRCOF. */

    /* Parameter adjustments */
    norms -= 4;
    scales -= 4;
    --rulpts;
    g_dim1 = *ndim;
    g_offset = 1 + g_dim1;
    g -= g_offset;
    w -= 6;
    --errcof;

    /* Function Body */
    if (*key == 1) {
	d132re_(wtleng, &w[6], &g[g_offset], &errcof[1], &rulpts[1]);
    } else if (*key == 2) {
	d113re_(wtleng, &w[6], &g[g_offset], &errcof[1], &rulpts[1]);
    } else if (*key == 3) {
	d09hre_(ndim, wtleng, &w[6], &g[g_offset], &errcof[1], &rulpts[1]);
    } else if (*key == 4) {
	d07hre_(ndim, wtleng, &w[6], &g[g_offset], &errcof[1], &rulpts[1]);
    }

/*   Compute SCALES and NORMS. */

    for (k = 1; k <= 3; ++k) {
	i__1 = *wtleng;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (w[k + 1 + i__ * 5] != 0.) {
		scales[k + i__ * 3] = -w[k + 2 + i__ * 5] / w[k + 1 + i__ * 5]
			;
	    } else {
		scales[k + i__ * 3] = 100.;
	    }
	    i__2 = *wtleng;
	    for (j = 1; j <= i__2; ++j) {
		we[j - 1] = w[k + 2 + j * 5] + scales[k + i__ * 3] * w[k + 1 
			+ j * 5];
/* L30: */
	    }
	    norms[k + i__ * 3] = 0.;
	    i__2 = *wtleng;
	    for (j = 1; j <= i__2; ++j) {
		norms[k + i__ * 3] += rulpts[j] * (d__1 = we[j - 1], abs(d__1)
			);
/* L40: */
	    }
	    norms[k + i__ * 3] = pow_ii(&c__2, ndim) / norms[k + i__ * 3];
/* L50: */
	}
/* L100: */
    }
    return 0;

/* ***END DINHRE */

} /* dinhre_ */

/* Subroutine */ int drlhre_(integer *ndim, doublereal *center, doublereal *
	hwidth, integer *wtleng, doublereal *g, doublereal *w, doublereal *
	errcof, integer *numfun, S_fp funsub, doublereal *scales, doublereal *
	norms, doublereal *x, doublereal *null, doublereal *basval, 
	doublereal *rgnerr, doublereal *direct)
{
    /* System generated locals */
    integer null_dim1, null_offset, g_dim1, g_offset, i__1, i__2;
    doublereal d__1, d__2, d__3;

    /* Local variables */
    static integer i__, j, k;
    static doublereal ratio, search, difmax;
    extern /* Subroutine */ int dfshre_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, integer *, S_fp, doublereal *, 
	    doublereal *);
    static doublereal frthdf, difsum;
    static integer divaxn;
    static doublereal rgnvol;

/* ***BEGIN PROLOGUE DRLHRE */
/* ***KEYWORDS basic numerical integration rule */
/* ***PURPOSE  To compute basic integration rule values. */
/* ***AUTHOR   Alan Genz, Computer Science Department, Washington */
/*            State University, Pullman, WA 99163-1210 USA */
/* ***LAST MODIFICATION 90-02-06 */
/* ***DESCRIPTION DRLHRE computes basic integration rule values for a */
/*            vector of integrands over a hyper-rectangular region. */
/*            These are estimates for the integrals. DRLHRE also computes */
/*            estimates for the errors and determines the coordinate axis */
/*            where the fourth difference for the integrands is largest. */

/*   ON ENTRY */

/*   NDIM   Integer. */
/*          Number of variables. */
/*   CENTER Real array of dimension NDIM. */
/*          The coordinates for the center of the region. */
/*   HWIDTH Real Array of dimension NDIM. */
/*          HWIDTH(I) is half of the width of dimension I of the region. */
/*   WTLENG Integer. */
/*          The number of weights in the basic integration rule. */
/*   G      Real array of dimension (NDIM,WTLENG). */
/*          The fully symmetric sum generators for the rules. */
/*          G(1,J), ..., G(NDIM,J) are the are the generators for the */
/*          points associated with the Jth weights. */
/*   W      Real array of dimension (5,WTLENG). */
/*          The weights for the basic and null rules. */
/*          W(1,1),...,W(1,WTLENG) are weights for the basic rule. */
/*          W(I,1),...,W(I,WTLENG), for I > 1 are null rule weights. */
/*   ERRCOF Real array of dimension 6. */
/*          The error coefficients for the rules. */
/*          It is assumed that the error is computed using: */
/*           IF (N1*ERRCOF(1) < N2 and N2*ERRCOF(2) < N3) */
/*             THEN ERROR = ERRCOF(3)*N1 */
/*             ELSE ERROR = ERRCOF(4)*MAX(N1, N2, N3) */
/*           ERROR = ERROR + EP*(ERRCOF(5)*ERROR/(ES+ERROR)+ERRCOF(6)) */
/*          where N1-N4 are the null rules, EP is the error */
/*          for the parent */
/*          subregion and ES is the error for the sibling subregion. */
/*   NUMFUN Integer. */
/*          Number of components for the vector integrand. */
/*   FUNSUB Externally declared subroutine. */
/*          For computing the components of the integrand at a point X. */
/*          It must have parameters (NDIM,X,NUMFUN,FUNVLS). */
/*           Input Parameters: */
/*            X      Real array of dimension NDIM. */
/*                   Defines the evaluation point. */
/*            NDIM   Integer. */
/*                   Number of variables for the integrand. */
/*            NUMFUN Integer. */
/*                   Number of components for the vector integrand. */
/*           Output Parameters: */
/*            FUNVLS Real array of dimension NUMFUN. */
/*                   The components of the integrand at the point X. */
/*   SCALES Real array of dimension (3,WTLENG). */
/*          Scaling factors used to construct new null rules based */
/*          on a linear combination of two successive null rules */
/*          in the sequence of null rules. */
/*   NORMS  Real array of dimension (3,WTLENG). */
/*          2**NDIM/(1-norm of the null rule constructed by each of the */
/*          scaling factors.) */
/*   X      Real Array of dimension NDIM. */
/*          A work array. */
/*   NULL   Real array of dimension (NUMFUN, 8) */
/*          A work array. */

/*   ON RETURN */

/*   BASVAL Real array of dimension NUMFUN. */
/*          The values for the basic rule for each component */
/*          of the integrand. */
/*   RGNERR Real array of dimension NUMFUN. */
/*          The error estimates for each component of the integrand. */
/*   DIRECT Real. */
/*          The coordinate axis where the fourth difference of the */
/*          integrand values is largest. */

/* ***REFERENCES */
/*   A.C.Genz and A.A.Malik, An adaptive algorithm for numerical */
/*   integration over an N-dimensional rectangular region, */
/*   J.Comp.Appl.Math., 6:295-302, 1980. */

/*   T.O.Espelid, Integration Rules, Null Rules and Error */
/*   Estimation, Reports in Informatics 33, Dept. of Informatics, */
/*   Univ. of Bergen, 1988. */

/* ***ROUTINES CALLED: DFSHRE, FUNSUB */

/* ***END PROLOGUE DRLHRE */

/*   Global variables. */


/*   Local variables. */


/* ***FIRST EXECUTABLE STATEMENT DRLHRE */


/*       Compute volume of subregion, initialize DIVAXN and rule sums; */
/*       compute fourth differences and new DIVAXN (RGNERR is used */
/*       for a work array here). The integrand values used for the */
/*       fourth divided differences are accumulated in rule arrays. */

    /* Parameter adjustments */
    --x;
    --hwidth;
    --center;
    norms -= 4;
    scales -= 4;
    w -= 6;
    g_dim1 = *ndim;
    g_offset = 1 + g_dim1;
    g -= g_offset;
    --errcof;
    --rgnerr;
    --basval;
    null_dim1 = *numfun;
    null_offset = 1 + null_dim1;
    null -= null_offset;

    /* Function Body */
    rgnvol = 1.;
    divaxn = 1;
    i__1 = *ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	rgnvol *= hwidth[i__];
	x[i__] = center[i__];
	if (hwidth[i__] > hwidth[divaxn]) {
	    divaxn = i__;
	}
/* L10: */
    }
    (*funsub)(ndim, &x[1], numfun, &rgnerr[1]);
    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {
	basval[j] = w[6] * rgnerr[j];
	for (k = 1; k <= 4; ++k) {
	    null[j + k * null_dim1] = w[k + 6] * rgnerr[j];
/* L20: */
	}
/* L30: */
    }
    difmax = 0.;
/* Computing 2nd power */
    d__1 = g[g_dim1 * 3 + 1] / g[(g_dim1 << 1) + 1];
    ratio = d__1 * d__1;
    i__1 = *ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	x[i__] = center[i__] - hwidth[i__] * g[(g_dim1 << 1) + 1];
	(*funsub)(ndim, &x[1], numfun, &null[null_dim1 * 5 + 1]);
	x[i__] = center[i__] + hwidth[i__] * g[(g_dim1 << 1) + 1];
	(*funsub)(ndim, &x[1], numfun, &null[null_dim1 * 6 + 1]);
	x[i__] = center[i__] - hwidth[i__] * g[g_dim1 * 3 + 1];
	(*funsub)(ndim, &x[1], numfun, &null[null_dim1 * 7 + 1]);
	x[i__] = center[i__] + hwidth[i__] * g[g_dim1 * 3 + 1];
	(*funsub)(ndim, &x[1], numfun, &null[(null_dim1 << 3) + 1]);
	x[i__] = center[i__];
	difsum = 0.;
	i__2 = *numfun;
	for (j = 1; j <= i__2; ++j) {
	    frthdf = (1 - ratio) * 2 * rgnerr[j] - (null[j + null_dim1 * 7] + 
		    null[j + (null_dim1 << 3)]) + ratio * (null[j + null_dim1 
		    * 5] + null[j + null_dim1 * 6]);

/*           Ignore differences below roundoff */

	    if (rgnerr[j] + frthdf / 4 != rgnerr[j]) {
		difsum += abs(frthdf);
	    }
	    for (k = 1; k <= 4; ++k) {
		null[j + k * null_dim1] = null[j + k * null_dim1] + w[k + 11] 
			* (null[j + null_dim1 * 5] + null[j + null_dim1 * 6]) 
			+ w[k + 16] * (null[j + null_dim1 * 7] + null[j + (
			null_dim1 << 3)]);
/* L40: */
	    }
	    basval[j] = basval[j] + w[11] * (null[j + null_dim1 * 5] + null[j 
		    + null_dim1 * 6]) + w[16] * (null[j + null_dim1 * 7] + 
		    null[j + (null_dim1 << 3)]);
/* L50: */
	}
	if (difsum > difmax) {
	    difmax = difsum;
	    divaxn = i__;
	}
/* L60: */
    }
    *direct = (doublereal) divaxn;

/*    Finish computing the rule values. */

    i__1 = *wtleng;
    for (i__ = 4; i__ <= i__1; ++i__) {
	dfshre_(ndim, &center[1], &hwidth[1], &x[1], &g[i__ * g_dim1 + 1], 
		numfun, (S_fp)funsub, &rgnerr[1], &null[null_dim1 * 5 + 1]);
	i__2 = *numfun;
	for (j = 1; j <= i__2; ++j) {
	    basval[j] += w[i__ * 5 + 1] * rgnerr[j];
	    for (k = 1; k <= 4; ++k) {
		null[j + k * null_dim1] += w[k + 1 + i__ * 5] * rgnerr[j];
/* L70: */
	    }
/* L80: */
	}
/* L90: */
    }

/*    Compute errors. */

    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {

/*    We search for the null rule, in the linear space spanned by two */
/*    successive null rules in our sequence, which gives the greatest */
/*    error estimate among all normalized (1-norm) null rules in this */
/*    space. */

	for (i__ = 1; i__ <= 3; ++i__) {
	    search = 0.;
	    i__2 = *wtleng;
	    for (k = 1; k <= i__2; ++k) {
/* Computing MAX */
		d__2 = search, d__3 = (d__1 = null[j + (i__ + 1) * null_dim1] 
			+ scales[i__ + k * 3] * null[j + i__ * null_dim1], 
			abs(d__1)) * norms[i__ + k * 3];
		search = max(d__2,d__3);
/* L100: */
	    }
	    null[j + i__ * null_dim1] = search;
/* L110: */
	}
	if (errcof[1] * null[j + null_dim1] <= null[j + (null_dim1 << 1)] && 
		errcof[2] * null[j + (null_dim1 << 1)] <= null[j + null_dim1 *
		 3]) {
	    rgnerr[j] = errcof[3] * null[j + null_dim1];
	} else {
/* Computing MAX */
	    d__1 = null[j + null_dim1], d__2 = null[j + (null_dim1 << 1)], 
		    d__1 = max(d__1,d__2), d__2 = null[j + null_dim1 * 3];
	    rgnerr[j] = errcof[4] * max(d__1,d__2);
	}
	rgnerr[j] = rgnvol * rgnerr[j];
	basval[j] = rgnvol * basval[j];
/* L130: */
    }

/* ***END DRLHRE */

    return 0;
} /* drlhre_ */


/* Subroutine */ int dtrhre_(integer *dvflag, integer *ndim, integer *numfun, 
	integer *sbrgns, doublereal *values, doublereal *errors, doublereal *
	centrs, doublereal *hwidts, doublereal *greate, doublereal *error, 
	doublereal *value, doublereal *center, doublereal *hwidth, doublereal 
	*dir)
{
    /* System generated locals */
    integer values_dim1, values_offset, errors_dim1, errors_offset, 
	    centrs_dim1, centrs_offset, hwidts_dim1, hwidts_offset, i__1;

    /* Local variables */
    static integer j;
    static doublereal great, direct;
    static integer subrgn, subtmp;

/* ***BEGIN PROLOGUE DTRHRE */
/* ***PURPOSE DTRHRE maintains a heap of subregions. */
/* ***DESCRIPTION DTRHRE maintains a heap of subregions. */
/*            The subregions are ordered according to the size */
/*            of the greatest error estimates of each subregion(GREATE). */

/*   PARAMETERS */

/*     DVFLAG Integer. */
/*            If DVFLAG = 1, we remove the subregion with */
/*            greatest error from the heap. */
/*            If DVFLAG = 2, we insert a new subregion in the heap. */
/*     NDIM   Integer. */
/*            Number of variables. */
/*     NUMFUN Integer. */
/*            Number of components of the integral. */
/*     SBRGNS Integer. */
/*            Number of subregions in the heap. */
/*     VALUES Real array of dimension (NUMFUN,SBRGNS). */
/*            Used to store estimated values of the integrals */
/*            over the subregions. */
/*     ERRORS Real array of dimension (NUMFUN,SBRGNS). */
/*            Used to store the corresponding estimated errors. */
/*     CENTRS Real array of dimension (NDIM,SBRGNS). */
/*            Used to store the center limits of the stored subregions. */
/*     HWIDTS Real array of dimension (NDIM,SBRGNS). */
/*            Used to store the hwidth limits of the stored subregions. */
/*     GREATE Real array of dimension SBRGNS. */
/*            Used to store the greatest estimated errors in */
/*            all subregions. */
/*     ERROR  Real array of dimension NUMFUN. */
/*            Used as intermediate storage for the error of a subregion. */
/*     VALUE  Real array of dimension NUMFUN. */
/*            Used as intermediate storage for the estimate */
/*            of the integral over a subregion. */
/*     CENTER Real array of dimension NDIM. */
/*            Used as intermediate storage for the center of */
/*            the subregion. */
/*     HWIDTH Real array of dimension NDIM. */
/*            Used as intermediate storage for the half width of */
/*            the subregion. */
/*     DIR    Integer array of dimension SBRGNS. */
/*            DIR is used to store the directions for */
/*            further subdivision. */

/* ***ROUTINES CALLED-NONE */
/* ***END PROLOGUE DTRHRE */

/*   Global variables. */


/*   Local variables. */

/*   GREAT  is used as intermediate storage for the greatest error of a */
/*          subregion. */
/*   DIRECT is used as intermediate storage for the direction of further */
/*          subdivision. */
/*   SUBRGN Position of child/parent subregion in the heap. */
/*   SUBTMP Position of parent/child subregion in the heap. */


/* ***FIRST EXECUTABLE STATEMENT DTRHRE */

/*   Save values to be stored in their correct place in the heap. */

    /* Parameter adjustments */
    --hwidth;
    --center;
    hwidts_dim1 = *ndim;
    hwidts_offset = 1 + hwidts_dim1;
    hwidts -= hwidts_offset;
    centrs_dim1 = *ndim;
    centrs_offset = 1 + centrs_dim1;
    centrs -= centrs_offset;
    --value;
    --error;
    errors_dim1 = *numfun;
    errors_offset = 1 + errors_dim1;
    errors -= errors_offset;
    values_dim1 = *numfun;
    values_offset = 1 + values_dim1;
    values -= values_offset;
    --greate;
    --dir;

    /* Function Body */
    great = greate[*sbrgns];
    direct = dir[*sbrgns];
    i__1 = *numfun;
    for (j = 1; j <= i__1; ++j) {
	error[j] = errors[j + *sbrgns * errors_dim1];
	value[j] = values[j + *sbrgns * values_dim1];
/* L5: */
    }
    i__1 = *ndim;
    for (j = 1; j <= i__1; ++j) {
	center[j] = centrs[j + *sbrgns * centrs_dim1];
	hwidth[j] = hwidts[j + *sbrgns * hwidts_dim1];
/* L10: */
    }

/*    If DVFLAG = 1, we will remove the region */
/*    with greatest estimated error from the heap. */

    if (*dvflag == 1) {
	--(*sbrgns);
	subrgn = 1;
L20:
	subtmp = subrgn << 1;
	if (subtmp <= *sbrgns) {
	    if (subtmp != *sbrgns) {

/*   Find max. of left and right child. */

		if (greate[subtmp] < greate[subtmp + 1]) {
		    ++subtmp;
		}
	    }

/*   Compare max.child with parent. */
/*   If parent is max., then done. */

	    if (great < greate[subtmp]) {

/*   Move the values at position subtmp up the heap. */

		greate[subrgn] = greate[subtmp];
		i__1 = *numfun;
		for (j = 1; j <= i__1; ++j) {
		    errors[j + subrgn * errors_dim1] = errors[j + subtmp * 
			    errors_dim1];
		    values[j + subrgn * values_dim1] = values[j + subtmp * 
			    values_dim1];
/* L25: */
		}
		dir[subrgn] = dir[subtmp];
		i__1 = *ndim;
		for (j = 1; j <= i__1; ++j) {
		    centrs[j + subrgn * centrs_dim1] = centrs[j + subtmp * 
			    centrs_dim1];
		    hwidts[j + subrgn * hwidts_dim1] = hwidts[j + subtmp * 
			    hwidts_dim1];
/* L30: */
		}
		subrgn = subtmp;
		goto L20;
	    }
	}
    } else if (*dvflag == 2) {

/*   If DVFLAG = 2, then insert new region in the heap. */

	subrgn = *sbrgns;
L40:
	subtmp = subrgn / 2;
	if (subtmp >= 1) {

/*   Compare max.child with parent. */
/*   If parent is max, then done. */

	    if (great > greate[subtmp]) {

/*   Move the values at position subtmp down the heap. */

		greate[subrgn] = greate[subtmp];
		i__1 = *numfun;
		for (j = 1; j <= i__1; ++j) {
		    errors[j + subrgn * errors_dim1] = errors[j + subtmp * 
			    errors_dim1];
		    values[j + subrgn * values_dim1] = values[j + subtmp * 
			    values_dim1];
/* L45: */
		}
		dir[subrgn] = dir[subtmp];
		i__1 = *ndim;
		for (j = 1; j <= i__1; ++j) {
		    centrs[j + subrgn * centrs_dim1] = centrs[j + subtmp * 
			    centrs_dim1];
		    hwidts[j + subrgn * hwidts_dim1] = hwidts[j + subtmp * 
			    hwidts_dim1];
/* L50: */
		}
		subrgn = subtmp;
		goto L40;
	    }
	}
    }

/*    Insert the saved values in their correct places. */

    if (*sbrgns > 0) {
	greate[subrgn] = great;
	i__1 = *numfun;
	for (j = 1; j <= i__1; ++j) {
	    errors[j + subrgn * errors_dim1] = error[j];
	    values[j + subrgn * values_dim1] = value[j];
/* L55: */
	}
	dir[subrgn] = direct;
	i__1 = *ndim;
	for (j = 1; j <= i__1; ++j) {
	    centrs[j + subrgn * centrs_dim1] = center[j];
	    hwidts[j + subrgn * hwidts_dim1] = hwidth[j];
/* L60: */
	}
    }

/* ***END DTRHRE */

    return 0;
} /* dtrhre_ */


/* Subroutine */ int d07hre_(integer *ndim, integer *wtleng, doublereal *w, 
	doublereal *g, doublereal *errcof, doublereal *rulpts)
{
    /* System generated locals */
    integer g_dim1, g_offset, i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double sqrt(doublereal);

    /* Local variables */
    static integer i__, j;
    static doublereal lam0, lam1, lam2, lamp, ratio, twondm;

/* ***BEGIN PROLOGUE D07HRE */
/* ***KEYWORDS basic integration rule, degree 7 */
/* ***PURPOSE  To initialize a degree 7 basic rule, and null rules. */
/* ***AUTHOR   Alan Genz, Computer Science Department, Washington */
/*            State University, Pullman, WA 99163-1210 USA */
/* ***LAST MODIFICATION 88-05-31 */
/* ***DESCRIPTION  D07HRE initializes a degree 7 integration rule, */
/*            two degree 5 null rules, one degree 3 null rule and one */
/*            degree 1 null rule for the hypercube [-1,1]**NDIM. */

/*   ON ENTRY */

/*   NDIM   Integer. */
/*          Number of variables. */
/*   WTLENG Integer. */
/*          The number of weights in each of the rules. */
/*          WTLENG MUST be set equal to 6. */

/*   ON RETURN */
/*   W      Real array of dimension (5,WTLENG). */
/*          The weights for the basic and null rules. */
/*          W(1,1),...,W(1,WTLENG) are weights for the basic rule. */
/*          W(I,1),...,W(I,WTLENG), for I > 1 are null rule weights. */
/*   G      Real array of dimension (NDIM, WTLENG). */
/*          The fully symmetric sum generators for the rules. */
/*          G(1, J), ..., G(NDIM, J) are the are the generators for the */
/*          points associated with the Jth weights. */
/*   ERRCOF Real array of dimension 6. */
/*          Heuristic error coefficients that are used in the */
/*          error estimation in BASRUL. */
/*   RULPTS Real array of dimension WTLENG. */
/*          A work array. */

/* ***REFERENCES A. Genz and A. Malik, */
/*             "An Imbedded Family of Fully Symmetric Numerical */
/*              Integration Rules", */
/*              SIAM J Numer. Anal. 20 (1983), pp. 580-588. */
/* ***ROUTINES CALLED-NONE */
/* ***END PROLOGUE D07HRE */

/*   Global variables */


/*   Local Variables */


/* ***FIRST EXECUTABLE STATEMENT D07HRE */


/*     Initialize generators, weights and RULPTS */

    /* Parameter adjustments */
    --rulpts;
    g_dim1 = *ndim;
    g_offset = 1 + g_dim1;
    g -= g_offset;
    w -= 6;
    --errcof;

    /* Function Body */
    i__1 = *wtleng;
    for (j = 1; j <= i__1; ++j) {
	i__2 = *ndim;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    g[i__ + j * g_dim1] = 0.;
/* L10: */
	}
	for (i__ = 1; i__ <= 5; ++i__) {
	    w[i__ + j * 5] = 0.;
/* L20: */
	}
	rulpts[j] = (doublereal) (*ndim << 1);
/* L30: */
    }
    twondm = (doublereal) pow_ii(&c__2, ndim);
    rulpts[*wtleng] = twondm;
    rulpts[*wtleng - 1] = (doublereal) ((*ndim << 1) * (*ndim - 1));
    rulpts[1] = 1.;

/*     Compute squared generator parameters */

    lam0 = .4707f;
    lamp = .5625f;
    lam1 = 4 / (15 - 5 / lam0);
    ratio = (1 - lam1 / lam0) / 27;
    lam2 = (5 - lam1 * 7 - ratio * 35) / (7 - lam1 * 35 / 3 - ratio * 35 / 
	    lam0);

/*     Compute degree 7 rule weights */

/* Computing 3rd power */
    d__1 = lam0 * 3;
    w[31] = 1 / (d__1 * (d__1 * d__1)) / twondm;
/* Computing 2nd power */
    d__1 = lam1;
    w[26] = (1 - lam0 * 5 / 3) / ((lam1 - lam0) * 60 * (d__1 * d__1));
    w[16] = (1 - lam2 * 5 / 3 - twondm * 5 * w[31] * lam0 * (lam0 - lam2)) / (
	    lam1 * 10 * (lam1 - lam2)) - ((*ndim - 1) << 1) * w[26];
    w[11] = (1 - lam1 * 5 / 3 - twondm * 5 * w[31] * lam0 * (lam0 - lam1)) / (
	    lam2 * 10 * (lam2 - lam1));

/*     Compute weights for 2 degree 5, 1 degree 3 and 1 degree 1 rules */

/* Computing 3rd power */
    d__1 = lam0;
    w[32] = 1 / (d__1 * (d__1 * d__1) * 36) / twondm;
/* Computing 2nd power */
    d__1 = lam0;
/* Computing 2nd power */
    d__2 = lam1;
    w[27] = (1 - twondm * 9 * w[32] * (d__1 * d__1)) / (d__2 * d__2 * 36);
    w[17] = (1 - lam2 * 5 / 3 - twondm * 5 * w[32] * lam0 * (lam0 - lam2)) / (
	    lam1 * 10 * (lam1 - lam2)) - ((*ndim - 1) << 1) * w[27];
    w[12] = (1 - lam1 * 5 / 3 - twondm * 5 * w[32] * lam0 * (lam0 - lam1)) / (
	    lam2 * 10 * (lam2 - lam1));
/* Computing 3rd power */
    d__1 = lam0;
    w[33] = 5 / (d__1 * (d__1 * d__1) * 108) / twondm;
/* Computing 2nd power */
    d__1 = lam0;
/* Computing 2nd power */
    d__2 = lam1;
    w[28] = (1 - twondm * 9 * w[33] * (d__1 * d__1)) / (d__2 * d__2 * 36);
    w[18] = (1 - lamp * 5 / 3 - twondm * 5 * w[33] * lam0 * (lam0 - lamp)) / (
	    lam1 * 10 * (lam1 - lamp)) - ((*ndim - 1) << 1) * w[28];
    w[23] = (1 - lam1 * 5 / 3 - twondm * 5 * w[33] * lam0 * (lam0 - lam1)) / (
	    lamp * 10 * (lamp - lam1));
/* Computing 3rd power */
    d__1 = lam0;
    w[34] = 1 / (d__1 * (d__1 * d__1) * 54) / twondm;
/* Computing 2nd power */
    d__1 = lam0;
/* Computing 2nd power */
    d__2 = lam1;
    w[29] = (1 - twondm * 18 * w[34] * (d__1 * d__1)) / (d__2 * d__2 * 72);
    w[19] = (1 - lam2 * 10 / 3 - twondm * 10 * w[34] * lam0 * (lam0 - lam2)) /
	     (lam1 * 20 * (lam1 - lam2)) - ((*ndim - 1) << 1) * w[29];
    w[14] = (1 - lam1 * 10 / 3 - twondm * 10 * w[34] * lam0 * (lam0 - lam1)) /
	     (lam2 * 20 * (lam2 - lam1));

/*     Set generator values */

    lam0 = sqrt(lam0);
    lam1 = sqrt(lam1);
    lam2 = sqrt(lam2);
    lamp = sqrt(lamp);
    i__1 = *ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	g[i__ + *wtleng * g_dim1] = lam0;
/* L40: */
    }
    g[(*wtleng - 1) * g_dim1 + 1] = lam1;
    g[(*wtleng - 1) * g_dim1 + 2] = lam1;
    g[(*wtleng - 4) * g_dim1 + 1] = lam2;
    g[(*wtleng - 3) * g_dim1 + 1] = lam1;
    g[(*wtleng - 2) * g_dim1 + 1] = lamp;

/*     Compute final weight values. */
/*     The null rule weights are computed from differences between */
/*     the degree 7 rule weights and lower degree rule weights. */

    w[6] = twondm;
    for (j = 2; j <= 5; ++j) {
	i__1 = *wtleng;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    w[j + i__ * 5] -= w[i__ * 5 + 1];
	    w[j + 5] -= rulpts[i__] * w[j + i__ * 5];
/* L50: */
	}
/* L70: */
    }
    i__1 = *wtleng;
    for (i__ = 2; i__ <= i__1; ++i__) {
	w[i__ * 5 + 1] = twondm * w[i__ * 5 + 1];
	w[6] -= rulpts[i__] * w[i__ * 5 + 1];
/* L80: */
    }

/*     Set error coefficients */

    errcof[1] = 5.;
    errcof[2] = 5.;
    errcof[3] = 1.;
    errcof[4] = 5.;
    errcof[5] = .5f;
    errcof[6] = .25f;

/* ***END D07HRE */

    return 0;
} /* d07hre_ */



/* Subroutine */ int d09hre_(integer *ndim, integer *wtleng, doublereal *w, 
	doublereal *g, doublereal *errcof, doublereal *rulpts)
{
    /* System generated locals */
    integer g_dim1, g_offset, i__1, i__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double sqrt(doublereal);

    /* Local variables */
    static integer i__, j;
    static doublereal lam0, lam1, lam2, lam3, lamp, ratio, twondm;

/* ***BEGIN PROLOGUE D09HRE */
/* ***KEYWORDS basic integration rule, degree 9 */
/* ***PURPOSE  To initialize a degree 9 basic rule and null rules. */
/* ***AUTHOR   Alan Genz, Computer Science Department, Washington */
/*            State University, Pullman, WA 99163-1210 USA */
/* ***LAST MODIFICATION 88-05-20 */
/* ***DESCRIPTION  D09HRE initializes a degree 9 integration rule, */
/*            two degree 7 null rules, one degree 5 null rule and one */
/*            degree 3 null rule for the hypercube [-1,1]**NDIM. */

/*   ON ENTRY */

/*   NDIM   Integer. */
/*          Number of variables. */
/*   WTLENG Integer. */
/*          The number of weights in each of the rules. */

/*   ON RETURN */
/*   W      Real array of dimension (5,WTLENG). */
/*          The weights for the basic and null rules. */
/*          W(1,1),...,W(1,WTLENG) are weights for the basic rule. */
/*          W(I,1),...,W(I,WTLENG), for I > 1 are null rule weights. */
/*   G      Real array of dimension (NDIM, WTLENG). */
/*          The fully symmetric sum generators for the rules. */
/*          G(1, J), ..., G(NDIM, J) are the are the generators for the */
/*          points associated with the Jth weights. */
/*   ERRCOF Real array of dimension 6. */
/*          Heuristic error coefficients that are used in the */
/*          error estimation in BASRUL. */
/*   RULPTS Real array of dimension WTLENG. */
/*          A work array. */

/* ***REFERENCES A. Genz and A. Malik, */
/*             "An Imbedded Family of Fully Symmetric Numerical */
/*              Integration Rules", */
/*              SIAM J Numer. Anal. 20 (1983), pp. 580-588. */
/* ***ROUTINES CALLED-NONE */
/* ***END PROLOGUE D09HRE */

/*   Global variables */


/*   Local Variables */


/* ***FIRST EXECUTABLE STATEMENT D09HRE */


/*     Initialize generators, weights and RULPTS */

    /* Parameter adjustments */
    --rulpts;
    g_dim1 = *ndim;
    g_offset = 1 + g_dim1;
    g -= g_offset;
    w -= 6;
    --errcof;

    /* Function Body */
    i__1 = *wtleng;
    for (j = 1; j <= i__1; ++j) {
	i__2 = *ndim;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    g[i__ + j * g_dim1] = 0.;
/* L10: */
	}
	for (i__ = 1; i__ <= 5; ++i__) {
	    w[i__ + j * 5] = 0.;
/* L20: */
	}
	rulpts[j] = (doublereal) (*ndim << 1);
/* L30: */
    }
    twondm = (doublereal) pow_ii(&c__2, ndim);
    rulpts[*wtleng] = twondm;
    if (*ndim > 2) {
	rulpts[8] = (doublereal) ((*ndim << 2) * (*ndim - 1) * (*ndim - 2) / 
		3);
    }
    rulpts[7] = (doublereal) ((*ndim << 2) * (*ndim - 1));
    rulpts[6] = (doublereal) ((*ndim << 1) * (*ndim - 1));
    rulpts[1] = 1.;

/*     Compute squared generator parameters */

    lam0 = .4707f;
    lam1 = 4 / (15 - 5 / lam0);
    ratio = (1 - lam1 / lam0) / 27;
    lam2 = (5 - lam1 * 7 - ratio * 35) / (7 - lam1 * 35 / 3 - ratio * 35 / 
	    lam0);
    ratio = ratio * (1 - lam2 / lam0) / 3;
    lam3 = (7 - (lam2 + lam1) * 9 + lam2 * 63 * lam1 / 5 - ratio * 63) / (9 - 
	    (lam2 + lam1) * 63 / 5 + lam2 * 21 * lam1 - ratio * 63 / lam0);
    lamp = .0625f;

/*     Compute degree 9 rule weights */

/* Computing 4th power */
    d__1 = lam0 * 3, d__1 *= d__1;
    w[*wtleng * 5 + 1] = 1 / (d__1 * d__1) / twondm;
    if (*ndim > 2) {
/* Computing 3rd power */
	d__1 = lam1 * 6;
	w[41] = (1 - 1 / (lam0 * 3)) / (d__1 * (d__1 * d__1));
    }
    w[36] = (1 - (lam0 + lam1) * 7 / 5 + lam0 * 7 * lam1 / 3) / (lam1 * 84 * 
	    lam2 * (lam2 - lam0) * (lam2 - lam1));
    w[31] = (1 - (lam0 + lam2) * 7 / 5 + lam0 * 7 * lam2 / 3) / (lam1 * 84 * 
	    lam1 * (lam1 - lam0) * (lam1 - lam2)) - w[36] * lam2 / lam1 - ((
	    *ndim - 2) << 1) * w[41];
    w[21] = (1 - ((lam0 + lam1 + lam2) / 7 - (lam0 * lam1 + lam0 * lam2 + 
	    lam1 * lam2) / 5) * 9 - lam0 * 3 * lam1 * lam2) / (lam3 * 18 * (
	    lam3 - lam0) * (lam3 - lam1) * (lam3 - lam2));
    w[16] = (1 - ((lam0 + lam1 + lam3) / 7 - (lam0 * lam1 + lam0 * lam3 + 
	    lam1 * lam3) / 5) * 9 - lam0 * 3 * lam1 * lam3) / (lam2 * 18 * (
	    lam2 - lam0) * (lam2 - lam1) * (lam2 - lam3)) - ((*ndim - 1) << 1) *
	     w[36];
    w[11] = (1 - ((lam0 + lam2 + lam3) / 7 - (lam0 * lam2 + lam0 * lam3 + 
	    lam2 * lam3) / 5) * 9 - lam0 * 3 * lam2 * lam3) / (lam1 * 18 * (
	    lam1 - lam0) * (lam1 - lam2) * (lam1 - lam3)) - ((*ndim - 1) << 1) *
	     (w[36] + w[31] + (*ndim - 2) * w[41]);

/*     Compute weights for 2 degree 7, 1 degree 5 and 1 degree 3 rules */

/* Computing 4th power */
    d__1 = lam0, d__1 *= d__1;
    w[*wtleng * 5 + 2] = 1 / (d__1 * d__1 * 108) / twondm;
    if (*ndim > 2) {
/* Computing 3rd power */
	d__1 = lam0;
/* Computing 3rd power */
	d__2 = lam1 * 6;
	w[42] = (1 - twondm * 27 * w[47] * (d__1 * (d__1 * d__1))) / (d__2 * (
		d__2 * d__2));
    }
/* Computing 2nd power */
    d__1 = lam0;
    w[37] = (1 - lam1 * 5 / 3 - twondm * 15 * w[*wtleng * 5 + 2] * (d__1 * 
	    d__1) * (lam0 - lam1)) / (lam1 * 60 * lam2 * (lam2 - lam1));
/* Computing 2nd power */
    d__1 = lam0;
    w[32] = (1 - (lam1 * 8 * lam2 * w[37] + twondm * w[*wtleng * 5 + 2] * (
	    d__1 * d__1)) * 9) / (lam1 * 36 * lam1) - w[42] * 2 * (*ndim - 2);
    w[22] = (1 - ((lam1 + lam2) / 5 - lam1 * lam2 / 3 + twondm * w[*wtleng * 
	    5 + 2] * lam0 * (lam0 - lam1) * (lam0 - lam2)) * 7) / (lam3 * 14 *
	     (lam3 - lam1) * (lam3 - lam2));
    w[17] = (1 - ((lam1 + lam3) / 5 - lam1 * lam3 / 3 + twondm * w[*wtleng * 
	    5 + 2] * lam0 * (lam0 - lam1) * (lam0 - lam3)) * 7) / (lam2 * 14 *
	     (lam2 - lam1) * (lam2 - lam3)) - ((*ndim - 1) << 1) * w[37];
    w[12] = (1 - ((lam2 + lam3) / 5 - lam2 * lam3 / 3 + twondm * w[*wtleng * 
	    5 + 2] * lam0 * (lam0 - lam2) * (lam0 - lam3)) * 7) / (lam1 * 14 *
	     (lam1 - lam2) * (lam1 - lam3)) - ((*ndim - 1) << 1) * (w[37] + w[
	    32] + (*ndim - 2) * w[42]);
/* Computing 4th power */
    d__1 = lam0, d__1 *= d__1;
    w[*wtleng * 5 + 3] = 5 / (d__1 * d__1 * 324) / twondm;
    if (*ndim > 2) {
/* Computing 3rd power */
	d__1 = lam0;
/* Computing 3rd power */
	d__2 = lam1 * 6;
	w[43] = (1 - twondm * 27 * w[48] * (d__1 * (d__1 * d__1))) / (d__2 * (
		d__2 * d__2));
    }
/* Computing 2nd power */
    d__1 = lam0;
    w[38] = (1 - lam1 * 5 / 3 - twondm * 15 * w[*wtleng * 5 + 3] * (d__1 * 
	    d__1) * (lam0 - lam1)) / (lam1 * 60 * lam2 * (lam2 - lam1));
/* Computing 2nd power */
    d__1 = lam0;
    w[33] = (1 - (lam1 * 8 * lam2 * w[38] + twondm * w[*wtleng * 5 + 3] * (
	    d__1 * d__1)) * 9) / (lam1 * 36 * lam1) - w[43] * 2 * (*ndim - 2);
    w[28] = (1 - ((lam1 + lam2) / 5 - lam1 * lam2 / 3 + twondm * w[*wtleng * 
	    5 + 3] * lam0 * (lam0 - lam1) * (lam0 - lam2)) * 7) / (lamp * 14 *
	     (lamp - lam1) * (lamp - lam2));
    w[18] = (1 - ((lam1 + lamp) / 5 - lam1 * lamp / 3 + twondm * w[*wtleng * 
	    5 + 3] * lam0 * (lam0 - lam1) * (lam0 - lamp)) * 7) / (lam2 * 14 *
	     (lam2 - lam1) * (lam2 - lamp)) - ((*ndim - 1) << 1) * w[38];
    w[13] = (1 - ((lam2 + lamp) / 5 - lam2 * lamp / 3 + twondm * w[*wtleng * 
	    5 + 3] * lam0 * (lam0 - lam2) * (lam0 - lamp)) * 7) / (lam1 * 14 *
	     (lam1 - lam2) * (lam1 - lamp)) - ((*ndim - 1) << 1) * (w[38] + w[
	    33] + (*ndim - 2) * w[43]);
/* Computing 4th power */
    d__1 = lam0, d__1 *= d__1;
    w[*wtleng * 5 + 4] = 2 / (d__1 * d__1 * 81) / twondm;
    if (*ndim > 2) {
/* Computing 3rd power */
	d__1 = lam0;
/* Computing 3rd power */
	d__2 = lam1 * 6;
	w[44] = (2 - twondm * 27 * w[49] * (d__1 * (d__1 * d__1))) / (d__2 * (
		d__2 * d__2));
    }
    w[39] = (2 - lam1 * 15 / 9 - twondm * 15 * w[*wtleng * 5 + 4] * lam0 * (
	    lam0 - lam1)) / (lam1 * 60 * lam2 * (lam2 - lam1));
/* Computing 2nd power */
    d__1 = lam0;
    w[34] = (1 - (lam1 * 8 * lam2 * w[39] + twondm * w[*wtleng * 5 + 4] * (
	    d__1 * d__1)) * 9) / (lam1 * 36 * lam1) - w[44] * 2 * (*ndim - 2);
    w[24] = (2 - ((lam1 + lam2) / 5 - lam1 * lam2 / 3 + twondm * w[*wtleng * 
	    5 + 4] * lam0 * (lam0 - lam1) * (lam0 - lam2)) * 7) / (lam3 * 14 *
	     (lam3 - lam1) * (lam3 - lam2));
    w[19] = (2 - ((lam1 + lam3) / 5 - lam1 * lam3 / 3 + twondm * w[*wtleng * 
	    5 + 4] * lam0 * (lam0 - lam1) * (lam0 - lam3)) * 7) / (lam2 * 14 *
	     (lam2 - lam1) * (lam2 - lam3)) - ((*ndim - 1) << 1) * w[39];
    w[14] = (2 - ((lam2 + lam3) / 5 - lam2 * lam3 / 3 + twondm * w[*wtleng * 
	    5 + 4] * lam0 * (lam0 - lam2) * (lam0 - lam3)) * 7) / (lam1 * 14 *
	     (lam1 - lam2) * (lam1 - lam3)) - ((*ndim - 1) << 1) * (w[39] + w[
	    34] + (*ndim - 2) * w[44]);
    w[15] = 1 / (lam1 * 6);

/*     Set generator values */

    lam0 = sqrt(lam0);
    lam1 = sqrt(lam1);
    lam2 = sqrt(lam2);
    lam3 = sqrt(lam3);
    lamp = sqrt(lamp);
    i__1 = *ndim;
    for (i__ = 1; i__ <= i__1; ++i__) {
	g[i__ + *wtleng * g_dim1] = lam0;
/* L40: */
    }
    if (*ndim > 2) {
	g[(g_dim1 << 3) + 1] = lam1;
	g[(g_dim1 << 3) + 2] = lam1;
	g[(g_dim1 << 3) + 3] = lam1;
    }
    g[g_dim1 * 7 + 1] = lam1;
    g[g_dim1 * 7 + 2] = lam2;
    g[g_dim1 * 6 + 1] = lam1;
    g[g_dim1 * 6 + 2] = lam1;
    g[g_dim1 * 5 + 1] = lamp;
    g[(g_dim1 << 2) + 1] = lam3;
    g[g_dim1 * 3 + 1] = lam2;
    g[(g_dim1 << 1) + 1] = lam1;

/*     Compute final weight values. */
/*     The null rule weights are computed from differences between */
/*     the degree 9 rule weights and lower degree rule weights. */

    w[6] = twondm;
    for (j = 2; j <= 5; ++j) {
	i__1 = *wtleng;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    w[j + i__ * 5] -= w[i__ * 5 + 1];
	    w[j + 5] -= rulpts[i__] * w[j + i__ * 5];
/* L50: */
	}
/* L70: */
    }
    i__1 = *wtleng;
    for (i__ = 2; i__ <= i__1; ++i__) {
	w[i__ * 5 + 1] = twondm * w[i__ * 5 + 1];
	w[6] -= rulpts[i__] * w[i__ * 5 + 1];
/* L80: */
    }

/*     Set error coefficients */

    errcof[1] = 5.;
    errcof[2] = 5.;
    errcof[3] = 1.f;
    errcof[4] = 5.;
    errcof[5] = .5f;
    errcof[6] = .25f;

/* ***END D09HRE */

    return 0;
} /* d09hre_ */


/* Subroutine */ int d113re_(integer *wtleng, doublereal *w, doublereal *g, 
	doublereal *errcof, doublereal *rulpts)
{
    /* Initialized data */

    static doublereal dim3g[14] = { .19,.5,.75,.8,.9949999999999999,
	    .99873449983514,.7793703685672423,.9999698993088767,
	    .7902637224771788,.4403396687650737,.4378478459006862,
	    .9549373822794593,.9661093133630748,.4577105877763134 };
    static doublereal dim3w[65]	/* was [13][5] */ = { .007923078151105734,
	    .0679717739278808,.001086986538805825,.1838633662212829,
	    .03362119777829031,.01013751123334062,.001687648683985235,
	    .1346468564512807,.001750145884600386,.07752336383837454,
	    .2461864902770251,.06797944868483039,.01419962823300713,
	    1.715006248224684,-.3755893815889209,.1488632145140549,
	    -.2497046640620823,.1792501419135204,.00344612675897389,
	    -.005140483185555825,.006536017839876425,-6.5134549392297e-4,
	    -.006304672433547204,.01266959399788263,-.005454241018647931,
	    .004826995274768427,1.936014978949526,-.3673449403754268,
	    .02929778657898176,-.1151883520260315,.05086658220872218,
	    .04453911087786469,-.022878282571259,.02908926216345833,
	    -.002898884350669207,-.02805963413307495,.05638741361145884,
	    -.02427469611942451,.02148307034182882,.517082819560576,
	    .01445269144914044,-.3601489663995932,.3628307003418485,
	    .007148802650872729,-.09222852896022966,.01719339732471725,
	    -.102141653746035,-.007504397861080493,.01648362537726711,
	    .05234610158469334,.01445432331613066,.003019236275367777,
	    2.05440450381852,.0137775998849012,-.576806291790441,
	    .03726835047700328,.006814878939777219,.05723169733851849,
	    -.04493018743811285,.02729236573866348,3.54747395055699e-4,
	    .01571366799739551,.04990099219278567,.0137791555266677,
	    .002878206423099872 };

    static integer i__, j;

/* ***BEGIN PROLOGUE D113RE */
/* ***AUTHOR   Jarle Berntsen, EDB-senteret, */
/*            University of Bergen, Thormohlens gt. 55, */
/*            N-5008 Bergen, NORWAY */
/* ***PURPOSE D113RE computes abscissas and weights of a 3 dimensional */
/*            integration rule of degree 11. */
/*            Two null rules of degree 9, one null rule of degree 7 */
/*            and one null rule of degree 5 to be used in error */
/*            estimation are also computed. */
/* ***DESCRIPTION D113RE will select the correct values of the abscissas */
/*            and corresponding weights for different */
/*            integration rules and null rules and assign them to G */
/*            and W. */
/*            The heuristic error coefficients ERRCOF */
/*            will also be computed. */


/*   ON ENTRY */

/*     WTLENG Integer. */
/*            The number of weights in each of the rules. */

/*   ON RETURN */

/*     W      Real array of dimension (5,WTLENG). */
/*            The weights for the basic and null rules. */
/*            W(1,1),...,W(1,WTLENG) are weights for the basic rule. */
/*            W(I,1),...,W(I,WTLENG), for I > 1 are null rule weights. */
/*     G      Real array of dimension (NDIM,WTLENG). */
/*            The fully symmetric sum generators for the rules. */
/*            G(1,J),...,G(NDIM,J) are the generators for the points */
/*            associated with the the Jth weights. */
/*     ERRCOF Real array of dimension 6. */
/*            Heuristic error coefficients that are used in the */
/*            error estimation in BASRUL. */
/*     RULPTS Real array of dimension WTLENG. */
/*            The number of points used by each generator. */

/* ***REFERENCES  J.Berntsen, Cautious adaptive numerical integration */
/*               over the 3-cube, Reports in Informatics 17, Dept. of */
/*               Inf.,Univ. of Bergen, Norway, 1985. */
/*               J.Berntsen and T.O.Espelid, On the construction of */
/*               higher degree three-dimensional embedded integration */
/*               rules, SIAM J. Numer. Anal.,Vol. 25,No. 1, pp.222-234, */
/*               1988. */
/* ***ROUTINES CALLED-NONE */
/* ***END PROLOGUE D113RE */

/*   Global variables. */


/*   Local variables. */


    /* Parameter adjustments */
    --rulpts;
    g -= 4;
    w -= 6;
    --errcof;

    /* Function Body */






/* ***FIRST EXECUTABLE STATEMENT D113RE */

/*   Assign values to W. */

    for (i__ = 1; i__ <= 13; ++i__) {
	for (j = 1; j <= 5; ++j) {
	    w[j + i__ * 5] = dim3w[i__ + j * 13 - 14];
/* L10: */
	}
    }

/*   Assign values to G. */

    for (i__ = 1; i__ <= 3; ++i__) {
	for (j = 1; j <= 13; ++j) {
	    g[i__ + j * 3] = 0.;
/* L20: */
	}
    }
    g[7] = dim3g[0];
    g[10] = dim3g[1];
    g[13] = dim3g[2];
    g[16] = dim3g[3];
    g[19] = dim3g[4];
    g[22] = dim3g[5];
    g[23] = g[22];
    g[25] = dim3g[6];
    g[26] = g[25];
    g[28] = dim3g[7];
    g[29] = g[28];
    g[30] = g[28];
    g[31] = dim3g[8];
    g[32] = g[31];
    g[33] = g[31];
    g[34] = dim3g[9];
    g[35] = g[34];
    g[36] = g[34];
    g[37] = dim3g[11];
    g[38] = dim3g[10];
    g[39] = g[38];
    g[40] = dim3g[12];
    g[41] = g[40];
    g[42] = dim3g[13];

/*   Assign values to RULPTS. */

    rulpts[1] = 1.;
    rulpts[2] = 6.;
    rulpts[3] = 6.;
    rulpts[4] = 6.;
    rulpts[5] = 6.;
    rulpts[6] = 6.;
    rulpts[7] = 12.;
    rulpts[8] = 12.;
    rulpts[9] = 8.;
    rulpts[10] = 8.;
    rulpts[11] = 8.;
    rulpts[12] = 24.;
    rulpts[13] = 24.;

/*   Assign values to ERRCOF. */

    errcof[1] = 4.;
    errcof[2] = 4.f;
    errcof[3] = .5f;
    errcof[4] = 3.f;
    errcof[5] = .5f;
    errcof[6] = .25f;

/* ***END D113RE */

    j=*wtleng; // silence compiler warning adf 021003
    return 0;
} /* d113re_ */


/* Subroutine */ int d132re_(integer *wtleng, doublereal *w, doublereal *g, 
	doublereal *errcof, doublereal *rulpts)
{
    /* Initialized data */

    static doublereal dim2g[16] = { .2517129343453109,.7013933644534266,
	    .9590960631619962,.9956010478552127,.5,.1594544658297559,
	    .3808991135940188,.6582769255267192,.8761473165029315,
	    .998243184053198,.9790222658168462,.6492284325645389,
	    .8727421201131239,.3582614645881228,.5666666666666666,
	    .2077777777777778 };
    static doublereal dim2w[70]	/* was [14][5] */ = { .0337969236013446,
	    .09508589607597761,.1176006468056962,.0265777458632695,
	    .0170144177020064,0.,.0162659309863741,.1344892658526199,
	    .1328032165460149,.0563747476999187,.0039082790813105,
	    .0301279877743215,.1030873234689166,.0625,.3213775489050763,
	    -.1767341636743844,.07347600537466072,-.03638022004364754,
	    .02125297922098712,.1460984204026913,.01747613286152099,
	    .1444954045641582,1.307687976001325e-4,5.380992313941161e-4,
	    1.042259576889814e-4,-.001401152865045733,.008041788181514763,
	    -.1420416552759383,.3372900883288987,-.1644903060344491,
	    .07707849911634622,-.0380447835850631,.02223559940380806,
	    .1480693879765931,4.467143702185814e-6,.150894476707413,
	    3.647200107516215e-5,5.77719899901388e-4,1.041757313688177e-4,
	    -.001452822267047819,.008338339968783705,-.147279632923196,
	    -.8264123822525677,.306583861409436,.002389292538329435,
	    -.1343024157997222,.088333668405339,0.,9.786283074168292e-4,
	    -.1319227889147519,.00799001220015063,.003391747079760626,
	    .002294915718283264,-.01358584986119197,.04025866859057809,
	    .003760268580063992,.6539094339575232,-.2041614154424632,
	    -.174698151579499,.03937939671417803,.006974520545933992,0.,
	    .006667702171778258,.05512960621544304,.05443846381278607,
	    .02310903863953934,.01506937747477189,-.0605702164890189,
	    .04225737654686337,.02561989142123099 };

    static integer i__, j;

/* ***BEGIN PROLOGUE D132RE */
/* ***AUTHOR   Jarle Berntsen, EDB-senteret, */
/*            University of Bergen, Thormohlens gt. 55, */
/*            N-5008 Bergen, NORWAY */
/* ***PURPOSE D132RE computes abscissas and weights of a 2 dimensional */
/*            integration rule of degree 13. */
/*            Two null rules of degree 11, one null rule of degree 9 */
/*            and one null rule of degree 7 to be used in error */
/*            estimation are also computed. */
/* ***DESCRIPTION D132RE will select the correct values of the abscissas */
/*            and corresponding weights for different */
/*            integration rules and null rules and assign them to */
/*            G and W. The heuristic error coefficients ERRCOF */
/*            will also be assigned. */


/*   ON ENTRY */

/*     WTLENG Integer. */
/*            The number of weights in each of the rules. */

/*   ON RETURN */

/*     W      Real array of dimension (5,WTLENG). */
/*            The weights for the basic and null rules. */
/*            W(1,1),...,W(1,WTLENG) are weights for the basic rule. */
/*            W(I,1),...,W(I,WTLENG), for I > 1 are null rule weights. */
/*     G      Real array of dimension (NDIM,WTLENG). */
/*            The fully symmetric sum generators for the rules. */
/*            G(1,J),...,G(NDIM,J) are the generators for the points */
/*            associated with the the Jth weights. */
/*     ERRCOF Real array of dimension 6. */
/*            Heuristic error coefficients that are used in the */
/*            error estimation in BASRUL. */
/*     RULPTS Real array of dimension WTLENG. */
/*            The number of points produced by each generator. */
/* ***REFERENCES S.Eriksen, */
/*              Thesis of the degree cand.scient, Dept. of Informatics, */
/*              Univ. of Bergen,Norway, 1984. */

/* ***ROUTINES CALLED-NONE */
/* ***END PROLOGUE D132RE */

/*   Global variables */


/*   Local variables. */


    /* Parameter adjustments */
    --rulpts;
    g -= 3;
    w -= 6;
    --errcof;

    /* Function Body */






/* ***FIRST EXECUTABLE STATEMENT D132RE */

/*   Assign values to W. */

    for (i__ = 1; i__ <= 14; ++i__) {
	for (j = 1; j <= 5; ++j) {
	    w[j + i__ * 5] = dim2w[i__ + j * 14 - 15];
/* L10: */
	}
    }

/*   Assign values to G. */

    for (i__ = 1; i__ <= 2; ++i__) {
	for (j = 1; j <= 14; ++j) {
	    g[i__ + (j << 1)] = 0.;
/* L20: */
	}
    }
    g[5] = dim2g[0];
    g[7] = dim2g[1];
    g[9] = dim2g[2];
    g[11] = dim2g[3];
    g[13] = dim2g[4];
    g[15] = dim2g[5];
    g[16] = g[15];
    g[17] = dim2g[6];
    g[18] = g[17];
    g[19] = dim2g[7];
    g[20] = g[19];
    g[21] = dim2g[8];
    g[22] = g[21];
    g[23] = dim2g[9];
    g[24] = g[23];
    g[25] = dim2g[10];
    g[26] = dim2g[11];
    g[27] = dim2g[12];
    g[28] = dim2g[13];
    g[29] = dim2g[14];
    g[30] = dim2g[15];

/*   Assign values to RULPTS. */

    rulpts[1] = 1.;
    for (i__ = 2; i__ <= 11; ++i__) {
	rulpts[i__] = 4.;
/* L30: */
    }
    rulpts[12] = 8.;
    rulpts[13] = 8.;
    rulpts[14] = 8.;

/*   Assign values to ERRCOF. */

    errcof[1] = 10.;
    errcof[2] = 10.;
    errcof[3] = 1.f;
    errcof[4] = 5.f;
    errcof[5] = .5f;
    errcof[6] = .25f;

/* ***END D132RE */
    j=*wtleng; // silence compiler warning adf 021003

    return 0;
} /* d132re_ */




integer pow_ii(integer *ap, integer *bp)
{
  integer pow, x, n;
  unsigned long u;
  
  x = *ap;
  n = *bp;
  
  if (n <= 0) {
    if (n == 0 || x == 1)
      return 1;
    if (x != -1)
      return x == 0 ? 1/x : 0;
    n = -n;
  }
  u = n;
  for(pow = 1; ; )
    {
      if(u & 01)
	pow *= x;
      if(u >>= 1)
	x *= x;
      else
	break;
    }
  return(pow);
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
