
260608

copy out of kat_aa
complex_t kphasefunc_mnmn(int n1, int m1, int n2, int m2, complex_t qx1, complex_t qy1, 
													complex_t qx2, complex_t qy2, double nr1, int mirror_index) 
{
  //! Bring in the mirror phase map
  mirror_t *mirror;
  mirror = &(inter.mirror_list[mirror_index]);

  //warn("Using set_k_mirror_phasefunc\n");
  int n, m;

  int err;
  int component;
  int node1_index, node2_index;
  int mismatch;

  int n1, m1, n2, m2;
  double nr1 = 0.0, nr2 = 0.0;
  double phase = 0.0;
	//  complex_t kx, ky;
  //double kmx, kmy;

  ABCD_t trans1;
  complex_t qx1, qy1, qx2, qy2;
  complex_t qxt, qyt;
  complex_t qxt2, qyt2;
  mirror_t *mirror;

  node_t node1, node2;

  // sanity check on input
  // mirror index should be in the range 0 <= mirror_index < inter.num_mirrors
  assert(mirror_index >= 0);
  assert(mirror_index < inter.num_mirrors);

  mirror = &(inter.mirror_list[mirror_index]);
  component = get_overall_component_index(MIRROR, mirror_index);

  node1_index = mirror->node1_index;
  node2_index = mirror->node2_index;

  node1 = inter.node_list[node1_index];
  node2 = inter.node_list[node2_index];

  if (NOT node1.gnd_node) {
    //node1 = inter.node_list[node1_index];
    nr1 = *node1.n;
  }

  if (NOT node2.gnd_node) {
    //node2 = inter.node_list[node2_index];
    nr2 = *node2.n;
  }

  mismatch = 0;
  if (NOT node1.gnd_node) {
    if (node1.component_index == component) {
      // reverse q so that beam comes _to_ the mirror from node1
      qx1 = cminus(cconj(node1.qx));
      qy1 = cminus(cconj(node1.qy));
    }
    else {
      qx1 = node1.qx;
      qy1 = node1.qy;
    }
  }

  if (NOT node2.gnd_node) {
    if (node2.component_index == component) {
      qx2 = node2.qx;
      qy2 = node2.qy;
    }
    else {
      // reverse q so that beam goes _from_ the mirror to node2,
      // this requires to use -beta for the recflection node2 -> node2 
      qx2 = cminus(cconj(node2.qx));
      qy2 = cminus(cconj(node2.qy));
    }
  }

  if (NOT node1.gnd_node) {
  warn("Relection1\n");
    // reflection1
    err = component_matrix(&trans1, component, node1_index, node1_index, 
        TANGENTIAL);
   if (!err) {
		 //warn("!err\n");
		 qxt = q1_q2(trans1, qx1, nr1, nr1);
		 
		 err = component_matrix(&trans1, component, node1_index, node1_index, 
														SAGITTAL);
		 qyt = q1_q2(trans1, qy1, nr1, nr1);
		 qxt2 = cminus(cconj(qx1));
		 qyt2 = cminus(cconj(qy1));
		 
		 if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
			 mismatch = mismatch | 1;
		 }
		 
		 for (n=0; n<inter.num_fields; n++) {
			 for (m=0; m<inter.num_fields; m++) {
				 get_tem_modes_from_field_index(&n1, &m1, n);
				 get_tem_modes_from_field_index(&n2, &m2, m);
				 
				 mirror->k11[n][m] = kphasefunc_mnmn(n1, m1, n2, m2, qx1, qy1, qxt2, qyt2, nr1,mirror_index);
			 }
		 }
	 }
  }

  if (NOT node2.gnd_node) {
  warn("Relection2\n");
    // reflection2
    // here we use -beta !
    err = component_matrix(&trans1, component, node2_index, node2_index, 
        TANGENTIAL);
    if (!err) {
      qxt = q1_q2(trans1, cminus(cconj(qx2)), nr2, nr2);
      err = component_matrix(&trans1, component, node2_index, node2_index, 
          SAGITTAL);
      qyt = q1_q2(trans1, cminus(cconj(qy2)), nr2, nr2);
      qxt2 = qx2;
      qyt2 = qy2;
			
      if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
        mismatch = mismatch | 2;
      }

      

      for (n=0; n<inter.num_fields; n++) {
        for (m=0; m<inter.num_fields; m++) {
          get_tem_modes_from_field_index(&n1, &m1, n);
          get_tem_modes_from_field_index(&n2, &m2, m);
          mirror->k22[n][m] = cconj(z_by_phr(kphasefunc_mnmn(n1, m1, n2, m2, qx2, qy2, qxt2, qyt2, nr1,mirror_index), -phase));
					//warn("k22[n][m]: %s\n",complex_form(mirror->k22[n][m]));
        }
      }
    }
  }
  return (mismatch);
}




26.05.08
complex_t kphasemap_mnmn(int action, int n1, int m1, int n2, int m2, complex_t qx1, complex_t qy1, complex_t qx2, 
												 complex_t qy2, double nr1, double nr2, int mirror_index, int map_index) 
{
  surface_map_t *map;
  map = inter.mirror_list[mirror_index].map[map_index];

  complex_t result;
  double x,y;
  complex_t u, udash;

  int i, j;
  double A,B;
  double k;
  
  if (action==REFLECTION) {
    // reflection
    if (!map->is_reflection) {
      // transmission maps have no influence on reflection
      return(result);
    }
    A=0.0;
    B=1.0;
    k = TWOPI / init.lambda * 2*nr1;
  }
  else {
    // transmission
    if (map->is_reflection) {
      A=1.0;
      B=-1.0;
      k = TWOPI / init.lambda * (nr1-nr2);
    }
    else {
      A=0.0;
      B=1.0;
      k = TWOPI / init.lambda;
    }
  }
  
  result=complex_0;

  double x_store;;
  if (map->is_phase) {
    for(i=0; i<map->cols; i++) {
      x_store=x=(i-map->x0)*map->xstep;
      for(j=0; j<map->rows; j++) {
        y=(j-map->y0)*map->ystep;
        x=x_store;
        rotate_vector(&x, &y, -1.0*map->angle);
        u = z_by_phr(u_nm(n1,m1,qx1,qy1,x,y,nr1),k*map->data[j][i]);
        udash = cconj(u_nm(n2,m2,qx1,qy1,x,y,nr1)) ;
        result=z_pl_z(result,z_by_z(u,udash));
      }
    }
  }
  else {
    for(i=0; i<map->cols; i++) {
      x_store=x=(i-map->x0)*map->xstep;
      for(j=0; j<map->rows; j++) {
        y=(j-map->y0)*map->ystep;        
        x=x_store;
        rotate_vector(&x, &y, -1.0*map->angle);
        u = z_by_x(u_nm(n1,m1,qx1,qy1,x,y,nr1),A+B*map->data[j][i]);
        udash = cconj(u_nm(n2,m2,qx1,qy1,x,y,nr1)) ;
        result=z_pl_z(result,z_by_z(u,udash));
      }
    }

  }

	result=z_by_x(result,map->xstep*map->ystep);
  //	result = rev_gouy(result, n1, m1, n2, m2, qx1, qx2, qy1, qy2);

  if (inter.debug & 64) {
    debug_msg("(%d %d) -> (%d %d) %d,%d %s\n",n1,m1,n2,m2,map->is_phase, map->is_reflection, complex_form(result));
  }

	return (result);
}




250508



//! Read map command
/*!
 * This command is used to associate discrete phase or amplitude maps with
 * a mirror or beamsplitter
 *
 * \param command_string the input command string
 *
 * 
 */
void read_coefficients(const char *command_string)
{
  char command_name[MAX_TOKEN_LEN] = { 0 };
  char component_name[MAX_TOKEN_LEN] = { 0 };
  char parameter_filename[LINE_LEN] = { 0 };
  char rest_string[REST_STRING_LEN] = { 0 };

  lastparam = -1;

	int num_vars_read = sscanf(command_string, 
												 "%14s %14s %s %80s", 
												 command_name, component_name, 
												 parameter_filename, 
												 rest_string);
	if (num_vars_read < 3) {
		gerror("line '%s':\n"
					 "expected 'coefficients component filename'\n", 
					 command_string);
	}
	else {
		if (num_vars_read > 3) {
			warn("line '%s':\ntext '%s' ignored\n", command_string, rest_string);
		}
	}

	int component_index = get_component_index_from_name(component_name);
  if (component_index == NOT_FOUND) {
    gerror("line '%s':\nno such component '%s'\n", 
        command_string, component_name);
  }
  int component_type = get_component_type_decriment_index(&component_index);
	if (component_type!=MIRROR)
		gerror("line '%s':\nmaps cannot be applied to component '%s'\n", 
	        command_string, component_name);
	
  mirror_t *mirror;
  mirror = &inter.mirror_list[component_index];

	if (mirror->num_maps == MAX_MAPS) {
		gerror("line '%s':\ntoo many maps for this mirror\n", command_string);
	}

	mirror->map[mirror->num_maps] = (surface_map_t *) calloc(2, sizeof (surface_map_t));
	if (mirror->map[mirror->num_maps]==NULL) {
		bug_error("memory allocation for surface map failed");
	}

	strcpy(mirror->map[mirror->num_maps]->filename,parameter_filename);	
	read_surface_map_coefficients(mirror->map[mirror->num_maps]);
	mirror->num_maps++;
}



//! Read map coefficients from a file
/*!
 * \param map structure to store map info and data
 */
void read_surface_map_coefficients(surface_map_t *map)
{
	FILE *mapfile;
	mapfile=fopen(map->filename, "r");
	
	if (mapfile==NULL) {
		gerror("could not open file '%s'\n",map->filename);
	}

	int found = search_file_for_headers(mapfile, 1);
	if (!found) {
		gerror("could not find map coefficient header in file '%s'\n",map->filename);
	}

	read_map_coefficient_header(mapfile, map);
	
	allocate_surface_map(map);

	int i;
	int loaded;
	complex_t tmpv1, tmpv2, tmpv3, tmpv4;
	int n1, n2, m1, m2;

	int num_lines=(int) (map->maxtem+1) * (map->maxtem+2);

	for (i=0;i<num_lines; i++) {
		loaded = fscanf(mapfile, "%d %d %d %d   %lf %lf %lf %lf %lf %lf %lf %lf", 
										&n1, &m1, &n2, &m2, &tmpv1.re,
										&tmpv1.im, &tmpv2.re, &tmpv2.im, &tmpv3.re, &tmpv3.im, &tmpv4.re, &tmpv4.im);
		if(loaded == EOF || loaded<12) {
			gerror("Missing values in file '%s', the specified maxtem value is not correct.\n",map->filename);
		}
		
		int field1 = get_field_index_from_tem(n1, m1);
		int field2 = get_field_index_from_tem(n2, m2);
		map->k11[field1][field2]=tmpv1;
		map->k22[field1][field2]=tmpv2;
		map->k12[field1][field2]=tmpv3;
		map->k21[field1][field2]=tmpv4;
	}
			
	// Close the file
	fclose(mapfile);

	map->function=0;
	map->has_map_source=false;
	map->has_coefficients=true;
	map->save_to_file=false;
}




//scb -- for phasemap
complex_t integrate_pm(double x, double y, int xindex, int yindex, complex_t qx, complex_t qy, complex_t qx2, complex_t qy2, int n1, int m1, int n2, int m2, double nr, int mirror_index ) {

  complex_t u, udash, uphase, result;
  double phase,k;

  k = TWOPI / init.lambda * nr;

  u = u_nm(n1,m1,qx,qy,x,y,nr);

  udash = cconj(u_nm(n2,m2,qx2,qy2,x,y,nr)) ;
 
  phase = getphase_pm(xindex,yindex,mirror_index);
  uphase = z_by_phr( u, 2.0*k*phase);

  result = z_by_z(uphase, udash);
 
	return cminus(result);
}

double getphase_pm(int x, int y, int mirror_index) {

  double phase;

  mirror_t *mirror;
  mirror = &(inter.mirror_list[mirror_index]);
	
  phase = zphase(mirror->map_refl.pmt[x][y]);
  return phase;
}




taken from kat_aa.c 110508


//! Set the respective coupling coefficients for the given mirror with PHASEMAP enabled
/*!
 * \param mirror_index index of mirror to set the coupling coefficient
 *
 * \return parameter describing the mismatch (if any) currently disabled scb
 */
int set_k_mirror_phasemap(int mirror_index)
{
  warn("Using set_k_mirror_phasemap\n");
  int n, m;

  int err;
  int component;
  int node1_index, node2_index;
  int mismatch;

  int n1, m1, n2, m2;
  double nr1 = 0.0, nr2 = 0.0;
  double phase = 0.0;
  //complex_t kx, ky;
  //double kmx, kmy;

  ABCD_t trans1;
  complex_t qx1, qy1, qx2, qy2;
  complex_t qxt, qyt;
  complex_t qxt2, qyt2;
  mirror_t *mirror;

  node_t node1, node2;

  // sanity check on input
  // mirror index should be in the range 0 <= mirror_index < inter.num_mirrors
  assert(mirror_index >= 0);
  assert(mirror_index < inter.num_mirrors);

  mirror = &(inter.mirror_list[mirror_index]);
  component = get_overall_component_index(MIRROR, mirror_index);

  node1_index = mirror->node1_index;
  node2_index = mirror->node2_index;

  node1 = inter.node_list[node1_index];
  node2 = inter.node_list[node2_index];

  if (NOT node1.gnd_node) {
    //node1 = inter.node_list[node1_index];
    nr1 = *node1.n;
  }

  if (NOT node2.gnd_node) {
    //node2 = inter.node_list[node2_index];
    nr2 = *node2.n;
  }

  mismatch = 0;
  if (NOT node1.gnd_node) {
    if (node1.component_index == component) {
      // reverse q so that beam comes _to_ the mirror from node1
      qx1 = cminus(cconj(node1.qx));
      qy1 = cminus(cconj(node1.qy));
    }
    else {
      qx1 = node1.qx;
      qy1 = node1.qy;
    }
  }

  if (NOT node2.gnd_node) {
    if (node2.component_index == component) {
      qx2 = node2.qx;
      qy2 = node2.qy;
    }
    else {
      // reverse q so that beam goes _from_ the mirror to node2,
      // this requires to use -beta for the recflection node2 -> node2 
      qx2 = cminus(cconj(node2.qx));
      qy2 = cminus(cconj(node2.qy));
    }
  }

  if (NOT node1.gnd_node) {
		warn("Relection1\n");
    // reflection1
    err = component_matrix(&trans1, component, node1_index, node1_index, 
													 TANGENTIAL);
		if (!err) {
			//warn("!err\n");
      qxt = q1_q2(trans1, qx1, nr1, nr1);
			
      err = component_matrix(&trans1, component, node1_index, node1_index, 
														 SAGITTAL);
      qyt = q1_q2(trans1, qy1, nr1, nr1);
      qxt2 = cminus(cconj(qx1));
      qyt2 = cminus(cconj(qy1));
			
      if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
        mismatch = mismatch | 1;
      }
           
      for (n=0; n<inter.num_fields; n++) {
        for (m=0; m<inter.num_fields; m++) {
          get_tem_modes_from_field_index(&n1, &m1, n);
          get_tem_modes_from_field_index(&n2, &m2, m);
					// kphasemap_mnmn(int n1, int m1, int n2, int m2, complex_t qx1, complex_t qy1, int nr1, int mirror_index)
          mirror->k11[n][m] = kphasemap_mnmn(n1, m1, n2, m2, qx1, qy1, qxt2, qyt2, nr1,mirror_index);
					//warn("k11[n][m]: %s\n",complex_form(mirror->k11[n][m]));
				}
      }
    }
  }

  if (NOT node2.gnd_node) {
  warn("Relection2\n");
    // reflection2
    // here we use -beta !
    err = component_matrix(&trans1, component, node2_index, node2_index, 
        TANGENTIAL);
    if (!err) {
      qxt = q1_q2(trans1, cminus(cconj(qx2)), nr2, nr2);
      err = component_matrix(&trans1, component, node2_index, node2_index, 
          SAGITTAL);
      qyt = q1_q2(trans1, cminus(cconj(qy2)), nr2, nr2);
      qxt2 = qx2;
      qyt2 = qy2;

      if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
        mismatch = mismatch | 2;
      }

      

      for (n=0; n<inter.num_fields; n++) {
        for (m=0; m<inter.num_fields; m++) {
          get_tem_modes_from_field_index(&n1, &m1, n);
          get_tem_modes_from_field_index(&n2, &m2, m);

          mirror->k22[n][m] = cconj(z_by_phr(kphasemap_mnmn(n1, m1, n2, m2, qx2, qy2, qxt2, qyt2, nr1,mirror_index), -phase));
	  //warn("k22[n][m]: %s\n",complex_form(mirror->k22[n][m]));
        }
      }
    }
  }

  return (mismatch);
}




taken from kat_calc, SIG_X etc...
				/*
        if (sig->type == SIG_X) {
          nr = *(sig_node1.n);
          q = sig_node1.qx;
          if (sig_node1.component_index == sig->component_index) {
            q = cminus(cconj(q));
          }

          w0 = 1.0 / w0_size(q, nr);
          tx1 = z_by_x(cconj(q), -1 * w0);
          txs1 = z_by_x(q, w0);
          gphase1 = gouy(q);

          nr = *(sig_node2.n);
          q = sig_node2.qx;
          if (sig_node2.component_index == sig->component_index) {
            q = cminus(cconj(q));
          }

          w0 = 1.0 / w0_size(q, nr);
          tx2 = z_by_x(cconj(q), -1*w0);
          txs2 = z_by_x(q, w0);
          gphase2 = gouy(q);
				*/





//! Some function or other ???
/*!
 * \param din ???
 * \param point ???
 * \param dout ???
 * \param value ???
 */
void function_(int *din, double point[3], int *dout, double value[3])
{
  double k;
  double x, y, x1, y1;
  double delta_z;
  complex_t z1, z2, z3;
  int i;

  // dummy to silence compiler warnings:
  i = *din;
  i = *dout;

  k = TWOPI / in.lambda * vlocal.lnr;

  x = point[0];
  y = point[1];


  x1 = x * vlocal.lcx;
  y1 = y * vlocal.lcy;
  delta_z = y * vlocal.lsy - x * vlocal.lsx;

  z2 = z_by_phr(u_nm(vlocal.ln1, vlocal.lm1,
                     z_pl_z(vlocal.lqx1, co(delta_z, 0)),
                     z_pl_z(vlocal.lqy1, co(delta_z, 0)), x1, y1, vlocal.lnr),
                -1.0 * k * delta_z);
  z3 = cconj(u_nm(vlocal.ln2, vlocal.lm2, vlocal.lqx2, vlocal.lqy2, x, y,
               vlocal.lnr));
  z1 = z_by_z(z2, z3);

  value[0] = z1.re;
  value[1] = z1.im;
}


//! Compute the Hermite-Gauss Function u2 (Siegmann S.686)
/*!
 * Need to add more info here as to what this function is.  See Siegman ???
 *
 * \param n mode number
 * \param q Gaussian beam parameter
 * \param x position on x-axis
 * \param nr refractive index
 */
complex_t u2(int n, complex_t q, double x, double nr)
{
  double z;
  double factor, pfactor, k;
  complex_t phase, z1, z2, z3, z4;
  const double a1 = 0.893243841738002308794364125788;   // (2/pi)^1/4

  // sanity checks on inputs
  // n should be greater than or equal to 0
  assert(n >= 0);
  // the refractive index should be greater than zero
  assert(nr > 0.0);

  z = z_q(q);
  factor = a1 / sqrt(npow(2, n) * fac(n) * w0_size(q, nr))
    * hermite(n, sqrt(2.0) * x / w_size(q, nr));
  k = TWOPI / in.lambda * nr;
  pfactor = k * x * x / (2.0 * zabs(q));

  //   phase.re=-1.0*k*(z+q.re*pfactor);
  // phase.im=-1.0*k*q.im*pfactor;

  z1 = div_complex(q_0(q), q);
  z2 = div_complex(z_by_z(q_0(q), cconj(q)), z_by_z(cconj(q_0(q)), q));
  z3 = zsqrt(z_by_z(z1, pow_complex(z2, n, 1)));

  /*
     z1=zsqrt(div_complex(q_0(q), q));
     z2=pow_complex(div_complex(z_by_z(q_0(q), cconj(q)), 
     (z_by_z(cconj(q_0(q)), q))), n, 2);
     z3=z_by_z(z1, z2);
   */

  //  phase=z_pl_z(co(k*z, 0), z_by_x(inv_complex(q), k*x*x/2.0));
  phase = z_by_x(inv_complex(q), -1.0 * k * x * x / 2.0);

  //  phase=z_by_x(ci(cconj(q)), -pfactor);
  factor *= exp(-1.0 * phase.im);

  z4 = z_by_xphr(z3, factor, phase.re);

  return (z4);
}
