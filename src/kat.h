// $Id$

/*!
 * \file kat.h
 * \brief Main header file for Finesse
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef KAT_H
#define KAT_H

#include "kat_constants.h"
// adf 30.11.14: experiment to see if this changes the optimisation results.
// Don't know if this compiles on windows
#include <x86intrin.h> 
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <assert.h>
#include "spMatrix.h"
#include "formulc.h"
#include "md5.h"
#include "klu.h"
#include "utarray.h"
#include "utlist.h"
#include "uthash.h"
#include <sys/time.h>

#if OS == MACOS
#include <stdint.h>
typedef struct timespec_kat
{
	__darwin_time_t	tv_sec;
	long		tv_nsec;
        uint64_t        mach;
} timespec_t;
#else
typedef struct timespec timespec_t;
#endif

#if INCLUDE_NICSLU
#include "nicsluc.h"
#endif

void my_finish(int ret);
/* ================================================================ */

//! complex number type

/*!
 * Please note that this is the Finesse implementation of the complex type,
 * NOT the C implementation.  Hence, we use the name complex_t when
 * specifying the type of a variable.
 *
 * \todo Change this to the C complex type?
 */
typedef struct complex {
    double re; //!< real part of a complex number
    double im; //!< imaginary part of a complex number
} complex_t;

/**
 * Determines how to scale the amplitude outputs.
 */
typedef enum amplitude_scaling {
    SQRT_2_EPS_C= 0, /** No scaling */
    SQRT_2=1, /** Scales amplitudes by sqrt(2) takes eps_0*c to be real value */
    ONE=2 /** Scales amplitudes by sqrt(2) takes eps_0*c = 1 */
} amplitude_scaling_t;

/** Type stating variable is a bit flag*/
typedef unsigned int bitflag;

/** Complex matrix*/
typedef complex_t** zmatrix;
typedef double** dmatrix;

typedef struct frequency{
    char name[LINE_LEN]; // name of the frequency
    double f;   
    int index; // index of the frequency struct in either the inter.carrier_frequencies or inter.signal_frequencies
    int result_index; /** This is the index for this frequency in the a_s,t_s and f_s arrays */
    int type; // CARRIER, MODULATOR, SIGNAL
    
    // variables used when frequency is a sideband
    double *f_mod; // the frequency that this sideband is modulated at
    int order; // order of the sideband, 0 if a carrier
    struct frequency *carrier; // not NULL if MODULATOR or SIGNAL
    
    // if signal sidebands are enabled and this frequency is a carrier field
    // these will point to the upper and lower signal sidebands
    struct frequency *sig_upper, *sig_lower;
    
    // if being tuned store what is tuning it
    int isTuned;
    void *tuning_component; 
    int tuning_type; 
    
    bool removed; // true if this has been removed from the frequency list
    struct frequency_node *node;
} frequency_t;

/**
 * Struct to be used with utlist.h as a single linked list element for a pointer
 */
typedef struct LL_ptr_el {
    void *ptr;
    struct LL_ptr_el *next;
} LL_ptr_el_t;

// node for doubly linked list of frequencies
typedef struct frequency_node{
    struct frequency_node *next, *prev;
    frequency_t *me;
} frequency_node_t;

typedef struct frequency_linked_list {
    frequency_node_t *head;
    frequency_node_t *end;
    int count;
} frequency_linked_list_t;

typedef struct q_io {
    complex_t qxi, qxo, qyi, qyo;
} q_io_t;

/** Type defines the different scalings of the quantum output detectors. */
typedef enum qoutput_scale {
    NOTSET = 0, /** Use default set in kat.ini */
    PSD = 1, /** Power spectral density */
    PSD_HF = 2, /** Power spectral density in units of hf */
    ASD = 3, /** Amplitude spectral density */
    ASD_HF = 4 /** Amplitude spectral density in units of sqrt(hf) */
} qoutput_scale_t;

typedef struct u_n_accel{
	complex_t *prefac;
	unsigned int n;
	double sqrt2_wz;
	double k;
	complex_t negK_2q;
} u_n_accel_t;

typedef struct u_nm_accel{
        u_n_accel_t *acc_n, *acc_m;
} u_nm_accel_t;

/**
 * Structure to cache the computation of:
 *      u_{n}(x, q1) * u^*_{n'}(x, q2)
 * For use with the ROMHOM and Riemann integrator to 
 * reduce recalculations of 2D beam amplitudes
 */
typedef struct unn_cache {
    size_t num_nodes;
    double *nodes;
    double **values;
    complex_t **zvalues;
    u_n_accel_t *acc1, *acc2;
} unn_cache_t;


/**
 * This is a generic structure to hold allocated memory for knm computations
 */
typedef struct knm_workspace {
    unn_cache_t ux_cache_11, ux_cache_12, ux_cache_21, ux_cache_22;
    unn_cache_t uy_cache_11, uy_cache_12, uy_cache_21, uy_cache_22;
    dmatrix d_u_xy;
    zmatrix z_u_xy;
    dmatrix W_xy;
} knm_workspace_t;

typedef struct roq_weights {
    bool enabled; /** True if this ROQ is being used */
    
    size_t num_xnodes;
    size_t num_ynodes;
    
    double *x_nodes;
    double *y_nodes;
    
    double zmin, zmax, w0min, w0max, R, nr1, nr2;
    int maxorder, mapSamples;
    
    size_t num_wx, num_wy;
    
    zmatrix w_Q1Q3, w_Q2Q4, w_Q1Q2, w_Q1Q4;
    zmatrix w_Q2Q3, w_Q3Q4, w_Q1Q2Q3Q4;
    
    knm_workspace_t knm_ws;
} roq_weights_t;

typedef struct rom_map {
    char filename[FILENAME_MAX];
    
    roq_weights_t roq11;
    roq_weights_t roq22;
    roq_weights_t roq12;
    roq_weights_t roq21;
} rom_map_t;

typedef enum map_integration_method {
    RIEMANN_SUM_NEW = 1,
    CUBA_CUHRE_SERIAL = 2,
    CUBA_CUHRE_PARA = 3,
    NEWTON_COTES = 4
} int_method_t;

/**
 * This structure contains information regarding how the coupling 
 * at a component's port is structured. This allows an optimised
 * matrix allocation to reduce redundant zero entries.
 */
typedef struct coupling_info {
    bool coupling_off; /** If true no coupling happens thus we have a pure identity matrix. */
    bool has_xeven; /** If true even mode x order differences are included */
    bool has_yeven; /** If true even mode y order differences are included */
    bool has_xodd;  /** If true odd mode x order differences are included */
    bool has_yodd;  /** If true odd mode y order differences are included */
    int  max_coupling_order; /** Sets the maximum order difference */
} coupling_info_t;

typedef struct bs_knm {
    //! coupling coefficient for TEM mode from mode 1 into mode 1
    complex_t **k12;
    //! coupling coefficient for TEM mode from mode 1 into mode 2
    complex_t **k21;
    //! coupling coefficient for TEM mode from mode 2 into mode 1
    complex_t **k34;
    //! coupling coefficient for TEM mode from mode 2 into mode 2
    complex_t **k43;
    //! coupling coefficient for TEM mode from mode 1 into mode 1
    complex_t **k13;
    //! coupling coefficient for TEM mode from mode 1 into mode 2
    complex_t **k31;
    //! coupling coefficient for TEM mode from mode 2 into mode 1
    complex_t **k24;
    //! coupling coefficient for TEM mode from mode 2 into mode 2
    complex_t **k42;
    
    bool IsIdentities;
} bs_knm_t;

typedef struct bs_knm_q {
    // Here qNtM_PQ means: 
    //  N = x or y depending on the coordinate
    //  M = 1 or 2 - 1 is incoming, 2 is outgoing
    //  P = The incoming node
    //  Q = The outgoing node
    //
    // 12 and 34 are reflections
    // 13 and 24 are transmissions
    //
    complex_t qxt1_12;
    complex_t qxt2_12;
    complex_t qyt1_12;
    complex_t qyt2_12;

    complex_t qxt1_21;
    complex_t qxt2_21;
    complex_t qyt1_21;
    complex_t qyt2_21;

    complex_t qxt1_34;
    complex_t qxt2_34;
    complex_t qyt1_34;
    complex_t qyt2_34;

    complex_t qxt1_43;
    complex_t qxt2_43;
    complex_t qyt1_43;
    complex_t qyt2_43;
    
    complex_t qxt1_13;
    complex_t qxt2_13;
    complex_t qyt1_13;
    complex_t qyt2_13;

    complex_t qxt1_31;
    complex_t qxt2_31;
    complex_t qyt1_31;
    complex_t qyt2_31;

    complex_t qxt1_24;
    complex_t qxt2_24;
    complex_t qyt1_24;
    complex_t qyt2_24;

    complex_t qxt1_42;
    complex_t qxt2_42;
    complex_t qyt1_42;
    complex_t qyt2_42;
} bs_knm_q_t;

typedef struct mirror_knm {
    //! coupling coefficient for TEM mode from mode 1 into mode 1
    zmatrix k11;
    //! coupling coefficient for TEM mode from mode 1 into mode 2
    zmatrix k12;
    //! coupling coefficient for TEM mode from mode 2 into mode 1
    zmatrix k21;
    //! coupling coefficient for TEM mode from mode 2 into mode 2
    zmatrix k22;
    
    bool IsIdentities;
} mirror_knm_t;

// Stores the q values for calculating the various KNM coefficients for a mirror
// Each q should be calculated using the incoming beam q and transformed using
// ABCD matrix

typedef struct mirror_knm_q {
    // Here qNtM_PQ means: 
    //  N = x or y depending on the coordinate
    //  M = 1 or 2 - 1 is incoming, 2 is outgoing
    //  P = The incoming node
    //  Q = The outgoing node
    //
    // 11 and 22 are reflections
    // 12 and 21 are transmissions
    //
    complex_t qxt1_11;
    complex_t qxt2_11;
    complex_t qyt1_11;
    complex_t qyt2_11;

    complex_t qxt1_12;
    complex_t qxt2_12;
    complex_t qyt1_12;
    complex_t qyt2_12;

    complex_t qxt1_21;
    complex_t qxt2_21;
    complex_t qyt1_21;
    complex_t qyt2_21;

    complex_t qxt1_22;
    complex_t qxt2_22;
    complex_t qyt1_22;
    complex_t qyt2_22;
} mirror_knm_q_t;

/**
 * Enum that states the different coupling coefficients and their 
 * directions between the nodes, NOTE: the values should be not
 * cross over with those in KNM_BS_NODE_DIRECTION
 */
typedef enum KNM_MIRROR_NODE_DIRECTION {
    MR11 = 1,
    MR12 = 2,
    MR21 = 3,
    MR22 = 4
} KNM_MIRROR_NODE_DIRECTION_t;

typedef enum KNM_MR_FLAG {
    MR11Calc = 1,
    MR12Calc = 2,
    MR21Calc = 4,
    MR22Calc = 8
} KNM_MR_FLAG;

typedef enum qnoise_input_type {
    COMPONENT_LOSS=1,
    OPEN_PORT=2,
    LASER_INPUT_NOISE=3,
    SQUEEZED_INPUT=4,
    MODULATOR_NOISE=5
} qnoise_input_type_t;

//! information about a node within the interferometer
typedef struct node {
    char name[MAX_TOKEN_LEN]; //!< node name
    int connect; //!< is the node connected?
    int io; //!< input(=2) or output(=1) node
    bool q_changed; //!< True if the q value has been changed to what it previously was
    complex_t qx; //!< x component of beam q parameter
    complex_t qy; //!< y component of beam q parameter
    complex_t _prev_qx; //!< x component of beam q parameter before retrace
    complex_t _prev_qy; //!< y component of beam q parameter before retrace
    int component_index; //!< component index
    double *n; //!< refractive index of an attached space
    bool q_is_set; //!< is Gaussian beam parameter set?
    bool gnd_node; //!< is this node a GND_NODE?
    
    int list_index;     // node index in the inter.node_list array
    int direction; // Determines if node is IN_OUT (port 1 is incoming and 2 outgoing)
                   // or OUT_IN (port 1 outgoing and port 2 incoming).
} node_t;

typedef struct qnoise_input {
    node_t *node; // node at which to inject the noise
    int port; // which node port to inject noise at
    qnoise_input_type_t type; // type of loss the needs to be modelled
    int component_type; // Type of component that this noise is associated with
    int component_index; // index of component in its component array - not overall component index
    bool isSqueezed; // true if input is squeezed
    
    /** This is a matrix of [inter.num_fields*inter.num_signals][2], where the first
     *  element is how that signal's mode couples into itself and the second is how
     *  the other sideband's mode couples into it.
     */
    zmatrix *M_2x2_noise_inputs;
    complex_t **M_diag_noise_inputs;
    
    zmatrix *M_EC_f0u_fcl_noise_inputs; /* Entangled carrier noise inputs between zero carrier upper and changeable carrier lower*/
    zmatrix *M_EC_f0l_fcu_noise_inputs; /* Entangled carrier noise inputs between zero carrier lower and changeable carrier upper */
} qnoise_input_t;


/* information about an ABCD matrix */
typedef struct ABCD {
    double A; //!< A element of ABCD matrix
    double B; //!< B element of ABCD matrix
    double C; //!< C element of ABCD matrix
    double D; //!< D element of ABCD matrix
} ABCD_t;

typedef enum aperture_type {
    CIRCULAR, SQUARE
} aperture_type_t;

/**
 * Structure to define a degree of freedom in a setup
 */
typedef struct dof {
    int list_index; /** Index of this in inter.dof_list */
    char name[MAX_TOKEN_LEN]; /** Name of the degree of freedom */
    int num_components; /** The number of components this DOF contains */
    int parameter; /** The component parameter to put to */
    int component_idx[MAX_DOF_ITEMS]; /** Index of components that make up this DOF */
    int component_type[MAX_DOF_ITEMS]; /** Type of component pointer at index position i */
    double factors[MAX_DOF_ITEMS]; /** Factor that describes DOF contribution */
} dof_t;

/** memory allocation information relevant for elements in the interferometer */
typedef struct memory {
    int num_modulators; //!< number of modulators
    int num_mirrors; //!< number of mirrors
    int num_beamsplitters; //!< number of beam splitters
    int num_spaces; //!< number of free spaces
    int num_lenses; //!< number of lenses
    int num_gratings; //!< number of gratings
    int num_squeezers; //!< number of squeezers
    int num_light_inputs; //!< number of input light fields (i.e. lasers)
    int num_diodes; //!< number of Faraday isolators (diodes)
    int num_dbss; //!< number of 4 port direction BSs
    int num_signal_inputs; //!< number of signal inputs
    int num_variables; //!< number of dummy parameters (used with xaxis)
    int num_sagnacs; //!< number of sagnac elements
    int num_blocks; //!< number of block elements
    int num_user_frequencies; 
    int num_transfer_funcs; 
    int num_feedbacks;
    int num_dof;
    
    //! number of Gaussian commands 
    /*!
     * comes from the user-defined Gaussian beam parameter
     */
    int num_gauss_cmds;

    int num_light_outputs; //!< number of light outputs
    int num_motion_outputs;
    //! number of beam parameter outputs
    /*!
     * i.e. the beam parameter detector
     */
    int num_beam_param_outputs;
    int num_quad_outputs;
    int num_cavity_param_outputs; //!< number of cavity parameter outputs
    int num_convolution_outputs; //!< number of convolution outputs
    int num_mirror_phase_outputs; //!< number of mirror_phase outputs
    int num_outputs; //!< number of outputs
    int num_minimize;
    int num_homodynes;
    int num_mhomodynes;
    int num_mirror_dithers;
    int num_bs_dithers;
    
    //! number of attributes 
    /*!
     * The number of attributes of mirrors, beamsplitters etc.
     */
    int num_attributes;

    int num_cavities; //!< number of cavities
    int num_gnuterm_cmds; //!< number of gnuterm commands
    int num_pyterm_cmds; //!< number of pyterm commands
    int num_diff_cmds; //!< number of differentiation commands defined
    int num_constants; //!< number of constants (defined in input file)
    int num_scale_cmds; //!< number of scale commands defined
    int num_nodes; //!< number of nodes
    int num_dump_nodes; //!< number of dump nodes (GND_NODEs)
    int num_reduced_nodes; //!< number of nodes minus number of dump nodes
    int num_components; //!< number of components
    int num_frequencies; //!< number of frequencies

    //! order of higher order modes
    /*!
     * The order of higher order (Hermite-Gauss) modes is defined as max(n+m)
     * for TEM_nm modes
     */
    int hg_mode_order;
    int num_quantum_components;
    int num_fields; //!< number of field amplitudes
    int num_maps;
    int num_surface_motion_maps; 
    int num_quadratures; //!< number of quadratures
    int num_pointers; //!< number of pointers to the sparse matrix
    int num_node_names; //!< number of node names
    int num_openloopgains;
    int num_force_out;
    
    double *restab; //!< pointer to the output data array

    //! list of all TEM modes
    /*!
     * Usually TEM modes are accessed using n and m as the index but in this
     * case they are indexed by the one number
     */
    int *all_tem_HG;
    int *all_tem_LG;
    
    char **names; //!< names of everything mentioned in .kat input file
    double *work; //!< workspace for dcuhre function
    int worklen; //!< the length of the workspace
    int num_set_cmds; //!< number of set commands in input file
    int num_put_cmds; //!< number of put commands in input file
    int num_tmp_put_cmds; //!< number of put commands in input file

    //! number of func commands in input file
    /*!
     * func commands are used for computing formulae in Finesse
     */
    int num_func_cmds;

    int num_locks; //!< number of locks

    //! help check for multiple pointers to same location when building matrices
    int *ilist1;
    //! help check for multiple pointers to same location when building matrices
    int *jlist1;
    //! help check for multiple pointers to same location when building matrices
    int *ilist2;
    //! help check for multiple pointers to same location when building matrices
    int *jlist2;
} memory_t;

typedef enum interpolation_method {
    NEAREST = 1,
    LINEAR = 2,
    SPLINE = 3
} interpolation_method_t;

typedef struct surface_merged_map {
    unsigned int interpolation_size; //!< Size of interpolation kernel, must be odd number greater than 3
    interpolation_method_t interpolation_method; //!< interpolation method to use: spline=1 or linear=2
    int_method_t integration_method; //!< Integration method to use for knm calculations
    unsigned int integration_NC_order; /*** Order of Newton-Coted method to use if required*/
    
    double x0; //!< x-coordinate of optical center of the map (in steps)
    double y0; //!< y-coordinate of optical center of the map (in steps)
    double xstep; //!< the size in x direction of one tile of the grid
    double ystep; //!< the size in y direction of one tile of the grid
    int rows; //!< vertical size of array
    int cols; //!< horizontal size of array

    double **t_phs; //!< transmission phase, to be computed from data of multiple maps
    double **t_abs; //!< transmission amplitude, to be computed from data of multiple maps
    double **r_phs; //!< reflection phase, to be computed from data of multiple maps
    double **r_abs; //!< reflection amplitude, to be computed from data of multiple maps
    double *x;
    double *y; //!< x and y variables for map interpolation, used by GSL interpolation

    char filename[LINE_LEN]; //!< this is the base filename for stored information, it will have .knm and .map appended to it depending on the data
    char name[LINE_LEN]; //!< name given in file
    bool noMapPresent; //!< Boolean flag that states if any maps have been applied
    bool phaseIsOnlyTransmission; //!< True if only a tranismission map was applied
    bool hasReflectivityMap; //!< True if one of the maps merged into this is a reflectivity map
    bool knm_calculated; //!< True if the knm have been calculated for the map already

    //config settings
    bool show_knm_neval;
    bool save_to_file;
    bool save_knm_matrices;
    bool knm_save_binary;
    bool save_interp_file;
    bool save_integration_points;
    
    // q values used for integration
    mirror_knm_q_t mirror_knm_q;
    bs_knm_q_t bs_knm_q;
        
    double _AoI; // Angle of incidence
    double _angle; // rotation about beam axis
    double _nr1, _nr2, _lambda;
    double _r_aperture, _xbeta, _ybeta;
    int _maxtem;
    double _relerr,_abserr;
    unsigned int _intmeth, _intermeth, _intersize;
    double _x_off, _y_off;
    
    // flags to keep track of messages that the user has been told on per mirror basis
    // this is to stop the same message appearing every integration for example
    bitflag usermessages;
    
    /** True if a reflection map is merged into this */
    bool hasReflectionMaps;
    
    /** True if a transmission map is merged into this */
    bool hasTransmissionMaps;
    
    double map_amplitude;
    
    knm_workspace_t knm_ws;
    
} surface_merged_map_t;

//! surface_map: holds the data of a mirror surface map

typedef struct surface_map {
    int type; //!< type of the surface map (phase, absorption, reflectivty)
    bool reflection; //!< affect reflected light?
    bool transmission; //!< affect transmitted light?
    int function; //!< 0:comes from a file, 1: comes from a fucntion
    char filename[LINE_LEN]; //!< file for reading in a numeric phase/amplitde map;
    unsigned char hash[MD5_HASH_SIZE]; //!< same size as the md5 digest
    char name[LINE_LEN]; //!< name given in file
    double scaling; //!< global scaling coefficient of data values
    double **data; //!< map values to be read from file
    double x0; //!< x-coordinate of optical center of the map (in steps)
    double y0; //!< y-coordinate of optical center of the map (in steps)
    double xstep; //!< the size in x direction of one tile of the grid
    double ystep; //!< the size in y direction of one tile of the grid
    int rows; //!< vertical size of array
    int cols; //!< horizontal size of array
    int has_coefficients; //!< coupling coefficients have been computed (0:no, 1:partly, 2: yes)
    bool has_map_source; //!< map source data is available
    double Loss; //!< Loss derived from mirror R,T
} surface_map_t;

//! information used for 'variable' a method for using dummy parameters

typedef struct variable {
    char name[MAX_TOKEN_LEN]; //!< parameter name
    double param; //!< parameter value
} variable_t;


enum MIRROR_USERMESSAGES{
    USING_POLAR=1,
    MAP_TOO_SMALL=2,
    INTERP_SAVE=4,
    KNM_TRANSPOSE=8,
    INT_METHODS=16,
    MAP_RES_TOO_LOW=32,
    APERTURE_NO_ANALYTIC=64,
    APERTURE_RIEMANN=128
};

typedef struct Q_freq_pair {
    double Q;
    double f;
} Q_freq_pair_t;

typedef struct transfer_func {
    char name[MAX_TOKEN_LEN];
    int index;
    int num_poles;
    int num_zeros;
    
    /** we can have a general transfer where we state the poles and zeros explicitly
     * using complex numbers or as a series of Q and pole frequencies.
     * Has values GENERAL_TF or Q_F_TF
     */
    int type; 
    
    double gain;
    double phase;
    
    Q_freq_pair_t pole_Q_f[MAX_POLES_ZEROS];
    Q_freq_pair_t zero_Q_f[MAX_POLES_ZEROS];
    
    complex_t poles[MAX_POLES_ZEROS];
    complex_t zeros[MAX_POLES_ZEROS];
    
} transfer_func_t;

typedef enum motion_type{
    Z=1,
    ROTX=2,
    ROTY=3,
    SURFACE=4
} motion_type_t;


typedef struct feedback {
    char name[MAX_TOKEN_LEN];
    char motion_name[MAX_TOKEN_LEN]; /** Motion entered by user */
    int list_index; /** index of this feedback item in the list */
    int output_list_idx; /** List index of output which will be used for feedback */
    
    int comp_list_idx; /** List index of component being fed back to */
    int comp_type; /** Type of component being fed back to */
    int comp_motion_idx; /** Motion index of the component that is to be fed to */
    
    complex_t ***a_d; /** Pointer to matrix elements for computing detector signal. a[signal frequency][outgoing mode] */
    complex_t *d_x; /** pointer to matrix element for computing how detector is fed back into motion of a component */
    
    int nnz_in_count; /** Number of non-zeros elements taken up in the matrix for input */
    int nnz_out_count; /** Number of non-zeros elements taken up in the matrix for output */
    
    transfer_func_t *tf;
} slink_t;

//! information pertaining to a mirror
typedef struct mirror {
    int comp_index; // component index
    
    /*! 
     * Note that when loss is included that the relationship between the
     * \b power reflectance, the transmittance and the loss is:
     * \f[
     * R + T + Loss = 1
     * \f]
     * which, written in terms of \b amplitude reflectance, transmittance 
     * and loss parameters is:
     * \f[
     * r^2 + t^2 + loss^2 = 1
     * \f]
     */
    double R; //!< mirror power reflectance
    double T; //!< mirror power transmittance

    //! Tuning of the mirror
    /*!
     * It is convenient to split distances between optical elements into two
     * parameters, one macroscopic `length' which is an integer multiple of
     * the wavelength of light used and a microscopic tuning which is the
     * remaining distance between the total distance and the macroscopic
     * length.  The tuning is given as a phase in radians with \f$ 2\pi \f$ 
     * referring to one wavelength.  In Finesse tunings are entered and printed 
     * in degrees.
     */
    double phi;

    double r_aperture; //!< physical size of mirror, infinite if set to 0
    aperture_type_t aperture_type;
    
    double map_amplitude;

    double x_off;
    double y_off;
    double Rcx; //!< radius of curvature in x plane (tangential)
    double Rcy; //!< radius of curvature in y plane (sagittal)

    bitflag mismatching;
    
    int node1_index; //!< index of the connected node
    int node2_index; //!< index of the connected node

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    //! matrix element of the component in the interferometer matrix
    complex_t ***a11;
    //! matrix element of the component in the interferometer matrix
    complex_t ***a12;
    //! matrix element of the component in the interferometer matrix
    complex_t ***a21;
    //! matrix element of the component in the interferometer matrix
    complex_t ***a22;

    // matrix element of the component in the interferometer matrix stores all 
    // frequencies and fields
    complex_t *****a11f, *****a12f;
    complex_t *****a21f, *****a22f;
    
    coupling_info_t a_cplng_11, a_cplng_12, a_cplng_21, a_cplng_22;
    
    int x_rhs_idx;  // RHS vector index for the mirror translational motion
    
    // these exist only for sideband
    complex_t ****x_a1; /** mirror motion effect on reflected a1 */
    complex_t ****x_a2; /** mirror motion effect on reflected a2 */ 
    complex_t ***x_x;  /** mirror motion effect on other motions */ 
    
    complex_t ****a1i_x; /** incoming beam incoming at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a2i_x; /** incoming beam incoming at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a1o_x; /** incoming beam outgoing at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a2o_x; /** incoming beam outgoing at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    
    complex_t *ic_a1i_x; /** Internal coupling of motion DOF */
    complex_t *ic_a1o_x; /** Internal coupling of motion DOF */
    complex_t *ic_a2i_x; /** Internal coupling of motion DOF */
    complex_t *ic_a2o_x; /** Internal coupling of motion DOF */
    
    /** Stores the coupling coefficients without applying reverse gouy or phase command options*/
    mirror_knm_t knm_no_rgouy;
    /** Stores the various coupling coefficients for reflection transmission */
    mirror_knm_t knm;
    
    mirror_knm_t knm_romhom;
    mirror_knm_t knm_map;
    mirror_knm_t knm_bayer_helms;
    mirror_knm_t knm_aperture;
    
    double *k12_sqrd_sum; // effective per mode loss due to scattering from node 1 to 2
    double *k21_sqrd_sum; // effective per mode loss due to scattering from node 2 to 1
    double *k22_sqrd_sum; // effective per mode loss due to scattering from node 2 to 2
    double *k11_sqrd_sum; // effective per mode loss due to scattering from node 1 to 1
    
    //! Transformation matrix for q of mirror; reflection in tangential plane (node 1 -> node1)
    ABCD_t qqr1t;
    //! Transformation matrix for q of mirror; reflection in sagittal plane (node 1 -> node1)
    ABCD_t qqr1s;
    //! Transformation matrix for q of mirror; reflection in tangential plane (node 2 -> node2)
    ABCD_t qqr2t;
    //! Transformation matrix for q of mirror; reflection in sagittal plane (node 2 -> node2)
    ABCD_t qqr2s;
    //! Transformation matrix for q of mirror; transmission in tangential plane
    ABCD_t qqtt;
    //! Transformation matrix for q of mirror; transmission in sagittal plane
    ABCD_t qqts;

    bitflag attribs; //!< bit-coded flag for which attribs have been set
    
    double mass; //!< mass of mirror
    double Ix;  /** Moment of inertia about x-axis */
    double Iy;  /** Moment of inertia about y-axis */
    double Ixy; /** Moment of inertia coupling between x-y axes */
    
    transfer_func_t *long_tf; // index of mechanical transfer function applied to this mirror
    transfer_func_t *rot_x_tf; // index of mechanical transfer function applied to this mirror
    transfer_func_t *rot_y_tf; // index of mechanical transfer function applied to this mirror
    
    int num_surface_motions; /** Number of surface motions */
    int num_motions; /** Count of how many motions are available - Including surface motions */
    motion_type_t *motion_type;
    
    double beta_x; //!< misalignment angle in x-plane
    double beta_y; //!< misalignment angle in y-plane

    int rebuild; //!< rebuild ABCD matrix and k_mnmn coefficients?
    
    //int map_type_set; //!< Which map types have been set
    surface_map_t * map[MAX_MAPS]; //!< Surface map storage 
    int num_maps; //!< number of surface maps   
    
    surface_merged_map_t map_merged;
    
    /** ROM maps are treated separately from usual maps as they cannot be
     merged with normal ones. */
    rom_map_t *map_rom;
    mirror_knm_q_t prev_rom_q; /** previous q values used for knm q */   
    
    mirror_knm_q_t qm; /* mismatch beam parameters */
    
    double angle; //!< angle of the mirror about the z axis
    /** q values used internally for the integration routines*/
    mirror_knm_q_t knm_q; 
    // flags stating whether a particular Knm should be calculated
    bitflag knm_calc_flags; // flag which states what knm components should be calculated
    bitflag knm_flags; //!< flags used to determine knm's should be calculated analyticaly or not
    
    /** Cores to use for knm calculations with cuba */
    int knm_cuba_cores;
    
    /** relative error for knm calculations with cuba */
    double knm_cuba_rel_err;
    
    /** absolute error for knm calculations with cuba */
    double knm_cuba_abs_err;
    
    int knm_order[NUM_KNM_TYPES]; //!< states which order the distortions to the mirror should apply
    int knm_change_q; //!< at which distortion effect the beam parameter changes
    
    bool knm_force_saved; //!< if true then the saved knms are forcefully used regardless of parameter differences
    
    bool K11_calc;
    bool K12_21_calc;
    bool K22_calc;
    //bool modematch_with_map; //!< If true bayer-helms for mode matching is switched off
    
    char name[MAX_TOKEN_LEN]; //!< mirror name
    
    // Full CCS matrix variables
    int ports;
    int nnz_count;
    
    UT_array *motion_links;
    
    /** Indices to surface motions applied to this mirror, see inter.surface_motion_map_list*/
    bool surface_motions_isROM[MAX_MAPS];
    int surface_motions[MAX_MAPS];
    transfer_func_t* surface_motions_tf[MAX_MAPS];
    
    /** Overlap between surface motion and outgoing modes from node 1 side. Reverse gouy is applied. Array size is of [inter.num_surface_motions][num_fields][num_fields].*/
    complex_t ***knm_surf_motion_1o;
    /** Overlap between surface motion and input modes from node 1 side. Reverse gouy is applied. Array size is of [inter.num_surface_motions][num_fields][num_fields].*/
    complex_t ***knm_surf_motion_1i;
    /** Coupling for reflected incoming beam due to surface motion and static maps present into signal sidebands from node 1 side. Array size [inter.num_surface_motions][num_fields][num_fields].*/
    complex_t ***knm_surf_x_a_1;
    /** Overlap between surface motion and output modes from node 2 side. Reverse gouy is applied. Array size [inter.num_surface_motions][num_fields][num_fields].*/
    complex_t ***knm_surf_motion_2o;
    /** Overlap between surface motion and input modes from node 2 side. Reverse gouy is applied. Array size [inter.num_surface_motions][num_fields][num_fields].*/
    complex_t ***knm_surf_motion_2i;
    /** Coupling for reflected incoming beam due to surface motion and static maps present into signal sidebands from node 2 side. Array size [inter.num_surface_motions][num_fields][num_fields].*/
    complex_t ***knm_surf_x_a_2;
    
    complex_t last_surf_knm_qx1;
    complex_t last_surf_knm_qx2;
    complex_t last_surf_knm_qy1;
    complex_t last_surf_knm_qy2;
    
    double dither_f, dither_phase, dither_m;
    int dither_order;
    int dither_list_index;
    
} mirror_t;

/**
 * The motion_link object describes how a motion *from* one component is linked
 * to another by some transfer function.
 */
typedef struct motion_link {
    int from_type;
    int from_list_idx;
    
    int to_type;
    int to_list_idx;
    
    int from_motion;
    int to_motion;
    
    transfer_func_t *f_f;      /** Force-to-Force transfer function */
    transfer_func_t *f_x_to;   /** Force-to-position transfer function for to motion */
    transfer_func_t *f_x_from; /** Force-to-position transfer function for from motion */
    
    int nnz_count;
    
    complex_t ***a1i_x; /** incoming beam incoming at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ***a2i_x; /** incoming beam incoming at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ***a3i_x; /** incoming beam incoming at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ***a4i_x; /** incoming beam incoming at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ***a1o_x; /** incoming beam outgoing at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ***a2o_x; /** incoming beam outgoing at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ***a3o_x; /** incoming beam outgoing at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ***a4o_x; /** incoming beam outgoing at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    
} motion_link_t;

extern UT_icd motion_link_icd;

//! information for a free space element

typedef struct block{
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< name of free space element

    double f_block_hz;

    complex_t ****a12f, ****a21f;

    int node1_index; //!< index of node1
    int node2_index; //!< index of node2

    int nnz_count;
} block_t;

typedef struct space {
    int list_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< name of free space element
    double L; //!< length of free space element

    bitflag mismatching;
    
    //! matrix element of the component in the interferometer matrix
    complex_t ***a12;
    complex_t ***a21;

    complex_t ****a12f;
    complex_t ****a21f;
    
    coupling_info_t a_cplng_12, a_cplng_21;
    
    complex_t **k12; //! coupling coefficient for TEM mode from mode 1 into mode 2
    complex_t **k21; //! coupling coefficient for TEM mode from mode 2 into mode 1
    double *k12_sqrd_sum; // effective per mode loss due to scattering from node 1 to 2
    double *k21_sqrd_sum; // effective per mode loss due to scattering from node 2 to 1
            
    int node1_index; //!< index of node1
    int node2_index; //!< index of node2

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    double n; //!< index of refraction
    int rebuild; //!< rebuild the element?
    bitflag attribs; //!< bit-coded flag for which attribs have been set

    ABCD_t qq; //!< Transformation matrix for q of free space

    complex_t ***q12; //!< quantum noise matrix element mode 1 -> mode 2
    complex_t ***q21; //!< quantum noise matrix element mode 2 -> mode 1

    q_io_t qm12;
    q_io_t qm21;
    
    // q[mode index][output][frequency][quadrature]

    double gouy_x; //!< Gouy phase in x direction
    double gouy_y; //!< Gouy phase in y direction
    bitflag knm_flags;
    
    int ports, nnz_count, nnz_count_conn_12, nnz_count_conn_21;
} space_t;

typedef struct sagnac {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< name of free space element

    double dphi; //!< Phase change applied to each beamt

    //! matrix element of the component in the interferometer matrix
    complex_t ***a12, ***a21;
    complex_t ****a12f, ****a21f;

    int node1_index; //!< index of node1
    int node2_index; //!< index of node2

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    int nnz_count;
} sagnac_t;

typedef struct fRT {
    char name[MAX_TOKEN_LEN];
    int id;
    double R, T, L;
    UT_hash_handle hh;
} fRT_t;

//! information describing a beam splitter

typedef struct beamsplitter {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< beam splitter name
    /*! 
     * Note that when loss is included that the relationship between the
     * \b power reflectance, the transmittance and the loss is:
     * \f[
     * R + T + Loss = 1
     * \f]
     * which, written in terms of \b amplitude reflectance, transmittance 
     * and loss parameters is:
     * \f[
     * r^2 + t^2 + loss^2 = 1
     * \f]
     */
    double R; //!< beam splitter power reflectance
    double T; //!< beam splitter power transmittance

    double backscatter;
    
    double phi; //!< beam splitter tuning \see mirror_t::phi
    double x_off;
    double y_off;
    
    double alpha_1; //!< angle of incidence set by user [deg]
    double alpha_2; //!< angle of incidence on side 2 calculated during ABCD calculations [deg]
    
    // ddb - These values shouldn't be stored. Should use the values from the spaces
    //double ir1; //!< refractive index side 1 computed during ABCD calculations
    //double ir2; //!< refractive index side 2 computed during ABCD calculations

    transfer_func_t *mech_tf; // index of mechanical transfer function applied to this mirror
    
    bitflag mismatching;
    
    double beta_x; //!< misalignment angle in x-plane
    double beta_y; //!< misalignment angle in y-plane

    int node1_index; //!< index of the connected node
    int node2_index; //!< index of the connected node
    int node3_index; //!< index of the connected node
    int node4_index; //!< index of the connected node

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix
    int node3_reduced_index; //!< node index for classical interferometer matrix
    int node4_reduced_index; //!< node index for classical interferometer matrix

    //! matrix element of the component in the interferometer matrix
    complex_t ***a12, ***a21, ***a13, ***a31;
    complex_t ***a24, ***a42, ***a34, ***a43;
    complex_t ***a11, ***a22, ***a33, ***a44;

    complex_t *****a12f, *****a21f, *****a13f, *****a31f;
    complex_t *****a24f, *****a42f, *****a34f, *****a43f;
    complex_t *****a11f, *****a22f, *****a33f, *****a44f;
    
    coupling_info_t a_cplng_12, a_cplng_21;
    coupling_info_t a_cplng_13, a_cplng_31;
    coupling_info_t a_cplng_24, a_cplng_42;
    coupling_info_t a_cplng_34, a_cplng_43;
    coupling_info_t a_cplng_11, a_cplng_22;
    coupling_info_t a_cplng_33, a_cplng_44;
    
    int x_rhs_idx; // RHS vector index for the mirror translational motion
    
    complex_t ****x_a1;  /** mirror motion effect on reflected a1 */
    complex_t ****x_a2;  /** mirror motion effect on reflected a2 */ 
    complex_t ****x_a3;  /** mirror motion effect on reflected a3 */
    complex_t ****x_a4;  /** mirror motion effect on reflected a4 */ 
    complex_t  ***x_x;   /** mirror motion effect on other motions*/
    
    complex_t ****a1i_x; /** incoming beam incoming at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a2i_x; /** incoming beam incoming at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a3i_x; /** incoming beam incoming at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a4i_x; /** incoming beam incoming at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a1o_x; /** incoming beam outgoing at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a2o_x; /** incoming beam outgoing at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a3o_x; /** incoming beam outgoing at a1 contribution to mirror motion, matrix of [sig freq][mode] */
    complex_t ****a4o_x; /** incoming beam outgoing at a2 contribution to mirror motion, matrix of [sig freq][mode] */
    
    complex_t *ic_a1i_x; 
    complex_t *ic_a2i_x; 
    complex_t *ic_a3i_x; 
    complex_t *ic_a4i_x; 
    complex_t *ic_a1o_x; 
    complex_t *ic_a2o_x; 
    complex_t *ic_a3o_x; 
    complex_t *ic_a4o_x; 
    
    /** Stores the coupling coefficients without applying reverse gouy or phase command options*/
    bs_knm_t knm_no_rgouy;
    //! Stores the total various coupling coefficients for reflection transmission
    bs_knm_t knm;

    // tangential ABCD matrices
    //! transformation matrix for q of element; reflection in tangential plane 1
    ABCD_t qqr1t;
    //! transformation matrix for q of element; transmission in tangential plane 1
    ABCD_t qqt1t;
    //! transformation matrix for q of element; reflection in tangential plane 2
    ABCD_t qqr2t;
    //! transformation matrix for q of element; transmission in tangential plane 2
    ABCD_t qqt2t;

    // sagittal ABCD matrices
    //! transformation matrix for q of element; reflection in sagittal plane 1
    ABCD_t qqr1s;
    //! transformation matrix for q of element; transmission in sagittal plane 1
    ABCD_t qqt1s;
    //! transformation matrix for q of element; reflection in sagittal plane 2
    ABCD_t qqr2s;
    //! transformation matrix for q of element; transmission in sagittal plane 2
    ABCD_t qqt2s;

    q_io_t qm12, qm21;
    q_io_t qm13, qm31;
    q_io_t qm24, qm42;
    q_io_t qm34, qm43;
    q_io_t qm11, qm22, qm33, qm44;
    
    bitflag attribs; //!< bit-coded flag for which attribs have been set
    double mass; //!< mass of mirror
    double Ix;  /** Moment of inertia about x-axis */
    double Iy;  /** Moment of inertia about y-axis */
    double Ixy; /** Moment of inertia coupling between x-y axes */
    
    transfer_func_t *long_tf; // index of mechanical transfer function applied to this mirror
    transfer_func_t *rot_x_tf; // index of mechanical transfer function applied to this mirror
    transfer_func_t *rot_y_tf; // index of mechanical transfer function applied to this mirror
    
    int num_motions; /** Count of how many motions are available - Not including surface motions */
    motion_type_t *motion_type;

    double Rcx; //!< radius of curvature in x-plane (tangential)
    double Rcy; //!< radius of curvature in y-plane (sagittal)

    int rebuild; //!< rebuild the element?
    
    //!< Stores the knm values computed from any maps applied
    bs_knm_t knm_map;
    //!< Stores the knm values computed from bayer-helms computations
    bs_knm_t knm_bayer_helms;
    
    double *k12_sqrd_sum; // effective per mode loss due to scattering from node 1 to 2
    double *k21_sqrd_sum; // effective per mode loss due to scattering from node 2 to 1
    double *k34_sqrd_sum; // effective per mode loss due to scattering from node 3 to 4
    double *k43_sqrd_sum; // effective per mode loss due to scattering from node 4 to 3
    double *k13_sqrd_sum; // effective per mode loss due to scattering from node 1 to 3
    double *k31_sqrd_sum; // effective per mode loss due to scattering from node 3 to 1
    double *k24_sqrd_sum; // effective per mode loss due to scattering from node 2 to 4
    double *k42_sqrd_sum; // effective per mode loss due to scattering from node 4 to 2
    
    //int map_type_set; //!< Which map types have been set
    surface_map_t * map[MAX_MAPS]; //!< Surface map storage 
    int num_maps; //!< number of surface maps   
    surface_merged_map_t map_merged;
    double map_rotation; //!< map_rotation of the mirror about the z axis
    bs_knm_q_t knm_q; //!< q values for knm calculation
    bs_knm_q_t qm; // mode mismatch beam parameters
    double r_aperture; //!< physical radius of beamsplitter
    
    int knm_order[NUM_KNM_TYPES]; //!< states which order the distortions to the bs should apply
    int knm_change_q; //!< at which distortion effect the beam parameter changes
    
    bool knm_force_saved; //!< if true then the saved knms are forcefully used regardless of parameter differences
    
    // flags stating whether a particular Knm should be calculated
    bitflag knm_calc_flags;
    bitflag knm_flags; //!< flags used to determine knm effects should be calculated analyticaly or not
    
    int ports;
    int nnz_count;
    
    UT_array *motion_links;
    
    double dither_f, dither_phase, dither_m;
    int dither_order;
    int dither_list_index; // Index of this components coupling in the frequency coupling list
    
    fRT_t *fRT; // Holds frequency dependent R, T and L
    
} beamsplitter_t;

//! grating type

typedef struct grating {
    int comp_index; // component index

    int node1_index; //!< index of node1
    int node2_index; //!< index of node2
    int node3_index; //!< index of node3
    int node4_index; //!< index of node4

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix
    int node3_reduced_index; //!< node index for classical interferometer matrix
    int node4_reduced_index; //!< node index for classical interferometer matrix

    //! matrix element of the component in the interferometer matrix
    complex_t ***a11, ***a12, ***a13, ***a14;
    complex_t ***a21, ***a22, ***a23, ***a24;
    complex_t ***a31, ***a32, ***a33, ***a34;
    complex_t ***a41, ***a42, ***a43, ***a44;
    
    complex_t ****a11f, ****a12f, ****a13f, ****a14f;
    complex_t ****a21f, ****a22f, ****a23f, ****a24f;
    complex_t ****a31f, ****a32f, ****a33f, ****a34f;
    complex_t ****a41f, ****a42f, ****a43f, ****a44f;
    
    double d; //!< grating period [nm]
    double alpha; //!< angle of incidence

    // range of allowed grating periods, depends
    // on the type of grating (i.e. how many ports)
    double dmin; //!< minimum of allowed grating periods
    double dmax; //!< maximum of allowed grating periods

    int num_of_ports; //!< number of ports
    double eta[5]; //!< list of efficiencies
    int etaset[5]; //!< have the efficiencies been set?
    /* rho0 is for input-output only. internally it will be mapped as the
     * fourth efficiency, i.e. eta[3] */
    double rho0; //!< another efficiency, see Bunkowski et.al.
    double phi[5]; //!< coupling phases

    bitflag attribs; //!< bit-coded flag for which attribs have been set
    int rebuild; //!< rebuild the element?

    double Rcx; //!< radius of curvature, tangential
    double Rcy; //!< radius of curvature, sagittal

    // tangential ABCD matrices
    //! transformation matrix for q of element; tangential plane 
    ABCD_t **qqt;

    // sagittal ABCD matrices
    //! transformation matrix for q of element; sagittal plane 
    ABCD_t **qqs;
    
    char name[MAX_TOKEN_LEN]; //!< grating name
    
    int nnz_count;
} grating_t;

//! information pertaining to a squeezer

typedef struct squeezer {
    char name[MAX_TOKEN_LEN]; //!< squeezer name

    double squeeze_angle; //!< squeezing angle
    double squeeze_factor; //!< degree of squeezing (0 <= r < Inf)

    int node1_index; //!< index of the connected node
    int node2_index; //!< index of the connected node

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    //! matrix element of the component in the interferometer matrix
    complex_t ***a11;
    //! matrix element of the component in the interferometer matrix
    complex_t ***a12;
    //! matrix element of the component in the interferometer matrix
    complex_t ***a21;
    //! matrix element of the component in the interferometer matrix
    complex_t ***a22;

    bitflag attribs; //!< bit-coded flag for which attribs have been set

    double carrier_phase; //!< phase of the carrier frequency

    complex_t ***q11; //!< quantum noise matrix element mode 1 -> mode 1
    complex_t ***q12; //!< quantum noise matrix element mode 1 -> mode 2
    complex_t ***q21; //!< quantum noise matrix element mode 2 -> mode 1
    complex_t ***q22; //!< quantum noise matrix element mode 2 -> mode 2
} squeezer_t;


//! information describing a signal

typedef struct signal {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< signal name

    int *node1_index; //!< index of node1
    int *node2_index; //!< index of node2
    int *node3_index; //!< index of node3
    int *node4_index; //!< index of node4

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix
    int node3_reduced_index; //!< node index for classical interferometer matrix
    int node4_reduced_index; //!< node index for classical interferometer matrix

    int component_index; //!< index of component where signal is injected
    int component_type; //!< the type of component
    int type; //!< the type of signal

    double phase; //!< signal phase
    double amplitude; //!< signal amplitude
    transfer_func_t *tf; // Transfer function to scale signal by
    
    // These temporary arrays are used for signal calculations.
    // They store separately the transmitted and reflected fields going
    // to a particular port of a mirror or bs.
    complex_t *at1; /** Field transmitted through to node 1 */
    complex_t *at2; /** Field transmitted through to node 2 */
    complex_t *at3; /** Field transmitted through to node 3 */
    complex_t *at4; /** Field transmitted through to node 4 */
    complex_t *ar1; /** Field reflected through to node 1 */
    complex_t *ar2; /** Field reflected through to node 2 */
    complex_t *ar3; /** Field reflected through to node 3 */
    complex_t *ar4; /** Field reflected through to node 4 */
    
    int surface_mode_index; /** If signal is driving a surface mode, then this stores the index of that surface motion */
} signal_t;

//! input light field information

typedef struct light_in {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< name of input light field
    int node_index; //!< index of the connected node
    int node_reduced_index; //!< node index for classical interferometer matrix
    complex_t a; //!< field amplitude
    double I0; //!< field power
    bool isHGModes; //!< If true then the fields are HG modes if not they are LG
    bool isTEMSet; //!< True if a tem command has been used to change the mode content of laser
    bool isSqueezed; /** True if the input is squeezed */
    
    signal_t *modulation_signal;
    
    //! power coefficients
    /*!
     * Used if the command `tem' is used for adding higher order modes to the 
     * input beam.
     */
    complex_t *power_coeff_list;

    double phase; //!< initial phase
    
    char __frequency_name[MAX_TOKEN_LEN]; // Frequency name inputted by user to look for
    
    frequency_t *f; 
    frequency_t *f_entangled_zero; /* Frequency component of the 0Hz pair when
                                    * using entangled carrier squeezing injection.
                                    * If this ever becomes a user configurable
                                    * frequency then care must be taken to ensure
                                    * all the frequency mixing tables and values
                                    * are updated and checked properly as currently
                                    * if the frequency of this component changes
                                    * then nothing else will update.
                                    */
    
    bitflag attribs; //!< bit-coded flag for which attribs have been set

    double noise_value; //!< laser noise, for use in quantum noise calc
    
    bool   use_entangled_carriers; // Use entangled pair of carriers
    double squeeze_db;
    double squeeze_angle;
    
    /** The port at the node in which this input is attached */
    int node_port;
} light_in_t;

//! output data information

typedef struct output_data {
    char name[MAX_TOKEN_LEN]; //!< name of output data

    double Omax[2]; //!< store max value if -max option used
    double Omax_x[2]; //!< store max value in x-plane if -max option used
    double Omax_y[2]; //!< store max value in y-plane if -max option used
    double Omin[2]; //!< store min value if -max option used
    double Omin_x[2]; //!< store min value in x-plane if -max option used
    double Omin_y[2]; //!< store min value in y-plane if -max option used

    int detector_type; //!< detector type
    int output_type; //!< output type
    double user_defined_scale; //!< user defined scaling factor
    
    int node_index; //!< index of connected node
    int node_reduced_index; //!< node index for classical interferometer matrix

    bool beam_param_not_set; //!< true if Gauss beam param not set for node

    //! tag `the other beam'
    /*!
     * At each node there are two directions of propagation.  There is one
     * chosen by default, but one can also specify the `other' beam.  This flag
     * says to use the `other' beam.
     */
    int is_second_beam;
    int detector_index; //!< detector index

    complex_t *qx; //!< x component of q Gaussian beam parameter
    complex_t *qy; //!< y component of q Gaussian beam parameter

    // result
    complex_t signal; //!< output signal

    double re; //!< real component
    double im; //!< imaginary component

    double abs; //!< absolute value
    double deg; //!< phase/argument in degrees

    bool noplot; //!< plot the output data?
    
    qoutput_scale_t quantum_scaling;
} output_data_t;

/**
 * This is a structure used as the item for qnoised detectors list of pure vacuum
 * field contributions.
 */
typedef struct demod_vac{
    /** True if this frequency is correlated with multiple carriers*/
    int num_carriers;
    /** index of correlated carrier frequencies*/
    int cidx[MAX_DEMOD];
    /** demodulated frequency index for the respective carrier*/
    int phi_idx[MAX_DEMOD];
    /** points to next element in the list*/
    struct demod_vac *next;
    /** demodulation contribution frequency relative to base carrier frequency */
    double f;
} demod_vac_t;

/**
 * Information about photo- or amplitude-detectors for output light field
 */
typedef struct light_out {
    int list_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< output light field name
    int num_demods; //!< number of demodulationnum_demods

    int attribs;
    
    double f[MAX_DEMOD + 1]; //!< list of demodulated frequencies
    double phi[MAX_DEMOD + 1]; //!< list of demodulated phases
    
    double homodyne_angle;
    
    //! flag whether phi is given by user, ommitted, or `max' stated instead
    int demod_phase_mode[MAX_DEMOD + 1];
    int sensitivity; //!< whether the pd is used to plot a sensitivity

    // specify certain TEM mode number of amplitude detector
    int field; //!< the kind of field
    bool field_is_set; //!< is the field set?

    // cordinates for beam shape plots
    double x; //!< x coordinate for beam shape plots
    double y; //!< y coordinate for beam shape plots

    double *masking_factor; //!< masking factors (see component "mask")
    bool beam_mask_is_set; //!< masking set?
    int detector_type; //!< detector type (from kat.ini)
    
    int output_idx;
    
    // frequency selection for beam shape
    bool freq_is_set; //!< is the frequency set?
    double freq; //!< the frequency for the beam shape
    
    /**
     * Note: Only created for qnoised detectors.
     * table of [carrier frequency][2**(num demodulations)]
     * Stores the actual frequencies that will be used in the demodulation process.
     */
    double **demod_f; 
    /**
     * Note: Only created for qnoised detectors.
     * table of [carrier frequency][2**(num demodulations)]
     * Stores the total demodulation phase for this demodulated frequency component.
     */
    double *demod_phi; 
    /**
     * Note: Only created for qnoised detectors.
     * Table of [carrier frequency][2**(num demodulations)]. Relates directly to
     * demod_f, if the equivalent frequency is a signal sideband this table
     * stores the index to which signal sideband it is.
     */
    int **demod_f_sig;
    
    /**
     * Note: Only created for qnoised detectors.
     * Singly linked List of vacuum noise contributions that are demodulated into the output
     * value. Uses UT_list.h.
     */
    demod_vac_t *demod_vac_contri;
    
} light_out_t;

typedef struct homodyne {
    int list_index;
    
    int node1_index;
    bool is_second_beam1;
    int light_out_1;
    
    int node2_index;
    bool is_second_beam2;
    int light_out_2;
    
    int output_index;
    
    double **demod_f;
    int **demod_f_sig;
    double *demod_phi;
    
    int sensitivity;
    bool is_quantum;
    double phase;
} homodyne_t;

typedef struct force_out{
    
    int list_index;
    int comp_type;
    int comp_index;
    int motion_index;
    
} force_out_t;

typedef struct openloopgain_out {
    int list_index;
    int comp_type;
    int comp_index;
    int motion_index;
    
} openloopgain_out_t;

typedef struct quadrature_out {
    int index;
    int detector_type;
    int output_index;
    int node_index;
    
    double phase;
    
    double carrier_freq;
    
    char name[MAX_TOKEN_LEN];
} quadrature_out_t;

//! output beam parameters information
typedef struct beampar_out {
    char name[MAX_TOKEN_LEN]; //!< beam parameter name
    int action; //!< flag for detector mode
    int x; //!< x=1 => horizontal, x=0 => vertical \todo change: orientation
    //! num of free spaces given by user in `bp' cmd for computing a Gouy phase
    int num_spaces;
    int *space_list; //!< free space list
    int output_index; //!< the index of the associated output
} beampar_out_t;


typedef enum motion_warnings {
    LONG_WARN = 1,
    ROTX_WARN = 2,
    ROTY_WARN = 4
} motion_warnings_t;

typedef struct motion_out {
    char name[MAX_TOKEN_LEN];
    int component_index;
    int component_type;
    int output_index;
    int motion_index;
    char motion_name[MAX_TOKEN_LEN];
    motion_warnings_t warnings;
} motion_out_t;
//! output convolution information

typedef struct convolution_out {
    char name[MAX_TOKEN_LEN]; //!< convolution name
    double freq; //!< field frequency (as for the 'beam' detector)
    bool freq_is_set; //!< user defined frequency has been set
    double xsize; //!< horizontal field size in 1/w0x
    double ysize; //!< vertical field size in 1/w0y
    int n; //!< first mode index n for HG_nm, p for LG_pl
    int m; //!< second mode index m for HG_nm, l for LG_pl
    int HGLG; //!< switch between HG modes (1) and LG modes (0)
    double w0x; //!< user defined beam parameter
    double z0x; //!< user defined beam parameter
    double w0y; //!< user defined beam parameter
    double z0y; //!< user defined beam parameter
    bool x_is_set; //!< have user defined parameters for x-direction been set?
    bool y_is_set; //!< have user defined parameters for y-direction been set?

    double *masking_factor; //!< masking factors (see component "mask")
    bool beam_mask_is_set; //!< masking set?
    int detector_type; //!< detector type (from kat.ini)

    int output_index; //!< the index of the associated output
} convolution_out_t;

//! output mirror phse information

typedef struct mirror_phase_out {
    char name[MAX_TOKEN_LEN]; //!< mirror_phase name
    int output_index; //!< the index of the associated output
} mirror_phase_out_t;


//! output beam parameters information

typedef  struct cavity_par_out {
    char name[MAX_TOKEN_LEN];
    char cavity_name[MAX_TOKEN_LEN]; //!< cavity name
    int cavity; //!< cavity index
    int action;
    int x; //!< x=1 => horizontal, x=0 => vertical \todo change: orientation
    int output_index; //!< the index of the associated output
} cavity_par_out_t;


//! information describing a modulator

typedef struct modulator {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< modulator name
    
    int node1_index; //!< index of node1
    int node2_index; //!< index of node2

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    double f_mod;
    
    bitflag mismatching;
    
    //! order of sidebands
    /*!
     * Each phase modulation creates an infinite number of new frequency
     * components (in pairs with +/- N*f_mod offsets) but only a few are
     * required to get the correct results.  This specifies how many should be
     * used.
     */
    int order;

    double loss; // TODO implement loss
    
    double modulation_index; //!< modulation index
    double phase; //!< modulation phase
    int type; //!< modulation type
    //  int mode;
    //! switch to single-sideband mode?
    /*!
     * When this is switched on, only one sideband is produced instead of
     * several pairs.
     */
    int single_sideband_mode;

    //! matrix element of field component in the interferometer matrix
    complex_t ***a12;
    //! matrix element of field component in the interferometer matrix
    complex_t ***a21;
    
    // matrix element of the component in the interferometer matrix stores all 
    // frequencies and fields. This differ from other component aXXF as it also
    // contains an extra f term for coupling to different frequencies
    // aXXf[fin][fout][field 1][field 2]
    complex_t *****a12f;
    complex_t *****a21f;
    
    coupling_info_t a_cplng_12, a_cplng_21;
    
    //! coupling coefficient for TEM mode from node 1 into node 2
    complex_t **k12;
    //! coupling coefficient for TEM mode from node 2 into node 1
    complex_t **k21;
    
    complex_t **ktm12; // Tilt modulation scattering matrix
    complex_t **ktm21; // Tilt modulation scattering matrix
    
    double *k12_sqrd_sum;
    double *k21_sqrd_sum;
    
    ABCD_t qq; //!< transformation matrix for q of modulator
    
    q_io_t qm12;
    q_io_t qm21;
    
    bitflag knm_flags;
    
    int rebuild; //!< rebuild the modulator?
    int nnz_count; // number of non-zeros this component takes up in matrix
    
    int numerical_f_couple, print_f_coupling;
} modulator_t;


//! information describing a diode

typedef struct diode {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< diode name

    int node1_index; //!< index of node1
    int node2_index; //!< index of node2
    int node3_index; //!< index of node3
    int node4_index; //!< index of node3
    
    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    double S; //!< ???
    double loss;
    
    bitflag mismatching;
    
    //! matrix element of the component in the interferometer matrix
    complex_t ***a12;
    complex_t ***a21;
    complex_t ***a23;
    complex_t ***a32;

    complex_t ****a12f;
    complex_t ****a21f;
    complex_t ****a23f;
    complex_t ****a32f;
    
    coupling_info_t a_cplng_12, a_cplng_21, a_cplng_23, a_cplng_32;
    
    complex_t **k12; //! coupling coefficient for TEM mode from node 1 into mode 2
    complex_t **k21; //! coupling coefficient for TEM mode from node 2 into mode 1
    complex_t **k32; //! coupling coefficient for TEM mode from node 3 into mode 2
    complex_t **k23; //! coupling coefficient for TEM mode from node 2 into mode 3
    
    double *k12_sqrd_sum; // effective per mode loss due to scattering from node 1 to 2
    double *k21_sqrd_sum; // effective per mode loss due to scattering from node 2 to 1
    double *k32_sqrd_sum; // effective per mode loss due to scattering from node 3 to 2
    double *k23_sqrd_sum; // effective per mode loss due to scattering from node 3 to 2
    
    ABCD_t qq; //!< transformation matrix for q of diode

    q_io_t qm12;
    q_io_t qm21;
    
    int rebuild; //!< rebuild the diode?
    bitflag knm_flags;
    
    int nnz_count;
    
    int option;
} diode_t;


typedef struct dbs {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< diode name

    int node1_index; //!< index of node1
    int node2_index; //!< index of node2
    int node3_index; //!< index of node3
    int node4_index; //!< index of node4
    
    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    double S; 
    
    bitflag mismatching;
    
    complex_t ****a34f;
    complex_t ****a13f;
    complex_t ****a21f;
    complex_t ****a42f;
    
    coupling_info_t a_cplng_34, a_cplng_13;
    coupling_info_t a_cplng_42, a_cplng_21;
    
    zmatrix k34, k13, k21, k42;
    
    double *k34_sqrd_sum; // effective per mode loss due to scattering from node 3 to 1
    double *k13_sqrd_sum; // effective per mode loss due to scattering from node 1 to 2
    double *k21_sqrd_sum; // effective per mode loss due to scattering from node 2 to 4
    double *k42_sqrd_sum; // effective per mode loss due to scattering from node 4 to 3
    
    ABCD_t qq; //!< transformation matrix for q of diode

    q_io_t qm13;
    q_io_t qm21;
    q_io_t qm34;
    q_io_t qm42;
    
    int rebuild; //!< rebuild the diode?
    bitflag knm_flags;
    
    int nnz_count;
} dbs_t;


//! information describing a lens

typedef struct lens {
    int comp_index; // component index
    
    char name[MAX_TOKEN_LEN]; //!< lens name

    int node1_index; //!< index of node1
    int node2_index; //!< index of node2

    int node1_reduced_index; //!< node index for classical interferometer matrix
    int node2_reduced_index; //!< node index for classical interferometer matrix

    double f_of_D; /** focal length or or optical power in dioptre. See flag is_focal_length*/
    bool is_focal_length; /** true if f_of_D is a focal length */
    int rebuild; //!< rebuild the lens?

    bitflag mismatching;
   
    //! matrix element of the component in the interferometer matrix
    complex_t ***a12;
    complex_t ***a21;

    complex_t ****a12f;
    complex_t ****a21f;
    
    coupling_info_t a_cplng_12, a_cplng_21;
    
    //! coupling coefficient for TEM mode from mode 1 into mode 2
    complex_t **k12;
    //! coupling coefficient for TEM mode from mode 2 into mode 1
    complex_t **k21;
    
    double *k12_sqrd_sum; // effective per mode loss due to scattering from node 1 to 2
    double *k21_sqrd_sum; // effective per mode loss due to scattering from node 2 to 1
    
    ABCD_t qq; //!< transformation matrix for q of lens

    q_io_t qm12;
    q_io_t qm21;
    
    complex_t ***q12; //!< quantum noise matrix element mode 1 -> mode 2
    complex_t ***q21; //!< quantum noise matrix element mode 2 -> mode 1
    bitflag knm_flags;
    
    int nnz_count;
} lens_t;

//! a structure of information for attributes of a compnent

typedef struct attr {
    char name[MAX_TOKEN_LEN]; //!< name of attribute
    int component; //!< component index
    int type; //!< the type of component
    double *M; //!< mass
    double *R; //!< radius of curvature
    double *alpha; //!< misalignment angle (elsewhere called beta...)
} attr_t;

//! structure of parameters used when setting an attribute of a component
typedef struct component_param {
    double **xparam; //!< array of parameter values
    int component_type; //!< the component type
    int component_index; //!< the component index
    const char *parameter; //!< the name of the parameter
    char *unit; //!< the units of the parameter
    const char *command_string; //!< command string used to set the attribute
    int *lborder; //!< lower boundary of attribute (???)
    int *uborder; //!< upper boundary of attribute (???)
    double *min; //!< minimum value of attribute
    double *max; //!< maximum value of attribute
} component_param_t;

/**
 * Options for the sparse matrix solver to use
 */
typedef enum solvers {
    //KLU      = 1, No longer used
    KLU_FULL = 2,
    //SUPERLU  = 3, Not implemented yet
#if INCLUDE_NICSLU == 1
    NICSLU   = 4,
#endif
#if INCLUDE_PARALUTION == 1
    PARALUTION = 5
#endif
} solvers_t;

//! options passed into program
typedef struct options {
    int help; //!< is the help page asked for?
    int maxmin; //!< is max and min output asked for?
    int perl1; //!< switch off some of the output?
    int trace_output; // If set trace data is output to text files for pykat processing
    int no_output_bksp; //!< use newline instead of backspace for outputs
    int quiet; //!< switch off all output?
    int new_arg; //!< is this a new feature of finesse?
    solvers_t sparse_solver; //!< should we use KLU or SPARSE package?
    int sparse_solver_manual_selection; //!< manual setting of optios.sparse_solver
    int check; //!< do consistency check of interferometer matrix?
    int servermode; //!< server mode flag
    int portnumber; //!< portnumber for client server communication
    int version; //!< print version and quit
    int noheader; //!< do not print header into output file
    int test; //!< for brumsofttest, do not call any plotting 
    bool use_coupling_reduction;
    char out_format[MAX_TOKEN_LEN];
    int nicslu_threads;
    int paralution_threads;
    int print_qnoise_matrix;
} options_t;

//! photo detector type

typedef struct pdtype {
    char *name; //!< photo detector name
    double ****cross; //!< beat coefficient on diode between mode n1m1 and n2m2
} pdtype_t;

//! information describing a gnuplot terminal

typedef struct gnuplot_terminal {
    char *name; //!< terminal name
    char *command; //!< terminal command
    char *suffix; //!< output file suffix (e.g. .gif for gif images)
    bool output_is_file; //!< does terminal output to a file?
} gnuplot_terminal_t;


//! information describing a python terminal

typedef struct python_terminal {
    char *name; //!< terminal name
    char *command; //!< terminal command
    char *suffix; //!< output file suffix (e.g. .gif for gif images)
    bool output_is_file; //!< does terminal output to a file?
} python_terminal_t;


//! cavity type

typedef struct cavity {
    char name[MAX_TOKEN_LEN]; //!< cavity name

    int component1_index; //!< start component for cavity tracing
    int component2_index; //!< stop component for cavity tracing

    int node1_index; //!< start node index
    int node2_index; //!< stop node index
    
    double stability_x; /** x direction stability factor */
    double stability_y; /** y direction stability factor */

    double rt_gouy_x; /** Roundtrip gouy x*/
    double rt_gouy_y; /** Roundtrip gouy y*/
    
    int stable; //!< is cavity stable?
    complex_t qx; //!< Gaussian beam parameter of eigenmode (horizontal)
    complex_t qy; //!< Gaussian beam parameter of eigenmode (vertical)
    double n; //!< index of refraction at first node
    double finesse; //!< cavity finesse
    double loss; //!< round-trip loss
    double length; //!< optical path length (round-trip)
    double FSR; //!< free spectral range
    double FWHM; //!< full width at half maximum
    double pole; //!< cavity pole (FWHM/2)
    
    ABCD_t Mx, My; // Roundtrip matrices
} cavity_t;

//! information describing a Gaussian beam

typedef struct gauss {
    char name[MAX_TOKEN_LEN]; //!< name of Gaussian beam
    int component_index; //!< component index
    int node_index; //!< index at node1

    complex_t qx; //!< complex beam parameter in x-plane
    complex_t qy; //!< complex beam parameter in y-plane

    double wx0; //!< x-component of beam waist
    double wy0; //!< y-component of beam waist
} gauss_t;

//! describes an axis; refers to the xaxis, x2axis and x3axis commands

typedef struct xaxis {
    double *xaxis; //!< x-axis
    double minus_xaxis; //!< minus value of x-axis (to be used for mx variables)
    double op; //!< used to store previous values
    char xname[LINE_LEN]; //!< name of axis
    
    double xmin; //!< minimum extent of axis
    double xmax; //!< maximum extent of axis
    double max; //!< maximum limit for a given axis
    double min; //!< minimum limit for a given axis

    int lborder; //!< x axis lower border
    int uborder; //!< x axis upper border

    int component_idx; //!< Index of component being varied
    int xsteps; //!< the number of steps between xmin and xmax
    int xtype; //!< linear or logarthmic?
} axis_t;

//! information describing a set command

/*!
 * Set assignes a variable name to an output signal so that it can be usd
 * with func or lock.  Syntax:
 *
 * set component parameter name
 */
typedef struct set_command {
    char name[LINE_LEN]; //!< name of the set
    char component_name[LINE_LEN]; //!< component name
    char parameter_name[LINE_LEN]; //!< parameter name

    double *value; //!< pointer to the output signal

    double max; //!< maximum of set
    double min; //!< minimum of set

    int lborder; //!< lower border of set
    int uborder; //!< upper border of set

    bool is_detector; //!< is `component parameter' a detector or normal component?
} set_command_t;

//! describes a put command

/*!
 * Put a variable into a parameter of a component
 */
typedef struct put_command {
    double *target; //!< pointer to component parameter value
    double *source; //!< pointer to the variable value

    char component_name[LINE_LEN]; //!< component name
    char parameter_name[LINE_LEN]; //!< parameter name
    char function_name[LINE_LEN]; //!< function name

    double max; //!< maximum of put
    double min; //!< minimum of put

    int lborder; //!< lower border of put
    int uborder; //!< upper border of put

    //! which mode is put command in?
    /*!
     * 0 = overwriting parameter
     * 1 = adding to parameter
     */
    int mode;
    double startvalue; //!< put start value
} put_command_t;



//! describes a tune command

/*!
 * Sets a new value to a parameter of a component as defined by a server-client connection
 */
typedef struct tune {
    double *target; //!< pointer to component parameter value

    char cname[LINE_LEN]; //!< component name
    char pname[LINE_LEN]; //!< parameter name
    double value; //!< parameter value

    double max; //!< maximum 
    double min; //!< minimum 

    int lborder; //!< lower border param value
    int uborder; //!< upper border param value

    double startvalue; //!< original parameter value
} tune_t;


//! lock type

typedef struct lock_command {
    char name[LINE_LEN]; //!< lock name
    char iname[LINE_LEN]; //!< variable name that lock should put to zero

    double *input; //!< lock input
    double value; //!< lock value
    double gain; //!< lock gain
    double accuracy; //!< accuracy of lock
    double offset; 
    
    double last1; //!< last lock value (for gradient calculation)
    double last2; //!< second to last lock value (for grad calcn)

    //! lock only the once?
    /*!
     * if once is set, then a lock is performed only for the first point on
     * the x-axis
     */
    int once;
} lock_command_t;

//! information describing a user-defined function

/*!
 * this is a user-defined function which is parsed by formulc and then
 * evaluated for each point on the x-axis.
 */
typedef struct func_command {
    char name[MAX_TOKEN_LEN]; //!< function name
    char fstring[LINE_LEN]; //!< function string
    double *input[MAX_TOKEN]; //!< list of input values
    char tokens[MAX_TOKEN]; //!< function tokens
    int num_tokens; //!< number of function tokens
    int type[MAX_TOKEN]; //!< array of token types
    int token_number[MAX_TOKEN]; //!< array of token indices (i.e. token index)
    int ninput; //!< number of input values
    formu formula; //!< the formula as parsed by formulc
    double result; //!< function result
    bool is_detector; //!< are any of func inputs a detector output?
    int output; //!< output index
} func_command_t;

//! derivative type

typedef struct derivative {
    double *xderiv; //!< derivative in x direction
    char name[LINE_LEN]; //!< derivative function name

    double max; //!< derivative maximum
    double min; //!< derivative minimum

    int lborder; //!< lower border of derivative
    int uborder; //!< upper border of derivative
} derivative_t;

//! describes the scale command for user-defined rescaled outputs

typedef struct scale {
    double factor; //!< scale factor
    int output; //!< scale output

    //! the type of scaling performed
    /*!
     * the scale can be invoked with a factor or with a keyword.  type stores
     * which keyword or that it was a number.
     */
    int type;
} scale_t;

//! simple constants in the input file

typedef struct constant {
    char name[MAX_TOKEN_LEN]; //!< constant name
    char value[LINE_LEN]; //!< constant value
    int name_len; //!< length of constant name
    int value_len; //!< length of constant value
} constant_t;

//! a collection of flags for the beam analyser

typedef struct beam_analyser {
    bool set; //!< has beam anaylser been defined?
    int x; //!< does analyser look in x-direction?
    int y; //!< does analyser look in y-direction?

    //! should xaxis and x2axis be swapped?
    /*!
     * this can be useful if one axis is the position on the beam analyser.
     * One can then run along the axis without recomputing the light fields.
     * By definition xaxis runs around x2axis, so sometimes one has to swap
     * them to avoid recomputation of fields with beam analysers.
     */
    int swap;
} beam_analyser_t;

typedef struct minimizer {
    char name[LINE_LEN];
    
    bool find_max; // if true does a maximize instead
    
    output_data_t *result_output; // output to store minimized variable
    
    output_data_t *target_output; // output to minimize
    int target_index;
    int target_type;
    char target_parameter[LINE_LEN]; //!< parameter name
    
    double *variable;
    double A; // The lower bound to search in
    double B; // The upper bound to search in
    
    double x_result; // last variable value that gave a minimium
    
    double abserr;
    int max_iter;
    
} minimizer_t;



//! global variables

typedef struct global_var {
    char pykat_pipename[FNAME_LEN];
    char input_fname[FNAME_LEN]; //!< input file name
    char output_fname[FNAME_LEN]; //!< output file name
    char gnuplot_fname[FNAME_LEN]; //!< gnuplot file name
#ifdef OSWIN
    char gnuplot_test_fname[FNAME_LEN]; //!< gnuplot file name
#endif
    char matlab_fname[FNAME_LEN]; //!< matlab file name
    char python_fname[FNAME_LEN]; //!< python file name
    char log_fname[FNAME_LEN]; //!< log file name
} global_var_t;

//! local variables

typedef struct local_var {
    complex_t *zsolution; //!< solution in z direction
    complex_t *zderiv1; //!< point used to calculate the slope
    complex_t *zderiv2; //!< point used to calculate the slope

    double x_p1; //!< ???
    double x_p2; //!< ???

    complex_t z_in_p1; //!< ???
    complex_t z_out_p1; //!< ???

    complex_t z_in_p2; //!< ???
    complex_t z_out_p2; //!< ???

    complex_t zk1; //!< ???
    int kn1; //!< ???
    int kn2; //!< ???
    complex_t kqx1; //!< ???
    complex_t kqx2; //!< ???
    double kgammax; //!< ???
    double knr; //!< ???

    double natLogsOfN[101]; //!< list of natural logs of N
    int natLogMax; //!< maximum value of n for natural log list

    int *component_trace_list; //!< stores overall component index for beam tracing
    int *trace_n[3]; //!< ???
    int *trace_c1; //!< ???
    int *trace_c2; //!< ???

    double *respt; //!< ???

    int ln1; //!< ???
    int ln2; //!< ???
    int lm1; //!< ???
    int lm2; //!< ???

    complex_t lqx1; //!< ???
    complex_t lqx2; //!< ???
    complex_t lqy1; //!< ???
    complex_t lqy2; //!< ???

    double lcx; //!< ???
    double lcy; //!< ???
    double lsx; //!< ???
    double lsy; //!< ???
    double lnr; //!< ???
} local_var_t;


/** Stores klu objects/structs for using klu as solver for matrices*/
typedef struct klu_objs {
    klu_l_symbolic *Symbolic;
    klu_l_numeric *Numeric;
    klu_l_common Common;
} klu_objs_t;

#if INCLUDE_NICSLU == 1
/** Stores klu objects/structs for using klu as solver for matrices*/
typedef struct nicslu_objs {
    SNicsLUc nicslu;
} nicslu_objs_t;
#endif

//! variables for KLU sparse routines

typedef struct klu_sparse_var {
    /* Adapted from the KLU user guide:
    The matrix is described with four parameters: 
    nz=inter.num_nonzero: integer scaler, number of non-zero elements (a `non-zero'
    element is present in the sparse matrix, however, it can be numerically zero.).
    n=inter.num_eqns: an integer scalar. The matrix is n-by-n. Note that KLU 
    only operates on square matrices. 
    Ap=klu.col_ptr: an integer array of size n+1. The first entry is col_ptr[0]=0, and the last 
    entry nz=col_ptr[n] is equal to the number of entries in the matrix. 
    Ai=klu.row_index: an integer array of size nz = col_ptr[n]. The row indices of 
    entries in column j of A are located in row_index [col_ptr [j] ... col_ptr [j+1]-1]. 
    The matrix is zero-based; row and column indices are in the range 0 to n-1. 
    Ax=klu.value: a double array of size nz for the real case, or 2*nz for the 
    complex case. For the complex  case, the real and imaginary parts are interleaved, 
    compatible with Fortran and the ANSI C99 Complex data type. KLU does not rely on 
    the ANSI C99 data type, however, for portability  reasons. The numerical values 
    in column j of a real matrix are located in Ax [Ap [j] ... Ap [j+1]-1]. For a 
    complex matrix, they appear in Ax [2*Ap [j] ... 2*Ap [j+1]-1], 
    as real/imaginary pairs (the real part appears first, followed by the imaginary part). 
     */

    /*
    From: http://www.cs.utk.edu/~dongarra/etemplates/node374.html
    Assuming we have a nonsymmetric sparse matrix A, we create three vectors: 
    one for floating point numbers (value) and the other two for integers 
    (row_index, col_ptr). The `value' vector stores the values of the nonzero 
    elements of the matrix A as they are traversed in a column-wise fashion. 
    The `col_ind' vector stores the column indexes of the elements in the `value' 
    vector. That is, if value(k)=a_{i,j}, then row_index(k)=i. 
    The col_ptr vector stores the locations in the val vector that start a 
    row; that is, if value(k)=a_{i,j), then col_ptr(j)<= k< col_ptr(j+1). 
    By convention, we define col_ptr(n+1) = nnz+1, where nnz is the number of 
    nonzeros in the matrix A. The storage savings for this approach is significant. 
    Instead of storing n^2 elements, we need only 2nnz+n+1 storage locations.
     */

    double *value; //!< sparse CCS matrix numerical values (length=2*inter.num_nonzero)
    long *row_index; //!< sparse CCS matrix row index        (length=inter.num_nonzero)
    long *col_ptr; //!< sparse CCS matrix column pointer   (length=inter.num_eqns+1)
    double *rhs; //!< ride-hand-side for sparse matrix   (length=2*inter.num_eqns)
    complex_t *crhs; // utility pointer, points to rhs above but is cast to complex type
} klu_sparse_var_t;

typedef struct node_connections{
    int type_1, type_2;
    void *comp_1, *comp_2;
} node_connections_t;

//! collects all the index variables together to help bounds checking

typedef struct indices {
    int base_index_1; //!< base index for calculation of matrix indices
    int base_index_2; //!< base index for calculation of matrix indices
    int base_index_3; //!< base index for calculation of matrix indices
    int base_index_4; //!< base index for calculation of matrix indices

    int row_index_1; //!< row index for interferometer matrix
    int row_index_2; //!< row index for interferometer matrix
    int row_index_3; //!< row index for interferometer matrix
    int row_index_4; //!< row index for interferometer matrix

    int col_index_1; //!< column index for interferometer matrix
    int col_index_2; //!< column index for interferometer matrix
    int col_index_3; //!< column index for interferometer matrix
    int col_index_4; //!< column index for interferometer matrix
} indices_t;

enum frequency_flags {
    PRINT_FREQUENCIES = 1,
    PRINT_MODULATOR_COUPLINGS = 2,
    PRINT_MIRROR_COUPLINGS = 4,
    PRINT_BS_COUPLINGS = 8
};

/**
 * Struct to store details on the mixing of frequencies
 */
typedef struct fmix {
    int comp1_index;
    int comp1_order;
    int comp2_index;
    int comp2_order;
} fmix_t;


typedef struct mhomodyne_pair {
    int node1_index;
    bool is_second_beam1;
    int light_out_1;
    
    int node2_index;
    bool is_second_beam2;
    int light_out_2; 
    
    char func_name[MAX_TOKEN_LEN];
    
} mhomodyne_pair_t;

typedef struct mhomodyne {
    int list_index;
    int num_node_pairs;
    
    mhomodyne_pair_t pairs[MAX_MHOMODYNE];
    
    int output_index;
    
    double **demod_f;
    int **demod_f_sig;
    double *demod_phi;
    
    int sensitivity;
    bool is_quantum;
} mhomodyne_t;

typedef enum {
    IGNORE_LOW_R_MIRROR_BS = 1,
    IGNORE_LOW_T_MIRROR_BS = 2,
    AVERAGE_Q = 4,
    PRINT_EACH_STEP = 8
} mismatches_options_t;

//! interferometer type
typedef struct interferometer {
    char *basename; //!< basename for input and output files

    constant_t *constant_list; //!< list of constants
    int num_constants; //!< number of constants

    // a linked list that is based on the temporary frequencies generated from
    // all the various different components, frequencies are removed if they
    // are not required or duplicates of other frequencies added. Finally these
    // linked frequencies are transformed into an array of the frequencies
    // actually used in the computations
    frequency_linked_list_t frequency_linked_list;
    frequency_t *tmp_f_list; // stores all frequencies that components will need
    frequency_t **carrier_f_list; // stores pointers to frequencies that will actually get used
    frequency_t **signal_f_list; // stores pointers to frequencies that will actually get used
    
    enum frequency_flags frequency_flag;
    
    mirror_t *mirror_list; //!< list of mirrors
    space_t *space_list; //!< list of free space sections
    beamsplitter_t *bs_list; //!< list of beam splitters
    grating_t *grating_list; //!< list of gratings
    squeezer_t *squeezer_list; //!< list of squeezers
    light_out_t *light_out_list; //!< list of output light beams
    output_data_t *output_data_list; //!< output data list
    motion_out_t *motion_out_list;   //!< list of optic motion outputs
    beampar_out_t *beampar_out_list; //!< output beam parameters
    cavity_par_out_t *cavity_par_out_list; //!< output cavity parameters
    convolution_out_t *convolution_out_list; //!< output convolution 
    mirror_phase_out_t *mirror_phase_out_list; //!< output mirror phase 
    light_in_t *light_in_list; //!< list of input light sources
    modulator_t *modulator_list; //!< list of modulators
    node_t *node_list; //!< node list
    signal_t *signal_list; //!< signal list
    derivative_t *deriv_list; //!< list of derivatives
    scale_t *scale_list; //!< list of scales
    diode_t *diode_list; //!< diode list
    dbs_t *dbs_list;
    lens_t *lens_list; //!< list of lenses
    attr_t *attr_list; //!< list of attributes
    gauss_t *gauss_list; //!< Gaussian beam list
    cavity_t *cavity_list; //!< list of cavities
    variable_t *variable_list; //!< list of variables (dummy parameters)
    sagnac_t *sagnac_list;
    block_t *block_list;
    transfer_func_t *tf_list;
    int *quantum_components; /** List of overall component indices which have been marked as quantum inputs, either losses, open ports, laser noise, etc. */
    quadrature_out_t *quad_out_list;
    qnoise_input_t *qnoise_in_list;
    node_connections_t *node_conn_list;
    surface_map_t *surface_motion_map_list;
    char **tmp_put_cmds;
    force_out_t *force_out_list;
    openloopgain_out_t *openloopgain_list;
    slink_t *slink_list;
    homodyne_t *homodyne_list;
    mhomodyne_t *mhomodyne_list;
    mirror_t **mirror_dither_list;
    beamsplitter_t **bs_dither_list;
    dof_t * dof_list;
    
    int num_mirrors; //!< number of mirrors
    int num_beamsplitters; //!< number of beam splitters
    int num_spaces; //!< number of free spaces
    int num_sagnacs; //!< number of sagnacs
    int num_nodes; //!< number of nodes
    int num_light_inputs; //!< number of input light fields
    int num_light_outputs; //!< number of output light fields
    int num_motion_outputs;
    int num_modulators; //!< number of modulators
    int num_signals; //!< number of signals
    int num_diff_cmds; //!< number of derivatives
    int num_outputs; //!< number of elements in output data
    int num_beam_param_outputs; //!< number of output beam parameters
    int num_cavity_param_outputs; //!< number of output cavity parameters
    int num_convolution_outputs; //!< number of output convolution
    int num_mirror_phase_outputs; //!< number of output convolution
    int num_scale_cmds; //!< number of scales
    int num_diodes; //!< number of diodes
    int num_dbss; //!< number of 4 port direction BSs
    int num_lenses; //!< number of lenses
    int num_attributes; //!< number of attributes
    int num_gauss_cmds; //!< number of Gaussian commands
    int num_cavities; //!< number of cavities
    int num_variables; //!< number of variables
    int num_gratings; //!< number of gratings
    int num_squeezers; //!< number of squeezers
    int num_dump_nodes; //!< number of dump nodes
    int num_reduced_nodes; //!< number of nodes minus number of dump nodes
    int num_transfer_funcs;
    int num_blocks;
    int num_qnoise_inputs;
    int num_quad_outputs;
    int num_force_out;
    int num_surface_motion_maps;
    int num_tmp_put_cmds;
    int num_slinks;
    int num_homodynes;
    int num_mhomodynes;
    int num_dof;
    int num_quantum_components; /** Number of nodes that are a quantum noise input */
    bool all_quantum_components; /** true if command 'vacuum all' is used. In this
                                  * case all components are classed as quantum inputs if  
                                  * they have losses or an open port, etc.*/
    
    int num_mirror_dithers;
    int num_bs_dithers;
    
    // !!! num_eqns is just for the old solver
    int num_eqns; //!< number of equations -> rank of sparse matrix    
    int num_nonzero; //!< number of non-zero elemensts in sparse matrix
    
    // Note num_quadratures not used anymore
    int num_quadratures; //!< number of quadratures
    int num_openloopgains;
    int num_components; //!< number of components
    int num_output_cols; //!< number of output columns
    int time; //!< printing computation time on/off
    int noscripts; //!< writing gnuplot/python/matlab script files on/off
    int noxaxis; //!< don't run over xaxis, just do one computation
    bool multi; //!< plot multiple surfaces?
    bool subplot; //!< create subplot for phase?
    int pause; //!< put "pause -1" in gnuplot file
    int printmatrix; //!< print matrix into file
    int mismatches;
    
    mismatches_options_t mismatches_options;
    
    bool printqnoiseinputs;
    
    //! beam tracing on/off 
    /*!
     * trace is a bit-encoded integer describing the kind of trace to perform.
     *   - 1:   list of TEM modes used
     *   - 2:   cavity eigenvalues and cavity parameters like FWHM, 
     *          FSR optical length and Finesse
     *   - 4:   mode mismatch parameters for the initial setup
     *   - 8:   beam parameters for every node, nodes are listed in
     *          the order found by the tracing algorithm
     *   - 16:  Gouy phases for all spaces
     *   - 32:  coupling coefficients for all components
     *   - 64:  mode matching parameters during calculation, if they change
     *          due to a parameter change, for example by changing a radius of 
     *          curvature.
     *   - 128: nodes found during the cavity tracing
     */
    int trace;
    int powers; //!< flag whether powers in all nodes should be printed once
    int grating; //!< verbose output for gratings
    int mycolor; //!< set nice color schemes for Gnuplot
    int width; //!< linewidth for Gnuplot
    int k; //!< which type of coupling coefficient computation

    int tem; //!< maximum order of hermite gauss modes
    bool tem_is_set; //!< maximum order of hermite gauss modes is set!
    int num_fields; //!< number of field amplitudes
    int num_motion_eqns; /** Total number of surface motions for all optics */
    int rebuild; //!< rebuild some k coeffs while tuning parameters
    beam_analyser_t beam; //!< beam analyser settings
    int set_tem_phase_zero; //!< set TEM_00 phase to 0 ?
    int mats; //!< size of matrix

    axis_t x1; //!< x axis
    axis_t x2; //!< y axis
    axis_t x3; //!< z axis

    // ---- new 
    int num_set_cmds; //!< number of sets
    set_command_t *set_list; //!< list of sets

    // --------
    int num_put_cmds; //!< number of puts
    put_command_t *put_list; //!< list of puts
    tune_t *tune_list; //!< list of parameters to be tunen by server-client commands 
    int num_tune_params; //!< number parameter values to be send/received

    int num_ROM_maps;
    rom_map_t *rom_maps;

    int num_locks; //!< number of locks
    lock_command_t *lock_list; //!< list of locks

    int num_func_cmds; //!< number of functions
    func_command_t *function_list; //!< list of functions

    int *func_order; //!< order in which functions should be processed
    int *lock_order; //!< order in which locks should be processed
    bool *is_locked; //!< has element obtained a lock?
    int *breakcount; //!< counter to determine break condition
    int num_locks_and_funcs; //!< the number of locks and functions
    int lock; //!< are there any locks?
    int showiterate; //!< show iteration output
    int startlock; //!< switch off lock when should only be done once

    int startnode; //!< optional starting node for the beam tracing
    int startnode_set; //!< has the command `startnode' been used?

    int retrace; //!< retrace the system?
    int retrace_manual; //!< manual override of the retrace setting?
    int splot; //!< use splot?
    bool x3_axis_is_set; //!< is x3axis set?

    //! use video mode?
    /*!
     * there are two types of output for x3axis: a huge 3D matrix or a set of
     * 2D outputs.  The latter is called video mode because one can make
     * little films this way.  This flag tells which will be th output.
     */
    int video;

    char y1name[MAX_TOKEN_LEN]; //!< label of left hand y axis
    char y2name[MAX_TOKEN_LEN]; //!< label of right hand y axis
    int yaxis[2]; //!< are we using the left and right y axes
    bool ploty2axis; //!< should we plot two y axes?
    int ytype; //!< y-axis log or linear?

    double f0; //!< carrier frequency
    double *f; //!< frequencies
    int *t; //!< is result a transfer function or error signal?
    
    //! warning! This is the variable used to store the total number of frequencies
    // in the extern variables f_s and t_s. The num_carrier_freqs and num_signal_freqs
    // should be used below instead
    int num_frequencies; //!< number of frequencies
    
    int num_carrier_freqs; //!< number of carrier + modulator + user frequencies
    int num_signal_freqs; //!< number of signal frequencies
    
    int num_tmp_frequencies; // number of frequencies created that have not yet been
                             // filtered
    int num_non_signal_frequencies; //!< number of non-signal frequencies

    bool do_minimize;
    minimizer_t *minimizer;
    
    double fsig;  //!< positive signal frequency
    double mfsig; //!< negative signal frequency

    double mismatches_lower; // lower level or mismatch to report
    double mismatches_R_T_limit;
    
    int *gnuterm; //!< list of gnuplot terminals
    char **gnutermfn; //!< gnuplot terminal function
    char *gnuplotcommand; //!< list of gnuplot plotting commands

    int *pyterm; //!< list of python terminals
    char **pytermfn; //!< python terminal function
    char *pythoncommand; //!< list of python commands
    
    char *matlabcommand; //!< list of matlab commands
    char *matlabplotcommand; //!< list of matlab  plotting commands
    
    
    int num_gnuterm_cmds; //!< number of gnuplot terminals
    int num_gnuplot_cmds; //!< number of gnuplot plotting commands
    int num_term_cmds; //!< number of all terminal commands
    
    int num_matlab_cmds; //!< number of matlab commands
    int num_matlabplot_cmds; //!< number of matlab plotting commands
    
    int num_pyterm_cmds; //!< number of python terminal
    int num_python_cmds; //!< number of python plotting commands

    bitflag debug; //!< debug flag

    double scale_factor; //!< plotting scale factor
    int scale_unit; //!< plotting scale units

    double null; //!< the value 0
    double *dnull; //!< pointer to the value 0

    //! what kind of shotnoise to compute (simple, or quantum)
    int shotnoise;

    bool setup; //!< has the setup function been called?

    int knm; //!< knm???
    
    bool warned_quantum_no_fsig;  
    bool warned_mod_up_low_coupling;
} interferometer_t;

typedef enum knm_flags {
    USE_BAYER_HELMS = 0,
    PRINT_COEFFS = 1,
    INT_X_AND_Y = 2,
    INT_X_OR_Y = 4,
    INT_APERTURE = 8,
    INT_CURVATURE = 16,
    INT_X_Y_DISP = 32,
    INT_BAYER_HELMS = 64,
    INT_BAYER_HELMS_OLD = 128
} knm_flags_t;

//! intialisation variables from kat.in
typedef struct init_variables {
    amplitude_scaling_t amplitude_scaling;
    
    char katinifile[LINE_LEN]; //!< path+name of init file read from variable KATINI
    bool katini_filename_set; //!< has filename has been set with KATINI?
    int init_file_is_found; //!< has the initialisation file been found?
    //! how often progress info is output as a percentage
    int percentage_step;
    int num_points_total; //!< total number of points (for progress info)

    double x_scale; //! scale of the mirror motions that are stored in the matrix
    double lambda; //!< wavelength of light
    double clight; //!< speed of light
    double eqsmall; //!< used to decide if two values differ
    double small; //!< definition of a "small" number
    double qeff; //!< quantum efficiency
    double epsilon_0_c; //!< permeability of free space
    double deriv_h; //!< Planck's constant for derivations
    double n0; //!< default refractive index

    char gnucommand[LINE_LEN]; //!< gnuplot command
    gnuplot_terminal_t gnuterm[MAX_GNUTERM]; //!< gnuplot terminal
    char *gnutermfn[MAX_GNUTERM]; //!< gnuplot terminal function
    int num_gnuplot_terminals; //!< number of gnuplot terminals
    
    int plotting; //!< default plotting with gnuplot or python?
    // index for python `screen' terminal
    int screenterm; 
    
    // terminal index for unix/win/osx gnuterm terminals 
    int winterm; //!< is the gnuplot windows terminal set
    int unixterm; //!< is a gnuplot unix terminal set
    int macosterm; //!< is a gnuplot macos terminal set

    int num_pdtypes; //!< number of photodetector types

    char pycommand[LINE_LEN]; //!< python command
    python_terminal_t pyterm[MAX_PYTERM]; //!< python terminal
    char *pytermfn[MAX_PYTERM]; //!< python terminal function

    int num_python_terminals; //!< number of gnuplot terminals
    
    pdtype_t pdt[MAX_PD_TYPE]; //!< photodetector type
    
    // locking
    int locksteps; //!< number of steps in locking
    //! number of steps for which loop state is checked
    int locktest1;
    //! number of checks before loop gain is increased or decreased
    int locktest2;

    double lockthresholdlow; //!< lower locking threshold
    double lockthresholdhigh; //!< upper locking threshold

    double gainfactor; //!< gain factor
    int autogain; //!< is gain automatic?
    int sequential; //!< is this a sequential lock?
    int autostop; //!< should we stop automatically?

    double gnuversion; //!< the version of gnuplot being used

    int maxintop; //!< maximum number of integration operations in dcuhre
    double abserr; //!< absolute error
    double relerr; //!< relative error
    int maxintcuba;
    unsigned int mapNCOrder;
    int_method_t mapintmethod;
    interpolation_method_t mapinterpmethod;
    int mapinterpsize;
    
    int cuba_numprocs;
    int calc_knm_transpose; //!< if 1 then the transpose element is calculated using phase factor
    
    qoutput_scale_t default_quantum_output_scale;
} init_variables_t;

/* ==================================================================== */

/* from kat.c */
int main(int argc, char *argv[]);
void banner(FILE *fp, char *input_fname, char *output_fname,
        char *gnuplot_fname);

/* from AllTests.c */
#ifdef TEST
void RunAllTests(void);
#endif

#endif // KAT_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
