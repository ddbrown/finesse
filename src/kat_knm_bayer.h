#ifndef KAT_KNM_BAYER_H
#define	KAT_KNM_BAYER_H

void compute_mirror_bayer_helms_knm(mirror_t *mirror, double nr1, double nr2);

bool compute_bs_bayer_helms_knm(beamsplitter_t *mirror, double nr1, double nr2);

#endif	/* KAT_KNM_BAYER_H */

