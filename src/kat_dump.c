/*!
 * \file kat_dump.c
 * \brief Routines to dump various pieces of information to file if required.
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#include "kat.h"
#include "kat_inline.c"
#include "kat_optics.h"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_aa.h"

extern init_variables_t init;
extern interferometer_t inter;
extern int n_index;
extern memory_t mem;
extern local_var_t vlocal;

extern double *f_s; //!< Frequency value list [frequency]
extern int *t_s; //!< Type_of_Signal list [frequency]
extern ifo_matrix_vars_t M_ifo_car; // carrier and modulator field matrix


extern options_t options;

//! Dump constants to file stream

/*!
 * \param fp the file pointer
 *
 * \todo untested
 */
void dump_constants(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_constants) {
        fprintf(fp, "The following constants are used:\n");
        int i;
        for (i = 0; i < inter.num_constants; i++) {
            fprintf(fp, "%d. %s = %s\n", i + 1, inter.constant_list[i].name,
                    inter.constant_list[i].value);
        }
    } else {
        fprintf(fp, "no constants\n");
    }
}

//! Dump photodectector type definitions

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_pdtype(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "detector type definitions\n"
            " (note that `n1m1 n2m2 factor' includes `n2m2 n1m1 factor')\n");

    int pd_type_index;
    for (pd_type_index = 0; pd_type_index < init.num_pdtypes; pd_type_index++) {
        int column_index = 0;
        int is_not_first_value = 0;

        pdtype_t pdt = init.pdt[pd_type_index];
        fprintf(fp, "%d. name: %s\n ", pd_type_index + 1, pdt.name);
        int j, k, l, m;
        for (j = 0; j <= mem.hg_mode_order; j++) {
            for (k = 0; k <= mem.hg_mode_order; k++) {
                for (l = j; l <= mem.hg_mode_order; l++) {
                    for (m = k; m <= mem.hg_mode_order; m++) {
                        if (pdt.cross[j][k][l][m] != 0.0) {
                            if (is_not_first_value) {
                                if (column_index > 4) {
                                    fprintf(fp, "\n ");
                                    column_index = 0;
                                } else {
                                    fprintf(fp, ", ");
                                }
                            }
                            fprintf(fp,
                                    " %d%d %d%d %s", j, k, l, m,
                                    double_form(pdt.cross[j][k][l][m]));
                            column_index++;
                            is_not_first_value++;
                        }
                    }
                }
            }
        }
        fprintf(fp, "\n");
    }
}

//! Dump coupling coefficients for all components to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_ks(FILE *fp) {
    mirror_t mirror;
    beamsplitter_t bs;
    space_t space;
    lens_t lens;
    diode_t diode;
    modulator_t modulator;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "coupling coeffcients k_mnkl for all components:\n");
    fprintf(fp, "k11 : reflection node1 to node1, \n");
    fprintf(fp, "k24 : transmission node2 to node4, etc.\n");
    fprintf(fp, "k11(i, j) n1 m1 -> n2 m2        i, j are field indices _not_ TEM indices \n");
    //          "(use `trace 1' to get a list of fields).\n");

    // \todo this should probably be its own routine
    int mirror_index;
    for (mirror_index = 0; mirror_index < inter.num_mirrors; mirror_index++) {
        mirror = inter.mirror_list[mirror_index];
        fprintf(fp, "mirror %s:\n", mirror.name);
        int field_index_outer;
        for (field_index_outer = 0;
                field_index_outer < inter.num_fields;
                field_index_outer++) {
            double k11_sum = 0.0;
            double k12_sum = 0.0;
            double k21_sum = 0.0;
            double k22_sum = 0.0;
            int field_index_inner;
            int n1, m1, n2, m2;
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);
            for (field_index_inner = 0;
                    field_index_inner < inter.num_fields;
                    field_index_inner++) {
                get_tem_modes_from_field_index(&n2, &m2, field_index_inner);

                fprintf(fp, " (%d, %d) %d %d -> %d %d k11=%s, ",
                        field_index_outer, field_index_inner, n1, m1, n2, m2,
                        complex_form15(mirror.knm.k11[field_index_outer][field_index_inner]));
                fprintf(fp, " k12=%s, ",
                        complex_form15(mirror.knm.k12[field_index_outer][field_index_inner]));
                fprintf(fp, " k21=%s, ",
                        complex_form15(mirror.knm.k21[field_index_outer][field_index_inner]));
                fprintf(fp, " k22=%s\n",
                        complex_form15(mirror.knm.k22[field_index_outer][field_index_inner]));

                k11_sum += zabs(mirror.knm.k11[field_index_outer][field_index_inner]);
                k12_sum += zabs(mirror.knm.k12[field_index_outer][field_index_inner]);
                k21_sum += zabs(mirror.knm.k21[field_index_outer][field_index_inner]);
                k22_sum += zabs(mirror.knm.k22[field_index_outer][field_index_inner]);
            }
            fprintf(fp, "   sum11 %s sum12 %s sum21 %s sum22 %s\n",
                    double_form(k11_sum), double_form(k12_sum),
                    double_form(k21_sum), double_form(k22_sum));
        }
    }

    // \todo this should probably be its own routine
    int beamsplitter_index;
    for (beamsplitter_index = 0;
            beamsplitter_index < inter.num_beamsplitters;
            beamsplitter_index++) {
        bs = inter.bs_list[beamsplitter_index];
        fprintf(fp, "beamsplitter %s:\n", bs.name);
        int field_index_outer;
        int n1, m1, n2, m2;
        for (field_index_outer = 0;
                field_index_outer < inter.num_fields;
                field_index_outer++) {
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);
            int field_index_inner;
            for (field_index_inner = 0;
                    field_index_inner < inter.num_fields;
                    field_index_inner++) {
                get_tem_modes_from_field_index(&n2, &m2, field_index_inner);
                fprintf(fp, " (%d, %d) %d %d -> %d %d k12=%s, ",
                        field_index_outer, field_index_inner, n1, m1, n2, m2,
                        complex_form15(bs.knm.k12[field_index_outer][field_index_inner]));
                fprintf(fp, " k21=%s, ",
                        complex_form15(bs.knm.k21[field_index_outer][field_index_inner]));
                fprintf(fp, " k13=%s, ",
                        complex_form15(bs.knm.k13[field_index_outer][field_index_inner]));
                fprintf(fp, " k31=%s, ",
                        complex_form15(bs.knm.k31[field_index_outer][field_index_inner]));
                fprintf(fp, " k24=%s, ",
                        complex_form15(bs.knm.k24[field_index_outer][field_index_inner]));
                fprintf(fp, " k42=%s, ",
                        complex_form15(bs.knm.k42[field_index_outer][field_index_inner]));
                fprintf(fp, " k34=%s, ",
                        complex_form15(bs.knm.k34[field_index_outer][field_index_inner]));
                fprintf(fp, " k43=%s\n",
                        complex_form15(bs.knm.k43[field_index_outer][field_index_inner]));
            }
        }
    }

    // \todo this should probably be its own routine
    int space_index;
    for (space_index = 0; space_index < inter.num_spaces; space_index++) {
        space = inter.space_list[space_index];
        fprintf(fp, "space %s:\n", space.name);

        int n1, m1, n2, m2;
        int field_index_outer;
        for (field_index_outer = 0;
                field_index_outer < inter.num_fields;
                field_index_outer++) {
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);
            int field_index_inner;
            for (field_index_inner = 0;
                    field_index_inner < inter.num_fields;
                    field_index_inner++) {
                get_tem_modes_from_field_index(&n2, &m2, field_index_inner);
                fprintf(fp, " (%d, %d) %d %d -> %d %d k12=%s, ",
                        field_index_outer, field_index_inner, n1, m1, n2, m2,
                        complex_form15(space.k12[field_index_outer][field_index_inner]));
                fprintf(fp, " k21=%s\n",
                        complex_form15(space.k21[field_index_outer][field_index_inner]));
            }
        }
    }

    // \todo this should probably be its own routine
    int lens_index;
    for (lens_index = 0; lens_index < inter.num_lenses; lens_index++) {
        lens = inter.lens_list[lens_index];
        fprintf(fp, "lens %s:\n", lens.name);
        int n1, m1, n2, m2;
        int field_index_outer;
        for (field_index_outer = 0;
                field_index_outer < inter.num_fields;
                field_index_outer++) {
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);
            int field_index_inner;
            for (field_index_inner = 0;
                    field_index_inner < inter.num_fields;
                    field_index_inner++) {
                get_tem_modes_from_field_index(&n2, &m2, field_index_inner);
                fprintf(fp, " (%d, %d) %d %d -> %d %d k12=%s, ",
                        field_index_outer, field_index_inner, n1, m1, n2, m2,
                        complex_form15(lens.k12[field_index_outer][field_index_inner]));
                fprintf(fp, " k21=%s\n",
                        complex_form15(lens.k21[field_index_outer][field_index_inner]));
            }
        }
    }

    // \todo this should probably be its own routine
    int modulator_index;
    for (modulator_index = 0;
            modulator_index < inter.num_modulators;
            modulator_index++) {
        modulator = inter.modulator_list[modulator_index];
        fprintf(fp, "modulator %s:\n", modulator.name);
        int n1, m1, n2, m2;
        int field_index_outer;
        for (field_index_outer = 0;
                field_index_outer < inter.num_fields;
                field_index_outer++) {
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);
            int field_index_inner;
            for (field_index_inner = 0;
                    field_index_inner < inter.num_fields;
                    field_index_inner++) {
                get_tem_modes_from_field_index(&n2, &m2, field_index_inner);
                fprintf(fp, " (%d, %d) %d %d -> %d %d k12=%s, ",
                        field_index_outer, field_index_inner, n1, m1, n2, m2,
                        complex_form15(modulator.k12[field_index_outer][field_index_inner]));
                fprintf(fp, " k21=%s\n",
                        complex_form15(modulator.k21[field_index_outer][field_index_inner]));
            }
        }
    }

    // \todo this should probably be its own routine
    int diode_index;
    for (diode_index = 0; diode_index < inter.num_diodes; diode_index++) {
        diode = inter.diode_list[diode_index];
        fprintf(fp, "diode %s:\n", diode.name);
        int n1, m1, n2, m2;
        int field_index_outer;
        for (field_index_outer = 0;
                field_index_outer < inter.num_fields;
                field_index_outer++) {
            get_tem_modes_from_field_index(&n1, &m1, field_index_outer);
            int field_index_inner;
            for (field_index_inner = 0;
                    field_index_inner < inter.num_fields;
                    field_index_inner++) {
                get_tem_modes_from_field_index(&n2, &m2, field_index_inner);
                fprintf(fp, " (%d, %d) %d %d -> %d %d k12=%s, ",
                        field_index_outer, field_index_inner, n1, m1, n2, m2,
                        complex_form15(diode.k12[field_index_outer][field_index_inner]));
                fprintf(fp, " k21=%s\n",
                        complex_form15(diode.k21[field_index_outer][field_index_inner]));
            }
        }
    }
}

//! Dump beam shape information to file

/*!
 * \param fp the file pointer
 * \param order ???
 * \param coeff ???
 * \param qx Gaussian beam parameter in x-plane
 * \param qy Gaussian beam parameter in y-plane
 * \param nr refractive index
 * \param x_range_width width of range of x values
 * \param y_range_width width of range of y values
 * \param num_x_steps ???
 * \param num_y_steps ???
 *
 * \todo refactor for readability
 * \todo check inputs with asserts
 *
 * \todo untested
 */
void dump_beamshape(FILE *fp, int order, complex_t **coeff,
        complex_t qx, complex_t qy, double nr,
        double x_range_width, double y_range_width,
        int num_x_steps, int num_y_steps) {
    double x_start_point = -x_range_width / 2.0;
    double y_start_point = -y_range_width / 2.0;

    double x_step = x_range_width / num_x_steps;
    double y_step = y_range_width / num_y_steps;
    
    double x_point = x_start_point;
    int x_step_index;
    for (x_step_index = 0; x_step_index < num_x_steps; x_step_index++) {
        double y_point = y_start_point;
        int y_step_index;
        for (y_step_index = 0; y_step_index < num_y_steps; y_step_index++) {
            complex_t z1 = complex_0;
            int k, l;
            for (k = 0; k <= order; k++) {
                for (l = 0; l <= order; l++) {
                    z1 = z_pl_z(z1, z_by_z(u_nm(k, l, qx, qy, x_point, y_point, nr), coeff[k][l]));
                }
            }
            fprintf(fp, "% #.12g % #.12g % #.12g\n", x_point, y_point, zabs(z1));
            y_point += y_step;
        }
        fprintf(fp, " \n");
        x_point += x_step;
    }
}

//! Dump the powers for all nodes (both beam directions

/*!
 * \param fp the file pointer
 *
 */
void dump_powers(FILE *fp, ifo_matrix_vars_t *matrix) {
    int component_index1, component_index2;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "---- printing power in all nodes ----------------------------------------------------\n");

    // allocate memory for carrier fields for all freqs. and all modes
    complex_t **a_c;
    a_c = (complex_t**) calloc(inter.num_fields + 1, sizeof (complex_t*));
    assert(a_c != NULL);
    int mode;
    for (mode = 0; mode < inter.num_fields; mode++) {
        a_c[mode] = (complex_t*) calloc(inter.num_frequencies + 1, sizeof (complex_t));
        assert(a_c[mode] != NULL);
    }

    int alternate = 0;
    int i;
    fprintf(fp, "idx: nodename; component1, component2 . . . . . . . . . . . .  power -->,   power <--\n");
    for (i = 0; i < inter.num_nodes; i++) {
        if (NOT inter.node_list[i].gnd_node) {
            which_components(i, &component_index1, &component_index2);

            if (inter.node_list[i].direction == OUT_IN && (component_index1 > 0)&& (component_index2 > 0)) {
                int tmp_idx = component_index1;
                component_index1 = component_index2;
                component_index2 = tmp_idx;
            }
            fprintf(fp, "%3d: node %s; %s, %s ",
                    i, node_print(i), get_component_name(component_index1), get_component_name(component_index2));
            int ii, dashed = 3 * 15 + 1 - strlen(get_component_name(component_index1)) - strlen(get_component_name(component_index2)) - strlen(node_print(i));

            if (dashed % 2) {
                fprintf(fp, " ");
                dashed--;
            }

            if (alternate == 0) {
                for (ii = 0; ii < dashed / 2; ii++) {
                    fprintf(fp, "- ");
                }

                alternate = 1;
            } else {
                for (ii = 0; ii < dashed / 2; ii++) {
                    fprintf(fp, "--");
                }
                alternate = 0;
            }
            int port;
            for (port = 0; port < 2; port++) {
                // store carrier solutions
                int field_index;
                for (field_index = 0; field_index < inter.num_fields; field_index++) {
                    // store the carrier frequency results
                    int f;
                    for (f = 0; f < inter.num_frequencies; f++) {
                        int oidx = get_rhs_idx(matrix, &inter.node_list[i], port + 1, f, field_index);
                        a_c[field_index][f] = (((complex_t*) M_ifo_car.rhs_values)[oidx]);
                    }
                }

                // ------------------------------------------------
                complex_t z_tmp, z_tmp2 = complex_0;
                
                // loop over all fields
                for (field_index = 0; field_index < inter.num_fields; field_index++) {
                    int n, m;
                    get_tem_modes_from_field_index(&n, &m, field_index);
                    if (!(inter.powers & 4) || (n == 0 && m == 0)) {
                        // initialise the temporary summation variable
                        z_tmp = complex_0;
                        // loop over all frequencies
                        int freq_index_outer;
                        for (freq_index_outer = 0;
                                freq_index_outer < inter.num_frequencies;
                                freq_index_outer++) {

                            // if the type of signal is not a signal sideband continue
                            if (t_s[freq_index_outer] != SIGNAL_FIELD) {
                                // loop over all frequencies
                                int freq_index_inner;
                                for (freq_index_inner = 0;
                                        freq_index_inner < inter.num_frequencies;
                                        freq_index_inner++) {
                                    if (!(inter.powers & 2) || f_s[freq_index_inner] == 0.0) {

                                        // if the current type of signal is not a signal sideband and
                                        // the frequencies of the amplitudes to multiply are the same
                                        // continue
                                        if (t_s[freq_index_inner] != SIGNAL_FIELD &&
                                                eq(f_s[freq_index_outer], f_s[freq_index_inner])) {

                                            // output debugging information about a_i a_j^*
                                            if (inter.debug & 8) {
                                                message("dump [%d, %d]=%s\n",
                                                        freq_index_outer, freq_index_inner,
                                                        complex_form15(z_by_zc(a_c[field_index][freq_index_outer],
                                                        a_c[field_index][freq_index_inner])));
                                            }

                                            // \f$ A_i = \sum_j a_i a_j^* \f$
                                            z_tmp = z_pl_z(z_tmp, z_by_zc(a_c[field_index][freq_index_outer],
                                                    a_c[field_index][freq_index_inner]));
                                        }
                                    }
                                }
                            } // end inner frequency sum
                        }
                    } // end outer frequency sum
                    
                    // E = \sum_n \sum_i c_n \sum_j a_i a_j^* 
                    z_tmp2 = z_pl_z(z_tmp2, z_tmp);
                } // fields
                z_tmp2 = z_by_x(z_tmp2, 0.5 * init.epsilon_0_c); // Watt Ihr Volt !!!!!!!!!!!

                fprintf(fp, "%11.6g", z_tmp2.re);

                if (port == 0)
                    fprintf(fp, ", ");
            }
            fprintf(fp, "\n");
        }
    }
    fprintf(fp, "-------------------------------------------------------------------------------------\n");
}


//! Dump the trace of the beam through the optical system

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_trace(FILE *fp) {
    int n;
    double nr;

    gauss_t g;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    if(!options.trace_output && fp != stdout) {
        fprintf(fp, "--- tracing the beam through optical system, \n");
        fprintf(fp, " found %d of %d nodes:\n", n_index, inter.num_nodes);
        fprintf(fp, " (w0 : waist radius, w : beam radius, z: distance to waist, \n");
        fprintf(fp, "  z_R: Rayleigh range, q : complex beam parameter\n");
        fprintf(fp, "  and gamma: far field angle\n");
    }
    
    int i;
    for (i = 0; i < n_index; i++) {
        n = vlocal.trace_n[0][i];
        g.qx = inter.node_list[n].qx;
        g.qy = inter.node_list[n].qy;
        nr = *inter.node_list[n].n;
        fprintf(fp, "%d: node %s(%d); %s(%d), %s(%d); n=%.10g  (%s --> %s)\n",
                i, node_print(n), n, get_component_name(vlocal.trace_n[1][i]),
                vlocal.trace_n[1][i], get_component_name(vlocal.trace_n[2][i]),
                vlocal.trace_n[2][i], nr,
                get_component_name(inter.node_list[n].component_index), node_print(n));

        if (!options.trace_output && zabs(z_m_z(g.qx, g.qy)) < init.eqsmall) {
            fprintf(fp,
                    "   x, y: w0=%sm w=%sm z=%sm z_R=%sm Rc=%sm\n   q=%s gamma=%srad\n",
                    xdouble_form(w0_size(g.qx, nr)), xdouble_form(w_size(g.qx, nr)),
                    xdouble_form(z_q(g.qx)), xdouble_form(z_r(g.qx)),
                    xdouble_form(roc(g.qx)), complex_form15(g.qx),
                    xdouble_form(w0_size(g.qx, nr) / z_r(g.qx)));
        } else {
            fprintf(fp,
                    "   x: w0=%sm  w=%sm z=%sm z_R=%sm Rc=%sm\n   q=%s gamma=%srad\n",
                    xdouble_form(w0_size(g.qx, nr)), xdouble_form(w_size(g.qx, nr)),
                    xdouble_form(z_q(g.qx)), xdouble_form(z_r(g.qx)),
                    xdouble_form(roc(g.qx)), complex_form15(g.qx),
                    xdouble_form(w0_size(g.qx, nr) / z_r(g.qx)));
            fprintf(fp,
                    "   y: w0=%sm  w=%sm z=%sm z_R=%sm Rc=%sm\n   q=%s gamma=%srad\n\n",
                    xdouble_form(w0_size(g.qy, nr)), xdouble_form(w_size(g.qy, nr)),
                    xdouble_form(z_q(g.qy)), xdouble_form(z_r(g.qy)),
                    xdouble_form(roc(g.qy)), complex_form15(g.qy),
                    xdouble_form(w0_size(g.qy, nr) / z_r(g.qy)));
        }
    }
    fprintf(fp, "\n");
}

//! Dump the initialisation variables to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_init(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (init.init_file_is_found == ENV_KATINI) {
        fprintf(fp, "Using init file %s\n", init.katinifile);
    } else if (init.init_file_is_found == DEFAULT_KATINI) {
        fprintf(fp, "Using init file ./%s\n", INIT_FILE);
    } else {
        fprintf(fp, "Using internal defaults \n");
    }

    fprintf(fp, "xscale : %.10g\n", init.x_scale);
    fprintf(fp, "lambda : %.10g\n", init.lambda);
    fprintf(fp, "clight : %.10g\n", init.clight);
    fprintf(fp, "deriv_h : %.10g\n", init.deriv_h);
    fprintf(fp, "qeff : %.10g\n", init.qeff);
    fprintf(fp, "epsilon_c : %.10g\n", init.epsilon_0_c);
    fprintf(fp, "locksteps: %d\n", init.locksteps);
    fprintf(fp, "locktest1: %d\n", init.locktest1);
    fprintf(fp, "locktest2: %d\n", init.locktest2);
    fprintf(fp, "lockthresholdlow: %.10g\n", init.lockthresholdlow);
    fprintf(fp, "lockthresholdhigh: %.10g\n", init.lockthresholdhigh);
    fprintf(fp, "abserr: %.10g\n", init.abserr);
    fprintf(fp, "relerr: %.10g\n", init.relerr);
    fprintf(fp, "maxintcuba: %d\n", init.maxintcuba);
    fprintf(fp, "gainfactor: %.10g\n", init.gainfactor);
    fprintf(fp, "autogain: %d\n", init.autogain);
    fprintf(fp, "cores found: %d\n", init.cuba_numprocs);
}

//! Dump the mirror information to file

/*!
 * \param fp the file pointer
 * \param mirror the mirror whose information is to be dumped to file
 *
 * \todo refactor for readability
 *
 * \todo untested
 */
void dump_mirror(FILE *fp, mirror_t mirror) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "Mirror : %s\n", mirror.name);
    fprintf(fp, "reflectivity (r)  : %.15g\n", mirror.R);
    fprintf(fp, "transmittance (t) : %.15g\n", mirror.T);
    fprintf(fp, "tuning (phi)      : %.15g\n", mirror.phi);

    if (mirror.attribs & XROC && mirror.attribs & YROC) {
        if (eq(mirror.Rcx, mirror.Rcy)) {
            fprintf(fp, "curvature (ROC)   : %.15g\n", mirror.Rcx);
        } else {
            fprintf(fp, "curvature x (ROC) : %.15g\n", mirror.Rcx);
            fprintf(fp, "curvature y (ROC) : %.15g\n", mirror.Rcy);
        }
    } else {
        if (mirror.attribs & XROC) {
            fprintf(fp, "curvature x (ROC) : %.15g\n", mirror.Rcx);
        }

        if (mirror.attribs & XROC) {
            fprintf(fp, "curvature y (ROC) : %.15g\n", mirror.Rcy);
        }
    }

    if (mirror.attribs & MASS) {
        fprintf(fp, "mass (M)          : %.15g\n", mirror.mass);
    }

    if (mirror.attribs & XANGLE) {
        fprintf(fp, "angle (xbeta)     : %.15g\n", mirror.beta_x);
    }

    if (mirror.attribs & YANGLE) {
        fprintf(fp, "angle (ybeta)     : %.15g\n", mirror.beta_y);
    }

    fprintf(fp, "node1 index       : %d\n", mirror.node1_index);
    fprintf(fp, "node2 index       : %d\n", mirror.node2_index);

    int i;
    surface_map_t *map;
    for (i = 0; i < mirror.num_maps; i++) {
        map = mirror.map[i];
        fprintf(fp, "Map (%d):\n", i);
        fprintf(fp, "  name: %s\n", map->name);
        if (map->function) {
            fprintf(fp, "  from function\n");
        } else {
            fprintf(fp, "  filename: %s\n", map->filename);
        }
        switch (map->type) {
            case PHASE_MAP:
                fprintf(fp, "  phase, ");
                break;
            case ABSORPTION_MAP:
                fprintf(fp, "  absorption, ");
                break;
            case REFLECTIVITY_MAP:
                fprintf(fp, "  reflectivity, ");
                break;
            default:
                bug_error("wrong map type");
        }
        if (map->reflection && map->transmission) {
            fprintf(fp, "both\n");
        } else if (map->reflection) {
            fprintf(fp, "reflection\n");
        } else {
            fprintf(fp, "transmission\n");
        }

        fprintf(fp, "  size %d x %d\n", map->cols, map->rows);
        fprintf(fp, "  center %.1g x %.1g\n", map->x0, map->y0);
        fprintf(fp, "  stepsize %g x %g\n", map->xstep, map->ystep);
        fprintf(fp, "  scaling %g\n", map->scaling);
        ;
    }
}

//! Dump the free space information to file

/*!
 * \param fp the file pointer
 * \param space the free space whose information is to be dumped to file
 *
 * \todo refactor for readability
 *
 * \todo untested
 */
void dump_space(FILE *fp, space_t space) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "Space : %s\n", space.name);
    fprintf(fp, "length (L)        : %.15g\n", space.L);
    fprintf(fp, "node1 index       : %d\n", space.node1_index);
    fprintf(fp, "node2 index       : %d\n", space.node2_index);

    if (space.attribs & GOUYX) {
        fprintf(fp, "Gouy phase x : %.15g\n", space.gouy_x);
    }

    if (space.attribs & GOUYY) {
        fprintf(fp, "Gouy phase y : %.15g\n", space.gouy_y);
    }
}

//! Dump the free sagnac information to file

/*!
 * \param fp the file pointer
 * \param space the free sagnac whose information is to be dumped to file
 *
 * \todo refactor for readability
 *
 * \todo untested
 */
void dump_sagnac(FILE *fp, sagnac_t sagnac) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "Sagnac : %s\n", sagnac.name);
    fprintf(fp, "phase (rad        : %.15g\n", sagnac.dphi);
    fprintf(fp, "node1 index       : %d\n", sagnac.node1_index);
    fprintf(fp, "node2 index       : %d\n", sagnac.node2_index);
}

//! Dump the diode information to file

/*!
 * \param fp the file pointer
 * \param diode the diode whose information is to be dumped to file
 *
 * \todo refactor for readability
 *
 * \todo untested
 */
void dump_diode(FILE *fp, diode_t diode) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "Isolator : %s\n", diode.name);
    fprintf(fp, "suppression       : %.15g\n", diode.S);
    fprintf(fp, "node1 index       : %d\n", diode.node1_index);
    fprintf(fp, "node2 index       : %d\n", diode.node2_index);
}

//! Dump the beam splitter information to file

/*!
 * \param fp the file pointer
 * \param bs the beam splitter whose information is to be dumped to file
 *
 * \todo refactor for readability
 *
 * \todo untested
 */
void dump_beamsplitter(FILE *fp, beamsplitter_t *bs) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "Beamsplitter : %s\n", bs->name);
    fprintf(fp, "reflectivity  (R) : %.15g\n", bs->R);
    fprintf(fp, "transmittance (T) : %.15g\n", bs->T);
    fprintf(fp, "tuning (phi)      : %.15g\n", bs->phi);

    if (bs->attribs & XROC && bs->attribs & YROC) {
        if (eq(bs->Rcx, bs->Rcy)) {
            fprintf(fp, "curvature (ROC)   : %.15g\n", bs->Rcx);
        } else {
            fprintf(fp, "curvature x (ROC) : %.15g\n", bs->Rcx);
            fprintf(fp, "curvature y (ROC) : %.15g\n", bs->Rcy);
        }
    } else {
        if (bs->attribs & XROC) {
            fprintf(fp, "curvature x (ROC) : %.15g\n", bs->Rcx);
        }

        if (bs->attribs & XROC) {
            fprintf(fp, "curvature y (ROC) : %.15g\n", bs->Rcy);
        }
    }

    if (bs->attribs & MASS) {
        fprintf(fp, "mass (M)          : %.15g\n", bs->mass);
    }

    if (bs->attribs & XANGLE) {
        fprintf(fp, "angle (xbeta)     : %.15g\n", bs->beta_x);
    }

    if (bs->attribs & YANGLE) {
        fprintf(fp, "angle (ybeta)     : %.15g\n", bs->beta_y);
    }

    fprintf(fp, "incidence (alpha) : %.15g\n", bs->alpha_1);
    fprintf(fp, "node1             : %d\n", bs->node1_index);
    fprintf(fp, "node2             : %d\n", bs->node2_index);
    fprintf(fp, "node3             : %d\n", bs->node3_index);
    fprintf(fp, "node4             : %d\n", bs->node4_index);
}

//! Dump the cavity information to file for all cavities

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_cavities(FILE *fp) {
    cavity_t cavity;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_cavities) {
        fprintf(fp, "%d cavit%s:\n",
                inter.num_cavities, inter.num_cavities == 1 ? "y" : "ies");
        int i;
        for (i = 0; i < inter.num_cavities; i++) {
            cavity = inter.cavity_list[i];
            fprintf(fp, "  %d %s: %s %s --> %s %s\n",
                    i, cavity.name,
                    get_component_name(cavity.component1_index),
                    get_node_name(cavity.node1_index),
                    get_component_name(cavity.component2_index),
                    get_node_name(cavity.node2_index));
        }
        fprintf(fp, "\n");
    }
}

//! Dump the information for a given cavity to file

/*!
 * \param fp the file pointer
 * \param cavity the cavity whose information is to be dumped to file
 *
 * \todo refactor for readability
 *
 * \todo untested
 */
void dump_cavity(FILE *fp, cavity_t cavity) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "Cavity : %s\n"
            "component1   : %s\n"
            "node1 index  : %s\n"
            "component2   : %s\n"
            "node2 index  : %s\n",
            cavity.name,
            get_component_name(cavity.component1_index),
            get_node_name(cavity.node1_index),
            get_component_name(cavity.component2_index),
            get_node_name(cavity.node2_index));
}

//! Dump the ABCD matrix information to file

/*!
 * \param fp the file pointer
 * \param s the ABCD matrix to dump to file
 *
 * \todo refactor for readability
 */
void dump_ABCD(FILE *fp, ABCD_t s) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, " %.15g          %.15g \n", s.A, s.B);
    fprintf(fp, " %.15g          %.15g \n", s.C, s.D);
}

//! Dump information for all gratings to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_gratings(FILE *fp) {
    grating_t grating;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_gratings > 0) {
        fprintf(fp, "%d grating%s:\n", inter.num_gratings,
                inter.num_gratings == 1 ? "" : "s");

        int i, j;
        for (i = 0; i < inter.num_gratings; i++) {
            grating = inter.grating_list[i];
            fprintf(fp, "  %d %s %s %s %s", i, grating.name, long_form(grating.d, 0),
                    node_print(grating.node1_index), node_print(grating.node2_index));

            if (grating.num_of_ports > 2) {
                fprintf(fp, " %s", node_print(grating.node3_index));
                if (grating.num_of_ports > 3) {
                    fprintf(fp, " %s", node_print(grating.node4_index));
                } else {
                    fprintf(fp, "\n");
                }
            } else {
                fprintf(fp, "\n");
            }

            fprintf(fp, "   dmin=%slambda, dmax=%slambda, alpha=%s,\n   ",
                    long_form(grating.dmin, 0),
                    long_form(grating.dmax, 0), long_form(grating.alpha, 0));

            switch (grating.num_of_ports) {
                case 2:
                    for (j = 0; j < 2; j++) {
                        fprintf(fp, "%seta%d=%s", j == 0 ? "" : ", ", j,
                                long_form(grating.eta[j], 0));
                    }
                    break;
                case 3:
                    for (j = 0; j < 3; j++) {
                        fprintf(fp, "%seta%d=%s", j == 0 ? "" : ", ", j,
                                long_form(grating.eta[j], 0));
                    }
                    fprintf(fp, ", rho0=%s", long_form(grating.eta[3], 0));
                    break;
                case 4:
                    for (j = 0; j < 4; j++) {
                        fprintf(fp, "%seta%d=%s", j == 0 ? "" : ", ", j,
                                long_form(grating.eta[j], 0));
                    }
                    break;
            }

            if (inter.grating_list[i].attribs == 0) {
                fprintf(fp, "\n");
            } else {
                fprintf(fp, "\n   (");

                if (inter.grating_list[i].attribs & XROC) {
                    fprintf(fp, " Rcx=%.15g", inter.grating_list[i].Rcx);
                }

                if (inter.grating_list[i].attribs & YROC) {
                    fprintf(fp, " Rcy=%.15g", inter.grating_list[i].Rcy);
                }
                fprintf(fp, ")\n");
            }
        }
    }
}

//! Dump information for all mirrors to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_mirrors(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_mirrors > 0) {
        fprintf(fp, "%d mirror%s:\n",
                inter.num_mirrors, inter.num_mirrors == 1 ? "" : "s");

        fprintf(fp, "  name: r: t: phi: node1: node2:\n");
        int i;
        for (i = 0; i < inter.num_mirrors; i++) {
            mirror_t mirror = inter.mirror_list[i];
            fprintf(fp, "  %d %s %s %s %s %s %s", i,
                    mirror.name,
                    long_form(mirror.R, 0),
                    long_form(mirror.T, 0),
                    long_form(mirror.phi, 0),
                    node_print(mirror.node1_index),
                    node_print(mirror.node2_index));

            if (mirror.attribs == 0) {
                fprintf(fp, "\n");
            } else {
                fprintf(fp, " (");

                if (mirror.attribs & XROC) {
                    fprintf(fp, " Rcx=%.15g", mirror.Rcx);
                }

                if (mirror.attribs & YROC) {
                    fprintf(fp, " Rcy=%.15g", mirror.Rcy);
                }

                if (mirror.attribs & MASS) {
                    fprintf(fp, " M=%.15g", mirror.mass);
                }

                if (mirror.attribs & XANGLE) {
                    fprintf(fp, " xbeta=%.15g", mirror.beta_x);
                }

                if (mirror.attribs & YANGLE) {
                    fprintf(fp, " ybeta=%.15g", mirror.beta_y);
                }

                fprintf(fp, " )\n");
            }


            int i;
            surface_map_t *map;
            for (i = 0; i < mirror.num_maps; i++) {
                map = mirror.map[i];
                fprintf(fp, "Map (%d):\n", i);
                fprintf(fp, "  name: %s\n", map->name);
                if (map->function) {
                    fprintf(fp, "  from function\n");
                } else {
                    fprintf(fp, "  filename: %s\n", map->filename);
                }
                switch (map->type) {
                    case PHASE_MAP:
                        fprintf(fp, "  phase, ");
                        break;
                    case ABSORPTION_MAP:
                        fprintf(fp, "  absorption, ");
                        break;
                    case REFLECTIVITY_MAP:
                        fprintf(fp, "  reflectivity, ");
                        break;
                    default:
                        bug_error("wrong map type");
                }
                if (map->reflection && map->transmission) {
                    fprintf(fp, "both\n");
                } else if (map->reflection) {
                    fprintf(fp, "reflection\n");
                } else {
                    fprintf(fp, "transmission\n");
                }

                fprintf(fp, "  size %d x %d\n", map->cols, map->rows);
                fprintf(fp, "  center %.1g x %.1g\n", map->x0, map->y0);
                fprintf(fp, "  stepsize %g x %g\n", map->xstep, map->ystep);
                fprintf(fp, "  scaling %g\n", map->scaling);
            }
        }
    }
}

//! Dump information for all spaces to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_spaces(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_spaces > 0) {
        fprintf(fp, "%d space%s:\n",
                inter.num_spaces, inter.num_spaces == 1 ? "" : "s");

        fprintf(fp, "  index: name: length: nr: node1: node2:\n");
        int i;
        for (i = 0; i < inter.num_spaces; i++) {
            fprintf(fp, "  %d %s %s %s %s %s", i, inter.space_list[i].name,
                    long_form(inter.space_list[i].L, 0),
                    long_form(inter.space_list[i].n, 0),
                    node_print(inter.space_list[i].node1_index),
                    node_print(inter.space_list[i].node2_index));
            if (inter.space_list[i].attribs) {
                fprintf(fp, " (");

                if (inter.space_list[i].attribs & GOUYX) {
                    fprintf(fp, " gx=%s", long_form(inter.space_list[i].gouy_x, 0));
                }

                if (inter.space_list[i].attribs & GOUYX) {
                    fprintf(fp, " gy=%s", long_form(inter.space_list[i].gouy_y, 0));
                }

                fprintf(fp, ")");
            }
            fprintf(fp, "\n");
        }
    }
}

//! Dump information for all lenses to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_lenses(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_lenses > 0) {
        fprintf(fp, "%d lens%s:\n",
                inter.num_lenses, inter.num_lenses == 1 ? "" : "es");
        int i;
        for (i = 0; i < inter.num_lenses; i++) {
            fprintf(fp, "  %d %s %s %s %s\n", i, inter.lens_list[i].name,
                    long_form(inter.lens_list[i].f_of_D, 0),
                    node_print(inter.lens_list[i].node1_index),
                    node_print(inter.lens_list[i].node2_index));
        }
    }
}

//! Dump information for all diodes to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_diodes(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_diodes > 0) {
        fprintf(fp, "%d isolator%s:\n",
                inter.num_diodes, inter.num_diodes == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_diodes; i++) {
            fprintf(fp, "  %d %s %s %s %s\n", i, inter.diode_list[i].name,
                    long_form(inter.diode_list[i].S, 0),
                    node_print(inter.diode_list[i].node1_index),
                    node_print(inter.diode_list[i].node2_index));
        }
    }
}

//! Dump information for all put commands to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_put_cmds(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_put_cmds > 0) {
        fprintf(fp, "%d put%s:\n",
                inter.num_put_cmds, inter.num_put_cmds == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_put_cmds; i++) {
            fprintf(fp, "  %d %s -> %s %s, startvalue %g\n", i,
                    inter.put_list[i].function_name, inter.put_list[i].component_name,
                    inter.put_list[i].parameter_name, inter.put_list[i].startvalue);
        }
    }
}

//! Dump information for all set commands to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 *
 * \todo num_set_cmds never greater than zero in test suite: hence untested
 */
void dump_all_set_cmds(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_set_cmds > 0) {
        fprintf(fp, "%d set%s:\n",
                inter.num_set_cmds, inter.num_set_cmds == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_set_cmds; i++) {
            fprintf(fp, "  %d %s -> %s %s = %g\n", i, inter.set_list[i].name,
                    inter.set_list[i].component_name,
                    inter.set_list[i].parameter_name, *inter.set_list[i].value);
        }
    }
}

//! Dump information for all func commands to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_func_cmds(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_func_cmds > 0) {
        fprintf(fp, "%d function%s -- (order of evaluation) tokens:\n",
                inter.num_func_cmds, inter.num_func_cmds == 1 ? "" : "s");

        int i;
        for (i = 0; i < inter.num_func_cmds; i++) {
            fprintf(fp, "  %d %s -> %s -- (%d)", i, inter.function_list[i].name,
                    inter.function_list[i].fstring, inter.func_order[i]);
            if (inter.function_list[i].num_tokens) {
                fprintf(fp, " with \'%s\', defined as:\n",
                        inter.function_list[i].tokens);

                int j;
                for (j = 0; j < inter.function_list[i].num_tokens; j++) {
                    fprintf(fp, "      %c = ", *(inter.function_list[i].tokens + j));

                    if (inter.function_list[i].type[j] == XSET) {
                        fprintf(fp, "%s (set)\n",
                                inter.set_list[inter.function_list[i].token_number[j]].name);
                    }

                    if (inter.function_list[i].type[j] == FUNC) {
                        fprintf(fp, "%s (function)\n",
                                inter.function_list[inter.function_list[i].token_number[j]].name);
                    }

                    if (inter.function_list[i].type[j] == LOCK) {
                        fprintf(fp, "%s (function)\n",
                                inter.lock_list[inter.function_list[i].token_number[j]].name);
                    }

                    if (inter.function_list[i].type[j] == MX1) {
                        fprintf(fp, "-1*xaxis\n");
                    }

                    if (inter.function_list[i].type[j] == MX2) {
                        fprintf(fp, "-1*x2axis\n");
                    }

                    if (inter.function_list[i].type[j] == MX3) {
                        fprintf(fp, "-1*x3axis\n");
                    }

                    if (inter.function_list[i].type[j] == X1) {
                        fprintf(fp, "xaxis\n");
                    }

                    if (inter.function_list[i].type[j] == X2) {
                        fprintf(fp, "x2axis\n");
                    }

                    if (inter.function_list[i].type[j] == X3) {
                        fprintf(fp, "x3axis\n");
                    }
                }
            } else {
                fprintf(fp, "\n");
            }
        }
    }
}

//! Dump the information for all lock commands to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 *
 * \todo num_locks never greater than zero in test suite: hence untested
 */
void dump_all_lock_cmds(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_locks > 0) {
        fprintf(fp, "%d lock%s -- (order of evaluation)\n", inter.num_locks,
                inter.num_locks == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_locks; i++) {
            fprintf(fp, "  %d %s (%d)", i, inter.lock_list[i].name,
                    inter.lock_order[i]);
            fprintf(fp, "\n");
        }

        if (inter.showiterate) {
            fprintf(fp, "showiterate steps: %d\n", inter.showiterate);
        }
    }
}

//! Dump the information for all beamsplitters to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_beamsplitters(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_beamsplitters > 0) {
        fprintf(fp, "%d beamsplitter%s:\n",
                inter.num_beamsplitters, inter.num_beamsplitters == 1 ? "" : "s");

        fprintf(fp,
                "  index: name: R: T: L: phi: node1: node2: node3: node4: alpha:\n");
        int i;
        for (i = 0; i < inter.num_beamsplitters; i++) {
            fprintf(fp, "  %d %s %s %s %s %s %s %s %s", i,
                    inter.bs_list[i].name,
                    long_form(inter.bs_list[i].R, 0),
                    long_form(inter.bs_list[i].T, 0),
                    long_form(inter.bs_list[i].phi, 0),
                    node_print(inter.bs_list[i].node1_index),
                    node_print(inter.bs_list[i].node2_index),
                    node_print(inter.bs_list[i].node3_index),
                    node_print(inter.bs_list[i].node4_index));
            fprintf(fp, " alpha= %.15g", inter.bs_list[i].alpha_1);

            if (inter.bs_list[i].attribs == 0) {
                fprintf(fp, "\n");
            } else {
                fprintf(fp, " (");

                if (inter.bs_list[i].attribs & XROC) {
                    fprintf(fp, " Rcx= %.15g", inter.bs_list[i].Rcx);
                }

                if (inter.bs_list[i].attribs & YROC) {
                    fprintf(fp, " Rcy= %.15g", inter.bs_list[i].Rcy);
                }

                if (inter.bs_list[i].attribs & MASS) {
                    fprintf(fp, " M= %.15g", inter.bs_list[i].mass);
                }

                if (inter.bs_list[i].attribs & XANGLE) {
                    fprintf(fp, " xbeta= %.15g", inter.bs_list[i].beta_x);
                }

                if (inter.bs_list[i].attribs & YANGLE) {
                    fprintf(fp, " ybeta= %.15g", inter.bs_list[i].beta_y);
                }

                fprintf(fp, " )\n");
            }
        }
    }
}

//! Dump the information for all modulators to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_modulators(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_modulators > 0) {
        fprintf(fp, "%d modulation%s:\n",
                inter.num_modulators, inter.num_modulators == 1 ? "" : "s");

        fprintf(fp, "  index: name: freq: mod_index: order: type: node1: node2:\n");
        int i;
        for (i = 0; i < inter.num_modulators; i++) {
            fprintf(fp, "  %d %s %s %s %d", i,
                    inter.modulator_list[i].name,
                    long_form(inter.modulator_list[i].f_mod, 0),
                    long_form(inter.modulator_list[i].modulation_index, 0),
                    inter.modulator_list[i].order);

            if (inter.modulator_list[i].type == MODTYPE_AM) {
                fprintf(fp, " am ");
            } else if (inter.modulator_list[i].type == MODTYPE_PM) {
                fprintf(fp, " pm ");
            } else {
                fprintf(fp, " unknown modulation type ");
            }

            fprintf(fp, "%s %s\n", node_print(inter.modulator_list[i].node1_index),
                    node_print(inter.modulator_list[i].node2_index));
        }
    }
}

//! Dump information for all nodes to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_nodes(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_nodes > 0) {
        fprintf(fp, "%d node%s:\n",
                inter.num_nodes, inter.num_nodes == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_nodes; i++) {
            fprintf(fp, "  %d %s\n", i, inter.node_list[i].name);
        }
    }
}

//! Dump information for all gauss commands to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_gauss_cmds(FILE *fp) {
    gauss_t g;
    double nr;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_gauss_cmds > 0) {
        fprintf(fp, "%d gauss parameter%s set:\n", inter.num_gauss_cmds,
                inter.num_gauss_cmds == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_gauss_cmds; i++) {
            g = inter.gauss_list[i];
            nr = *inter.node_list[g.node_index].n;
            fprintf(fp, "  %d: %s (nr=%g)\n", i, inter.gauss_list[i].name, nr);

            if (zabs(z_m_z(g.qx, g.qy)) < init.eqsmall) {
                fprintf(fp, "   x:y w0=%sm z=%sm,  q=%s\n",
                        xdouble_form(w0_size(g.qx, nr)), xdouble_form(z_q(g.qx)),
                        complex_form(g.qx));
            } else {
                fprintf(fp, "   x: w0=%sm z=%sm,  q=%s\n",
                        xdouble_form(w0_size(g.qx, nr)), xdouble_form(z_q(g.qx)),
                        complex_form(g.qx));
                fprintf(fp, "   y: w0=%sm z=%sm,  q=%s\n",
                        xdouble_form(w0_size(g.qy, nr)), xdouble_form(z_q(g.qy)),
                        complex_form(g.qy));
            }
        }
    }
}

//! Dump the x-axis information to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_xaxis(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.x1.xtype == FLIN) {
        fprintf(fp,
                "x-axis : linear scale for %s from %s to %s in %d steps.\n",
                inter.x1.xname, double_form(inter.x1.xmin),
                double_form(inter.x1.xmax), inter.x1.xsteps);
    } else if (inter.x1.xtype == FLOG) {
        fprintf(fp,
                "x-axis : logarithmic scale for %s from %s to %s in %d steps.\n",
                inter.x1.xname, double_form(inter.x1.xmin),
                double_form(inter.x1.xmax), inter.x1.xsteps);
    } else {
        fprintf(fp, "Warning : No x-axis given\n");
    }

    if (inter.splot) {
        fprintf(fp, "second xaxis (surface plot) :");
        if (inter.x2.xtype == FLIN) {
            fprintf(fp,
                    "x2-axis : linear scale for %s from %s to %s in %d steps.\n",
                    inter.x2.xname, double_form(inter.x2.xmin),
                    double_form(inter.x2.xmax), inter.x2.xsteps);
        } else if (inter.x2.xtype == FLOG) {
            fprintf(fp,
                    "x2-axis : logarithmic scale for %s from %s to %s in %d steps.\n",
                    inter.x2.xname, double_form(inter.x2.xmin),
                    double_form(inter.x2.xmax), inter.x2.xsteps);
        } else {
            fprintf(fp, "Warning : No x2-axis given\n");
        }
    }
    if (inter.x3_axis_is_set) {
        fprintf(fp, "third xaxis (4D plot) :");
        if (inter.x3.xtype == FLIN) {
            fprintf(fp,
                    "x3-axis : linear scale for %s from %s to %s in %d steps.\n",
                    inter.x3.xname, double_form(inter.x3.xmin),
                    double_form(inter.x3.xmax), inter.x3.xsteps);
        } else if (inter.x3.xtype == FLOG) {
            fprintf(fp,
                    "x3-axis : logarithmic scale for %s from %s to %s in %d steps.\n",
                    inter.x3.xname, double_form(inter.x3.xmin),
                    double_form(inter.x3.xmax), inter.x3.xsteps);
        }
    }
}

//! Dump the differentiation information to file

/*!
 * \param fp the file pointer
 */
void dump_deriv(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_diff_cmds) {
        fprintf(fp, "Differentiations:\n");
    }

    int deriv_index;
    for (deriv_index = 0; deriv_index < inter.num_diff_cmds; deriv_index++) {
        fprintf(fp, "%d: differentiate by %s ",
                deriv_index, inter.deriv_list[deriv_index].name);
        fprintf(fp, "\n");
    }
}

//! Dump the y-axis information to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 *
 * \todo mostly untested
 */
void dump_yaxis(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.yaxis[0]) {
        if (inter.ytype == FLIN) {
            fprintf(fp, "y1-axis : linear scale, %s\n", inter.y1name);
        } else if (inter.ytype == FLOG) {
            fprintf(fp, "y1-axis : logarithmic scale, %s\n", inter.y1name);
        } else {
            fprintf(fp, "Warning : No proper y1-axis given\n");
        }
    } else {
        fprintf(fp, "Warning : No y1-axis given\n");
    }

    if (inter.yaxis[1]) {
        if (inter.ytype == FLIN) {
            fprintf(fp, "y2-axis : linear scale, %s\n", inter.y2name);
        } else if (inter.ytype == FLOG) {
            fprintf(fp, "y2-axis : logarithmic scale, %s\n", inter.y2name);
        } else {
            fprintf(fp, "Warning : No proper y2-axis given\n");
        }
    } else
        fprintf(fp, "No y2-axis given\n");
}

//! Dump the information for all light inputs to file

/*!
 * \param fp the file pointer
 */
void dump_all_light_inputs(FILE *fp) {

    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_light_inputs > 0) {
        fprintf(fp, "%d input%s:\n",
                inter.num_light_inputs,
                inter.num_light_inputs == 1 ? "" : "s");
        int light_input_index;
        for (light_input_index = 0;
                light_input_index < inter.num_light_inputs;
                light_input_index++) {
            light_in_t *laser;
            laser = &inter.light_in_list[light_input_index];

            fprintf(fp, "  %d %s %s %s %s %s\n", light_input_index,
                    laser->name,
                    long_form(laser->I0, 0),
                    long_form(laser->f->f, 0),
                    long_form(laser->phase, 0),
                    node_print(laser->node_index));

            if (inter.num_fields > 0) {
                fprintf(fp, "  input light modes (with non-zero power):\n");
                int field_index;
                for (field_index = 0; field_index < inter.num_fields; field_index++) {
                    int n, m;
                    get_tem_modes_from_field_index(&n, &m, field_index);
                    complex_t amplitude =
                            z_by_ph(co(sqrt(laser->I0), 0.0), laser->phase);
                    amplitude =
                            z_by_z(amplitude, laser->power_coeff_list[field_index]);

                    if (zabs(amplitude) != 0) {
                        fprintf(fp, " TEM_(%d, %d) = %sW (phase = %s deg)\n",
                                n, m, double_form(zabs(amplitude)),
                                double_form(laser->phase));
                    }
                }
            }

        }
    }
}

//! Dump the information for all light outputs to file

/*!
 * \param fp the file pointer
 *
 * \todo BP* branches untested
 */
void dump_all_light_outputs(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_outputs > 0) {
        fprintf(fp, "%d output%s:\n",
                inter.num_outputs, inter.num_outputs == 1 ? "" : "s");
        int output_index;
        for (output_index = 0; output_index < inter.num_outputs; output_index++) {
            output_data_t out = inter.output_data_list[output_index];
            fprintf(fp, "  %d %s:", output_index, out.name);
            fprintf(fp, " type=%d ", out.detector_type);
            fprintf(fp, " node index%s ", node_print(out.node_index));
            if (out.user_defined_scale != 1.0)
                fprintf(fp, "  (rescaling output by %s)\n",
                    xdouble_form(out.user_defined_scale));

            int detector_index = out.detector_index;
            int detector_type = out.detector_type;
            if (detector_type == PD0 ||
                    detector_type == PD1 ||
                    detector_type == SHOT) {
                light_out_t light_output = inter.light_out_list[detector_index];

                int demod_index;
                for (demod_index = 1;
                        demod_index <= light_output.num_demods;
                        demod_index++) {
                    fprintf(fp, " f%d %s",
                            demod_index, freq_form(light_output.f[demod_index]));
                    if (detector_type != AD &&
                            out.output_type != COMPLEX) {
                        if (light_output.demod_phase_mode[demod_index] == MAX_PHASE) {
                            message(" ph%d max", demod_index);
                        } else {
                            message(" ph%d %s",
                                    demod_index, double_form(light_output.phi[demod_index]));
                        }
                    }
                }
                fprintf(fp, " %s\n",
                        light_output.sensitivity == ON ? "(sensitivity)" : "");
            } else if (detector_type == BP) {
                beampar_out_t bpout = inter.beampar_out_list[detector_index];
                fprintf(fp, " parameter= ");
                switch (bpout.action) {
                    case BPW0:
                        fprintf(fp, "w0");
                        break;
                    case BPW:
                        fprintf(fp, "w");
                        break;
                    case BPG:
                        fprintf(fp, "g");
                        break;
                    case BPZ:
                        fprintf(fp, "z");
                        break;
                    case BPZR:
                        fprintf(fp, "zr");
                        break;
                    case BPRC:
                        fprintf(fp, "r");
                        break;
                    case BPQ:
                        fprintf(fp, "q");
                        break;
                    default:
                        bug_error("dump outputs bp1");
                }
                fprintf(fp, " (%s)\n", bpout.x ? "x" : "y");
            } else if (detector_type == PG) {
                beampar_out_t bpout = inter.beampar_out_list[detector_index];
                fprintf(fp, " (%s) ", bpout.x ? "x" : "y");
                fprintf(fp, " space-list=");
                int space_index;
                for (space_index = 0; space_index < bpout.num_spaces; space_index++) {
                    fprintf(fp, " %s",
                            inter.space_list[bpout.space_list[space_index]].name);
                }
                fprintf(fp, "\n");
            } else if (detector_type == FEEDBACK) {
                lock_command_t tlock = inter.lock_list[detector_index];
                fprintf(fp, " input: %s, gain: %g, accuracy: %g \n",
                        tlock.iname, tlock.gain, tlock.accuracy);
            } else if (detector_type == UFUNCTION) {
                fprintf(fp, " (function)\n");
            }
        }
    }
}

//! Dump the information for all gnuplot commands to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 *
 * \todo main else branch untested
 */
void dump_all_gnuplot_cmds(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);


    fprintf(fp, " Default plotting : ");
    if (init.plotting == PLOT_GNUPLOT) {
        fprintf(fp, " Gnuplot\n");
    } else if (init.plotting == PLOT_PYTHON) {
        fprintf(fp, " Python\n");
    }

    if (inter.num_gnuterm_cmds == NO_GNUTERM) {
        fprintf(fp, " Gnuplot will not be started!\n");
    } else {
        fprintf(fp, "%d GNUPLOT terminal%s:\n", inter.num_gnuterm_cmds,
                inter.num_gnuterm_cmds == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_gnuterm_cmds; i++) {
            fprintf(fp, " no.%d (kat.ini no.%d) file=%d ", i,
                    inter.gnuterm[i], init.gnuterm[inter.gnuterm[i]].output_is_file);
            if (init.gnuterm[inter.gnuterm[i]].output_is_file) {
                fprintf(fp, " filename='%s'\n", inter.gnutermfn[i]);
            } else {
                fprintf(fp, "\n");
            }
            fprintf(fp, " command=%s", init.gnuterm[inter.gnuterm[i]].command);
        }
    }

    if (inter.num_pyterm_cmds == NO_PYTERM) {
        fprintf(fp, " Python will not be started!\n");
    } else {
        fprintf(fp, "%d Python terminal%s:\n", inter.num_pyterm_cmds,
                inter.num_pyterm_cmds == 1 ? "" : "s");
        int i;
        for (i = 0; i < inter.num_pyterm_cmds; i++) {
            fprintf(fp, " no.%d (kat.ini no.%d) file=%d ", i,
                    inter.pyterm[i], init.pyterm[inter.pyterm[i]].output_is_file);
            if (init.pyterm[inter.pyterm[i]].output_is_file) {
                fprintf(fp, " filename='%s'\n", inter.pytermfn[i]);
            } else {
                fprintf(fp, "\n");
            }
            fprintf(fp, " command=%s", init.pyterm[inter.pyterm[i]].command);
        }
    }


}

//! Dump the information for all signals to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 *
 * \todo SIG_AMP/FRQ/X/Y branches untested
 */
void dump_all_signals(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    if (inter.num_signals) {
        fprintf(fp, ".....\n%d Signal%s at frequency %s :\n", inter.num_signals,
                inter.num_signals == 1 ? "" : "s", freq_form(inter.fsig));

        int signal_index;
        for (signal_index = 0; signal_index < inter.num_signals; signal_index++) {
            signal_t signal = inter.signal_list[signal_index];
            fprintf(fp, "%d : %s ", signal_index, get_component_name(signal_index));
            fprintf(fp, "type: ");

            switch (signal.type) {
                case SIG_AMP:
                    fprintf(fp, "amp");
                    break;
                case SIG_PHS:
                    fprintf(fp, "phs");
                    break;
                case SIG_FRQ:
                    fprintf(fp, "freq");
                    break;
                case SIG_X:
                    fprintf(fp, "xbeta");
                    break;
                case SIG_Y:
                    fprintf(fp, "ybeta");
                    break;
            }
            fprintf(fp, "(phase = %.15g, amp = %.15g)\n",
                    signal.phase, signal.amplitude);
        }
    }
}

//! Dump the frequencies to file (e.g. carrier and sidebands) ???

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_all_frequencies(FILE *fp) {
    int n;
    int lower1, lower2;
    double f, f1, f2;
    light_in_t *light_input;
    modulator_t *modulator;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "Frequencies in calculation :\n");
    n = 0;
    int light_input_index;
    for (light_input_index = 0;
            light_input_index < inter.num_light_inputs;
            light_input_index++) { // alle carrier
        light_input = &(inter.light_in_list[light_input_index]);
        f = f1 = light_input->f->f;
        fprintf(fp, "  ---\n %d  carrier %d : %.15g Hz\n",
                n, light_input_index, light_input->f->f);
        n++;

        if (inter.num_signals) {
            // signal freqs around carrier
            lower2 = 1;
            int signal_sideband_index;
            for (signal_sideband_index = 0;
                    signal_sideband_index < 2;
                    signal_sideband_index++) {
                f = f1 + lower2 * inter.fsig;
                fprintf(fp, " %d  signal : %.15g Hz\n", n, f);
                n++;
                lower2 = -1;
            }
        }

        int modulator_index;
        for (modulator_index = 0;
                modulator_index < inter.num_modulators;
                modulator_index++) {
            modulator = &(inter.modulator_list[modulator_index]);
            int modulation_order_index;
            for (modulation_order_index = 0;
                    modulation_order_index < modulator->order;
                    modulation_order_index++) {
                lower1 = 1;
                int mod_sideband_index;
                for (mod_sideband_index = 0;
                        mod_sideband_index < 2;
                        mod_sideband_index++) {
                    if (modulator->single_sideband_mode == ON) {
                        mod_sideband_index++;
                    }
                    f = f2 = f1 + lower1 * (modulation_order_index + 1) * modulator->f_mod;
                    fprintf(fp, " %d  mod %d (order %d) : %.15g Hz\n",
                            n, modulator_index, modulation_order_index + 1, f);
                    n++;
                    lower1 = -1;

                    if (inter.num_signals) {
                        // signal freqs around carrier
                        lower2 = 1;
                        int h;
                        for (h = 0; h < 2; h++) {
                            f = f2 + lower2 * inter.fsig;
                            fprintf(fp, " %d  signal : %.15g Hz\n", n, f);
                            n++;
                            lower2 = -1;
                        }
                    }

                }
            }
        }
    }
    fprintf(fp, "\n");
}

//! Dump the input parameters to file

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void dump_input(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, " --- parameters in kat.ini: --- \n");
    dump_init(fp);
    if (inter.debug & 2) {
        dump_pdtype(fp);
    }
    fprintf(fp, " --- components: --- \n");
    dump_all_mirrors(fp);
    dump_all_spaces(fp);
    dump_all_lenses(fp);
    dump_all_beamsplitters(fp);
    dump_all_gratings(fp);
    dump_all_diodes(fp);
    dump_all_modulators(fp);
    dump_all_signals(fp);
    dump_all_cavities(fp);
    fprintf(fp, " --- nodes: --- \n");
    dump_all_nodes(fp);
    fprintf(fp, " --- gauss parameters: --- \n");
    dump_all_gauss_cmds(fp);
    fprintf(fp, " --- inputs, outputs: --- \n");
    dump_all_light_inputs(fp);
    dump_all_light_outputs(fp);
    fprintf(fp, " --- variables, functions, locks, puts: --- \n");
    dump_all_set_cmds(fp);
    dump_all_func_cmds(fp);
    dump_all_lock_cmds(fp);
    dump_all_put_cmds(fp);
    fprintf(fp, " --- x- and y-axis: --- \n");
    dump_xaxis(fp);
    dump_yaxis(fp);
    fprintf(fp, " --- switches --- \n");
    fprintf(fp, " retrace is switched %s\n", inter.retrace == 1 ? "ON" : "OFF");

    if (options.sparse_solver == KLU_FULL) {
        fprintf(fp, " sparse matrix library used: KLU FULL\n");
#if INCLUDE_NICSLU == 1
    } else if (options.sparse_solver == NICSLU) {
        fprintf(fp, " sparse matrix library used: NICSLU\n");
#endif
#if INCLUDE_PARALUTION == 1
    } else if (options.sparse_solver == PARALUTION) {
        fprintf(fp, " sparse matrix library used: PARALUTION\n");
#endif
    } else
        bug_error("sparse_solver %i was not expected", options.sparse_solver);

    fprintf(fp, " --- \nnumber of traces in plot: %d", inter.num_output_cols - 1);
    fprintf(fp, "\n --- Gnuplot/Python terminals  --- \n");
    dump_all_gnuplot_cmds(fp);
    fprintf(fp, " ---------------\n");
}

//! Basic debugging output routine

/*!
 * \param fp the file pointer
 *
 * \todo refactor for readability
 */
void debug(FILE *fp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);

    dump_input(fp);
    dump_all_frequencies(fp);
    dump_deriv(fp);
    fflush(fp);
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
