 //$Id$

/*!
 * \file kat_inline.c
 * \brief Inline function routines
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#include <gsl/gsl_math.h>
#include "kat_io.h"
#include "kat_optics.h" 
#include "kat.h"

extern init_variables_t init;
extern memory_t mem;

extern const complex_t complex_i;
extern const complex_t complex_1;
extern const complex_t complex_m1;
extern const complex_t complex_0;


//! Returns (-1)^n

/*!
 * \param n Input exponent power
 */
static inline double msign(int n) {
    return (n % 2) ? -1.0 : 1.0;
}

static inline double sign(double n) {
    return (n < 0) ? -1.0 : 1.0;
}


// compute -1^n in order to determine whether a mode is mirrored or not

static inline double turnit(int n2) {
    return msign(n2);
}

//! rotate 2D vector given by x,y by a given angle 

/*!
 * \param x, y pointers to x and y coordinates
 * \param angle angle to rotate given in degrees
 * \param 
 *
 */
static inline void rotate_vector(double *x, double *y, double alpha) {
    double alpha_rad = alpha*RAD;
    double x2 = cos(alpha_rad)* (*x) + sin(alpha_rad) * (*y);
    double y2 = -1.0 * sin(alpha_rad)* (*x) + cos(alpha_rad) * (*y);
    *x = x2;
    *y = y2;
    return;
}


//! For given field number returns n, m as in TEM_nm

/*!
 * \param n n index of TEM mode
 * \param m m index of TEM mode
 * \param index Field number
 *
 * \see kat_aux for the reverse function
 */
static inline void get_tem_modes_from_field_index(int *n, int *m, int index) {
    *n = mem.all_tem_HG[2 * index];
    *m = mem.all_tem_HG[2 * index + 1];
}

static inline void get_tem_modes_from_LG_field_index(int *p, int *l, int index) {
    *p = mem.all_tem_LG[2 * index];
    *l = mem.all_tem_LG[2 * index + 1];
}

//! Returns modulus (norm) of the complex number z

/*!
 * From numerical recipes
 *
 * \param z Input complex number
 */
static inline double zmod(complex_t z) {
    double r, i, t1, t2;

    r = fabs(z.re);
    i = fabs(z.im);

    if (r == 0.0) {
        t1 = i;
    } else if (i == 0.0) {
        t1 = r;
    } else if (r > i) {
        t2 = i / r;
        t1 = r * sqrt(1.0 + t2 * t2);
    } else {
        t2 = r / i;
        t1 = i * sqrt(1.0 + t2 * t2);
    }

    return (t1);
}

//! Returns 1 if n is odd

/*!
 * \param n Integer to check
 */
static inline int odd(int n) {
    return (n % 2);
    /*
    div_t e;
    e = div(abs(n), 2);
    return (e.rem); */
}

//! q from z and w_0

/*!
 * \param w0 Beam waist
 * \param z Distance from the waist
 * \param nr Refractive index
 *
 * \return q Gaussian beam parameter
 */
static inline complex_t q_w0z(double w0, double z, double nr) {
    complex_t q;

    q.re = z;
    q.im = w0 * w0 * PI / init.lambda * nr;

    return (q);
}

//! z from q

/*!
 * \param q  Gaussian beam parameter
 */
static inline double z_q(complex_t q) {
    return (q.re);
}

//! z from q

/*!
 * \param q  Gaussian beam parameter
 */
static inline double z_mag_sqrd(complex_t q) {
    return (q.re*q.re + q.im*q.im);
}

//! Rayleigh length for given parameter q

/*!
 * \param q  Gaussian beam parameter
 */
static inline double z_r(complex_t q) {
    return (q.im);
}

//! Rayleigh length for given parameter q

/*!
 * \param w0 beam waist
 */
static inline double z_r_w0(double w0, double nr) {
    return PI * w0*w0 / (nr*init.lambda);
}

//! q_0 for given q

/*!
 * \param q  Gaussian beam parameter
 */
static inline complex_t q_0(complex_t q) {
    complex_t z;

    z.im = q.im;
    z.re = 0.0;

    return (z);
}

//! w_0 for given q

/*!
 * \param q gaussian beam parameter
 * \param nr Refractive index
 * \param q  Gaussian beam parameter

 */
static inline double w0_size(complex_t q, double nr) {
    return (sqrt(init.lambda * q.im / (PI * nr)));
}

//! w for given q

/*!
 * \param q  Gaussian beam parameter
 * \param nr Refractive index
 */
static inline double w_size(complex_t q, double nr) {
    return (zmod(q) * sqrt(init.lambda / (nr * PI * q.im))); // adf 30.11.11
}

//! ROC (radius of curvature) for given q 

/*!
 * TODO Check: MAYBE ONLY TRUE for vacuum/air!
 * \param q  Gaussian beam parameter
 */
static inline double roc(complex_t q) {
    return ((q.re * q.re + q.im * q.im) / q.re);
}

//! Return the real part of the given complex number

/*!
 * \param z A complex number
 */
static inline double re(complex_t z) {
    return (z.re);
}

//! Return the imaginary part of the given complex number

/*!
 * \param z A complex number
 */
static inline double im(complex_t z) {
    return (z.im);
}

//! Return the square of the given number

/*!
 * \param x A double precision number
 */
static inline double sqr(double x) {
    return (x * x);
}

//! Returns the maximum of two input double precision numbers

/*!
 * \param x Input real number
 * \param y Input real number
 */
static inline double max(double x, double y) {
    return x > y ? x : y;
}

//! Returns the minimum of two input double precision numbers

/*!
 * \param x Input real number
 * \param y Input real number
 */
static inline double min(double x, double y) {
    return x < y ? x : y;
}

//! Returns the minimum of two integer numbers

/*!
 * \param n Input integer number
 * \param m Input integer number
 */
static inline int nmin(int n, int m) {
    return n < m ? n : m;
    /*
    if (n < m) {
      return n;
    }
    else {
      return m;
    }
     */
}

//! True if two double precision numbers are within 1e-13 of each other

/*!
 * \param x Input double precision number
 * \param y Input double precision number
 * TODO: check if can be replaced by int gsl_fcmp (double x, double y, double epsilon)
 */
static inline int eq(double x, double y) {
    if (x == 0.0 && y == 0.0) {
        return (1);
    } else {
        return (fabs(x - y) / max(fabs(x), fabs(y)) < 1e-13);
    }
}

//! True if two double precision numbers (x-y) and z are within 1e-13 of each other (used by get_amplitude_sum)

/*!
 * \param x Input double precision number
 * \param y Input double precision number
 * \param z Input double precision number
 * TODO: check if can be replaced by int gsl_fcmp (double x, double y, double epsilon)
 */
static inline int teq(double x, double y, double z) {
    if (x == 0.0 && y == 0.0 && z == 0.0) {
        return (1);
    } else {
        return (fabs(x - y - z) / max(max(fabs(x), fabs(y)), fabs(z)) < 1e-13);
    }
}


//! True if two double precision numbers are within 1e-9 of each other

/*!
 * \param x Input double precision number
 * \param y Input double precision number
 * TODO: check if can be replaced by int gsl_fcmp (double x, double y, double epsilon)
 */
static inline int eq_wide(double x, double y) {
    if (x == 0.0 && y == 0.0) {
        return (1);
    } else {
        return (fabs(x - y) / max(fabs(x), fabs(y)) < 1e-9);
    }
}

//! True if two double precision numbers are within (absolute) 1e-13 of each other

/*!
 * \param x Input double precision number
 * \param y Input double precision number
 * TODO: check if can be replaced by int gsl_fcmp (double x, double y, double epsilon)
 */
static inline int peq(double x, double y) {
    return (fabs(fabs(x) - fabs(y)) < 1e-13);
}

//! True if two complex numbers are within "1e-13" of each other

/*!
 * \param x Input complex number
 * \param y Input complex number
 * TODO: check if can be replaced by int gsl_fcmp (double x, double y, double epsilon) or
 * something similar for complex
 */
static inline int ceq(const complex_t x, const complex_t y) // improved 260402
{
    double maximum;
    int re, im;

    re = im = 0;
    maximum = max(max(fabs(x.re), fabs(y.re)), max(fabs(x.im), fabs(y.im)));

    if (maximum < 1e-13) {
        return (1);
    }

    if (x.re == 0.0 && y.re == 0.0) {
        re = 1;
    } else {
        re = (fabs(x.re - y.re) / maximum < 1e-13);
    }

    if (x.im == 0.0 && y.im == 0.0) {
        im = 1;
    } else { /* braces problem is here!!! */
        im = (fabs(x.im - y.im) / maximum < 1e-13);
    }

    return (re & im);
}

//! True if two complex numbers are within "1e-5" of each other

/*!
 * \param x Input complex number
 * \param y Input complex number
 *
 * \todo untested
 * TODO: check if can be replaced by int gsl_fcmp (double x, double y, double epsilon) or something
 * similar for complex
 */
static inline int capprox(const complex_t x, const complex_t y) {
    double maximum;
    int re, im;

    re = im = 0;
    maximum = max(max(fabs(x.re), fabs(y.re)), max(fabs(x.im), fabs(y.im)));

    if (maximum < 1e-5) {
        return (1);
    }

    if (x.re == 0.0 && y.re == 0.0) {
        re = 1;
    } else {
        re = (fabs(x.re - y.re) / maximum < 1e-5);
    }

    if (x.im == 0.0 && y.im == 0.0) {
        im = 1;
    } else {
        im = (fabs(x.im - y.im) / maximum < 1e-5);
    }

    return (re & im);
}

//! Absolute value squared of a complex number

/*!
 * \param z Input complex number
 *
 * \return \f$|z|^2\f$
 */
static inline double zabs(complex_t z) {
    return z.re * z.re + z.im * z.im;
}

//! Phase of a complex number (in radians)

/*!
 * \param z Input complex number
 */
static inline double zphase(complex_t z) {
    return atan2(z.im, z.re);
}

//! Phase of a complex number (in degrees)

/*!
 * \param z Input complex number
 */
static inline double zdeg(complex_t z) {
    return DEG * atan2(z.im, z.re);
}

//! Phase of a complex number in degrees with a positive angle

/*!
 * \param z Input complex number
 *
 * \todo untested
 */
static inline double zdegp(complex_t z) {
    double w = DEG * atan2(z.im, z.re);

    return (w >= 0.0 ? w : w + 360.0);
}

//! Phase of a complex number in degrees with a negative angle

/*!
 * \param z Input complex number
 *
 * \todo untested
 */
static inline double zdegm(complex_t z) {
    double w = DEG * atan2(z.im, z.re);

    return (w <= 0.0 ? w : w - 360.0);
}

//! The absolute value of a complex number in decibels

/*!
 * \param z Input complex number
 */
static inline double zdb(complex_t z) {
    return 20.0 * log10(zabs(z));
}

//! Construct a complex number from the real and imaginary parts

/*!
 * \param re Real part of complex number
 * \param im Imaginary part of complex number
 */
static inline complex_t co(double re, double im) {
    complex_t zz = {re, im};
    return zz;
}

//! Construct a complex number from the amplitude and phase (in radians)

/*!
 * \param x Real amplitude of complex number
 * \param ph Phase (argument) of complex number (radians)
 */
static inline complex_t get_complex_from_real_and_phase(double x, double ph) {
    complex_t zz;

    zz.re = x * cos(ph);
    zz.im = x * sin(ph);

    return zz;
}

//! Construct a complex number from the amplitude and phase (in radians)

/*!
 * \param x Real amplitude of complex number
 * \param ph Phase (argument) of complex number (radians)
 */
static inline complex_t co2r(double x, double ph) {
    complex_t zz;

    zz.re = x * cos(ph);
    zz.im = x * sin(ph);

    return zz;
}

//! Construct a complex number from the amplitude and phase (in degrees)

/*!
 * \param x Real amplitude of complex number
 * \param ph Phase (argument) of complex number (degrees)
 */
static inline complex_t co2d(double x, double ph) {
    complex_t zz;

    zz.re = x * cos(ph * RAD);
    zz.im = x * sin(ph * RAD);

    return zz;
}

//! Complex conjugate

/*!
 * \param z Input complex number
 */
static inline complex_t cconj(complex_t z) {
    complex_t zz;

    zz.re = z.re;
    zz.im = -z.im;

    return zz;
}

//! Return the negative of a complex number

/*!
 * \param z Input complex number
 */
static inline complex_t cminus(complex_t z) {
    complex_t zz;

    zz.re = -z.re;
    zz.im = -z.im;

    return zz;
}

//! Complex number times i

/*!
 * \param z Input complex number
 */
static inline complex_t ci(complex_t z) {
    complex_t zz;

    zz.re = -z.im;
    zz.im = z.re;

    return zz;
}

static inline void z_inc_z(complex_t *z, complex_t zi) {
    z->re += zi.re;
    z->im += zi.im;
}

static inline void z_inc_zc(complex_t *z, complex_t zi) {
    z->re += zi.re;
    z->im -= zi.im;
}

//! Multiply two complex numbers together

/*!
 * \param z Input complex number
 * \param zc Input complex number
 */
static inline complex_t z_by_z(complex_t z, complex_t zc) {
    return co(z.re * zc.re - z.im * zc.im, z.im * zc.re + z.re * zc.im);
}

//! Multiply one complex number by the conjugate of another

/*!
 * \param z Input complex number
 * \param zc Input conjugated complex number
 */
static inline complex_t z_by_zc(complex_t z, complex_t zc) {
    complex_t zz;

    zz.re = z.re * zc.re + z.im * zc.im;
    zz.im = z.im * zc.re - z.re * zc.im;

    return zz;
}

static inline complex_t zc_by_z(complex_t zc, complex_t z) {
    complex_t zz;

    zz.re = z.re * zc.re + z.im * zc.im;
    zz.im = z.im * zc.re - z.re * zc.im;

    return zz;
}

static inline complex_t zc_by_zc(complex_t z1, complex_t z2) {
    return cconj(z_by_z(z1, z2));
}

//! Complex addition

/*!
 * \param z1 Input complex number
 * \param z2 Input complex number
 */
static inline complex_t z_pl_z(complex_t z1, complex_t z2) {
    complex_t zz;

    zz.re = z1.re + z2.re;
    zz.im = z1.im + z2.im;

    return zz;
}

//! Complex addition of one number with the conjugate of another

/*!
 * \param z1 Input complex number
 * \param z2 Input conjugated complex number
 */
static inline complex_t z_pl_zc(complex_t z1, complex_t z2) {
    complex_t zz;

    zz.re = z1.re + z2.re;
    zz.im = z1.im - z2.im;

    return zz;
}

//! Complex subtraction

/*!
 * \param z1 Input complex number
 * \param z2 Input complex number
 */
static inline complex_t z_m_z(complex_t z1, complex_t z2) {
    complex_t zz;

    zz.re = z1.re - z2.re;
    zz.im = z1.im - z2.im;

    return zz;
}

//! Complex subtraction of one number with the conjugate of another

/*!
 * \param z1 Input complex number
 * \param z2 Input conjugated complex number
 */
static inline complex_t z_m_zc(complex_t z1, complex_t z2) {
    complex_t zz;

    zz.re = z1.re - z2.re;
    zz.im = z1.im + z2.im;

    return zz;
}

//! Multiplication of a complex number by a real number

/*!
 * \param z Input complex number
 * \param x Input real number
 */
static inline complex_t z_by_x(complex_t z, double x) {
    complex_t zz;

    zz.re = z.re * x;
    zz.im = z.im * x;

    return zz;
}

//! Rotation of the phase of a complex number by an amount in degrees

/*!
 * \param z Input complex number
 * \param ph Input phase (degrees)
 */
static inline complex_t z_by_ph(complex_t z, double ph) {
    complex_t zz;
    double phr = ph * RAD;
    double cphr = cos(phr);
    double sphr = sin(phr);
    zz.re = z.re * cphr - z.im * sphr;
    zz.im = z.re * sphr + z.im * cphr;
    return zz;
}       

static inline complex_t z_by_ph2(complex_t z, double cphr, double sphr) {
    return co(z.re * cphr - z.im * sphr, z.re * sphr + z.im * cphr);
}

//! Multiplication of complex number with a real number and phase in degrees

/*!
 * \param z Input complex number
 * \param x Input real number
 * \param ph Input phase (degrees)
 */
static inline complex_t z_by_xph(complex_t z, double x, double ph) {
    complex_t zz;
    double phr = ph * RAD;
    double cphr = cos(phr);
    double sphr = sin(phr);
    
    zz.re = x * (z.re * cphr - z.im * sphr);
    zz.im = x * (z.re * sphr + z.im * cphr);

    return zz;
}

//! Rotation of the phase of a complex number by an amount in radians

/*!
 * \param z Input complex number
 * \param ph Input phase (radians)
 */
static inline complex_t z_by_phr(complex_t z, double ph) {
    complex_t zz;
    double cph = cos(ph);
    double sph = sin(ph);
    zz.re = z.re * cph - z.im * sph;
    zz.im = z.re * sph + z.im * cph;

    return zz;
}

//! Multiplication of complex number with a real number and phase in radians

/*!
 * \param z Input complex number
 * \param x Input real number
 * \param ph Input phase (radians)
 */
static inline complex_t z_by_xphr(complex_t z, double x, double ph) {
    complex_t zz;
    double cph = cos(ph);
    double sph = sin(ph);
    
    zz.re = x * (z.re * cph - z.im * sph);
    zz.im = x * (z.re * sph + z.im * cph);

    return zz;
}

//! Set the phase of a complex number to zero

/*!
 * \param z Input complex number
 */
static inline complex_t z_zero_ph(complex_t z) {
    return co(zmod(z), 0.0);
}

//! Invert a complex number

/*!
 * \param z Input complex number
 *
 * \return the inverse of the input complex number
 */
static inline complex_t inv_complex(complex_t z) {
    double tmp_d;
    complex_t tmp_c;

    tmp_d = zabs(z);

    if (ceq(z, complex_0)) {
        bug_error("complex division by zero");
    }

    tmp_c.re = z.re / tmp_d;
    tmp_c.im = -z.im / tmp_d;

    return (tmp_c);
}

//! Complex division

/*! 
 * Numerical Recipes S.949
 *
 * \param z1 Nominator
 * \param z2 Denominator
 */
static inline complex_t div_complex(complex_t z1, complex_t z2) {
    complex_t z;
    double tmp_d, r, den;

    tmp_d = zabs(z2);
    if (tmp_d == 0) {
        bug_error("complex division by zero");
    }

    if (fabs(z2.re) >= fabs(z2.im)) {
        r = z2.im / z2.re;
        den = z2.re + r * z2.im;
        z.re = (z1.re + r * z1.im) / den;
        z.im = (z1.im - r * z1.re) / den;
    } else {
        r = z2.re / z2.im;
        den = z2.im + r * z2.re;
        z.re = (r * z1.re + z1.im) / den;
        z.im = (r * z1.im - z1.re) / den;
    }

    return (z);
}

//! Complex square root

/*! 
 * Numerical Recipes S.949
 *
 * \param z Input complex number
 */
static inline complex_t zsqrt(complex_t z) {
    const complex_t cnull = {0., 0.};
    complex_t z1;
    double x, y, w, r;


    if ((z.re == 0.0) && (z.im == 0.0)) {
        return (cnull);
    } else {
        x = fabs(z.re);
        y = fabs(z.im);

        if (x >= y) {
            r = y / x;
            w = sqrt(x) * sqrt(0.5 * (1.0 + sqrt(1.0 + r * r)));
        } else {
            r = x / y;
            w = sqrt(y) * sqrt(0.5 * (r + sqrt(1.0 + r * r)));
        }

        if (z.re >= 0.0) {
            z1.re = w;
            z1.im = z.im / (2.0 * w);
        } else {
            z1.im = (z.im >= 0) ? w : -w;
            z1.re = z.im / (2.0 * z1.im);
        }

        return (z1);
    }
}

//! Returns x^n with n an integer

/*!
 * \param x Input real number
 * \param n Input exponent power
 *
 * \return x to the power of n
 *
 */
static inline double npow(double x, int n) {
    // TODO: replace npow in code elsewhere by this
    return (gsl_pow_int(x, n));
}

//! Round the input value to a certain number of decimal places

/*!
 * \param value_to_round value to round
 *
 * \todo add extra arg to specify how many digits to round to
 */
static inline double round_to_dp(double value_to_round) {
    double rounded_value;
    if (value_to_round > 0.0) {
        // round dx to 12 digits
        double digits = npow(10, 12 - floor(log10(value_to_round))); // changed to npow adf 011211
        rounded_value = floor(value_to_round * digits) / digits;
    } else {
        // use the old version of rounding
        rounded_value = floor(value_to_round * 1.e12) * 1.e-12;
    }
    return rounded_value;
}


//! Adjust the phase of a coupling coefficient with respect to the Gouy phases. 

/*!
 * Used in k_mnmn because in FINESSE the Gouy phase is added explicitly to the
 * amplitude coefficients in a `space' where as the coupling coefficients
 * are derived using a formula in which the Gouy phase resides in the
 * equation for the spatial profile.
 *
 * \param k coupling coefficient between beams
 * \param n1 mode number of beam 1
 * \param m1 mode number of beam 1
 * \param n2 mode number of beam 2
 * \param m2 mode number of beam 2
 * \param qx1 Gaussian q parameter in x-plane for beam 1
 * \param qx2 Gaussian q parameter in x-plane for beam 2
 * \param qy1 Gaussian q parameter in y-plane for beam 1
 * \param qy2 Gaussian q parameter in y-plane for beam 2
 *
 * \see Test_rev_gouy()
 */
static inline complex_t rev_gouy(complex_t k, int n1, int m1, int n2, int m2, complex_t qx1,
        complex_t qx2, complex_t qy1, complex_t qy2) {
    // sanity checks on inputs
    // [n-m][1-2] should be >= 0
    assert(m1 >= 0);
    assert(m2 >= 0);
    assert(n1 >= 0);
    assert(n2 >= 0);

    return z_by_phr(k, -1.0 * (((n1 + 0.5) * gouy(qx1) + (m1 + 0.5) * gouy(qy1)) -((n2 + 0.5) * gouy(qx2) + (m2 + 0.5) * gouy(qy2))));
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
