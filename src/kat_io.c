
/*!
 * \file kat_io.c
 * \brief Input-output routines.
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#include "kat.h"
#include "kat_config.h"
#include "kat_io.h"
#include "kat_inline.c"
#include "kat_aux.h"
#include "kat_init.h"
#include "kat_aa.h"
#include "kat_optics.h"
#include "kat_knm_mirror.h"
#include "kat_knm_bs.h"
#include <stdarg.h>
#include "md5.h"
#include "base64.h"
#include "f2c.h"
#include "kat_read.h"
#include <sys/types.h> 
#include <sys/stat.h>
#include <unistd.h>
#include "kat_paralution.h"

//extern init_variables_t in;
extern interferometer_t inter;
extern global_var_t vglobal;
extern char mydate[];
extern memory_t mem;
extern options_t options;
extern FILE *fp_log;

//! Print the progress of the simulation to stderr
size_t prev_progress_print_size = 0;
extern FILE* pykat_file;

char progress_action[LINE_LEN] = {0};
char progress_message[LINE_LEN] = {0};

typedef struct n1n2m1m2 {
    int n1, n2, m1, m2;
} n1n2m1m2_t;

n1n2m1m2_t *nms;

//! Checks if a file exists, returns 1 if so and 0 otherwise

/*!
 * \param filename, string giving the filename
 */
int check_if_file_exists(char *filename) {
    struct stat file_stat;

    if (stat(filename, &file_stat) == 0)
        return (1);
    else
        return (0);
}

//! Renames a file to filename.bak if the file exists

/*!
 * \param filename, string giving the filename
 */
void make_backup_of_file(char *filename) {

    char newname[LINE_LEN];

    if (check_if_file_exists(filename)) {
        strcpy(newname, filename);
        strcat(newname, ".bak");
        rename(filename, newname);
    }
}

//! opens file for writing (making a backup first) and returns handle

/*!
 * \param filename, string giving the filename
 */
void open_file_to_write_ascii(char *filename, FILE **handle) {
    make_backup_of_file(filename);
    *handle = fopen(filename, "w");
    if (*handle == NULL) {
        gerror("Could not open file '%s'\n", filename);
    }
}

void open_file_to_read_ascii(char *filename, FILE **handle) {
    *handle = fopen(filename, "r");
    if (*handle == NULL) {
        gerror("Could not open file '%s'\n", filename);
    }
}

void open_file_to_write_binary(char *filename, FILE **handle) {
    make_backup_of_file(filename);
    *handle = fopen(filename, "wb");
    if (*handle == NULL) {
        gerror("Could not open file '%s'\n", filename);
    }
}

void open_file_to_read_binary(char *filename, FILE **handle) {
    *handle = fopen(filename, "rb");
    if (*handle == NULL) {
        gerror("Could not open file '%s'\n", filename);
    }
}

void _read_merged_map_ascii_data(char* name, surface_merged_map_t *map, MAP_COMPONENTS_t num) {
    assert(map != NULL);

    double **data = NULL; // remove compiler warning
    FILE *file = NULL;
    char buf[MAX_MAP_LINE];
    char filenamemap[LINE_LEN];
    int row_count = 0, col_count = 0;
    double val;

    switch (num) {
        case R_ABS:
            data = map->r_abs;
            sprintf(buf, "%s_r_abs.map", map->filename);
            break;
        case R_PHS:
            data = map->r_phs;
            sprintf(buf, "%s_r_phs.map", map->filename);
            break;
        case T_ABS:
            data = map->t_abs;
            sprintf(buf, "%s_t_abs.map", map->filename);
            break;
        case T_PHS:
            data = map->t_phs;
            sprintf(buf, "%s_t_phs.map", map->filename);
            break;
        default:
            bug_error("%i was passed when the value should only be between 0-3\n", num);
    }

    assert(data!=NULL);
    
    if (inter.debug & 128 && !options.quiet)
        message("* Reading map file %s...\n", buf);

    strcpy(filenamemap, buf);

    if (!check_if_file_exists(filenamemap))
        gerror("The map file %s is missing. Please delete all other .map and .knm files\n"
            "    for mirror %s and regenerate them or replace the missing file\n", filenamemap, name);

    open_file_to_read_ascii(filenamemap, &file);

    while (fgets(buf, MAX_MAP_LINE - 1, file) != NULL) {
        // ensure there is an end of line value, this should make sure that
        // if there happens to be more numbers than can fit in the buffer this
        // should allow the scanf to pick up the end and then the difference
        // in column count should be found
        buf[MAX_MAP_LINE - 1] = '\n';

        if (row_count > map->rows)
            gerror("There are more rows than expected in the map file %s.\n"
                "    Please fix or delete the files and regenerate the map data.\n", filenamemap);

        col_count = 0;

        while (true) {
            // the -1 is here because below we check when only 1 matched was 
            // found using sscanf and then we break, so we would only get here
            // if there were too many columns
            if (col_count > map->cols - 1)
                gerror("There are more columns than expected in the map file %s in row %i,\n"
                    "    Please fix or delete the files and regenerate the map data.\n", filenamemap, row_count + 1);

            int n = sscanf(buf, "%lg %[^\n]s", &val, buf);

            if (n == 2) {
                data[col_count][row_count] = val;
                col_count++;
            } else if (n == 1) {
                data[col_count][row_count] = val;
                col_count++;
                break;
            } else if ((buf[0] == '\n') && (row_count == map->rows))
                break;
            else
                gerror("Problem occured when reading map file %s, please ensure it has not been tampered with.\n"
                    "    If not delete files and regenerate the map data.\n", filenamemap);
        }

        if ((buf[0] == '\n') && (row_count == map->rows))
            break;

        if (col_count != map->cols)
            gerror("There was not the expected amount of columns in the map file %s in row %i.\n"
                "    Please fix or delete the files and regenerate the map data\n", filenamemap, row_count);

        row_count++;
    }

    if (row_count != map->rows)
        gerror("There was not the expected amount of rows in the map file %s.\n"
            "    Please fix or delete the files and regenerate the map data\n", filenamemap);

    fclose(file);
}

void _read_merged_map_binary_data(char *name, surface_merged_map_t *map) {

    FILE *file;
    unsigned char* filebuf;
    unsigned long filesize;
    char buf[LINE_LEN];
    char filename[FILENAME_MAX];
    MD5_CTX ctx;

    sprintf(buf, "%s.map.bin", map->filename);
    strcpy(filename, buf);

    if (!check_if_file_exists(filename))
        gerror("The map file %s is missing. Please delete all .map.bin, .knm.bin and .knm files\n"
            "    for mirror %s and regenerate them or replace the missing files\n", filename, name);

    if ((inter.debug & 128) && !options.quiet)
        message("* Reading map file %s...\n", buf);

    MD5Init(&ctx);

    //calculate length that the file should be
    filesize = sizeof (ctx.digest);
    filesize += sizeof (map->rows);
    filesize += sizeof (map->cols);
    filesize += sizeof (double) * 4 * map->rows * map->cols;

    open_file_to_read_binary(filename, &file);

    fseek(file, 0L, SEEK_END);
    unsigned long fs = ftell(file);
    fseek(file, 0L, SEEK_SET);

    if (filesize != fs)
        gerror("The merged map data in %s has a different filesize to what was expected\n"
            "    for the mirror %s.  Please delete all .map.bin, .knm.bin and .knm files\n"
            "    for mirror %s and regenerate them or replace with the correct files\n", filename, name);

#if DEBUG
    filebuf = (unsigned char*) calloc(filesize, sizeof (unsigned char));
#else
    filebuf = (unsigned char*) malloc(filesize);
#endif

    fread(filebuf, sizeof (unsigned char), filesize, file);
    fseek(file, 0L, SEEK_SET); // get back to the start of the file

    MD5Update(&ctx, &filebuf[16], filesize - 16);
    MD5Final(&ctx);

    int rows, cols;
    unsigned long pos = 0;

    // compare checksum
    if (memcmp(ctx.digest, filebuf, sizeof (ctx.digest)))
        gerror("The checksum for file %s did not match the stored checksum value.\n"
            "    Please delete all .map.bin, .knm.bin and .knm files for mirror %s\n"
            "    and regenerate them or replace with the correct files\n", filename, name);

    pos += sizeof (ctx.digest);

    rows = *((int*) &filebuf[pos]);
    pos += sizeof (map->rows);

    cols = *((int*) &filebuf[pos]);
    pos += sizeof (map->cols);

    if (rows != map->rows || cols != map->cols)
        gerror("The merged map data in %s has a different number of rows and columns to\n"
            "    what was expected for the mirror %s. Please delete all .map.bin,\n"
            "    .knm.bin and .knm files for mirror %s and regenerate them or\n"
            "     replace with the correct files\n", filename, name);

    size_t s = sizeof (map->r_abs[0][0]);

    int i;
    for (i = 0; i < map->cols; i++) {
        memcpy(map->r_abs[i], &filebuf[pos], s * rows);
        pos += cols*s;
        EOF_CHECK;
        memcpy(map->r_phs[i], &filebuf[pos], s * rows);
        pos += cols*s;
        EOF_CHECK;
        memcpy(map->t_abs[i], &filebuf[pos], s * rows);
        pos += cols*s;
        EOF_CHECK;
        memcpy(map->t_phs[i], &filebuf[pos], s * rows);
        pos += cols*s;
        EOF_CHECK;
    }

    if (pos != filesize)
        bug_error("Whilst reading the binary merged map we didn't read the entire file.\n");

    free(filebuf);
    fclose(file);
}

void _read_knm_binary_data(char *name, surface_merged_map_t *map, void *knm, bool converting, KNM_COMPONENT_t knmcmp){
    FILE *file;
    unsigned char* filebuf;
    unsigned long filesize;
    char buf[LINE_LEN];
    char filename[FILENAME_MAX];
    MD5_CTX ctx;

    sprintf(buf, "%s.knm.bin", map->filename);
    strcpy(filename, buf);

    if (!check_if_file_exists(filename))
        gerror("The map file %s is missing. Please delete all .map.bin, .knm.bin and .knm files\n"
            "    for mirror %s and regenerate them or replace the missing files\n", filename, name);

    if (inter.debug && !options.quiet)
        message("* Reading binary knm file %s...\n", buf);

    MD5Init(&ctx);
    // this is the num_fields of the simulation that wrote the 
    // data file, not the currrent one!
    int num_fields = (int) (map->_maxtem + 1) * (map->_maxtem + 2) / 2;
    int numknm = num_fields*num_fields;

    //calculate length that the file should be
    filesize = sizeof (ctx.digest);
    filesize += sizeof (int);
    // length for n1,n2,m1,m2 for each knm
    filesize += numknm * 4 * sizeof (unsigned char);
    
    // length of each knm
    if(knmcmp == MIRROR_CMP)
        filesize += numknm * 4 * (sizeof (complex_t));
    else if(knmcmp == BEAMSPLITTER_CMP)
        filesize += numknm * 8 * (sizeof (complex_t));
    else
        bug_error("couldn't handle knmcmp in _read_knm_binary_data");

    open_file_to_read_binary(filename, &file);

    fseek(file, 0L, SEEK_END);
    unsigned long fs = ftell(file);
    fseek(file, 0L, SEEK_SET);

    if (filesize != fs)
        gerror("The merged map data in %s has a different filesize to what was expected for\n"
            "    the mirror %s.  Please delete all .map.bin, .knm.bin and .knm files for\n"
            "    mirror %s and regenerate them or replace with the correct files\n", filename, name);

#if DEBUG
    filebuf = (unsigned char*) calloc(filesize, sizeof (unsigned char));
#else
    filebuf = (unsigned char*) malloc(filesize);
#endif

    if (filebuf == NULL)
        gerror("Out of memory: Could not allocate enough memory to create binary file buffer.\n");

    // load the whole file into memory
    fread(filebuf, sizeof (unsigned char), filesize, file);
    fseek(file, 0L, SEEK_SET); // get back to the start of the file

    MD5Update(&ctx, &filebuf[16], filesize - 16);
    MD5Final(&ctx);

    unsigned long pos = 0;

    // compare checksum
    if (memcmp(ctx.digest, filebuf, sizeof (ctx.digest)))
        gerror("The checksum for file %s did not match the stored checksum value.\n"
            "    Please delete all .map.bin, .knm.bin and .knm files for mirror %s\n"
            "    and regenerate them or replace with the correct files\n", filename, name);

    pos = sizeof (ctx.digest);

    int numknm_read = *((int*) &filebuf[pos]);
    pos += sizeof (numknm_read);

    // if the number of knm we calculated there would be earlier is not the
    // same as what has just been read there is a problem
    if (numknm_read != numknm)
        gerror("The number of knm's in the file %s was not what was expected.\n"
            "    Please delete all .map.bin, .knm.bin and .knm files for mirror %s\n"
            "    and regenerate them or replace with the correct files\n", filename, name);

#ifdef DEBUG
    if(knmcmp == MIRROR_CMP) {
        if (!IS_MIRROR_KNM_ALLOCD((mirror_knm_t*)knm))
            bug_error(" ! Mirror knm matric was not allocated\n");
    } else if(knmcmp == BEAMSPLITTER_CMP) {
        if (!IS_BS_KNM_ALLOCD((bs_knm_t*)knm))
            bug_error(" ! Beamsplitter knm matric was not allocated\n");
    }
#endif

    int n, m, n1, n2, m1, m2;
    unsigned char _n1, _n2, _m1, _m2;
    size_t cs = sizeof (complex_t);

    for (n = 0; n < num_fields; n++) {
        for (m = 0; m < num_fields; m++) {
            _n1 = *((unsigned char*) &filebuf[pos]);
            pos += 1;
            EOF_CHECK;
            _n2 = *((unsigned char*) &filebuf[pos]);
            pos += 1;
            EOF_CHECK;
            _m1 = *((unsigned char*) &filebuf[pos]);
            pos += 1;
            EOF_CHECK;
            _m2 = *((unsigned char*) &filebuf[pos]);
            pos += 1;
            EOF_CHECK;

            n1 = (int) _n1;
            n2 = (int) _n2;
            m1 = (int) _m1;
            m2 = (int) _m2;

            if (converting) {
                nms[n + num_fields * m].n1 = n1;
                nms[n + num_fields * m].n2 = n2;
                nms[n + num_fields * m].m1 = m1;
                nms[n + num_fields * m].m2 = m2;
            }

            if (((n1 + m1) <= inter.tem) && ((n2 + m2) <= inter.tem)) {
                int f1 = get_field_index_from_tem(n1, m1);
                int f2 = get_field_index_from_tem(n2, m2);

                switch(knmcmp){
                    case MIRROR_CMP:
                        memcpy(&(((mirror_knm_t*)knm)->k11[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((mirror_knm_t*)knm)->k12[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((mirror_knm_t*)knm)->k21[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((mirror_knm_t*)knm)->k22[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        break;
                    case BEAMSPLITTER_CMP:
                        memcpy(&(((bs_knm_t*)knm)->k12[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((bs_knm_t*)knm)->k21[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((bs_knm_t*)knm)->k34[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((bs_knm_t*)knm)->k43[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((bs_knm_t*)knm)->k13[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((bs_knm_t*)knm)->k31[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((bs_knm_t*)knm)->k24[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        memcpy(&(((bs_knm_t*)knm)->k42[f1][f2]), &filebuf[pos], cs);
                        pos += cs;
                        EOF_CHECK;
                        break;
                }
                
            } else {
                if(knmcmp == MIRROR_CMP)
                    pos += cs * 4;  
                else if(knmcmp == BEAMSPLITTER_CMP)
                    pos += cs * 8;
                
                //EOF_CHECK;
                if(pos > filesize) 
                    gerror(" ! %s:%i :: Prematurely reached the end of the map file %s. The file" 
                            " could be corrupt or the incorrect map file for the mirror. Please" 
                            " delete all .map.bin, .knm.bin and .knm files for mirror %s" 
                            " and regenerate them or replace with the correct files\n",__FILE__,__LINE__, filename);
            }
        }
    }


    fclose(file);
    free(filebuf);
}

//bool read_mirror_knm_files(mirror_t *mirror, bool converting) {
bool read_knm_file(void *cmp, bool converting, KNM_COMPONENT_t knmcmp) {
#define READ(A) \
linenum++;\
fgets(buf, LINE_LEN-1,file); \
if(feof(file)) gerror("Unexpectedly reached the end of the coupling coefficient file '%s'. Please delete this file and recompute it\n as it could be corrupt or for the wrong version of Finesse.\n",knmfilename); \
((A == 0) ? gerror("Line %i ('%s') could not be read in map file %s\n" , linenum, buf, knmfilename) : NULL)

    char *name=NULL;
    surface_map_t **maps=NULL;
    surface_merged_map_t *map=NULL;
    void* knm=NULL;
    int num_maps=0; 
    
    if(knmcmp == MIRROR_CMP) {
        mirror_t *mirror = (mirror_t*)cmp;
        
        name = mirror->name;
        map = &mirror->map_merged;
        knm = (void*)&mirror->knm_map;
        num_maps = mirror->num_maps;
        maps = mirror->map;
        
    } else if(knmcmp == BEAMSPLITTER_CMP) {
        beamsplitter_t *bs = (beamsplitter_t*)cmp;
        name = bs->name;
        map = &bs->map_merged;
        knm = (void*)&bs->knm_map;
        num_maps = bs->num_maps;
        maps = bs->map;
        
    } else
        bug_error("Couldn't handle component in read_knm_file()");
    
    FILE *file = NULL;

    assert(knm != NULL);
    assert(map != NULL);

    char buf[LINE_LEN];
    int linenum = 0;
    char knmfilename[LINE_LEN];

    sprintf(knmfilename, "%s.knm", map->filename);

    if (!check_if_file_exists(knmfilename)) {
        return false;
    } else {
        open_file_to_read_ascii(knmfilename, &file);

        // read three lines of comments
        READ(sscanf(buf, "%% %s", buf));
        READ(sscanf(buf, "%% %s", buf));
        READ(sscanf(buf, "%% %s", buf));

        int n, mapnum = 0;
        
        if(knmcmp == BEAMSPLITTER_CMP){
            READ(sscanf(buf, "component : %s", buf));
            
            if (strcmp(buf, "BEAMSPLITTER") != 0)
                gerror("Type of knm file '%s' read was not for a BEAMSPLITTER component, %s\n", knmfilename, name);
            
        } else if(knmcmp == MIRROR_CMP){
            READ(sscanf(buf, "component : %s", buf));
            
            if (strcmp(buf, "MIRROR") != 0)
                gerror("Type of knm file '%s' read was not for a MIRROR component, %s\n", knmfilename, name);
            
        } else
            bug_error("Could not understand knmcmp %i\n", knmcmp);
        
        READ(sscanf(buf, "mapnum : %i", &mapnum));
        
        int bphstrans = 0;
        READ(sscanf(buf, "transonly : %i", &bphstrans));
        map->phaseIsOnlyTransmission = (bool) bphstrans;
        
        int bhasreflmap = 0;
        READ(sscanf(buf, "hasreflmap : %i", &bhasreflmap));
        map->hasReflectivityMap = (bool) bhasreflmap;
        
        // if we haven't got the same number of maps recompute

        if (mapnum != num_maps && !converting) {
            if (inter.debug && !options.quiet)
                message("* KNM file read: Different number of maps on component %s\n", name);

            return false;
        } else {
            // if we are converting get the map numbers from the file
            num_maps = mapnum;
            
            if ( mapnum > 0 )
                map->noMapPresent = false;
            else
                map->noMapPresent = true;
        }
        
        int mn;
        char mapfile[LINE_LEN];
        char B64maphash[LINE_LEN];
        unsigned char maphash[LINE_LEN];

        // if we are converting the knn file the various map files
        // won't be loaded so no need to check them
        if (!converting) {
            for (n = 0; n < mapnum; n++) {
                READ(sscanf(buf, "map%i : %s %s", &mn, mapfile, B64maphash));
                // if the map is not the same file name then the order could have 
                // changed or different maps have been used.
                if (strcmp(maps[n]->filename, mapfile) != 0) {
                    if (inter.debug && !options.quiet)
                        message("* KNM file read: Map order on component %s is different to knm file\n", name);

                    return false;
                }

                // make sure it isnt too long
                if (strlen(B64maphash) > 30) {
                    if (inter.debug && !options.quiet)
                        message("* KNM file read: The Base64 hash key is not correct for file %s\n", mapfile);

                    return false;
                }

                //First need to decode the base64 hash
                if (!decode_base64(maphash, B64maphash)) {
                    if (inter.debug && !options.quiet)
                        message("* KNM file read: Could not decode the Base64 hash key correctly for file %s\n", mapfile);

                    return false;
                }

                // compare the hash that has been read when reading the maps
                // and that in the file
                if (memcmp((char*) maps[n]->hash, (char*) maphash, 16) != 0) {
                    if (inter.debug && !options.quiet)
                        message("* KNM file read: Map hash code was not the same for file %s\n", mapfile);

                    return false;
                }
            }
        } else {
            // however if we are converting then we do need to store the map
            // names to write again later, so we fake the maps here
            long currpos;

            if(knmcmp == MIRROR_CMP)
                ((mirror_t*)cmp)->num_maps = num_maps;
            else if(knmcmp == BEAMSPLITTER_CMP)
                ((beamsplitter_t*)cmp)->num_maps = num_maps;
            else
                bug_error("Could not handle knmcmp value %i",knmcmp);
            
            int i = 0;
            
            while (true) {
                currpos = ftell(file);
                fgets(buf, LINE_LEN - 1, file);

                if (feof(file))
                    gerror("Unexpectedly reached the end of the coupling coefficient file '%s'. Please delete this file and recompute it\n as it could be corrupt or for the wrong version of Finesse.\n",knmfilename);

                // check if the start of the line is a map definition
                if (strcspn(buf, "map") == 0 && strlen(buf) != 0) {
                    char b64Hash[LINE_LEN];
                    
                    maps[i] = (surface_map_t *) calloc(1, sizeof (surface_map_t));

                    if (maps[i] == NULL) {
                        bug_error("memory allocation for surface map failed");
                    }
                    
                    sscanf(buf, "map%i : %s %s", &mn, maps[i]->filename, b64Hash);

                    if ( !decode_base64(maps[i]->hash, b64Hash)){
                        gerror("Could not decode the BASE64 string %s for map %i in %s",b64Hash, maps[i]->filename, knmfilename);
                    }
        
                    // if the number of maps read so far is different to the map
                    // index there is a problem
                    if(mn != i)
                        bug_error("Problem converting line '%s' as the map index did not increase by 1 each time", buf);

                    i++; // keep track of how many maps we have read
                    
                } else
                    break;
            }
            
            // check that hte number of maps read is the same as what was specified in the file
            if(mapnum != i)
                bug_error("Only %i maps were loaded when %i should have been",i, mapnum);

            //reset to the start of the line so the next read works
            fseek(file, currpos, SEEK_SET);
        }

        READ(sscanf(buf, "integration method : %i", &map->_intmeth));
        READ(sscanf(buf, "interpolation method : %i", &map->_intermeth));
        READ(sscanf(buf, "interpolation size : %i", &map->_intersize));

        // if the interpolation method, size or the integration method used
        // has changed then return false and recompute the map data
        if (map->_intmeth != map->integration_method && !converting) {
            if (inter.debug && !options.quiet)
                message("* KNM file read: Different integration method used on component %s\n", name);
            return false;
        }
        if (map->_intermeth != map->interpolation_method && !converting) {
            if (inter.debug && !options.quiet)
                message("* KNM file read: Different interpolation method used on component %s\n", name);
            return false;
        }
        if (map->_intersize != map->interpolation_size && !converting) {
            if (inter.debug && !options.quiet)
                message("* KNM file read: Different interpolation size used on component %s\n", name);
            return false;
        }
        if(knmcmp == MIRROR_CMP){
            READ(sscanf(buf, "qx1_11 : %lg %lg", &(map->mirror_knm_q.qxt1_11.re), &(map->mirror_knm_q.qxt1_11.im)));
            READ(sscanf(buf, "qy1_11 : %lg %lg", &(map->mirror_knm_q.qyt1_11.re), &(map->mirror_knm_q.qyt1_11.im)));
            READ(sscanf(buf, "qx2_11 : %lg %lg", &(map->mirror_knm_q.qxt2_11.re), &(map->mirror_knm_q.qxt2_11.im)));
            READ(sscanf(buf, "qy2_11 : %lg %lg", &(map->mirror_knm_q.qyt2_11.re), &(map->mirror_knm_q.qyt2_11.im)));

            READ(sscanf(buf, "qx1_12 : %lg %lg", &(map->mirror_knm_q.qxt1_12.re), &(map->mirror_knm_q.qxt1_12.im)));
            READ(sscanf(buf, "qy1_12 : %lg %lg", &(map->mirror_knm_q.qyt1_12.re), &(map->mirror_knm_q.qyt1_12.im)));
            READ(sscanf(buf, "qx2_12 : %lg %lg", &(map->mirror_knm_q.qxt2_12.re), &(map->mirror_knm_q.qxt2_12.im)));
            READ(sscanf(buf, "qy2_12 : %lg %lg", &(map->mirror_knm_q.qyt2_12.re), &(map->mirror_knm_q.qyt2_12.im)));

            READ(sscanf(buf, "qx1_21 : %lg %lg", &(map->mirror_knm_q.qxt1_21.re), &(map->mirror_knm_q.qxt1_21.im)));
            READ(sscanf(buf, "qy1_21 : %lg %lg", &(map->mirror_knm_q.qyt1_21.re), &(map->mirror_knm_q.qyt1_21.im)));
            READ(sscanf(buf, "qx2_21 : %lg %lg", &(map->mirror_knm_q.qxt2_21.re), &(map->mirror_knm_q.qxt2_21.im)));
            READ(sscanf(buf, "qy2_21 : %lg %lg", &(map->mirror_knm_q.qyt2_21.re), &(map->mirror_knm_q.qyt2_21.im)));

            READ(sscanf(buf, "qx1_22 : %lg %lg", &(map->mirror_knm_q.qxt1_22.re), &(map->mirror_knm_q.qxt1_22.im)));
            READ(sscanf(buf, "qy1_22 : %lg %lg", &(map->mirror_knm_q.qyt1_22.re), &(map->mirror_knm_q.qyt1_22.im)));
            READ(sscanf(buf, "qx2_22 : %lg %lg", &(map->mirror_knm_q.qxt2_22.re), &(map->mirror_knm_q.qxt2_22.im)));
            READ(sscanf(buf, "qy2_22 : %lg %lg", &(map->mirror_knm_q.qyt2_22.re), &(map->mirror_knm_q.qyt2_22.im)));

        }else if(knmcmp == BEAMSPLITTER_CMP){
            READ(sscanf(buf, "qx1_12 : %lg %lg", &(map->bs_knm_q.qxt1_12.re), &(map->bs_knm_q.qxt1_12.im)));
            READ(sscanf(buf, "qy1_12 : %lg %lg", &(map->bs_knm_q.qyt1_12.re), &(map->bs_knm_q.qyt1_12.im)));
            READ(sscanf(buf, "qx2_12 : %lg %lg", &(map->bs_knm_q.qxt2_12.re), &(map->bs_knm_q.qxt2_12.im)));
            READ(sscanf(buf, "qy2_12 : %lg %lg", &(map->bs_knm_q.qyt2_12.re), &(map->bs_knm_q.qyt2_12.im)));

            READ(sscanf(buf, "qx1_21 : %lg %lg", &(map->bs_knm_q.qxt1_21.re), &(map->bs_knm_q.qxt1_21.im)));
            READ(sscanf(buf, "qy1_21 : %lg %lg", &(map->bs_knm_q.qyt1_21.re), &(map->bs_knm_q.qyt1_21.im)));
            READ(sscanf(buf, "qx2_21 : %lg %lg", &(map->bs_knm_q.qxt2_21.re), &(map->bs_knm_q.qxt2_21.im)));
            READ(sscanf(buf, "qy2_21 : %lg %lg", &(map->bs_knm_q.qyt2_21.re), &(map->bs_knm_q.qyt2_21.im)));

            READ(sscanf(buf, "qx1_34 : %lg %lg", &(map->bs_knm_q.qxt1_34.re), &(map->bs_knm_q.qxt1_34.im)));
            READ(sscanf(buf, "qy1_34 : %lg %lg", &(map->bs_knm_q.qyt1_34.re), &(map->bs_knm_q.qyt1_34.im)));
            READ(sscanf(buf, "qx2_34 : %lg %lg", &(map->bs_knm_q.qxt2_34.re), &(map->bs_knm_q.qxt2_34.im)));
            READ(sscanf(buf, "qy2_34 : %lg %lg", &(map->bs_knm_q.qyt2_34.re), &(map->bs_knm_q.qyt2_34.im)));

            READ(sscanf(buf, "qx1_43 : %lg %lg", &(map->bs_knm_q.qxt1_43.re), &(map->bs_knm_q.qxt1_43.im)));
            READ(sscanf(buf, "qy1_43 : %lg %lg", &(map->bs_knm_q.qyt1_43.re), &(map->bs_knm_q.qyt1_43.im)));
            READ(sscanf(buf, "qx2_43 : %lg %lg", &(map->bs_knm_q.qxt2_43.re), &(map->bs_knm_q.qxt2_43.im)));
            READ(sscanf(buf, "qy2_43 : %lg %lg", &(map->bs_knm_q.qyt2_43.re), &(map->bs_knm_q.qyt2_43.im)));
            
            READ(sscanf(buf, "qx1_13 : %lg %lg", &(map->bs_knm_q.qxt1_13.re), &(map->bs_knm_q.qxt1_13.im)));
            READ(sscanf(buf, "qy1_13 : %lg %lg", &(map->bs_knm_q.qyt1_13.re), &(map->bs_knm_q.qyt1_13.im)));
            READ(sscanf(buf, "qx2_13 : %lg %lg", &(map->bs_knm_q.qxt2_13.re), &(map->bs_knm_q.qxt2_13.im)));
            READ(sscanf(buf, "qy2_13 : %lg %lg", &(map->bs_knm_q.qyt2_13.re), &(map->bs_knm_q.qyt2_13.im)));

            READ(sscanf(buf, "qx1_31 : %lg %lg", &(map->bs_knm_q.qxt1_31.re), &(map->bs_knm_q.qxt1_31.im)));
            READ(sscanf(buf, "qy1_31 : %lg %lg", &(map->bs_knm_q.qyt1_31.re), &(map->bs_knm_q.qyt1_31.im)));
            READ(sscanf(buf, "qx2_31 : %lg %lg", &(map->bs_knm_q.qxt2_31.re), &(map->bs_knm_q.qxt2_31.im)));
            READ(sscanf(buf, "qy2_31 : %lg %lg", &(map->bs_knm_q.qyt2_31.re), &(map->bs_knm_q.qyt2_31.im)));

            READ(sscanf(buf, "qx1_24 : %lg %lg", &(map->bs_knm_q.qxt1_24.re), &(map->bs_knm_q.qxt1_24.im)));
            READ(sscanf(buf, "qy1_24 : %lg %lg", &(map->bs_knm_q.qyt1_24.re), &(map->bs_knm_q.qyt1_24.im)));
            READ(sscanf(buf, "qx2_24 : %lg %lg", &(map->bs_knm_q.qxt2_24.re), &(map->bs_knm_q.qxt2_24.im)));
            READ(sscanf(buf, "qy2_24 : %lg %lg", &(map->bs_knm_q.qyt2_24.re), &(map->bs_knm_q.qyt2_24.im)));

            READ(sscanf(buf, "qx1_42 : %lg %lg", &(map->bs_knm_q.qxt1_42.re), &(map->bs_knm_q.qxt1_42.im)));
            READ(sscanf(buf, "qy1_42 : %lg %lg", &(map->bs_knm_q.qyt1_42.re), &(map->bs_knm_q.qyt1_42.im)));
            READ(sscanf(buf, "qx2_42 : %lg %lg", &(map->bs_knm_q.qxt2_42.re), &(map->bs_knm_q.qxt2_42.im)));
            READ(sscanf(buf, "qy2_42 : %lg %lg", &(map->bs_knm_q.qyt2_42.re), &(map->bs_knm_q.qyt2_42.im)));
        }

        READ(sscanf(buf, "nr1 : %lg", &(map->_nr1)));
        READ(sscanf(buf, "nr2 : %lg", &(map->_nr2)));
        READ(sscanf(buf, "lambda : %lg", &(map->_lambda)));
        READ(sscanf(buf, "map rotation angle : %lg\n", &(map->_angle)));
        READ(sscanf(buf, "angle of incidence : %lg\n", &(map->_AoI)));
        READ(sscanf(buf, "maxtem : %i", &(map->_maxtem)));
        READ(sscanf(buf, "r_aperture : %lg", &(map->_r_aperture)));
        READ(sscanf(buf, "xbeta : %lg", &(map->_xbeta)));
        READ(sscanf(buf, "ybeta : %lg", &(map->_ybeta)));

        READ(sscanf(buf, "rows : %i", &(map->rows)));
        READ(sscanf(buf, "cols : %i", &(map->cols)));
        READ(sscanf(buf, "xstep : %lg", &(map->xstep)));
        READ(sscanf(buf, "ystep : %lg", &(map->ystep)));
        READ(sscanf(buf, "x0 : %lg", &(map->x0)));
        READ(sscanf(buf, "y0 : %lg", &(map->y0)));

        READ(sscanf(buf, "abserr : %lg", &(map->_abserr)));
        READ(sscanf(buf, "relerr : %lg", &(map->_relerr)));

        if (init.abserr != map->_abserr && !converting) {
            if (inter.debug && !options.quiet)
                message("* KNM file read: Different absolute error compared to file %s\n", knmfilename);
            return false;
        }

        if (init.relerr != map->_relerr && !converting) {
            if (inter.debug && !options.quiet)
                message("* KNM file read: Different relative error compared to file %s\n", knmfilename);
            return false;
        }

        int save_binary;
        READ(sscanf(buf, "knm_binary : %i", &save_binary));

        if (!save_binary && map->knm_save_binary && !converting)
            gerror("Mirror %s is set to use binary knm and map data files, but the\n"
                "    saved values were generated in ASCII. Please delete the files\n"
                "    and generate them again in binary or switch the components save\n"
                "    mode to ASCII.\n", name);

        if (save_binary && !map->knm_save_binary && !converting)
            gerror("Mirror %s is set not to use binary knm and map data files, but the\n"
                "    saved values were generated in binary. Please delete the files and\n"
                "    generate them again in ASCII or switch the components save mode to binary.\n", name);

        // if we are converting then we need to store the save method
        // so we can read the map files later
        if (converting)
            map->knm_save_binary = save_binary;

        // Now read the knm's
        READ(sscanf(buf, "%% -knm%s", buf));

        int m, n1, m1, n2, m2;
        int num_knms;

        READ(sscanf(buf, "num_knms : %i", &num_knms));

        complex_t t11, t12, t21, t22;
        complex_t t34, t43, t13, t31, t24, t42;
        
        // allocate some memory to store the n1..m2 for each element in
        // we do this becaues the get_field_index_from_tem function
        // wont work as nothing has been initialised and the map->knm allocation
        // below
        int num_fields = (int) ((map->_maxtem + 1) * (map->_maxtem + 2) / 2);

        if (converting) {
            nms = (n1n2m1m2_t*) malloc(num_fields * num_fields * sizeof (n1n2m1m2_t));

            // we also have to set inter.tem/mem.num_fields to use get_field_index_from_tem
            // and allocate the memory
            inter.tem = map->_maxtem;
            mem.num_fields = map->_maxtem;
            long int bytes=0;
            
            if(knmcmp == BEAMSPLITTER_CMP){
                bs_knm_alloc((bs_knm_t*)knm, &bytes);
            } else if(knmcmp == MIRROR_CMP){
                mirror_knm_alloc((mirror_knm_t*)knm, &bytes);
            } else
                bug_error("Could not determine knmcmp argument in read_knm_file ");
        }
        
        // allocate memory to store the knm values in. This allocates
        // enough memory to deal with the current simulation setting
        // of maxtem
        if(knmcmp == BEAMSPLITTER_CMP){
            if (!IS_BS_KNM_ALLOCD(((bs_knm_t*)knm)))
                bug_error("beamsplitter knm has not been allocated");
        } else if(knmcmp == MIRROR_CMP){
            if (!IS_MIRROR_KNM_ALLOCD(((mirror_knm_t*)knm)))
                bug_error("mirror knm has not been allocated");
        } else
            bug_error("Could not determine knmcmp argument in read_knm_file ");

        if (map->knm_save_binary)
            _read_knm_binary_data(name, map, knm, converting, knmcmp);
        else {
            if (inter.debug & 128 && !options.quiet)
                message("* Reading knm data for component %s from %s\n", name, knmfilename);

            for (n = 0; n < num_fields; n++) {
                for (m = 0; m < num_fields; m++) {
                    if(knmcmp == MIRROR_CMP){
                        READ(sscanf(buf, "%d %d %d %d   %lg %lg %lg %lg %lg %lg %lg %lg"
                            , &n1, &m1, &n2, &m2
                            , &(t11.re), &(t11.im)
                            , &(t22.re), &(t22.im)
                            , &(t12.re), &(t12.im)
                            , &(t21.re), &(t21.im)));
                    }else{
                        READ(sscanf(buf, "%d %d %d %d   %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg"
                            , &n1, &m1, &n2, &m2
                            , &(t12.re), &(t12.im)
                            , &(t21.re), &(t21.im)
                            , &(t34.re), &(t34.im)
                            , &(t43.re), &(t43.im)
                            , &(t13.re), &(t13.im)
                            , &(t31.re), &(t31.im)
                            , &(t24.re), &(t24.im)
                            , &(t42.re), &(t42.im)));
                    }

                    if (converting) {
                        nms[n + num_fields * m].n1 = n1;
                        nms[n + num_fields * m].n2 = n2;
                        nms[n + num_fields * m].m1 = m1;
                        nms[n + num_fields * m].m2 = m2;
                    }
                    
                    if (((n1 + m1) <= inter.tem) && ((n2 + m2) <= inter.tem)) {
                        int field1 = get_field_index_from_tem(n1, m1);
                        int field2 = get_field_index_from_tem(n2, m2);

                        if(knmcmp == BEAMSPLITTER_CMP){
                            (*((bs_knm_t*)knm)).k12[field1][field2] = t12;
                            (*((bs_knm_t*)knm)).k21[field1][field2] = t21;
                            (*((bs_knm_t*)knm)).k34[field1][field2] = t34;
                            (*((bs_knm_t*)knm)).k43[field1][field2] = t43;
                            (*((bs_knm_t*)knm)).k13[field1][field2] = t13;
                            (*((bs_knm_t*)knm)).k31[field1][field2] = t31;
                            (*((bs_knm_t*)knm)).k24[field1][field2] = t24;
                            (*((bs_knm_t*)knm)).k42[field1][field2] = t42;
                        } else if(knmcmp == MIRROR_CMP){
                            (*((mirror_knm_t*)knm)).k11[field1][field2] = t11;
                            (*((mirror_knm_t*)knm)).k12[field1][field2] = t12;
                            (*((mirror_knm_t*)knm)).k21[field1][field2] = t21;
                            (*((mirror_knm_t*)knm)).k22[field1][field2] = t22;
                        } else
                            bug_error("Could not determine knmcmp argument in read_knm_file ");
                    }
                }
            }
        }

        // now we have loaded in the calculated knms flag it
        map->knm_calculated = true;

        if (!map->noMapPresent) {
            // allocate memory to store the amplitude and phase data in
            alloc_merged_map_data(map);

            if (map->knm_save_binary) {
                _read_merged_map_binary_data(name, map);
            } else {
                // Code will fail from here on if it cannot find the correct map files
                _read_merged_map_ascii_data(name, map, R_ABS); // reads r_abs
                _read_merged_map_ascii_data(name, map, R_PHS); // reads r_phs
                _read_merged_map_ascii_data(name, map, T_ABS); // reads t_abs
                _read_merged_map_ascii_data(name, map, T_PHS); // reads t_phs
            }
        }

        // Finally we have successfully loaded the data and various parameters
        // now return true stating that the map merging does not need doing.
        return true;
    }
}

/**
 * The knmcmp must be set correctly to match the pointer you are passing in!
 * Otherwise all sorts of memory reading problems are likely.
 * 
 * @param map
 * @param knm pointer to knm storage struct, i.e. mirror_knm_t, bs_knm_t
 * @param converting true is we are converting the files between ascii<->binary
 * @param knmcmp
 */
void _write_knm_binary_data(surface_merged_map_t *map, void *knm, bool converting, KNM_COMPONENT_t knmcmp) {
    FILE *file;
    unsigned char* filebuf;
    unsigned long filesize;
    char buf[LINE_LEN];
    char filename[FILENAME_MAX];
    MD5_CTX ctx;
        
    sprintf(buf, "%s.knm.bin", map->filename);
    strcpy(filename, buf);

    MD5Init(&ctx);

    int num_fields;

    // if converting we won't have num_fields calculated proprely
    if (converting)
        num_fields = (int) (map->_maxtem + 1) * (map->_maxtem + 2) / 2;
    else
        num_fields = mem.num_fields;

    int numknm = num_fields*num_fields;

    //calculate length that the file should be
    filesize = sizeof (ctx.digest);
    filesize += sizeof (int);
    // save space for the n1,n2,m1,m2 for each knm
    filesize += numknm * 4 * sizeof (unsigned char);
    
    // save space for each complex knm
    if(knmcmp == MIRROR_CMP)
        filesize += numknm * 4 * (sizeof (complex_t));
    else if(knmcmp == BEAMSPLITTER_CMP)
        filesize += numknm * 8 * (sizeof (complex_t));
    else
        bug_error("couldn't handle knmcmp in _write_knm_binary_data");
    
    filebuf = (unsigned char*) malloc(filesize);

    if (filebuf == NULL)
        bug_error("Out of memory: Could not allocate enough memory to create binary file buffer.");

    open_file_to_write_binary(filename, &file);

    // finally write everything to the file
    if (inter.debug && !options.quiet)
        message("* Writing knm binary data to %s...\n", filename);

    //seek to the byte after the md5 checksum as it will be written later
    int pos = sizeof (ctx.digest);

    memcpy(&(filebuf[pos]), &numknm, sizeof (numknm));
    pos += sizeof (numknm);

    int n, m, n1, n2, m1, m2;
    size_t cs = sizeof (complex_t);
    unsigned char _n1, _n2, _m1, _m2;

    for (n = 0; n < num_fields; n++) {
        for (m = 0; m < num_fields; m++) {
            if (converting) {
                n1 = nms[n + num_fields * m].n1;
                n2 = nms[n + num_fields * m].n2;
                m1 = nms[n + num_fields * m].m1;
                m2 = nms[n + num_fields * m].m2;
            } else {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
            }

            _n1 = (unsigned char) n1;
            _m1 = (unsigned char) m1;
            _n2 = (unsigned char) n2;
            _m2 = (unsigned char) m2;

            memcpy(&(filebuf[pos]), &_n1, 1);
            pos += 1;
            memcpy(&(filebuf[pos]), &_n2, 1);
            pos += 1;
            memcpy(&(filebuf[pos]), &_m1, 1);
            pos += 1;
            memcpy(&(filebuf[pos]), &_m2, 1);
            pos += 1;
            
            switch(knmcmp){
                case MIRROR_CMP:
                    memcpy(&(filebuf[pos]), &(((mirror_knm_t*)knm)->k11[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((mirror_knm_t*)knm)->k12[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((mirror_knm_t*)knm)->k21[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((mirror_knm_t*)knm)->k22[n][m]), cs);
                    pos += cs;
                    break;
                case BEAMSPLITTER_CMP:
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k12[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k21[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k34[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k43[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k13[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k31[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k24[n][m]), cs);
                    pos += cs;
                    memcpy(&(filebuf[pos]), &(((bs_knm_t*)knm)->k42[n][m]), cs);
                    pos += cs;
                    break;
                default:
                    bug_error("Could not handle knm type in _write_knm_binary_data");
            }
        }
    }

    MD5Update(&ctx, &filebuf[sizeof (ctx.digest)], filesize - sizeof (ctx.digest));
    MD5Final(&ctx);
    memcpy(filebuf, ctx.digest, sizeof (ctx.digest));

    fwrite(filebuf, 1, filesize, file);
    fflush(file);
    fclose(file);

    free(filebuf);
}

void _write_merged_map_binary_data(surface_merged_map_t *map) {

    FILE *file = NULL;
    unsigned char* filebuf = NULL;
    unsigned long filesize;
    char buf[LINE_LEN];
    char filename[FILENAME_MAX];
    MD5_CTX ctx;

    sprintf(buf, "%s.map.bin", map->filename);
    strcpy(filename, buf);

    MD5Init(&ctx);

    //calculate length that the file should be
    filesize = sizeof (ctx.digest);
    filesize += sizeof (map->rows);
    filesize += sizeof (map->cols);
    filesize += sizeof (double) * 4 * map->rows * map->cols;

#if DEBUG
    filebuf = (unsigned char*) calloc(filesize, sizeof (unsigned char));
#else
    filebuf = (unsigned char*) malloc(filesize);
#endif

    if (filebuf == NULL)
        bug_error("Out of memory: Could not allocate enough memory to create binary file buffer.");

    unsigned long pos = 0;
    pos += sizeof (ctx.digest);

    memcpy(&(filebuf[pos]), &map->rows, sizeof (map->rows));
    pos += sizeof (map->rows);

    memcpy(&(filebuf[pos]), &map->cols, sizeof (map->cols));
    pos += sizeof (map->cols);

    int i;
    size_t s = sizeof (map->r_abs[0][0]);

    if (s != sizeof (map->r_phs[0][0]) || s != sizeof (map->t_abs[0][0]) || s != sizeof (map->t_phs[0][0]))
        bug_error("The byte size of the various merged map elements is not the same!");

    unsigned int bytes = map->rows * s;

    for (i = 0; i < map->cols; i++) {
        memcpy(&(filebuf[pos]), map->r_abs[i], bytes);
        pos += bytes;
        memcpy(&(filebuf[pos]), map->r_phs[i], bytes);
        pos += bytes;
        memcpy(&(filebuf[pos]), map->t_abs[i], bytes);
        pos += bytes;
        memcpy(&(filebuf[pos]), map->t_phs[i], bytes);
        pos += bytes;

        if (pos > filesize)
            bug_error("Tried to write more data than was allocated whilst writing binary map data\n");
    }

    //create a md5 checksum for all the file apart from the checksum itself
    MD5Update(&ctx, &filebuf[16], filesize - 16);
    MD5Final(&ctx);

    // copy the md5 key to file buffer
    memcpy(filebuf, ctx.digest, sizeof (ctx.digest));

    open_file_to_write_binary(filename, &file);
    // finally write everything to the file
    message("* Writing to merged map binary data to %s...\n", filename);
    fwrite(filebuf, sizeof (filebuf[0]), filesize, file);
    fflush(file);
    fclose(file);

    free(filebuf);
}

void _write_merged_map_ascii_data(char* name, surface_merged_map_t *map, MAP_COMPONENTS_t num) {
    double **data = NULL;
    char buf[LINE_LEN];
    char filenamemap[FILENAME_MAX];
    FILE *file = NULL;

    switch (num) {
        case R_ABS:
            data = map->r_abs;
            sprintf(buf, "%s_r_abs.map", map->filename);
            break;
        case R_PHS:
            data = map->r_phs;
            sprintf(buf, "%s_r_phs.map", map->filename);
            break;
        case T_ABS:
            data = map->t_abs;
            sprintf(buf, "%s_t_abs.map", map->filename);
            break;
        case T_PHS:
            data = map->t_phs;
            sprintf(buf, "%s_t_phs.map", map->filename);
            break;
        default:
            bug_error("wrong map data format");
    }

    strcpy(filenamemap, buf);
    
    if (inter.debug && !options.quiet)
        message("* Writing merged map data for mirror '%s' to file '%s'\n", name, filenamemap);
    
    open_file_to_write_ascii(filenamemap, &file);

    int i, j;

    for (i = 0; i < map->cols; i++) {
        for (j = 0; j < map->rows; j++) { //boom cols -> rows 300313 adf
            fprintf(file, "%.15g ", data[i][j]);
        }
        fprintf(file, "\n");
    }

    fprintf(file, "\n");

    fclose(file);
}


void write_knm_file(void *cmp, bool converting, KNM_COMPONENT_t knmcmp) {
    FILE *file = NULL;
    char *name = NULL;
    surface_map_t **maps = NULL;
    surface_merged_map_t *map = NULL;
    void* knm = NULL;
    int num_maps = 0; 
    
    if(knmcmp == MIRROR_CMP) {
        mirror_t *mirror = (mirror_t*)cmp;
        
        name = mirror->name;
        map = &mirror->map_merged;
        knm = (void*)&mirror->knm_map;
        num_maps = mirror->num_maps;
        maps = mirror->map;
        
    } else if(knmcmp == BEAMSPLITTER_CMP) {
        beamsplitter_t *bs = (beamsplitter_t*)cmp;
        name = bs->name;
        map = &bs->map_merged;
        knm = (void*)&bs->knm_map;
        num_maps = bs->num_maps;
        maps = bs->map;
        
    } else
        bug_error("Couldn't handle component in write_knm_file()");
    
    assert(map != NULL);
    assert(knm != NULL);

    //surface_merged_map_t *map = &(mirror->map_merged);

    assert(map != NULL);

    char filename[FILENAME_MAX];

    sprintf(filename, "%s.knm", map->filename);

    if (inter.debug && !options.quiet)
        message("* Saving knm information for component '%s' in file '%s'\n", name, filename);

    open_file_to_write_ascii(filename, &file);

    // Start writing header

    time_t now = time(NULL);
    fprintf(file, "%% Written by Finesse(%s)  -  %s", GIT_REVISION, asctime(localtime(&now)));
    fprintf(file, "%% Input file '%s'\n", vglobal.input_fname);
    fprintf(file, "%% --DO NOT CHANGE THIS FILE---------------------------------\n");
    
    if(knmcmp == MIRROR_CMP)
        fprintf(file, "component : MIRROR\n");
    else if(knmcmp == BEAMSPLITTER_CMP)
        fprintf(file, "component : BEAMSPLITTER\n");
    else
        bug_error("Did not recognise knmcomp argument");
    
    fprintf(file, "mapnum : %i\n", num_maps);
    fprintf(file, "transonly : %i\n", map->phaseIsOnlyTransmission);
    fprintf(file, "hasreflmap : %i\n", map->hasReflectivityMap);
    
    int i;
    for (i = 0; i < num_maps; i++) {
        char *B64_hash;
        B64_hash = encode_base64(sizeof (maps[i]->hash), maps[i]->hash);
        fprintf(file, "map%i : %s %s\n", i, maps[i]->filename, B64_hash);
        free(B64_hash);
    }
    
    fprintf(file, "integration method : %i\n", (converting) ? map->_intmeth : map->integration_method);
    fprintf(file, "interpolation method : %i\n", (converting) ? map->_intermeth : map->interpolation_method);
    fprintf(file, "interpolation size : %i\n", (converting) ? map->_intersize : map->interpolation_size);
    
    
    if(knmcmp == MIRROR_CMP){
        fprintf(file, "qx1_11 : %.15g %.15g\n", map->mirror_knm_q.qxt1_11.re, map->mirror_knm_q.qxt1_11.im);
        fprintf(file, "qy1_11 : %.15g %.15g\n", map->mirror_knm_q.qyt1_11.re, map->mirror_knm_q.qyt1_11.im);
        fprintf(file, "qx2_11 : %.15g %.15g\n", map->mirror_knm_q.qxt2_11.re, map->mirror_knm_q.qxt2_11.im);
        fprintf(file, "qy2_11 : %.15g %.15g\n", map->mirror_knm_q.qyt2_11.re, map->mirror_knm_q.qyt2_11.im);
        
        fprintf(file, "qx1_12 : %.15g %.15g\n", map->mirror_knm_q.qxt1_12.re, map->mirror_knm_q.qxt1_12.im);
        fprintf(file, "qy1_12 : %.15g %.15g\n", map->mirror_knm_q.qyt1_12.re, map->mirror_knm_q.qyt1_12.im);
        fprintf(file, "qx2_12 : %.15g %.15g\n", map->mirror_knm_q.qxt2_12.re, map->mirror_knm_q.qxt2_12.im);
        fprintf(file, "qy2_12 : %.15g %.15g\n", map->mirror_knm_q.qyt2_12.re, map->mirror_knm_q.qyt2_12.im);

        fprintf(file, "qx1_21 : %.15g %.15g\n", map->mirror_knm_q.qxt1_21.re, map->mirror_knm_q.qxt1_21.im);
        fprintf(file, "qy1_21 : %.15g %.15g\n", map->mirror_knm_q.qyt1_21.re, map->mirror_knm_q.qyt1_21.im);
        fprintf(file, "qx2_21 : %.15g %.15g\n", map->mirror_knm_q.qxt2_21.re, map->mirror_knm_q.qxt2_21.im);
        fprintf(file, "qy2_21 : %.15g %.15g\n", map->mirror_knm_q.qyt2_21.re, map->mirror_knm_q.qyt2_21.im);

        fprintf(file, "qx1_22 : %.15g %.15g\n", map->mirror_knm_q.qxt1_22.re, map->mirror_knm_q.qxt1_22.im);
        fprintf(file, "qy1_22 : %.15g %.15g\n", map->mirror_knm_q.qyt1_22.re, map->mirror_knm_q.qyt1_22.im);
        fprintf(file, "qx2_22 : %.15g %.15g\n", map->mirror_knm_q.qxt2_22.re, map->mirror_knm_q.qxt2_22.im);
        fprintf(file, "qy2_22 : %.15g %.15g\n", map->mirror_knm_q.qyt2_22.re, map->mirror_knm_q.qyt2_22.im);
        
    }else if(knmcmp == BEAMSPLITTER_CMP){
        fprintf(file, "qx1_12 : %.15g %.15g\n", map->bs_knm_q.qxt1_12.re, map->bs_knm_q.qxt1_12.im);
        fprintf(file, "qy1_12 : %.15g %.15g\n", map->bs_knm_q.qyt1_12.re, map->bs_knm_q.qyt1_12.im);
        fprintf(file, "qx2_12 : %.15g %.15g\n", map->bs_knm_q.qxt2_12.re, map->bs_knm_q.qxt2_12.im);
        fprintf(file, "qy2_12 : %.15g %.15g\n", map->bs_knm_q.qyt2_12.re, map->bs_knm_q.qyt2_12.im);

        fprintf(file, "qx1_21 : %.15g %.15g\n", map->bs_knm_q.qxt1_21.re, map->bs_knm_q.qxt1_21.im);
        fprintf(file, "qy1_21 : %.15g %.15g\n", map->bs_knm_q.qyt1_21.re, map->bs_knm_q.qyt1_21.im);
        fprintf(file, "qx2_21 : %.15g %.15g\n", map->bs_knm_q.qxt2_21.re, map->bs_knm_q.qxt2_21.im);
        fprintf(file, "qy2_21 : %.15g %.15g\n", map->bs_knm_q.qyt2_21.re, map->bs_knm_q.qyt2_21.im);

        fprintf(file, "qx1_34 : %.15g %.15g\n", map->bs_knm_q.qxt1_34.re, map->bs_knm_q.qxt1_34.im);
        fprintf(file, "qy1_34 : %.15g %.15g\n", map->bs_knm_q.qyt1_34.re, map->bs_knm_q.qyt1_34.im);
        fprintf(file, "qx2_34 : %.15g %.15g\n", map->bs_knm_q.qxt2_34.re, map->bs_knm_q.qxt2_34.im);
        fprintf(file, "qy2_34 : %.15g %.15g\n", map->bs_knm_q.qyt2_34.re, map->bs_knm_q.qyt2_34.im);

        fprintf(file, "qx1_43 : %.15g %.15g\n", map->bs_knm_q.qxt1_43.re, map->bs_knm_q.qxt1_43.im);
        fprintf(file, "qy1_43 : %.15g %.15g\n", map->bs_knm_q.qyt1_43.re, map->bs_knm_q.qyt1_43.im);
        fprintf(file, "qx2_43 : %.15g %.15g\n", map->bs_knm_q.qxt2_43.re, map->bs_knm_q.qxt2_43.im);
        fprintf(file, "qy2_43 : %.15g %.15g\n", map->bs_knm_q.qyt2_43.re, map->bs_knm_q.qyt2_43.im);

        fprintf(file, "qx1_13 : %.15g %.15g\n", map->bs_knm_q.qxt1_13.re, map->bs_knm_q.qxt1_13.im);
        fprintf(file, "qy1_13 : %.15g %.15g\n", map->bs_knm_q.qyt1_13.re, map->bs_knm_q.qyt1_13.im);
        fprintf(file, "qx2_13 : %.15g %.15g\n", map->bs_knm_q.qxt2_13.re, map->bs_knm_q.qxt2_13.im);
        fprintf(file, "qy2_13 : %.15g %.15g\n", map->bs_knm_q.qyt2_13.re, map->bs_knm_q.qyt2_13.im);

        fprintf(file, "qx1_31 : %.15g %.15g\n", map->bs_knm_q.qxt1_31.re, map->bs_knm_q.qxt1_31.im);
        fprintf(file, "qy1_31 : %.15g %.15g\n", map->bs_knm_q.qyt1_31.re, map->bs_knm_q.qyt1_31.im);
        fprintf(file, "qx2_31 : %.15g %.15g\n", map->bs_knm_q.qxt2_31.re, map->bs_knm_q.qxt2_31.im);
        fprintf(file, "qy2_31 : %.15g %.15g\n", map->bs_knm_q.qyt2_31.re, map->bs_knm_q.qyt2_31.im);

        fprintf(file, "qx1_24 : %.15g %.15g\n", map->bs_knm_q.qxt1_24.re, map->bs_knm_q.qxt1_24.im);
        fprintf(file, "qy1_24 : %.15g %.15g\n", map->bs_knm_q.qyt1_24.re, map->bs_knm_q.qyt1_24.im);
        fprintf(file, "qx2_24 : %.15g %.15g\n", map->bs_knm_q.qxt2_24.re, map->bs_knm_q.qxt2_24.im);
        fprintf(file, "qy2_24 : %.15g %.15g\n", map->bs_knm_q.qyt2_24.re, map->bs_knm_q.qyt2_24.im);

        fprintf(file, "qx1_42 : %.15g %.15g\n", map->bs_knm_q.qxt1_42.re, map->bs_knm_q.qxt1_42.im);
        fprintf(file, "qy1_42 : %.15g %.15g\n", map->bs_knm_q.qyt1_42.re, map->bs_knm_q.qyt1_42.im);
        fprintf(file, "qx2_42 : %.15g %.15g\n", map->bs_knm_q.qxt2_42.re, map->bs_knm_q.qxt2_42.im);
        fprintf(file, "qy2_42 : %.15g %.15g\n", map->bs_knm_q.qyt2_42.re, map->bs_knm_q.qyt2_42.im);

    }
    
    fprintf(file, "nr1 : %.15g\n", map->_nr1);
    fprintf(file, "nr2 : %.15g\n", map->_nr2);
    fprintf(file, "lambda : %.15g\n", map->_lambda);
    fprintf(file, "map rotation angle : %.15g\n", map->_angle);
    fprintf(file, "angle of incidence : %.15g\n", map->_AoI);
    fprintf(file, "maxtem : %i\n", map->_maxtem);
    fprintf(file, "r_aperture : %.15g\n", map->_r_aperture);
    fprintf(file, "xbeta : %.15g\n", map->_xbeta);
    fprintf(file, "ybeta : %.15g\n", map->_ybeta);
    fprintf(file, "rows : %i\n", map->rows);
    fprintf(file, "cols : %i\n", map->cols);
    fprintf(file, "xstep : %.15g\n", map->xstep);
    fprintf(file, "ystep : %.15g\n", map->ystep);
    fprintf(file, "x0 : %.15g\n", map->x0);
    fprintf(file, "y0 : %.15g\n", map->y0);
    fprintf(file, "abserr : %.15g\n", (converting) ? map->_abserr : init.abserr);
    fprintf(file, "relerr : %.15g\n", (converting) ? map->_relerr : init.relerr);
    fprintf(file, "knm_binary : %i\n", map->knm_save_binary);

    // End writing header

    // Start writing knm

    int num_fields;

    // if converting we won't have num_fields calculated proprely
    if (converting)
        num_fields = (int) (map->_maxtem + 1) * (map->_maxtem + 2) / 2;
    else
        num_fields = mem.num_fields;

    fprintf(file, "%% -knm\n");
    fprintf(file, "num_knms : %i\n", num_fields * num_fields);

    if (map->knm_save_binary) {
        _write_knm_binary_data(map, knm, converting, knmcmp);
    } else {
        int n, m, n1, m1, n2, m2;

        for (n = 0; n < num_fields; n++) {
            for (m = 0; m < num_fields; m++) {
                if (converting) {
                    n1 = nms[n + num_fields * m].n1;
                    n2 = nms[n + num_fields * m].n2;
                    m1 = nms[n + num_fields * m].m1;
                    m2 = nms[n + num_fields * m].m2;
                } else {
                    get_tem_modes_from_field_index(&n1, &m1, n);
                    get_tem_modes_from_field_index(&n2, &m2, m);
                }

                if(knmcmp == MIRROR_CMP){
                    fprintf(file, "%d %d %d %d   ", n1, m1, n2, m2);
                    fprintf(file, "%.15g %.15g ", ((mirror_knm_t*)knm)->k11[n][m].re, ((mirror_knm_t*)knm)->k11[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((mirror_knm_t*)knm)->k22[n][m].re, ((mirror_knm_t*)knm)->k22[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((mirror_knm_t*)knm)->k12[n][m].re, ((mirror_knm_t*)knm)->k12[n][m].im);
                    fprintf(file, "%.15g %.15g\n", ((mirror_knm_t*)knm)->k21[n][m].re, ((mirror_knm_t*)knm)->k21[n][m].im);
                } else if(knmcmp == BEAMSPLITTER_CMP) {
                    fprintf(file, "%d %d %d %d   ", n1, m1, n2, m2);
                    fprintf(file, "%.15g %.15g ", ((bs_knm_t*)knm)->k12[n][m].re, ((bs_knm_t*)knm)->k12[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((bs_knm_t*)knm)->k21[n][m].re, ((bs_knm_t*)knm)->k21[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((bs_knm_t*)knm)->k34[n][m].re, ((bs_knm_t*)knm)->k34[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((bs_knm_t*)knm)->k43[n][m].re, ((bs_knm_t*)knm)->k43[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((bs_knm_t*)knm)->k13[n][m].re, ((bs_knm_t*)knm)->k13[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((bs_knm_t*)knm)->k31[n][m].re, ((bs_knm_t*)knm)->k31[n][m].im);
                    fprintf(file, "%.15g %.15g ", ((bs_knm_t*)knm)->k24[n][m].re, ((bs_knm_t*)knm)->k24[n][m].im);
                    fprintf(file, "%.15g %.15g\n", ((bs_knm_t*)knm)->k42[n][m].re, ((bs_knm_t*)knm)->k42[n][m].im);
                } else
                    bug_error("Did not recognise knmcomp argument");

                
            }
        }

        fprintf(file, "\n");
    }

    if(file)
        fclose(file);
        
    // if no maps are present no need to write them to a file
    if (!map->noMapPresent) {
        if (map->knm_save_binary) {
            _write_merged_map_binary_data(map);
        } else {
            // now we need to write the actual map data
            _write_merged_map_ascii_data(name, map, R_ABS);
            _write_merged_map_ascii_data(name, map, R_PHS);
            _write_merged_map_ascii_data(name, map, T_ABS);
            _write_merged_map_ascii_data(name, map, T_PHS);
        }
    }
}


void write_comm_knm_to_matrix_file(complex_t **data, char *buf){
    FILE *file;
    int n, m;
    
    if (inter.debug && !options.quiet) 
        message("* Outputing knm matrix to %s\n", buf);

    open_file_to_write_ascii(buf, &file);

    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            complex_t val = data[n][m];

            fprintf(file, "%.15e%s%.15ei%s", val.re
                    , (val.im > 0) ? "+" : "-"
                    , fabs(val.im)
                    , (m == inter.num_fields - 1) ? "" : " ");
        }

        fprintf(file, "\n");
    }

    fclose(file);
}

/**
 * Writes the coupling coefficient matrix to a file in a matrix form. To be used
 * for testing commutation issues.
 * 
 * @param filename Name of output file
 * @param mirror mirror whose knm to output
 */
void write_mirror_knm_to_matrix_file(char *filename, mirror_knm_t *knm) {
    int i;
    complex_t **data = NULL; // removing compiler warning
    char buf[LINE_LEN];

    //if the knm hasn't been allocated, then it hasn't been used, so no point
    //in writing it to a file
    if (!IS_MIRROR_KNM_ALLOCD(knm))
        return;

    if (knm != NULL) {

        for (i = MR11; i <= MR22; i++) {
            switch (i) {
                case MR11: data = knm->k11;
                    sprintf(buf, "%s_K11.mat", filename);
                    break;
                case MR12: data = knm->k12;
                    sprintf(buf, "%s_K12.mat", filename);
                    break;
                case MR21: data = knm->k21;
                    sprintf(buf, "%s_K21.mat", filename);
                    break;
                case MR22: data = knm->k22;
                    sprintf(buf, "%s_K22.mat", filename);
                    break;
                default: bug_error("Unexpected KNM");
            }
            
            write_comm_knm_to_matrix_file(data, buf);
        }
    } else {
        warn("Could not write knm to output file as knm variables were not set\n");
    }
}

/**
 * Writes the coupling coefficient matrix to a file in a matrix form. To be used
 * for testing commutation issues.
 * 
 * @param filename Name of output file
 * @param bs beamsplitter whose knm to output
 */
void write_bs_knm_to_matrix_file(char *filename, bs_knm_t *knm) {
    int i;
    complex_t **data = NULL; // removing compiler warning
    char buf[LINE_LEN];

    //if the knm hasn't been allocated, then it hasn't been used, so no point
    //in writing it to a file
    if (!IS_BS_KNM_ALLOCD(knm))
        return;

    if (knm != NULL) {

        for (i = BS12; i <= BS42; i++) {
            switch (i) {
                case BS12: 
                    data = knm->k12;
                    sprintf(buf, "%s_K12.mat", filename);
                    break;
                case BS21:
                    data = knm->k21;
                    sprintf(buf, "%s_K21.mat", filename);
                    break;
                case BS34: 
                    data = knm->k34;
                    sprintf(buf, "%s_K34.mat", filename);
                    break;
                case BS43: 
                    data = knm->k43;
                    sprintf(buf, "%s_K43.mat", filename);
                    break;
                case BS13: 
                    data = knm->k13;
                    sprintf(buf, "%s_K13.mat", filename);
                    break;
                case BS31:
                    data = knm->k31;
                    sprintf(buf, "%s_K31.mat", filename);
                    break;
                case BS24: 
                    data = knm->k24;
                    sprintf(buf, "%s_K24.mat", filename);
                    break;
                case BS42:
                    data = knm->k42;
                    sprintf(buf, "%s_K42.mat", filename);
                    break;
                default: bug_error("Unexpected KNM");
            }

            write_comm_knm_to_matrix_file(data, buf);
        }
    } else {
        warn("Could not write knm to output file as knm variables were not set\n");
    }
}









void clear_progress(){
    int i=0;
    
    for(i=0; i < (int)prev_progress_print_size; i++){
        fprintf(stderr, " ");
    }
    
    fprintf(stderr, "\r");
}

/*!
 * \param num_points the total number of points in the simulation
 * \param current_point the current point
 * \param action string stating what the progress is for
 */
void print_progress(int num_points, int current_point) {
    
    int progress_percentage;
    
    if(num_points == 0)
        progress_percentage = 0;
    else
        progress_percentage = ((100 * (current_point - 1)) / num_points + 1);
    
    int text_length = 0, i=0;
    
    if (progress_percentage > 100)
        progress_percentage = 100;
    
    if (progress_percentage < 0)
        progress_percentage = 0;
    
    if(options.perl1) {
        text_length = fprintf(pykat_file, "progress:%s\t%d\t%s\n", progress_action, progress_percentage, progress_message);
    } else {
        text_length = fprintf(stderr, PROG_STRING, "", progress_action, progress_percentage, progress_message);
        // if the previous progress text length wasn't as long as the last then we
        // might have some left over text showing.

        int extra_chars = prev_progress_print_size - text_length;

        if (extra_chars > 0){
            for(i=0; i < extra_chars; i++){
                fprintf(stderr," ");
            }
        }

        if(options.no_output_bksp)
            fprintf(stderr, PROG_END_NEWLINE);

        // return to start of carriage so that any other printing just goes over
        // the last progress output
        fprintf(stderr,"\r");
        fflush(stderr);
        
        prev_progress_print_size = text_length;
    }
}

/*
 * Sets the informative text as to what the program is currently doing when
 * the progress is printed. Should be a short statement as to what the program
 * is doing. e.g. calculating, integrating, saving, calling system, etc..
 */
void set_progress_action_text(const char* action, ...){
    assert(action != NULL);
    assert(strlen(action) < LINE_LEN);
    va_list argptr;
    va_start(argptr, action);
    vsprintf(progress_action, action, argptr);
    va_end(argptr);
}

/*
 * Sets the informative text as to what the program is currently doing when
 * the progress is printed. The message appears at the end of the progress and
 * should be more informative of the action at present
 */
void set_progress_message_text(const char* msg, ...){
    
    assert(msg != NULL);
    assert(strlen(msg) < LINE_LEN);
    va_list argptr;
    va_start(argptr, msg);
    vsprintf(progress_message, msg, argptr);
    va_end(argptr);
}

//! Print the progress of the simulation andd estimated remianing time to stderr

/*!
 * \param num_points the total number of points in the simulation
 * \param current_point the current point
 */
void print_progress_and_time(int num_points, int current_point, time_t starttime) {
    
    double estimated_time = ((num_points - current_point) / (double) current_point) * difftime(time(NULL), starttime);
    sprint_time(progress_message, estimated_time);
    
    print_progress(num_points, current_point);
    
    fflush(stderr);
}




//! Return a character representation of the node at the given index 

/*!
 * \param node_index the node index
 */
const char *node_print(int node_index) {
    static char gndstring[] = "dump";
    
    if (node_index == NOT_FOUND)
        bug_error("Node index was set to NOT_FOUND, probably shouldn't be");
                
    // sanity check input
    assert(node_index >= 0);
    assert(node_index < inter.num_nodes);
    
    if (node_index < inter.num_nodes) {
        return duplicate_string(inter.node_list[node_index].name);
    }
    gerror("node_print: node undefined\n");

    return gndstring; /* only to silence compiler warning */
}

//! Error reporting routine with bug report information

/*!
 * \param error_msg the error string to print
 * \param function_name the name of the function where error occurred
 * \param source_fname the name of the file in which error occurred
 * \param line_number the line number of the file where error occurred
 */
void _bug_error(const char *function_name, const char *source_fname,
        int line_number, const char *error_msg, ...) {
    va_list argptr; // the pointer to the argument
    char format_str[ERR_MSG_LEN]; // to hold the error message format string

    if( strlen(error_msg) > sizeof(format_str) )
        bug_error("Message string is too long for buffer.\n");
    
    // turn on bold text
    bold_on(stderr);

    // make the format string
    sprintf(format_str,
            "\n\nBug detected!\n"
            "In function %s() of file %s at line %d.\n"
            "Error message: %s\n\n"
            "  Please report the version of Finesse you are using and the input\n"
            "  file that caused the bug at www.gwoptics.org/finesse/bug/. Thank you.\n",
            function_name, source_fname, line_number, error_msg);

    // variable argument stuff
    // point to the first unnamed arge (after the last named arg)
    va_start(argptr, error_msg);
    // print the error message
    vfprintf(stderr, format_str, argptr);
    va_end(argptr); // clean up

    bold_off(stderr);

    // output to the log file as well
    if (fp_log != NULL) {
        va_start(argptr, error_msg);
        vfprintf(fp_log, format_str, argptr);
        va_end(argptr); // clean up
    }

    my_finish(1);
}


//! Generic error reporting routine with variable argument list

/*!
 * \param error_msg mandatory error string, works similarly to printf
 *
 * \todo untested
 */
void gerror(const char *error_msg, ...) {
    va_list argptr; // the pointer to the argument

    // flush everything first to get correct order of output
    fflush(stdout);
    fflush(stderr);
    
    // turn on bold, and report the error
    bold_on(stderr);
    fprintf(stderr, "\n*** Error: \n");

    // the variable argument stuff
    // point to the first unnamed arg (after the last named arg)
    va_start(argptr, error_msg);
    vfprintf(stderr, error_msg, argptr);
    va_end(argptr); // clean up

    // turn off bold
    bold_off(stderr);

    // outpuut to the log file as well
    if (fp_log != NULL) {
        va_start(argptr, error_msg);
        vfprintf(fp_log, error_msg, argptr);
        va_end(argptr); // clean up
    }

    my_finish(1);
}

//! Generic error reporting routine with variable argument list

/*! 
 * As gerror but without stopping the program.
 *
 */
void server_gerror(const char *error_msg, ...) {
    va_list argptr; // the pointer to the argument

    // turn on bold, and report the error
    bold_on(stderr);
    fprintf(stderr, "\n*** Error: \n");

    // the variable argument stuff
    // point to the first unnamed arg (after the last named arg)
    va_start(argptr, error_msg);
    vfprintf(stderr, error_msg, argptr);
    va_end(argptr); // clean up

    // turn off bold
    bold_off(stderr);

    // outpuut to the log file as well
    if (fp_log != NULL) {
        va_start(argptr, error_msg);
        vfprintf(fp_log, error_msg, argptr);
        va_end(argptr); // clean up
    }
    fflush(stderr);
    fflush(fp_log);

    if (NOT options.servermode) {
        my_finish(1);
    }
}


//! Set bold font weight on

/*!
 * \param fp the file pointer
 */
void bold_on(FILE *fp) {
    // make sure the file pointer is not null
    assert(fp != NULL);

    // print ANSI code for BOLD
    if (OS == UNIX) {
        fputs("\x1B[1m", fp);
    }
}

//! Set bold font weight off

/*!
 * \param fp the file pointer
 */
void bold_off(FILE *fp) {
    // make sure the file pointer is not null
    assert(fp != NULL);

    // print ANSI code for RESET ALL
    if (OS == UNIX) {
        fputs("\x1B[0m", fp);
    }
}

//! Base message reporting routine for the warn() and message() functions.

/*!
 * This routine forms the basis of the warn() and message() functions.  They
 * are just wrappers for this routine, and are defined as macros with
 * variable argument lists.  The message_type parameter can be either 
 * WARNING or MESSAGE.
 *
 * \param message_string the message string to print
 * \param message_type the type of message one wants to print
 */
void _user_message(const char *message_string, int message_type, ...) {
    va_list argptr; // the pointer to the argument
    char format_str[ERR_MSG_LEN] = {0};
    FILE *fp_output;

    if( strlen(message_string) > sizeof(format_str) ){
        bug_error("Message string `%s` is too long for buffer (%d).\n", message_string, sizeof(format_str));
    }
    // if we have a warning (i.e. have used warn()) then make stderr the
    // output stream, if we have just a plain message, use stdout as the
    // output stream, otherwise, barf
    fp_output = stdout; // assign a default output stream

    if (message_type == WARNING) {
        // fp_output = stderr;
        // prepend '** ' to WARNING strings
        strcpy(format_str, "** ");
    } else if (message_type == MESSAGE) {
        // prepend ' ' to MESSAGE strings
        strcpy(format_str, " ");
    } else {
        gerror("Incorrect message type passed to _user_message()\n");
    }
     
    strcat(format_str, message_string);

    // the variable argument stuff
    if (!options.quiet) {

        // put bold on if is a warning
        if (message_type == WARNING) {
            bold_on(fp_output);
        }

        // the variable argument stuff
        // point to the first unnamed arg (after the last named arg)
        va_start(argptr, message_type);
        vfprintf(fp_output, format_str, argptr);
        va_end(argptr); // clean up

        // switch bold back off if required
        if (message_type == WARNING) {
            bold_off(fp_output);
        }
    }

    // send the messages to the log file as well
    if (fp_log != NULL) {
        va_start(argptr, message_type);
        vfprintf(fp_log, format_str, argptr);
        va_end(argptr); // clean up
    }
}

//! Print debugging information to standard output

/*!
 * \param function_name Function name where debug message is used
 * \param message_string The string to be printed in the message
 *
 * \todo untested
 */
void _debug_msg(const char *function_name, const char *message_string, ...) {
    if (!(inter.debug & 65536))
        return;

    va_list argptr; // the pointer to the argument
    char format_str[BUFF_LEN]; //<! \todo the value used here should be \#defined

    if( strlen(message_string) > sizeof(format_str) )
        bug_error("Message string is too long for buffer.\n");
        
    // prepend the function name to the format string
    sprintf(format_str, "%s(): %s", function_name, message_string);

    // the variable argument stuff
    // point to the first unnamed arg (after the last named arg)
    va_start(argptr, message_string);
    vprintf(format_str, argptr);
    va_end(argptr); // clean up
}


//! Print version number

/*!
 *
 */
void print_version(FILE *file) {
    fprintf(file, "Finesse %s (%s), %s\n", VERSION, GIT_REVISION, mydate);
}



//! Print the second help screen

/*!
 *
 */
void print_help2(void) {
    printf(
            "------------------------------------------------------------------------\n"
            "  FINESSE %s   - Help Screen (2) -                    %s\n"
            "------------------------------------------------------------------------\n",
            VERSION, mydate);
    printf(
            " ** Alternative calls:\n"
            "  kat --server <portnumber (11000 to 11010)> [options] inputfile \n"
            "   starts Finesse in server mode listening on a TCP/IP port\n"
            "  kat --convert knm_file_prefix 1/2 [new_knm_file_prefix]\n"
            "   converts binary knm file to ASCII and vice versa\n"            
            " ** Some conventions:\n"
            "  names (for components and nodes) must be less than 15 characters long\n"
            "  angles of incidence, phases and tunings are given in [deg]\n"
            "  (a tuning of 360 deg corresponds to a position change of lambda)\n"
            "  misalignment angles are given in [rad]\n"
            " ** Signal frequency variable\n"
            "  The variables $fs and $mfs can be used in functions, put commands\n"
            "  and as the frequency value in any of the detectors. $fs is the positive\n"
            "  frequency and $mfs is the negative.\n"
            " ** Motion and suspension variables\n"
            "  Motion names used in xd detectors, slinks and xlinks to select\n"
            "  particular motions of a suspended optic:\n"
            "  z = longitudinal motion\n"
            "  rx, yaw = x-z plane rotation\n"
            "  ry, pitch = y-z plane rotation\n"
            "  sN = The Nth surface motion set with the smotion command, from 0 -> N-1\n"
            "\n"
            "  To set the suspension transfer function with the attr command, use:\n"
            "  zmech = Longitudinal motion\n"
            "  rxmech = yaw motion\n"
            "  rymech = pitch motion\n"
            "\n"
            " ** Geometrical conventions:\n"
            "  tangential plane: x, z (index n), saggital plane: y, z (index m)\n"
            "  xbeta refers to a rotation in the x, z plane, i.e. around the y-axis\n"
            "  R<0 when the center of the respective sphere is down beam\n"
            "  (the beam direction is defined locally through the node order:\n"
            "  i.e. mirror: node1 -> node2, beam splitter: node1 -> node3)\n"
            "  beam parameter z<0 when waist position is down beam\n"
            " ** mismatches [limit] [n]: `n' bit coded word, for mismatch output options.\n"
            "                                `limit' [0 < limit < 1] Lower mismatch limit for display cut off\n"
            "     mismatches 1: Ignore low R mirrors/bs\n"
            "     mismatches 2: Ignore low T mirrors/bs\n"
            "     mismatches 4: Display average of x and y beam parameters\n"
            "     mismatches 8: Display mismatch output each step\n"
            " ** frequency n: `n' bit coded word, produces the following output:\n"
            "     frequency 1: print all frequencies computed\n"
            "     frequency 2: print frequency coupling matrix for each modulator\n"
            " ** powers [n]: `n' bit coded word, produces the following output:\n"
            "     powers  :  prints the DC power in all nodes\n"
            "     powers 2:  prints powers for f = 0 Hz fields only\n"
            "     powers 4:  prints powers for TEM00 mode only\n"
            " ** trace n: `n' bit coded word, produces the following output:\n"
            "     trace 1:   list of TEM modes used\n"
            "     trace 2:   cavity eigenvalues and cavity parameters like FWHM, \n"
            "                FSR optical length and Finesse\n"
            "     trace 4:   mode mismatch parameters for the initial setup\n"
            "     trace 8:   beam parameters for every node, nodes are listed in\n"
            "                the order found by the tracing algorithm\n"
            "     trace 16:  Gouy phases for all spaces\n"
            "     trace 32:  coupling coefficients for all components\n"
            "     trace 64:  mode matching parameters during calculation, if\n"
            "                they change due to a parameter change, for example\n"
            "                by changing a radius of curvature.\n"
            "     trace 128: nodes found during the cavity tracing\n"
            " ** phase 0-7: also bit coded, i.e. 3 means `1 and 2'\n"
            "     phase 1:   phase of coupling coefficients k_00 set 0\n"
            "     phase 2:   Gouy phase of TEM_00 set to 0\n"
            "     phase 4:   `ad name n m f' yields amplitude without Gouy phase\n"
            "     (default: phase 3)\n"
            " ** bp, possible parameters for this detector:\n"
            "     w  : beam radius\n"
            "     w0 : waist radius\n"
            "     z  : distance to waist\n"
            "     zr : Rayleigh range\n"
            "     g  : Gouy phase\n"
            "     r  : Radius of curvature (phase front) in meters\n"
            "     q  : Gaussian beam parameter\n"
            " ** isol S, suppression given in dB:\n"
            "     amplitude coefficient computed as 10^-(S/20)\n"
            /*
              " ** grating types are defined via their number of ports:\n"
              "     - 2: 1st order Littrow (eta_0, eta_1)\n"
              "     - 3: 2nd order Littrow (eta_0, eta_1, eta_2, rho_0)\n"
              "     - 4: not Littrow (eta_0, eta_1, eta_2, eta_3)\n"
             */
            /* 
               "debug n: `n' bit coded word, every bit gives various debugging output\n"
               "     x\n"
               "scale factor : factor can be also ampere/meter/deg\n"
               "     debug 1:   list of internal input parameters\n"
               "     x\n"
               "     x\n"
               "attr ... : one to three per line Rc, Rx, Ry but xbeta, ybeta \n"
               "     `...\n"
               "  \n"
             */
            " ** maxtem O/off : O=n+m (TEM_nm) the order of TEM modes, \n"
            "     `off' switches the TEM modus off explicitly\n"
            " ** conf:\n"
            "    This command is used to configure settings of a component\n"
            "    that are not physical parameters.\n"
            "   o Modulators\n"
            "    - conf component numerical_f_couple n\n"
            "      n = 1 (on - default) or 0 (off)\n"
            "      Numerically compares all frequencies and couples them if they\n"
            "      match the modulation frequency or its harmonics\n"
            "    - conf component print_f_coupling n\n"
            "      n = 1 (on) or 0 (off - default)\n"
            "      print which frequencies couple to which at this modulator\n"
            "      for the carrier and sideband fields\n" 
            "   o Mirors and Beamsplitters\n"
            "    - conf component integration_method n\n"
            "      sets the numerical integration method. Cubature refers to a\n"
            "      self-adapting routine which is faster but less robust\n"
            "      1 - Riemann Sum\n"
            "      2 - Cubature - Serial\n"
            "      3 - Cubature - Parallel (default)\n"
            "    - conf component interpolation_method n\n"
            "      sets the interpolation method for the numerical integration of\n"
            "      surface maps (use NN for maps with sharp edges):\n"
            "      1 - Nearest Neighbour\n"
            "      2 - Linear (default)\n"
            "      3 - Spline\n"
            "    - conf component interpolation_size n\n"
            "      sets the size of the interpolation kernel, must be odd and > 0\n"
            "      (default=3)\n"
            "    - conf component knm_flags n\n"
            "      sets the knm computation flags which define if coeffs are calculated\n"
            "      numerically or analytically if possible, see below for values of 'n'\n"
            "    - conf component show_knm_neval 0/1\n"
            "      sshows the number of integrand evaluations used for the map integration\n"
            "    - conf component save_knm_matrices 0/1\n"
            "      if true the knm matrices are saved to .mat files\n"
            "      for distortion, surface map and the final result\n"
            "    - conf component save_knm_binary 0/1\n"
            "      if true the knm and merged map data is stored in a\n"
            "      binary format rather than ASCII, see --convert option\n"
            "      for converting between the 2 formats\n"
            "    - conf component save_interp_file 0/1\n"
            "      if true a file is written for each knm\n"
            "      to output each interpolated point. The file\n"
            "      has 4 columns: x, y, amplitude, phase.\n"
            "    - conf component save_integration_points 0/1\n"
            "      if true all points used for integration are\n"
            "      saved to a file (use this only with the Riemann\n"
            "      integrator, Cubature can use millions of points!)\n"
            "    - conf component knm_order 12/21\n"
            "      changes the order in which the coupling coefficient matrices\n"
            "      are computed. 1 = Map, 2 = Bayer-Helms\n"
            "    - conf component knm_change_q 1/2\n"
            "      specifies the expansion beam parameter q_L\n"
            "      if 1 then q_L = q'_1 and if 2 then q_L = q_2\n"
            " ** knm flags: `n' bit coded word, set computation of coupling coeffs\n"
            "     use with the `conf compoent knm_flag' command:\n"
            "     0  : analytic solution of all effects used\n"
            "     1  : verbose, i.e. print coupling coefficients\n"
            "     2  : numerical integration if x and y misalignment is set\n"
            "     4  : numerical integration if x or y misalignment is set\n"
            "     8  : calculates aperture knm by integration\n"
            "     16 : calculates curvature knm by integration\n"
            "     32 : calculates bayer-helms knm by integration\n"
            "     (default: knm 8)\n"
            );
}

//! Print help screen

/*!
 *
 */
void print_help() {
    
    printf(
            "------------------------------------------------------------------------\n"
            "  FINESSE %s     - Help Screen -                      %s\n"
            "\n"
            "  Features included in this binary: \n"
            "    Cuba map integrator  - %s\n"
            "    NICSLU matrix solver - %s\n"
            "    Kat server mode      - %s\n"
            "------------------------------------------------------------------------\n",
            VERSION, mydate, (INCLUDE_CUBA)? "yes" : "no", (INCLUDE_NICSLU)? "yes" : "no", (INCLUDE_NET)? "yes" : "no");
    printf(
            "** Usage (1) kat [options] infile [outfile [gnufile]] \n"
            "   or    (2) kat [options] basename\n"
            "   in (2) e.g. basename 'test' means input filename :"
            " 'test.kat', \n"
            "   output filename : 'test.out' and Gnuplot file"
            " name : 'test.gnu'.\n"
            "** Support :\n"
            "   User support forums:     http://www.gwoptics.org/finesse/forums/\n"
            "   Online syntax reference: http://www.gwoptics.org/finesse/reference/\n"
            "** Available options:\n"
            " -v : prints version number and build date\n"
            " -h : prints this help (-hh prints second help screen)\n"
            " -c : check consistency of interferometer matrix\n"
            " -max : prints max/min\n"
            " -klu-full : switch to KLU solver for parallel frequencies (default)\n"
            " -klu      : switch to KLU (Legacy solver)\n"
            " --server : starts Finesse in server mode\n"
            " --noheader : suppresses header information in output data files\n"
            " --pykat : runs Finesse in Pykat interaction mode\n"
            " --quiet : suppresses almost all screen outputs\n"
            " --convert : convert knm files between text and binary formats\n"
            "** Available interferometer components:\n"
            " l name P f [phase] node                          - laser\n"
            " m name R T phi node1 node2                       - mirror\n"
            " (or: m1 name T Loss phi ...          \n"
            "      m2 name R Loss phi ... )        \n"
            " s name L [n] node1 node2                         - space\n"
            " bs name R T phi alpha node1 node2 node3 node4    - beamsplitter\n"
            " (or: bs1 name T Loss phi ... \n"
            "      bs2 name R Loss phi ... )             \n"
            " gr[n] name d node1 node2 [node3 [node4]]         - grating\n"
            " isol name S node1 node2                          - isolator\n"
            " dbs name node1 node2 node3 node4                 - directional BS\n"
            " mod name f midx order am/pm [phase] node1 node2  - modulator\n"
            " lens name f node1 node2                          - thin lens (Focal length)\n"
            " lens* name P node1 node2                          - thin lens (Dioptres)\n"
            " sq name f r angle node                           - squeezed input\n"
            "** Detectors:\n"
            " pd[n] name [f1 [phase1 [f2... ]]] node[*]        - photodetector [mixer]\n"
            " pdS[n] name [f1 phase1 [f2... ]] node[*]         - sensitivity\n"
            " pdN[n] name [f1 phase1 [f2... ]] node[*]         - norm. photodetector\n"
            " ad name [n m] f node[*]                          - amplitude detector\n"
            " hd name phase node1 node2                        - homodyne detector\n"
            " bp name x/y parameter node[*]                    - plots beam parameters\n"
            " cp name cavity_name x/y parameter                - plots cavity parameters\n"
            " gouy name x/y space-list                         - plots gouy phase\n"
            " beam name [f] node[*]                            - plots beam shape\n"
            " qd name f phase node[*]                          - quantum quadrature detector\n"
            " sd name f [n m] node[*]                          - squeezing detector\n"
            //" shot name node[*]                                - shot noise\n"
            " qshot[S/N] name n f1 [phase1 [f2...]] node[*]    - quantum shotnoise detector\n"
            " qnoised[S/N] name n f1 [phase1 [f2...]] node[*]  - quantum noise detector\n"
            " qhd name phase node1 node2                       - quantum noise homodyne detector\n"
            " q[n]hd name funca node1 node2 [funcb node3 node4]- mixed quantum noise homodyne detector\n"
            " pgaind name component motion                     - open loop param. gain det.\n"
            " xd name component motion                         - motion detector (See second help for motions)"
            "** Available commands:\n"
            " fsig name component [type] f phase [amp]         - apply signal\n"
            " fsig name component [type] f transfer_func       - signal wth transfer function\n"
            " fsig name f                                      - set signal/noise frequency\n"
            " fadd f1 f2 f3 ... fN                             - add frequencies to list\n"
            " tem[*] input n m factor phase                    - input power in HG/LG modes\n"
            " mask detector n m factor                         - mode mask for outputs\n"
            " pdtype detector type-name                        - set detector type\n"
            " attr component M value Rcx/y value x/ybeta value - attributes of m/bs\n"
            " (alignment angles beta in [rad])\n"
            " map component filename                           - read mirror map file\n"
            " knm component_name filename_prefix [flag]        - save coefficients to file\n"
            " smotion component map_file transfer_function     - set surface motion\n"
            " maxtem order                                     - TEM order: n+m<=order\n"
            " gauss name component node w0 z [wy0 zy]          - set q parameter\n"
            " gauss* name component node q [qy] (q as 'z z_R') - set q parameter\n"
            " gauss** name component node w(z) Rc [wy(z) Rcy]  - set q parameter\n"
            " cav name component1 node component2 node         - trace beam in cavity\n"
            " startnode node                                   - startnode of trace\n"
            " lambda wavelength                                - overwrite wavelength\n"
            " retrace [off|force]                              - re-trace beam on/off\n"
            " phase 0-7  (default: 3)                          - change Gouy phases\n"
            " (1: phi(00)=0, 2: gouy(00)=0, 4: switch ad phase)\n"
            " conf component_name setting value                - configures component\n"
            " vacuum components_names                          - specific quantum noise\n"
            " tf name factor phase [{p/z f Q [p/z f2 Q2 ...]]  - f,Q transfer function\n"
            " tf2 name factor phase [p1,p2,...] [z1,z2,...]    - complex transfer function\n"
            "** Plot and Output related commands :\n"
            " xaxis[*] component param. lin/log min max steps  - parameter to tune\n"
            " x2axis[*] component param. lin/log min max steps - second axis for 3D plot\n"
            " noxaxis                                          - ignore xaxis commands\n"
            " const name value                                 - constant $name\n"
            " var name value                                   - tunabel variable $name\n"
            " set name component parameter                     - variable $name\n"
            " func name = function-string                      - function $name\n"
            " lock[*] name $var gain accuracy [offset]         - lock: make $var+offset to 0\n"
            " put[*] component parameter $var/$x1/$x2/$fs/$mfs - updates parameter\n"
            " noplot output                                    - no plot for 'output'\n"
            " trace verbosity                                  - verbose tracing\n"
            " yaxis [lin/log] abs:deg/db:deg/re:im/abs/db/deg  - y-axis definition\n"
            " scale factor [output]                            - y-axis rescaling\n"
            " diff component parameter                         - differentiation\n"
            " deriv_h value                                    - step size for diff\n"
            "** Auxiliary plot commands :\n"
            " gnuterm terminal [filename]                      - Gnuplot terminal\n"
            " pyterm terminal                                  - Python terminal\n"
            " pause                                            - pauses after plotting\n"
            " multi                                            - plots all surfaces\n"
            "                                                    save/load knm file\n"
            " GNUPLOT \\ ... \\ END                              - set of extra commands\n"
            "                                                    for plotting.\n");
}

//! Prints max and min values of all output traces

/*!
 *
 * \todo refactor for readability
 * \todo untested
 */
void max_min(void) {
    int naxes;

    if (inter.yaxis[1]) {
        naxes = 1;
    } else {
        naxes = 0;
    }

    int j;
    for (j = 0; j < inter.num_outputs; j++) {
        output_data_t *output_data;
        output_data = &(inter.output_data_list[j]);

        /*
          if (inter.sum)
          j=inter.num_light_outputs;
         */

        if (!output_data->noplot) {
          message("%s", output_data->name);
          if (naxes) {
            message("(y1)");
          }
          
          if (inter.x3_axis_is_set) {
            if (inter.x3.xtype == FLOG) {
                message("x3 %.10g", *inter.x3.xaxis / inter.x3.op);
            } else {
              message("x3 %.10g", *inter.x3.xaxis - inter.x3.op);
            }
          }

          if (!inter.noxaxis) {
            message("max");
            
            message("x %.10g", output_data->Omax_x[0]);
            
            if (inter.splot) {
              message("y %.10g", output_data->Omax_y[0]);
            }
          }

          
          message(": %.10g", output_data->Omax[0]);

          if (!inter.noxaxis) {
            message("min");
          
            message("x %.10g", output_data->Omin_x[0]);
            if (inter.splot) {
              message("y %.10g ", output_data->Omin_y[0]);
            }
            message(": %.10g", output_data->Omin[0]);
          }
          message("\n");

          if (naxes) {
            message("%s (y2)", output_data->name);
            if (!inter.noxaxis) {
              message("max");
            }
            message("x %.10g", output_data->Omax_x[1]);
            
            if (inter.splot) {
                message("y %.10g", output_data->Omax_y[1]);
            }
            message(": %.10g", output_data->Omax[1]);

            if (!inter.noxaxis) {
              message("min");
              message("x %.10g", output_data->Omin_x[1]);
              
              if (inter.splot) {
                message("y %.10g", output_data->Omin_y[1]);
              }
              message(": %.10g", output_data->Omin[1]);
              
            }
            message("\n");
          }
        }
    }
}

void convert_knm_file(const char *filename, const char *newfilename, KNM_COMPONENT_t knmcmp) {

    char fname[FILENAME_MAX];
    strcpy(fname, filename);

    mirror_t mirror;
    beamsplitter_t bs; 
    
    void *ptr=NULL;
    char *mapfname=NULL,*cmpname=NULL;
    bool *knmsavebinary=NULL;
    
    if(knmcmp == MIRROR_CMP){
        message("* Converting mirror knm file type\n");
        ptr = (void*)&mirror;
        mapfname = mirror.map_merged.filename;
        cmpname = mirror.name;
        knmsavebinary = &(mirror.map_merged.knm_save_binary);
    }else if(knmcmp == BEAMSPLITTER_CMP){
        message("* Converting beamsplitter knm file type\n");
        ptr = (void*)&bs;
        mapfname = bs.map_merged.filename;
        cmpname = bs.name;
        knmsavebinary = &(bs.map_merged.knm_save_binary);
    } else
        bug_error("Did not recognise knm component in convert_knm_file");
    
    char *i = strstr(fname, ".knm");

    // check if we have found a file extension, pointer is null if not found
    if (i != 0) {
        int ix = i - &fname[0];
        // end the string before the .knm
        // as we add it later
        fname[ix] = '\0';
    }

    strcpy(mapfname, fname);
    // need a mirror name for the error messages, pick m1 here?
    strcpy(cmpname, "m1");
    
    message("* Opening knm file...\n");
    if (!read_knm_file(ptr, true, knmcmp))
        gerror("An error occurred whilst reading the knm file.\n Please check"
            " it exists and has been correctly generated.\n");

    if (strlen(newfilename) != 0) {
        char* i = strstr(newfilename, ".knm");
        strcpy(fname, newfilename);
        // check if we have found a file extension
        if (i != 0) {
            int ix = i - &fname[0];
            // end the string before the .knm
            // as we add it later
            fname[ix] = '\0';
        }

        strcpy(mapfname, fname);
        message("* Saving using new file name...\n");
    }

    // invert the save type
    *knmsavebinary = !(*knmsavebinary);

    if (*knmsavebinary)
        message("* Converting from ASCII to binary format\n");
    else
        message("* Converting from binary to ASCII format\n");

    write_knm_file(ptr, true, knmcmp);

    message("* knm files have now been converted and saved with prefix %s", mapfname);
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
