#ifndef KAT_KNM_MIRROR_H
#define	KAT_KNM_MIRROR_H

#include "kat_optics.h"
#include "kat.h"

#define CHECK_MIRROR_KNM_T(A) \
assert(A!=0); \
assert(A->k11!=0); \
assert(A->k12!=0); \
assert(A->k21!=0); \
assert(A->k22!=0);

#define IS_MIRROR_KNM_ALLOCD(A) \
(((A)->k11 != NULL) && ((A)->k12 != NULL) \
&& ((A)->k21 != NULL) && ((A)->k22 != NULL))

#define CALC_MR_KNM(A, KNM) (((A)->knm_calc_flags & MR##KNM##Calc) == MR##KNM##Calc)

/*** Contains the parameters needed to do mirror map integration using GSL */
typedef struct knm_mirror_map_int_params {
    int n1, m1, n2, m2;
    u_nm_accel_t *acc_11_nr1_1; //accelerator for calculating k11
    u_nm_accel_t *acc_11_nr1_2;

    u_nm_accel_t *acc_22_nr2_1; //accelerator for calculating k22
    u_nm_accel_t *acc_22_nr2_2;

    u_nm_accel_t *acc_21_nr2_1; //accelerator for calculating k21
    u_nm_accel_t *acc_21_nr1_2;

    u_nm_accel_t *acc_12_nr1_1; //accelerator for calculating k12
    u_nm_accel_t *acc_12_nr2_2;

    mirror_knm_q_t *knm_q;
    
    surface_merged_map_t *merged_map;
    
    mirror_t *mirror;
    
    double nr1, nr2;
    double A, Cr, Ct, Br, Bt;
    double kr, kt, k0;
    double xmin[2], xmax[2];
    
    // Prefactors of u_nm that depend on z,n,m,n',m' only, 1c2 part means nr1
    // medium times conjugate of nr2 medium etc.
    complex_t znm1c1;
    complex_t znm1c2;
    complex_t znm2c1;
    complex_t znm2c2;
    
    KNM_MIRROR_NODE_DIRECTION_t knum;
    double J1, J2;
    double aperture_radius; // 0 if infinite radius
    bool polar_limits_used;
    int integrating;
    double betax, betay;
    bool usingMap;
    bitflag knm_calc_flags;
} mr_knm_map_int_params_t;

void alloc_knm_accel_mirror_mem(long *bytes);

void calc_mirror_knm_surf_motions_map(mirror_t *m, double nr1, double nr2, complex_t qx1, complex_t qy1, complex_t qx2, complex_t qy2);
void calc_mirror_knm_surf_motions_rom(mirror_t *mirror, double nr1, double nr2, complex_t qx11, complex_t qy11, complex_t qx22, complex_t qy22, bitflag astigmatism);

void mirror_knm_matrix_copy(mirror_knm_t* src, mirror_knm_t* dest);
void mirror_knm_matrix_mult(mirror_knm_t* A, mirror_knm_t* B, mirror_knm_t *result);
void mirror_knm_alloc(mirror_knm_t* knm, long *bytes);
void mirror_knm_free(mirror_knm_t* knm);

void compute_mirror_knm_romhom(mirror_t *mirror, double nr1, double nr2, int mismatch, int astigmatism);
void compute_mirror_knm_integral(mirror_t *mirror, double nr1, double nr2, bitflag integrating, bitflag mismatch, bitflag astigmatism);
void calculate_mirror_qt_qt2(KNM_MIRROR_NODE_DIRECTION_t KNM, mirror_knm_q_t *knm_q, complex_t qx1,
                             complex_t qy1, complex_t qx2, complex_t qy2, double nr1, double nr2, int n1idx, int n2idx, int cidx);
void check_mirror_knm_mismatch_astig(bitflag knm_calc_flags, mirror_knm_q_t *knm_q, bitflag *mismatch, bitflag *astigmatism);
bool should_integrate_mirror_aperture_knm(mirror_t *mirror, int mismatch, int astigmatism);
void mirror_knm_matrix_ident(mirror_knm_t* M);

void calc_mirror_limit_ws(double *wx1,double *wx2,double *wy1,double *wy2, KNM_MIRROR_NODE_DIRECTION_t knum, mr_knm_map_int_params_t *p);

void get_mirror_int_limit(mr_knm_map_int_params_t *p, int knum);
void get_mr_acc_zc_values(KNM_MIRROR_NODE_DIRECTION_t knum, mr_knm_map_int_params_t *pmr, u_nm_accel_t **a1, u_nm_accel_t **a2, complex_t **zc);
void dump_knm_map_integration_params(FILE *fp, mr_knm_map_int_params_t tmp);
void dump_mirror_knm_q(FILE *fp, mirror_knm_q_t tmp);
void dump_u_nm_accel(FILE *fp, u_nm_accel_t tmp);
void dump_maps(FILE *fp, mr_knm_map_int_params_t tmp);
void write_map_to_file(char *filename, int cols, int rows, double **data);


#endif	/* KAT_KNM_MIRROR_H */

