
/*!
 * \file kat.c
 * \brief Main routine and banner routine for finesse
 */

/*! \mainpage Finesse - Frequency domain INterferomEter Simulation SoftwarE
 *
 * Version: 0.99.x  (documentation built from $Revision$)
 *
 * \author Andreas Freise
 *
 * FINESSE is a fast interferometer simulation program.  For a given optical
 * setup, it computes the light field amplitudes at every point in the
 * interferometer assuming a steady state.  To do so, the interferometer
 * description is translated into a set of linear equations that are solved
 * numerically. For convenience, a number of standard analyses can be
 * performed automatically by the program, namely computing
 * modulation-demodulation error signals, transfer functions, shot-noise
 * limited sensitivities, and beam shapes.  FINESSE can perform the analysis
 * using the plane-wave approximation or Hermite-Gauss modes. The latter
 * allows computation of the properties of optical systems like telescopes
 * and the effects of mode matching and mirror angular positions. 
 *
 *  @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#include "kat.h"
#include "kat_config.h"
#include "kat_mem.h"
#include "kat_read.h"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_init.h"
#include "kat_calc.h"
#include "kat_spa.h"
#include "klu.h" 
#include "kat_inline.c"
#include "kat_matrix_ccs.h"
#include "kat_quant.h"
#include "kat_aa.h"
#include "utarray.h"

#if INCLUDE_PARALUTION == 1
#include <kat_paralution.h>
#endif

#ifdef SERVERMODE
#include "kat_server.h"
#endif  

#ifdef OSUNIX
#include <fpu_control.h>
#endif

#if OS == MACOS
#include <mach/mach.h>
#include <mach/mach_time.h>
#endif

#ifdef OSWIN
#undef UCHAR // formulc.h defines this already and so does windows.h, so dont do it again!
#include <windows.h>
#include <io.h>
#include <fcntl.h> 
#endif

init_variables_t init; //!< Structure to hold initialisation information
interferometer_t inter; //!< Interferometer and simulation variables
options_t options; //!< Options passed to finesse
memory_t mem; //!< Structure to hold memory allocation information

local_var_t vlocal; //!< Unchanged variables for faster function calls

int *t_s; //!< Type_of_Signal list [component type]
double *f_s; //!< Frequency value list [frequency]
complex_t ***a_s; //!< Amplitude list [field] [output] [frequeny]

global_var_t vglobal; //!< Structure to hold in, out and gnu filenames
klu_sparse_var_t klu; //!< Variables for KLU sparse matrix routines

ifo_matrix_vars_t M_ifo_car = {{0}};
ifo_matrix_vars_t M_ifo_sig = {{0}};

matrix_ccs_t M_qni = {0};

UT_icd motion_link_icd = {sizeof (motion_link_t), NULL, NULL, NULL};

int carrier_frequencies_tuned = 0;
int signal_frequencies_tuned = 0;

time_t now; //!< Current time
timespec_t clock_start; // clock cycle start for less than 1s runtime timing
char mydate[12] = {0}; //!< Date as at time of compilation

char functokens[27] = {0}; //!< Array of function tokens

FILE *fp_log; //!< Log file output file pointer
bool bCygserverRunning;

extern bool enableTiming;

#ifdef OSMAC
extern mach_timebase_info_data_t sTimebaseInfo;
#endif

transfer_func_t mtf_free_mass;
transfer_func_t mtf_unity;

char *__merged_kat_buffer = {0};
FILE *__merged_kat_file = NULL; /** This is a pointer to memory for storing a combined
                                 kat and some other included kat file when using the
                                 include command. Should not be accessed directly and 
                                 freed at the end of the run. */

#ifdef OSWIN
HANDLE pykat_pipe;
#else
int pykat_pipe;
#endif
int pykat_fd;
FILE* pykat_file;

//! The main routine

/*!
 * \param argc The number of input parameters
 * \param argv The array of input parameters
 */

int main(int argc, char *argv[]) {
    getTime(&clock_start);
    
#ifdef OSUNIX
    /*
      This puts the X86 FPU in 64-bit precision mode.  The default
      under Linux is to use 80-bit mode, which produces subtle
      differences from FreeBSD and other systems, eg,
      (int)(1000*atof("0.3")) is 300 in 64-bit mode, 299 in 80-bit
      mode.

      To be tested! Especially the function 'pow' might cause problems.
     */
    fpu_control_t cw;

    _FPU_GETCW(cw);
    cw &= ~_FPU_EXTENDED;
    cw |= _FPU_DOUBLE;
    _FPU_SETCW(cw);
#endif

#ifdef OSMAC
    // Some timer setup for OSX
    
    // If this is the first time we've run, get the timebase.
    // We can use denom == 0 to indicate that sTimebaseInfo is 
    // uninitialised because it makes no sense to have a zero 
    // denominator is a fraction.

    if ( sTimebaseInfo.denom == 0 ) {
        (void) mach_timebase_info(&sTimebaseInfo);
    }
   
    
#ifdef OSX_BUILD_VER
    
#define str(s) #s
#define xstr(s) str(s)
    
    const char *vcmd = "sysctl -n kern.osrelease | cut -d . -f 1";
    FILE *rs = popen(vcmd, "r");
    if(!rs) gerror("Couldn't open pipe to check OSX version at runtime\n");
    
    char buffer[LINE_LEN] = {0};
    
    if(fgets(buffer, sizeof(buffer), rs) == NULL){
        gerror("Couldn't check OSX version at runtime using command: '%s'\n", vcmd);
    }
    
    if(strlen(buffer) == 0)
        gerror("Empty result when running: '%s'\n", vcmd);
    
    // have to subtract 4 from the major number
    int major = atoi(buffer) - 4;
    char build_version[LINE_LEN] = {0};
    strcpy(build_version, xstr(OSX_BUILD_VER));
    
    if(strtok(build_version, ".") == NULL)
        bug_error("The build macro OSX_BUILD_VER was not in the expected format: '%s'\n", build_version);
    
    int build_major  = atoi(strtok(NULL, "."));
    
    if (major < build_major)
        gerror("This version of FINESSE was built for a minimum of \n"
               "10.%i, we have detected you are running 10.%i. Please\n"
               "obtain a version suitable for your version of OSX.\n\n", build_major, major);
    
    pclose(rs);
#endif
#endif
    
    FILE *ifp;
    FILE *fp = NULL;
    (void) fp;

    // add functions to formulc, probably a better place to put this...
    fnew("log10", (Func) log10, 1, 0);
    fnew("abs", (Func) fabs, 1, 0);

    // setup default free mass for optics
    mtf_free_mass.gain = 1.0;
    mtf_free_mass.phase = 0.0;
    mtf_free_mass.num_poles = 2;
    mtf_free_mass.num_zeros = 0;
    mtf_free_mass.type = GENERAL_TF;
    mtf_free_mass.index = -1;
    strcpy(mtf_free_mass.name, "free_mass");
    // set 2 complex poles with a value of 0
    mtf_free_mass.poles[0] = complex_0;
    mtf_free_mass.poles[1] = complex_0;

    mtf_unity.gain = 1.0;
    mtf_unity.phase = 0.0;
    mtf_unity.num_poles = 0;
    mtf_unity.num_zeros = 0;
    mtf_unity.type = Q_F_TF;
    mtf_unity.index = -2;
    strcpy(mtf_free_mass.name, "unity");

    bCygserverRunning = isCygserverRunning();

    char *s, *s2;
    int basenamelength;
    char errmsg[ERR_MSG_LEN] = {0};

    inter.setup = false;

    strcpy(mydate, MYTIME);
    strcpy(functokens, "abcdfghijklmnopqrstuvwxyz");

    /* **** get options **** */

    // reset options for ...
    options.quiet = 0; // switch off all output
    options.help = 0; // print help
    options.sparse_solver = KLU_FULL; // use KLU as default
    options.sparse_solver_manual_selection = 0; // overwrite KLU-SPARSE setting manually
    options.check = 0; // do a consistency check of the interferometer matrix
    options.perl1 = 0; // switch off some output
    options.maxmin = 0; // print max/min values
    options.servermode = 0; // switch between standalone and server mode
    options.portnumber = 0; // port number for client server connections (11000 to 110010)
    options.version = 0; // print version number and quit?
    options.noheader = 0; // do not print header in output file
    options.no_output_bksp = 0;
    options.use_coupling_reduction = false;
    options.nicslu_threads = 2;
    options.print_qnoise_matrix = 0;
    
    strcpy(options.out_format, DEFAULT_OUT_FORMAT);

    bool converting = false;

    // process command line arguments
    int num_options = 0;
    while ((argc > 1) && (argv[1 + num_options][0] == '-')) {
        bool found = false;

        if (converting)
            gerror("The --convert option can only be used by itself");

        // process -h option (print first help page)
        if (strcasecmp(argv[1 + num_options], "-h") == 0) {
            options.help = 1;
            ++num_options;
            --argc;
            found = true;
        }            // process -hh option (print second help page)
        else if (strcasecmp(argv[1 + num_options], "-hh") == 0) {
            options.help = 2;
            ++num_options;
            --argc;
            found = true;
        }
        else if (strcasecmp(argv[1 + num_options], "-print-qnoise") == 0) {
            options.print_qnoise_matrix = 1;
            ++num_options;
            --argc;
            found = true;
        }
        else if (strcasecmp(argv[1 + num_options], "--convert") == 0) {
            // this should be the first argument 
            if (num_options != 0)
                gerror("The --convert option can only be used by itself");

            converting = true;
            ++num_options;
            --argc;
            found = true;
        }            // process -v option 
        else if (strcasecmp(argv[1 + num_options], "-v") == 0) {
            options.version = 1;
            ++num_options;
            --argc;
            found = true;
        }            // process -max option (print max/min values)
        else if (strcasecmp(argv[1 + num_options], "-max") == 0) {
            options.maxmin = 1;
            ++num_options;
            --argc;
            found = true;
        }            // process -klu option (switch to KLU package)
        else if (strcasecmp(argv[1 + num_options], "-klu") == 0) {
            warn("Old KLU solver has been removed, defaulting to new KLU");
            options.sparse_solver = KLU_FULL;
            options.sparse_solver_manual_selection = 1;
            ++num_options;
            --argc;
            found = true;
        }
        else if (strcasecmp(argv[1 + num_options], "-klu-full") == 0) {
            options.sparse_solver = KLU_FULL;
            options.sparse_solver_manual_selection = 1;
            ++num_options;
            --argc;
            found = true;
            
        } else if (strncmp(argv[1 + num_options], "-nicslu", 7) == 0) {
#if INCLUDE_NICSLU == 1
            options.sparse_solver = NICSLU;
            options.sparse_solver_manual_selection = 1;

            if (sscanf(argv[1 + num_options], "-nicslu=%i", &options.nicslu_threads) != 1) {
                const char* val = getenv("KAT_NICSLU_THREADS");

                if (val != NULL) {
                    options.nicslu_threads = atoi(val);

                    if (options.nicslu_threads < 1) {
                        warn("Could not read number of threads from environment variable\nKAT_NICSLU_THREADS, setting threads to 2.\n");
                        options.nicslu_threads = 2;
                    } else {
                        debug_msg("Set number of nicslu threads to %i\n", options.nicslu_threads);
                    }
                } else
                    gerror("NICSLU flag usage is `-nicslu=N` where N is the number of threads to use\nOr set the KAT_NICSLU_THREADS environment variable to be an integer number of threads to use.");
            }
            if (options.nicslu_threads < 1)
                gerror("Number of NICSLU threads must be greater than 1\n");

            ++num_options;
            --argc;
            found = true;
#else
            gerror("NICSLU solver was not included in this build of FINESSE, please rebuild and include it.\n");
#endif
        } else if (strncmp(argv[1 + num_options], "-paralution", 11) == 0) {
#if INCLUDE_PARALUTION == 1
            options.sparse_solver = PARALUTION;
            options.sparse_solver_manual_selection = 1;

            if (sscanf(argv[1 + num_options], "-paralution=%i", &options.paralution_threads) != 1) {
                const char* val = getenv("KAT_PARALUTION_THREADS");

                if (val != NULL) {
                    options.paralution_threads = atoi(val);

                    if (options.paralution_threads < 1) {
                        warn("Could not read number of threads from environment variable\nKAT_PARALUTION_THREADS, setting threads to 2.\n");
                        options.paralution_threads = 2;
                    } else {
                        debug_msg("Set number of paralution threads to %i\n", options.paralution_threads);
                    }
                } else
                    gerror("PARALUTION flag usage is `-paralution=N` where N is the number of threads to use\nOr set the KAT_PARALUTION_THREADS environment variable to be an integer number of threads to use.");
            }
            
            if (options.paralution_threads < 1)
                gerror("Number of PARALUTION threads must be greater than 1\n");

            ++num_options;
            --argc;
            found = true;
#else
            gerror("PARALUTION solver was not included in this build of FINESSE, please rebuild and include it.\n");
#endif
        }            // process -sparse option (switch to SPARSE package)
        else if (strcasecmp(argv[1 + num_options], "-sparse") == 0) {
            warn("SPARSE has been removed, defaulting to new KLU");
            options.sparse_solver = KLU_FULL;
            options.sparse_solver_manual_selection = 1;
            ++num_options;
            --argc;
            found = true;
        }            // process --perl1 option (switch off some output)
        else if (strncmp(argv[1 + num_options], "--pykat", 7) == 0) {
            
            char* pname = strchr(argv[1 + num_options], '=');
            
            if (pname != NULL)
                pname++;
            else
                gerror("There was no equal sign found for pykat string, e.g --pykat=pipe_name\n");

            if (strlen(pname) == 0 || strlen(pname) > FNAME_LEN-10) {
                gerror("--pykat output must specify a pipe name, length 0 - %d\n", FNAME_LEN-10);
            }
            
#ifdef OSWIN
            sprintf(vglobal.pykat_pipename, "\\\\.\\pipe\\%s", pname);
#else
            sprintf(vglobal.pykat_pipename, "%s", pname);
#endif
            options.perl1 = 1;
            ++num_options;
            --argc;
            found = true;  
            
        } else if (strcasecmp(argv[1 + num_options], "--perl1") == 0) {
            warn("Use --pykat option instead of --perl1, it has been depreciated.");
            options.perl1 = 1;
            sprintf(vglobal.pykat_pipename, "");
            ++num_options;
            --argc;
            found = true;  
        } else if (strcasecmp(argv[1 + num_options], "--trace") == 0) {
            options.trace_output = 1;
            ++num_options;
            --argc;
            found = true;
        } else if (strcasecmp(argv[1 + num_options], "--no-backspace") == 0) {
            options.no_output_bksp = 1;
            ++num_options;
            --argc;
            found = true;
        }
            // process --quiet option (switch off all output)
        else if (strcasecmp(argv[1 + num_options], "--quiet") == 0) {
            options.quiet = 1;
            ++num_options;
            --argc;
            found = true;
        }            // process --noheader option (do not print header in output file)
        else if (strcasecmp(argv[1 + num_options], "--noheader") == 0) {
            options.noheader = 1;
            ++num_options;
            --argc;
            found = true;
        }
            // process --test option (runs unit tests, NOT for user distro)
        else if (strcasecmp(argv[1 + num_options], "--test") == 0) {
          //printf("\n##### Running tests #####\n");
          // RunAllTests();
          //printf("##### Finished tests #####\n");
            // clean up nicely
            //fflush(stdout);
            //fflush(stderr);
            //exit(0);
            options.test = 1;
            ++num_options;
            --argc;
            found = true;
        }

            // process --new option (switch on new features)
        else if (strcasecmp(argv[1 + num_options], "--new") == 0) {
            if (options.new_arg == -1) {
                gerror("options --new and --old mutually exclusive\n");
            }
            options.new_arg = 1;
            ++num_options;
            --argc;
            found = true;
        }            // process --old option (try to switch back to last version)
        else if (strcasecmp(argv[1 + num_options], "--old") == 0) {
            if (options.new_arg == 1) {
                gerror("options --new and --old mutually exclusive\n");
            }
            options.new_arg = -1;
            ++num_options;
            --argc;
            found = true;
        }            // process -c option (do consistency check of interferometer matrix)
        else if (strcasecmp(argv[1 + num_options], "-c") == 0) {
            options.check = 1;
            ++num_options;
            --argc;
            found = true;
        } else if (strncmp(argv[1 + num_options], "-cr", 3) == 0) {
            char in[10] = {0};
            int n = sscanf(argv[1 + num_options], "-cr=%4s", in);

            if (n == 1) {
                if (strcmp("on", in) == 0)
                    options.use_coupling_reduction = true;
                else if (strcmp("off", in) == 0)
                    options.use_coupling_reduction = false;
                else
                    gerror("Coupling reduction flag usage is `-cr=on` or `-cr=off`.\n");

            } else
                gerror("Coupling reduction flag usage is `-cr=on` or `-cr=off`.\n");

            ++num_options;
            --argc;
            found = true;
        }            // process format option
        else if (strncmp(argv[1 + num_options], "-format=", 8) == 0) {
            char* format = strchr(argv[1 + num_options], '=');

            if (format != NULL)
                format++;
            else
                gerror("There was no equal sign found for format string, e.g -format=%#.15g");

            if (strlen(format) == 0)
                gerror("-format output must specify a string");

            strcpy(options.out_format, format);
            strcat(options.out_format, " "); // add on an extra space

            ++num_options;
            --argc;
            found = true;
        }            // process -f option (print data to file with more precision)
        else if (strcasecmp(argv[1 + num_options], "-f") == 0) {
            warn("-f option depreciated, default is now 15dp output.");
            ++num_options;
            --argc;
            found = true;
        } else if (strcasecmp(argv[1 + num_options], "--perf-timing") == 0) {
            enableTiming = true;
            ++num_options;
            --argc;
            found = true;
        }            // process --server option (switch into server mode)
        else if (strcasecmp(argv[1 + num_options], "--server") == 0) {
            options.servermode = 1;
            ++num_options;
            --argc;
            // try to read port number 
            if ((num_options < argc) && (argv[1 + num_options][0] != '-')) {
                if (!(my_atoi(argv[1 + num_options], &options.portnumber)) &&
                        options.portnumber >= 11000 &&
                        options.portnumber <= 11010) {
                    found = true;
                    ++num_options;
                    --argc;
                }
            }

            if (!found) {
                banner(stdout, "*", "*", "*");
                gerror("usage: kat --server <portnumber (11000 to 11010)> "
                        "[options] inputfile ...\n");
            }
        }

        // warn that option is not found
        if (!found) {
            banner(stdout, "*", "*", "*");
            gerror("unknown option '%s'\n", argv[1 + num_options]);
        }
    }
    
    // print version number
    if (options.version == 1) {
        print_version(stdout);
        my_finish(0);
    }

    // print the first help page
    if (options.help == 1) {
        print_help();
        my_finish(0);
    }

    // print the second help page
    if (options.help == 2) {
        print_help2();
        my_finish(0);
    }

    // switch of max if in servermode
    if (options.maxmin && options.servermode) {
        warn("the option '-max' cannot be used in server mode.\n");
        options.maxmin = 0;
    }

    if (!converting) {
        // check that the correct number of args are passed in
        if (argc < 2 || argc > 4) {
            banner(stdout, "*", "*", "*");
            strcpy(errmsg, "usage: kat [options] infile [outfile [gnufile]]\n");
            strcat(errmsg, "       or usage: kat [options] basename\n");
            strcat(errmsg, "       (kat -h displays help message)\n");
            gerror(errmsg);
        }
    } else {
        // should only be the .knm file name after the -convert command
        if (argc != 3 && argc != 4)
            gerror("usage: kat --convert knm_file_prefix 1/2 (1:mirror, 2:beamsplitter) [new_knm_file_prefix]\n"
                "       or usage: kat [options] basename\n"
                "       (kat -h displays help message)\n");

        KNM_COMPONENT_t knmcmp;
        char *str_knmcmp = argv[2 + num_options];

        knmcmp = (KNM_COMPONENT_t)atoi(str_knmcmp);

        if (knmcmp != MIRROR_CMP && knmcmp != BEAMSPLITTER_CMP)
            gerror("The argument for the component '%s' was not valid, enter 1 or 2 for either a mirror or beamsplitter.", str_knmcmp);

        if (argc == 3) {
            convert_knm_file(argv[1 + num_options], "", knmcmp);
        } else if (argc == 4) {
            convert_knm_file(argv[1 + num_options], argv[3 + num_options], knmcmp);
        }

        // don't do anything after the convert so exit
        exit(0);
    }

    // process the arguments
    if (argc == 2) { // only the basename is specified...
        inter.basename = duplicate_string(argv[num_options + 1]);
        basenamelength = strlen(inter.basename);
        if (basenamelength > 4) {
            s2 = inter.basename + strlen(inter.basename) - 4;
            if (strcasecmp(s2, ".kat") == 0) {
                inter.basename[strlen(inter.basename) - 4] = '\0';
            }
        }

        s2 = inter.basename + strlen(inter.basename) - 1;
        if (*s2 == '.') {
            *s2 = '\0';
        }

        // adf 31072012 removed as it was breaking absolute and relative pathes on OSX/Unix
        // stripping leading dots and slashes
        //while (*inter.basename == '.' || *inter.basename == '/' || *inter.basename == '\\') {
        //    inter.basename++;
        //}

        // ddb - use the cygpath routine to change path to unix like one
#ifdef __CYGWIN__
        strcpy(inter.basename, get_path_i(inter.basename, true));
#endif  
        strcpy(vglobal.input_fname, inter.basename);
        strcat(vglobal.input_fname, ".kat");
        strcpy(vglobal.output_fname, inter.basename);
        strcat(vglobal.output_fname, ".out");
        strcpy(vglobal.gnuplot_fname, inter.basename);
        strcat(vglobal.gnuplot_fname, ".gnu");
#ifdef OSWIN
        strcpy(vglobal.gnuplot_test_fname, ".");
        strcat(vglobal.gnuplot_test_fname, inter.basename);
        strcat(vglobal.gnuplot_test_fname, ".tmp");
#endif
        strcpy(vglobal.log_fname, inter.basename);
        strcat(vglobal.log_fname, ".log");
    } else if (argc == 3) { // the input and output file names are specified...
        inter.basename = duplicate_string(argv[num_options + 1]);
        if ((s = strchr(inter.basename, '.')) != NULL) {
            *s = '\0';
        }
        strcpy(vglobal.input_fname, argv[num_options + 1]);
        strcpy(vglobal.output_fname, argv[num_options + 2]);
        strcpy(vglobal.gnuplot_fname, inter.basename);
        strcat(vglobal.gnuplot_fname, ".gnu");
#ifdef OSWIN
        strcpy(vglobal.gnuplot_test_fname, ".");
        strcat(vglobal.gnuplot_test_fname, inter.basename);
        strcat(vglobal.gnuplot_test_fname, ".tmp");
#endif
        strcpy(vglobal.log_fname, inter.basename);
        strcat(vglobal.log_fname, ".log");
    } else if (argc == 4) { // input, output and gnuplot file names are given...
        inter.basename = duplicate_string(argv[num_options + 1]);
        if ((s = strchr(inter.basename, '.')) != NULL) {
            *s = '\0';
        }
        strcpy(vglobal.input_fname, argv[num_options + 1]);
        strcpy(vglobal.output_fname, argv[num_options + 2]);
        strcpy(vglobal.gnuplot_fname, argv[num_options + 3]);
#ifdef OSWIN
        strcpy(vglobal.gnuplot_test_fname, ".");
        strcat(vglobal.gnuplot_test_fname, argv[num_options + 3]);
        strcat(vglobal.gnuplot_test_fname, ".tmp");
#endif
        strcpy(vglobal.log_fname, inter.basename);
        strcat(vglobal.log_fname, ".log");
    }
    
    // setup pykat pipe if running
    if (options.perl1) {
        
        if(strlen(vglobal.pykat_pipename) == 0){
            pykat_file = stdout;
        } else {
#ifdef OSWIN
            pykat_pipe = CreateNamedPipe( 
                                vglobal.pykat_pipename, // name of the pipe 
                                PIPE_ACCESS_OUTBOUND, // 1-way pipe -- send only 
                                PIPE_TYPE_BYTE, // send data as a byte stream 
                                1, // only allow 1 instance of this pipe 
                                0, // no outbound buffer 
                                0, // no inbound buffer 
                                0, // use default wait time 
                                NULL // use default security attributes 
                                ); 

            if (pykat_pipe == NULL || pykat_pipe == INVALID_HANDLE_VALUE) {
                gerror("Couldn't open pykat pipe. Error=%lu", GetLastError());
            }

            BOOL result = ConnectNamedPipe(pykat_pipe, NULL);

            if (!result) {
                gerror("Failed to make connection on named pipe. Error=%lu", GetLastError());
            }

            pykat_fd = _open_osfhandle((intptr_t)pykat_pipe, _O_WRONLY);
            pykat_file = _fdopen(pykat_fd, "w");
#else
            pykat_pipe = mkfifo(vglobal.pykat_pipename, S_IRWXU);

            int pykat_fd = open(vglobal.pykat_pipename, O_WRONLY);

            // makes write pipe nonblocking when opened
            // fcntl(pykat_fd, F_SETFL, fcntl(pykat_fd, F_GETFL) | O_NONBLOCK);

            pykat_file = fdopen(pykat_fd, "w");

            // inform pykat what version we're running
            fprintf(pykat_file, "version:%s\n", VERSION);
        
#endif
        }
    }    
    
    int tid_main = startTimer("MAIN");
    int tid_startup = startTimer("STARTUP");

    // Generating name for Matlab output files
    // (some of this code is repeated in kat_gnu.c void matlabfile(FILE *fp, char *fname)
    // TODO define new string ...)
    char matlab_func_str[LINE_LEN];
    strcpy(matlab_func_str, inter.basename);
    int replace_error = replace_string(matlab_func_str, "-", "_", 1, 1);
    if (replace_error == -1) {
        bug_error("could not replace '-' in matlab string");
    }
    strcpy(vglobal.matlab_fname, matlab_func_str);
    strcat(vglobal.matlab_fname, ".m");

    // Generating name for Python output files
    strcpy(vglobal.python_fname, inter.basename);
    strcat(vglobal.python_fname, ".py");

    // open the log file
    open_file_to_write_ascii(vglobal.log_fname, &fp_log);

    // set up message reporting if perl1/quiet option isn't set
    if (!options.quiet) {
        banner(stdout, vglobal.input_fname, vglobal.output_fname,
                vglobal.gnuplot_fname);
    }
    
    // output the banner to the log file in any case
    if (fp_log != NULL) {
        banner(fp_log, vglobal.input_fname, vglobal.output_fname,
                vglobal.gnuplot_fname);
    }

    // check to see if we can open the input file
    if ((ifp = fopen(vglobal.input_fname, "r")) == NULL) {
        gerror("cannot open '%s'\n", vglobal.input_fname);
    }

    // this function should check for an include command in the kat file
    // if it finds one it will read in the included file and prepend it to
    // this file.
    check_for_include_kat(&ifp);

    if (options.use_coupling_reduction) {
        warn("COUPLING REDUCTION ENABLED! New feature, use with care...\n");
    }

    // read in environmental variables
    read_env();
    endTimer(tid_startup);

    int tid_init = startTimer("INIT");
    // read the initialisation file
    read_init();
    initialise_simulation_variables();
    endTimer(tid_init);

    // prescan and reading of input file
    int tid_prescan = startTimer("PRESCAN");
    pre_scan(ifp);
    endTimer(tid_prescan);


    int tid_read = startTimer("READ_FILE");
    read_file(ifp);
    fclose(ifp);
    fflush(stdout);

    // ensure that all light inputs are actually connected to something
    {
        int i;

        for (i = 0; i < inter.num_light_inputs; i++) {
            light_in_t *in = &inter.light_in_list[i];
            assert(in != NULL);

            node_connections_t *nc = &inter.node_conn_list[in->node_index];
            assert(nc != NULL);

            if ((nc->comp_1 == NULL && nc->comp_2 == NULL)) {
                gerror("Input %s must be connected to another optical component, e.g. a space (detectors do not count)\n", in->name);
            }
        }
    }

    endTimer(tid_read);

    int i = 0;

    // TODO: move this frequency computation part into a separate function for clarity
    {
        // set type of matrices
        M_ifo_car.type = STANDARD;
        M_ifo_sig.type = SIGNAL_QCORR;

        // first we need to reduce the carrier frequencies if multiple input
        // light sources are present at the same frequency. Also need to take
        // into account if one of those laser frequencies are being tuned or put
        reduce_frequency_linked_list();

        // The number of carrier frequencies have now been reduced so that
        // only unique and tuned frequencies exist. Now we generate all the 
        // modulation and sideband frequencies around them, as well as the
        // user frequency rules
        fill_frequency_linked_list();

        // now do yet another reduction to remove more duplicates that are not tuned...
        reduce_frequency_linked_list();
        
        inter.num_carrier_freqs = 0;
        inter.num_signal_freqs = 0;

        // convert the linked list into an array for index access
        inter.carrier_f_list = (frequency_t**) calloc(inter.frequency_linked_list.count, sizeof (frequency_t*));
        inter.signal_f_list = (frequency_t**) calloc(inter.frequency_linked_list.count, sizeof (frequency_t*));

        if (inter.carrier_f_list == NULL || inter.signal_f_list == NULL)
            gerror("Memory allocation for used frequency list failed");

        // Now loop over the reduced frequencies and add them to a static array
        // which will be used for accessing frequencies for eitehr carriers
        // or signals
        frequency_node_t *curr = inter.frequency_linked_list.head;

        while (curr != NULL) {
            if (inter.num_signal_freqs + inter.num_carrier_freqs >= inter.frequency_linked_list.count)
                bug_error("Trying to add more frequencies than memory allocated for");

            if (curr->me->isTuned) {
                if (curr->me->type == SIGNAL_FIELD)
                    signal_frequencies_tuned = 1;
                else
                    carrier_frequencies_tuned = 1;
            }

            if (curr->me->type == CARRIER_FIELD
                    || curr->me->type == MODULATOR_FIELD
                    || curr->me->type == USER_FIELD) {

                curr->me->index = inter.num_carrier_freqs;
                inter.carrier_f_list[inter.num_carrier_freqs] = curr->me;

                curr = curr->next;

                inter.num_carrier_freqs++;

            } else if (curr->me->type == SIGNAL_FIELD) {
                curr->me->index = inter.num_signal_freqs;
                inter.signal_f_list[inter.num_signal_freqs] = curr->me;

                curr = curr->next;

                inter.num_signal_freqs++;
            }
        }

        // ensure we have everything
        assert(inter.num_carrier_freqs + inter.num_signal_freqs == inter.frequency_linked_list.count);

        if (inter.frequency_flag & PRINT_FREQUENCIES)
            print_used_frequency_list();
        
        // Check the frequency dependant BS (R,T) values if set
        beamsplitter_t *bs;
        int k;

        for(k=0; k<inter.num_beamsplitters; k++) {
            bs = &inter.bs_list[k];

            if(bs->fRT) {
                fRT_t *s;

                for(s=bs->fRT; s != NULL; s=s->hh.next) {
                    bool success = false;

                    int j;
                    for(j=0; j<inter.num_carrier_freqs; j++){
                        if(strcasecmp(inter.carrier_f_list[j]->name, s->name) == 0){
                            success = true;
                            break;
                        }
                    }

                    for(j=0; j<inter.num_signal_freqs; j++){
                        if(strcasecmp(inter.signal_f_list[j]->name, s->name) == 0){
                            success = true;
                            break;
                        }
                    }

                    if(!success){
                        gerror("%s fRT config: Could not find frequency `%s`\n", bs->name, s->name);
                    }
                }
            }
        }
        
        // Check inputs to see if they used a frequency name to link with it
        for(i=0; i<inter.num_light_inputs; i++){
            light_in_t *lin = &inter.light_in_list[i];
            
            if(lin->f == NULL) {
                assert(strlen(lin->__frequency_name) > 0);
                int j;
                
                for(j=0; j<inter.num_carrier_freqs; j++){
                    if(strcmp(inter.carrier_f_list[j]->name, lin->__frequency_name) == 0) {
                        lin->f = inter.carrier_f_list[j];
                        break;
                    }
                }
                
                if(lin->f == NULL){
                    gerror("Could not find frequency `%s` for input %s\n.", lin->__frequency_name, lin->name);
                }
            }
            
        }
        
        // inform matrix which list of frequencies they are associated with
        M_ifo_car.frequencies = inter.carrier_f_list;
        M_ifo_car.num_frequencies = inter.num_carrier_freqs;

        M_ifo_sig.frequencies = inter.signal_f_list;
        M_ifo_sig.num_frequencies = inter.num_signal_freqs;

        // Now allocate memory for storing the coupling between matrices
        alloc_matrix_ccs_freq_couplings(&M_ifo_car);

        if (inter.num_signal_freqs > 0)
            alloc_matrix_ccs_freq_couplings(&M_ifo_sig);


        // Now we need to fill in the f_couple_allocd array for all the elements
        // that might couple the various frequencies. This is used to determine
        // whether to add the elements needed for coupling between the frequencies.
        // The worst case scenario here is when a put command is setting the value
        // of the frequency, if this is the case then there is no way of telling what
        // value it will be at some point. So it may end up coupling or it may not
        // so we have to allocate the memory and matrix elements for it just incase
        update_frequency_coupling(1);

        if (inter.frequency_flag & PRINT_MODULATOR_COUPLINGS) {
            for (i = 0; i < inter.num_modulators; i++) {
                
                message("Carrier Frequencies\n");
                print_frequency_couplings(inter.modulator_list[i].name, inter.num_carrier_freqs,
                        M_ifo_car.mod_does_f_couple[i],
                        M_ifo_car.mod_f_couple_order[i]);

                if (inter.num_signal_freqs > 0) {
                    message("Signal Frequencies\n");
                    print_frequency_couplings(inter.modulator_list[i].name, inter.num_signal_freqs,
                            M_ifo_sig.mod_does_f_couple[i],
                            M_ifo_sig.mod_f_couple_order[i]);
                }
            }
        }

        if (inter.frequency_flag & PRINT_MIRROR_COUPLINGS) {
            for (i = 0; i < inter.num_mirror_dithers; i++) {
                message("Carrier Frequencies\n");
                print_frequency_couplings(inter.mirror_dither_list[i]->name, inter.num_carrier_freqs,
                        M_ifo_car.m_does_f_couple[i],
                        M_ifo_car.m_f_couple_order[i]);


                if (inter.num_signal_freqs > 0) {
                    message("Signal Frequencies\n");
                    print_frequency_couplings(inter.mirror_dither_list[i]->name, inter.num_signal_freqs,
                            M_ifo_sig.m_does_f_couple[i],
                            M_ifo_sig.m_f_couple_order[i]);
                }
            }
        }

        if (inter.frequency_flag & PRINT_BS_COUPLINGS) {
            for (i = 0; i < inter.num_bs_dithers; i++) {
                message("Carrier Frequencies\n");
                print_frequency_couplings(inter.bs_dither_list[i]->name, inter.num_carrier_freqs,
                        M_ifo_car.bs_does_f_couple[i],
                        M_ifo_car.bs_f_couple_order[i]);


                if (inter.num_signal_freqs > 0) {
                    message("Signal Frequencies\n");
                    print_frequency_couplings(inter.bs_dither_list[i]->name, inter.num_signal_freqs,
                            M_ifo_sig.bs_does_f_couple[i],
                            M_ifo_sig.bs_f_couple_order[i]);
                }
            }
        }

        // We also need to compute which index each frequency will be placed in the result arrays: f_s,a_s and t_s

        // Important!! To get more comparable results with the old Finesse code the order
        // in which the frequencies are stored and later used for computations is important.
        // This can be the difference between things equally exactly 0 or not for example.

        int f, g;
        frequency_t *f1, *f2;
        inter.num_frequencies = 0;

        for (f = 0; f < inter.num_carrier_freqs; f++) {
            f1 = inter.carrier_f_list[f];
            f1->result_index = inter.num_frequencies++;
            t_s[f1->result_index] = f1->type;
            f_s[f1->result_index] = f1->f;

            // now add signal frequencies if they exist
            for (g = 0; g < inter.num_signal_freqs; g++) {
                f2 = inter.signal_f_list[g];

                if (f2->carrier == f1) {
                    f2->result_index = inter.num_frequencies++;
                    // store the signal frequencies after the carriers
                    t_s[f2->result_index] = f2->type;
                    f_s[f2->result_index] = f2->f;
                }
            }
        }

        assert(inter.num_frequencies == inter.num_signal_freqs + inter.num_carrier_freqs);

        // Now we have computed and parsed how many frequencies and various degrees of freedom
        // each component has we can allocate more sensibly the the memory and compute the total
        // number of motion equations.
        int i;
        for (i = 0; i < inter.num_mirrors; i++) {
            mirror_t *m = &inter.mirror_list[i];
            inter.num_motion_eqns += alloc_mirror_motions(m, inter.num_signal_freqs);
        }

        for (i = 0; i < inter.num_beamsplitters; i++) {
            beamsplitter_t *bs = &inter.bs_list[i];
            inter.num_motion_eqns += alloc_bs_motions(bs, inter.num_signal_freqs);
        }
    }

    // check whether we should automatically retrace
    if (inter.rebuild & 2 && !inter.retrace_manual) {
        inter.retrace = 1;
        message("Using automatic retrace\n");
    }

    // print helpful info if required
    if ((inter.tem > 4 && options.check) || inter.tem > 10) {
        message("  ...initialising system, please wait!\n");
    }

    fflush(stdout);

    // set everything up
    int tid_setup = startTimer("SETUP_SYSTEM");
    setup_system();
    endTimer(tid_setup);

    fflush(stdout);

    // do sparse matrix stuff
    unsigned tid = startTimer("SPA_CREATE");
    
    // solve everything in one matrix
    long bytes = 0;

    // number of carriers are just the number of carrier frequencies we have with how many HG fields
    // then the number of reduced nodes, carriers are not computed at dump nodes
    M_ifo_car.M.num_eqns = M_ifo_car.num_frequencies * inter.num_fields * 2 * inter.num_reduced_nodes;

    assert(M_ifo_car.M.num_eqns > 0);

    alloc_ifo_matrix_ccs(&bytes, &M_ifo_car);

    set_coupling_info();

    check_node_direction();

    // position each of the components in the matrix
    build_ccs_matrix(&M_ifo_car);
    get_ccs_matrix(&M_ifo_car);
    set_light_input_ports();
    //get_ccs_matrix_sparse_input_blocks();

    if (M_ifo_sig.num_frequencies > 0) {
        // The signal matrix also depends on the number of signal frequencies and also the number
        // of nodes including dump nodes as these will later be used for computing quantum noise fluctuations
        // Dump nodes only have an input port and no output hence twice the reduced nodes.
        M_ifo_sig.M.num_eqns = (M_ifo_sig.num_frequencies
                * inter.num_fields
                * (2 * inter.num_reduced_nodes))
                + inter.num_motion_eqns + inter.num_slinks;

        alloc_ifo_matrix_ccs(&bytes, &M_ifo_sig);

        // only allocate dump nodes if we are computing the quantum noise fluctuations
        build_ccs_matrix(&M_ifo_sig);

        get_ccs_matrix(&M_ifo_sig);
    } else {
        // ensure that there is definitely no equations to solve
        // for the signal matrix if we have no signal frequencies
        M_ifo_sig.M.num_eqns = -1;
    }

    endTimer(tid);

    tid = startTimer("INIT_QUANT");
    init_quantum_components();
    endTimer(tid);

    // check the connections in the interferometer
    tid = startTimer("CHECK_CONN");
    check_connections();
    endTimer(tid);

    if (!options.servermode) {
        make_space();
    }

    // fill in the matrix
    tid = startTimer("SPARSE_FILL");

    fill_ccs_matrix(&M_ifo_car);

    if (M_ifo_sig.M.num_eqns > 0)
        fill_ccs_matrix(&M_ifo_sig);

    endTimer(tid);

    // print matrix to file for looking at the sparsity for example 
    if (inter.printmatrix) {
        dump_matrix_ccs(&M_ifo_car.M, "klu_full_matrix_car.dat");
        if (M_ifo_sig.M.num_eqns > 0)
            dump_matrix_ccs(&M_ifo_sig.M, "klu_full_matrix_sig.dat");
    }

    // Preorder KLU Matrix
    tid = startTimer("MATRIX_PREORDER");
    if (options.sparse_solver == KLU_FULL) {

        klu_objs_t *klu_obj = (klu_objs_t*) M_ifo_car.solver_opts;

        klu_l_defaults(&(klu_obj->Common));

        klu_obj->Common.ordering = 0;
        klu_obj->Common.scale = 2;
        klu_obj->Common.btf = 1;
        klu_obj->Common.maxwork = 0;

        klu_obj->Symbolic = klu_l_analyze(M_ifo_car.M.num_eqns,
                (UF_long*) M_ifo_car.M.col_ptr,
                (UF_long*) M_ifo_car.M.row_idx,
                &(klu_obj->Common));

        if (klu_obj->Common.status != KLU_OK)
            gerror("An error occurred in KLU during analysis: STATUS=%i\n", klu_obj->Common.status);

        klu_obj->Numeric = klu_zl_factor((UF_long*) M_ifo_car.M.col_ptr,
                (UF_long*) M_ifo_car.M.row_idx,
                M_ifo_car.M.values,
                klu_obj->Symbolic, &(klu_obj->Common));

        if (klu_obj->Common.status == KLU_SINGULAR)
            gerror("Carrier matrix is singular!!\n", klu_obj->Common.status);
        else if (klu_obj->Common.status != KLU_OK)
            gerror("An error occurred in carrier matrix KLU during factorisation: STATUS=%i\n", klu_obj->Common.status);

        if (klu_obj->Common.status == KLU_OK)
            klu_zl_sort(klu_obj->Symbolic, klu_obj->Numeric, &(klu_obj->Common));

        if (M_ifo_sig.num_frequencies > 0) {
            klu_obj = (klu_objs_t*) M_ifo_sig.solver_opts;
            // set defaults for signal matrix computation 
            klu_obj = (klu_objs_t*) M_ifo_sig.solver_opts;

            klu_l_defaults(&(klu_obj->Common));

            klu_obj->Common.ordering = 0;
            klu_obj->Common.scale = 2;
            klu_obj->Common.btf = 1;
            klu_obj->Common.maxwork = 0;

            klu_obj->Symbolic = klu_l_analyze(M_ifo_sig.M.num_eqns,
                    (UF_long*) M_ifo_sig.M.col_ptr,
                    (UF_long*) M_ifo_sig.M.row_idx,
                    &(klu_obj->Common));

            if (klu_obj->Common.status != KLU_OK) {
                gerror("An error occurred in signal matrix KLU during analysis: STATUS=%i\n", klu_obj->Common.status);
            }

            klu_obj->Numeric = klu_zl_factor((UF_long*) M_ifo_sig.M.col_ptr,
                    (UF_long*) M_ifo_sig.M.row_idx,
                    M_ifo_sig.M.values,
                    klu_obj->Symbolic, &(klu_obj->Common));

            if (klu_obj->Common.status == KLU_SINGULAR)
                warn("Signal matrix is singular!!\n", klu_obj->Common.status);
            else if (klu_obj->Common.status != KLU_OK)
                gerror("An error occurred in KLU during factorisation: STATUS=%i\n", klu_obj->Common.status);

            if (klu_obj->Common.status == KLU_OK)
                klu_zl_sort(klu_obj->Symbolic, klu_obj->Numeric, &(klu_obj->Common));
        }

#if INCLUDE_NICSLU == 1
    } else if (options.sparse_solver == NICSLU) {
        int ret = 0, i = 0;

        nicslu_objs_t * arr[2] = {(nicslu_objs_t*) M_ifo_car.solver_opts, NULL};
        ifo_matrix_vars_t * ifoM[2] = {&M_ifo_car, &M_ifo_sig};

        if (M_ifo_sig.num_frequencies > 0)
            arr[1] = (nicslu_objs_t*) M_ifo_sig.solver_opts;

        for (i = 0; i < 2; i++) {
            nicslu_objs_t *objs = arr[i];

            if (objs == NULL) break;

            handle_NICSLU(NicsLUc_Initialize(&objs->nicslu));

            // complex__t and complex_t are the same structure
            // so we can just cast to it
            handle_NICSLU(NicsLUc_CreateMatrix(&objs->nicslu, ifoM[i]->M.num_eqns,
                    ifoM[i]->M.num_nonzeros,
                    (complex__t*) ifoM[i]->M.values,
                    (uint__t*) ifoM[i]->M.row_idx,
                    (uint__t*) ifoM[i]->M.col_ptr));

            // set to non-zero to signify we are using CSC format
            objs->nicslu.cfgi[0] = 1;

            handle_NICSLU(NicsLUc_Analyze(&objs->nicslu));
            debug_msg("analysis time: %.8g\n", objs->nicslu.stat[0]);

            if (options.nicslu_threads > 1) {
                debug_msg("Using mutlithreaded NICSLU, processes = %i\n", options.nicslu_threads);

                ret = handle_NICSLU(NicsLUc_CreateScheduler(&objs->nicslu));
                debug_msg("time of creating scheduler: %.8g\n", objs->nicslu.stat[4]);
                debug_msg("suggestion: %s.\n", ret == 0 ? "parallel" : "sequential");

                handle_NICSLU(NicsLUc_CreateThreads(&objs->nicslu, options.nicslu_threads, TRUE));
                debug_msg("total cores: %d, threads created: %d\n", (int) (objs->nicslu.stat[9]), (int) (objs->nicslu.cfgi[5]));

                handle_NICSLU(NicsLUc_BindThreads(&objs->nicslu, FALSE));
            }

            // NicsLUc_CreateScheduler will return whether the matrix is suitable
            // for multithreaded factorisation or not
            if (ret == NICS_OK && options.nicslu_threads > 1) {

                handle_NICSLU(NicsLUc_Factorize_MT(&objs->nicslu));
                debug_msg("factorization time: %.8g\n", objs->nicslu.stat[1]);

            } else {
                handle_NICSLU(NicsLUc_Factorize(&objs->nicslu));
                debug_msg("factorization time: %.8g\n", objs->nicslu.stat[1]);
            }
        }
#endif
#if INCLUDE_PARALUTION == 1
    } else if (options.sparse_solver == PARALUTION) {
        setup_paralution(options.paralution_threads);
        
        setup_paralution_matrix(&M_ifo_car);
        
        if (M_ifo_sig.num_frequencies > 0) {
            setup_paralution_matrix(&M_ifo_sig);
        }
        
#endif
    } else
        bug_error("spare_solver %i was not expected", options.sparse_solver);

    endTimer(tid);

    // Now we start computing the xaxis
    if (inter.x3_axis_is_set) {
        x3_out();
    } else if (options.servermode) {
#ifdef SERVERMODE
        initserver();
#else
        warn("Servermode not implemented in Windows binary\n");
#endif
    } else {
        x12_out();
    }

    tid = startTimer("MATRIX_DESTROY");

    free_ccs_matrix(&M_ifo_car);
    
    endTimer(tid);

    // TODO need to add more memory freeing but for now here is the mirror stuff
    //free_memory_for_mirror_list();

    // be nice and clean up after ourselves
    my_finish(0);

    endTimer(tid_main);
    
    closeTimerFile();

    if (__merged_kat_buffer != NULL)
        free(__merged_kat_buffer);
    
    return 0;
}

/*!
 * \param return_value the return value to return from the program as a whole
 */
void my_finish(int return_value) {
    
    if(options.perl1){
        if(strlen(vglobal.pykat_pipename) != 0){
            fclose(pykat_file);
#ifdef OSWIN
            CloseHandle(pykat_pipe);
#else
            unlink(vglobal.pykat_pipename);
#endif
        }
    }
    
#if INCLUDE_PARALUTION == 1
    if(options.sparse_solver == PARALUTION)
        cleanup_paralution();
#endif
    
#ifndef TEST
    
    fflush(stdout);
    fflush(stderr);
#ifdef OSWIN
    remove(vglobal.gnuplot_test_fname);
#endif
    exit(return_value);
    
    // close the log file nicely
    if (fp_log != NULL) {
        fclose(fp_log);
    }
#else
    message("Returned with return code: %d\n", return_value);
#endif
}

//! Prints the banner for Finesse

/*!
 * \param fp The file pointer for direction of the output
 * \param input_fname The input file name
 * \param output_fname The output file name
 * \param gnuplot_fname The name of the gnuplot file
 */
void banner(FILE *fp, char *input_fname, char *output_fname, char *gnuplot_fname) {
    
    now = time(NULL);

    char extra_space[80] = "                                                     ";
    int num_spaces = 33 - strlen(VERSION) - strlen(GIT_REVISION);
    if (num_spaces < 0 || num_spaces > 20) {
        num_spaces = 0;
    }

    char debug_build[LINE_LEN] = {0};

#ifdef DEBUG
    sprintf(debug_build, "WARNING: USING DEBUG BUILD!!");
#else
    //sprintf(debug_build,"");
#endif

    // print the banner if not quiet
    fprintf(fp, "\n"
            "------------------------------------------------------------------------\n"
            "                     FINESSE %s %.*s (build %s)\n"
            "       o_.-=.        Frequency domain INterferomEter Simulation SoftwarE\n"
            "      (\\'\".\\|        %s         http://www.gwoptics.org/finesse/\n"
            "      .>' (_--.      %s\n"
            "   _=/d   ,^\\        Input file %s,\n"
            "  ~~ \\)-'   '        Output file %s,\n"
            "     / |             Gnuplot file %s \n"
            "    '  '                                        %s"
            "------------------------------------------------------------------------\n",
            VERSION, num_spaces, extra_space, GIT_REVISION, mydate, debug_build,
            get_path_i(input_fname, false), get_path_i(output_fname, false), get_path_i(gnuplot_fname, false),
            asctime(localtime(&now)));
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
