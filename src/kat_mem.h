// $Id$

/*!
 * \file kat_mem.h
 * \brief Header file for memory allocation routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_MEM_H
#define KAT_MEM_H

void pre_scan(FILE *fp);


void allocate_zmatrix(complex_t ***A, size_t N, long int *bytes);
void allocate_zmatrix2(complex_t ***A, size_t Nx, size_t Ny, long int *bytes);
void free_zmatrix(zmatrix A);
void allocate_dmatrix(dmatrix *A, size_t N, long int *bytes);
void allocate_dmatrix2(dmatrix *A, size_t Nx, size_t Ny, long int *bytes);
void free_dmatrix(dmatrix A);

void allocate_complex_pointer_matrix(complex_t ****A, size_t N, long int *bytes);

int alloc_mirror_motions(mirror_t *m, int num_sig_freqs);
int alloc_bs_motions(beamsplitter_t *m, int num_sig_freqs);

int allocate_memory_for_feedback_list(long int *bytes);
int allocate_memory_for_mech_transfer_funcs(long int *bytes);
int allocate_memory_for_motion_detector_list(long int *bytes);
int allocate_memory(long int *bytes, long int *bytes1, long int *bytes2);
int allocate_memory_for_gnuterms(long int *bytes);
int allocate_memory_for_pyterms(long int *bytes);
int allocate_memory_for_frequency_list(long int *bytes);
int allocate_memory_for_mirror_list(long int *bytes);
int allocate_memory_for_beamsplitter_list(long int *bytes);
int allocate_memory_for_grating_list(long int *bytes);
int allocate_memory_for_squeezer_list(long int *bytes);
int allocate_memory_for_space_list(long int *bytes);
int allocate_memory_for_sagnac_list(long int *bytes);
int allocate_memory_for_block_list(long int *bytes);
int allocate_memory_for_light_input_list(long int *bytes);
int allocate_memory_for_detector_list(long int *bytes);
int allocate_memory_for_output_data_list(long int *bytes);
int allocate_memory_for_beam_param_list(long int *bytes);
int allocate_memory_for_cavity_param_list(long int *bytes);
int allocate_memory_for_convolution_list(long int *bytes);
int allocate_memory_for_mirror_phase_list(long int *bytes);
int allocate_memory_for_modulator_list(long int *bytes);
int allocate_memory_for_signal_list(long int *bytes);
int allocate_memory_for_derivative_list(long int *bytes);
int allocate_memory_for_scale_cmd_list(long int *bytes);
int allocate_memory_for_variable_list(long int *bytes);
int allocate_memory_for_diode_list(long int *bytes);
int allocate_memory_for_lens_list(long int *bytes);
int allocate_memory_for_locks(long int *bytes);
int allocate_memory_for_function_list(long int *bytes);
int allocate_memory_for_amplitude_list(long int *bytes);
int allocate_memory_for_quantum_amplitude_list(long int *bytes);
int allocate_memory_for_quantum_input_list();
int allocate_memory_for_dbs_list(long int *bytes);

void free_memory_for_mirror_list();

void make_space(void);
void read_lambda(const char *command_string);

#endif // KAT_MEM_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */
