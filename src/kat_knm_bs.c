#include "kat.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_mem.h"
#include "kat_fortran.h"
#include "kat_optics.h"
#include "kat_aux.h"
#include "kat_dump.h"
#include "kat_check.h"
#include "kat_aa.h"
#include "kat_knm_int.h"
#include "kat_knm_bs.h"
#include "kat_knm_bayer.h"

extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;
extern local_var_t vlocal;

extern FILE *fp_log;
extern bs_knm_t bstmp;
extern FILE * ipfile;
extern FILE * idfile;

bs_knm_t bstmap;

extern const complex_t complex_i; //!< sqrt(-1) or 0 + i
extern const complex_t complex_1; //!< 1 but in complex space: 1 + 0i
extern const complex_t complex_m1; //!< -1 but in complex space: -1 + 0i
extern const complex_t complex_0; //!< 0 but in complex space: 0 + 0i

// naming convention here work as:
// acc_WX_nrY_Z
//      WX - coupling coefficient
//      Y - refractive index mode W if Z=1 or X if Z=2
//      Z - input mode(1) or output mode (2)
static u_nm_accel_t *acc_12_nr1_1, *acc_12_nr1_2;
static u_nm_accel_t *acc_21_nr1_1, *acc_21_nr1_2;
static u_nm_accel_t *acc_34_nr2_1, *acc_34_nr2_2;
static u_nm_accel_t *acc_43_nr2_1, *acc_43_nr2_2;

static u_nm_accel_t *acc_13_nr1_1, *acc_13_nr2_2;
static u_nm_accel_t *acc_31_nr2_1, *acc_31_nr1_2;
static u_nm_accel_t *acc_24_nr1_1, *acc_24_nr2_2;
static u_nm_accel_t *acc_42_nr2_1, *acc_42_nr1_2;

inline void CHECK_BS_KNM_T(bs_knm_t *A){
    assert(A!=NULL);
    assert(A->k12!=NULL);
    assert(A->k21!=NULL);
    assert(A->k34!=NULL);
    assert(A->k43!=NULL);
    assert(A->k13!=NULL);
    assert(A->k31!=NULL);
    assert(A->k24!=NULL);
    assert(A->k42!=NULL);
		(void)*A; //silence compiler warning
}

inline bool IS_BS_KNM_ALLOCD(bs_knm_t *A){
    return (((A)->k12 != NULL) && ((A)->k21 != NULL)
            && ((A)->k34 != NULL) && ((A)->k43 != NULL)
            && ((A)->k13 != NULL) && ((A)->k31 != NULL)
            && ((A)->k24 != NULL) && ((A)->k42 != NULL));
}

void alloc_knm_accel_bs_mem(long int *bytes) {
    // don't allocate if we are not using higher order modes
    if (inter.tem_is_set) {
        // need to pick out the maximum number order for allocating memory
        int max_m=mem.hg_mode_order, max_n=mem.hg_mode_order;

        // make sure they are null before allocating new memory
        assert(acc_12_nr1_1 == NULL);
        assert(acc_12_nr1_2 == NULL);
        assert(acc_21_nr1_1 == NULL);
        assert(acc_21_nr1_2 == NULL);
        assert(acc_34_nr2_1 == NULL);
        assert(acc_34_nr2_2 == NULL);
        assert(acc_43_nr2_1 == NULL);
        assert(acc_43_nr2_2 == NULL);
        assert(acc_13_nr1_1 == NULL);
        assert(acc_13_nr2_2 == NULL);
        assert(acc_31_nr2_1 == NULL);
        assert(acc_31_nr1_2 == NULL);
        assert(acc_24_nr1_1 == NULL);
        assert(acc_24_nr2_2 == NULL);
        assert(acc_42_nr2_1 == NULL);
        assert(acc_42_nr1_2 == NULL);

        // allocate memory for all the accelerators
        acc_12_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_12_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_21_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_21_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_34_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_34_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_43_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_43_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);

        acc_13_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_13_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_31_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_31_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_24_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_24_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_42_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
        acc_42_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
    }
}

void bs_knm_free(bs_knm_t* knm) {
    assert(knm != NULL);

    if (!IS_BS_KNM_ALLOCD(knm))
        bug_error("We have tried to free memory that has not been allocated");

    int num_fields = mem.num_fields;
    int field_index;

    for (field_index = 0; field_index < num_fields; field_index++) {
        free(knm->k12[field_index]);
        free(knm->k21[field_index]);
        free(knm->k34[field_index]);
        free(knm->k43[field_index]);
        free(knm->k13[field_index]);
        free(knm->k31[field_index]);
        free(knm->k24[field_index]);
        free(knm->k42[field_index]);
        // remembering to NULL any loose pointers...
        knm->k12[field_index] = NULL;
        knm->k21[field_index] = NULL;
        knm->k34[field_index] = NULL;
        knm->k43[field_index] = NULL;
        knm->k13[field_index] = NULL;
        knm->k31[field_index] = NULL;
        knm->k24[field_index] = NULL;
        knm->k42[field_index] = NULL;
    }

    free(knm->k12);
    free(knm->k21);
    free(knm->k34);
    free(knm->k43);
    free(knm->k13);
    free(knm->k31);
    free(knm->k24);
    free(knm->k42);

    knm->k12 = NULL;
    knm->k21 = NULL;
    knm->k34 = NULL;
    knm->k43 = NULL;
    knm->k13 = NULL;
    knm->k31 = NULL;
    knm->k24 = NULL;
    knm->k42 = NULL;
}

void bs_knm_alloc(bs_knm_t* knm, long int *bytes) {
    if (knm == NULL)
        bug_error("bs_knm_t pointer is nulled");

    if (IS_BS_KNM_ALLOCD(knm))
        bug_error("We have tried to allocate memory to a bs_knm when it is already allocated, that or we have not initialised the pointers to be NULL");

    allocate_zmatrix(&knm->k12, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k21, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k34, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k43, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k13, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k31, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k24, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k42, mem.num_fields, bytes);
}

// multiply A*B then put result in new matrix

void bs_knm_matrix_mult(bs_knm_t* A, bs_knm_t* B, bs_knm_t *result) {
    assert(result != NULL && A != NULL && B != NULL);
    assert(result->k12 != NULL && A->k12 != NULL && B->k12 != NULL);
    assert(result->k21 != NULL && A->k21 != NULL && B->k21 != NULL);
    assert(result->k34 != NULL && A->k34 != NULL && B->k34 != NULL);
    assert(result->k43 != NULL && A->k43 != NULL && B->k43 != NULL);
    assert(result->k13 != NULL && A->k13 != NULL && B->k13 != NULL);
    assert(result->k31 != NULL && A->k31 != NULL && B->k31 != NULL);
    assert(result->k24 != NULL && A->k24 != NULL && B->k24 != NULL);
    assert(result->k42 != NULL && A->k42 != NULL && B->k42 != NULL);

    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    int l, n, m;

    bs_knm_t *tmp;

    // if A or B are just identity matrices then just copy them into result
    if (A->IsIdentities) {
        if (B != result)
            bs_knm_matrix_copy(B, result);

    } else if (B->IsIdentities) {
        if (A != result)
            bs_knm_matrix_copy(A, result);

    } else {
        // if result != A | B then no need to use a temporary matrix to store result 
        // just bung it in the result matrix
        if ((result == A) || (result == B))
            tmp = &bstmap;
        else
            tmp = result;

        for (n = 0; n < num_fields; n++) {
            for (m = 0; m < num_fields; m++) {
                // initialise to 0 here just incase some other data is there
                tmp->k12[n][m] = complex_0;
                tmp->k21[n][m] = complex_0;
                tmp->k34[n][m] = complex_0;
                tmp->k43[n][m] = complex_0;
                tmp->k13[n][m] = complex_0;
                tmp->k31[n][m] = complex_0;
                tmp->k24[n][m] = complex_0;
                tmp->k42[n][m] = complex_0;

                for (l = 0; l < num_fields; l++) {
                    tmp->k12[n][m] = z_pl_z(tmp->k12[n][m], z_by_z(A->k12[n][l], B->k12[l][m]));
                    tmp->k21[n][m] = z_pl_z(tmp->k21[n][m], z_by_z(A->k21[n][l], B->k21[l][m]));
                    tmp->k34[n][m] = z_pl_z(tmp->k34[n][m], z_by_z(A->k34[n][l], B->k34[l][m]));
                    tmp->k43[n][m] = z_pl_z(tmp->k43[n][m], z_by_z(A->k43[n][l], B->k43[l][m]));
                    tmp->k13[n][m] = z_pl_z(tmp->k13[n][m], z_by_z(A->k13[n][l], B->k13[l][m]));
                    tmp->k31[n][m] = z_pl_z(tmp->k31[n][m], z_by_z(A->k31[n][l], B->k31[l][m]));
                    tmp->k24[n][m] = z_pl_z(tmp->k24[n][m], z_by_z(A->k24[n][l], B->k24[l][m]));
                    tmp->k42[n][m] = z_pl_z(tmp->k42[n][m], z_by_z(A->k42[n][l], B->k42[l][m]));
                }
            }
        }

        if ((result == A) || (result == B))
            bs_knm_matrix_copy(&bstmap, result);
    }
}

// Should copy one matrix into another

void bs_knm_matrix_copy(bs_knm_t* src, bs_knm_t* dest) {
    assert(src != NULL);
    assert(dest != NULL);

    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    int i;

    assert(dest->k12 != NULL);
    assert(dest->k21 != NULL);
    assert(dest->k34 != NULL);
    assert(dest->k43 != NULL);
    assert(dest->k13 != NULL);
    assert(dest->k31 != NULL);
    assert(dest->k24 != NULL);
    assert(dest->k42 != NULL);
    
    assert(src->k12 != NULL);
    assert(src->k21 != NULL);
    assert(src->k34 != NULL);
    assert(src->k43 != NULL);
    assert(src->k13 != NULL);
    assert(src->k31 != NULL);
    assert(src->k24 != NULL);
    assert(src->k42 != NULL);
    
    for (i = 0; i < num_fields; i++) {
        memcpy(dest->k12[i], src->k12[i], num_fields * sizeof (complex_t));
        memcpy(dest->k21[i], src->k21[i], num_fields * sizeof (complex_t));
        memcpy(dest->k34[i], src->k34[i], num_fields * sizeof (complex_t));
        memcpy(dest->k43[i], src->k43[i], num_fields * sizeof (complex_t));
        memcpy(dest->k13[i], src->k13[i], num_fields * sizeof (complex_t));
        memcpy(dest->k31[i], src->k31[i], num_fields * sizeof (complex_t));
        memcpy(dest->k24[i], src->k24[i], num_fields * sizeof (complex_t));
        memcpy(dest->k42[i], src->k42[i], num_fields * sizeof (complex_t));
    }
}

/** sets matrix to identity matrix */
void bs_knm_matrix_ident(bs_knm_t* M) {
    assert(M != NULL);
    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    int i, j;

    for (i = 0; i < num_fields; i++) {
        for (j = 0; j < num_fields; j++) {
            M->k12[i][j] = (i == j) ? complex_1 : complex_0;
            M->k21[i][j] = (i == j) ? complex_1 : complex_0;
            M->k34[i][j] = (i == j) ? complex_1 : complex_0;
            M->k43[i][j] = (i == j) ? complex_1 : complex_0;
            M->k13[i][j] = (i == j) ? complex_1 : complex_0;
            M->k31[i][j] = (i == j) ? complex_1 : complex_0;
            M->k24[i][j] = (i == j) ? complex_1 : complex_0;
            M->k42[i][j] = (i == j) ? complex_1 : complex_0;
        }
    }
}

// To save on alot of replicating the code and potential errors a macro for checking
// the mismatch and astigmatism    
#define CHK_BS_AST_MST(val, KNM) \
if ((knm_calc_flags & BS##KNM##Calc) == BS##KNM##Calc){\
    if (!ceq(knm_q->qxt1_##KNM, knm_q->qxt2_##KNM) || !ceq(knm_q->qyt1_##KNM, knm_q->qyt2_##KNM)){\
        *mismatch = *mismatch | val;\
    }\
    if (!ceq(knm_q->qxt1_##KNM, knm_q->qyt1_##KNM) || !ceq(knm_q->qxt2_##KNM, knm_q->qyt2_##KNM)) {\
        *astigmatism = *astigmatism | val;\
    }\
}

void check_bs_knm_mismatch_astig(bitflag knm_calc_flags, bs_knm_q_t *knm_q, bitflag *mismatch, bitflag *astigmatism){
    CHK_BS_AST_MST(1, 12)
    CHK_BS_AST_MST(2, 21)
    CHK_BS_AST_MST(4, 34)
    CHK_BS_AST_MST(8, 43)
    
    CHK_BS_AST_MST(16, 13)
    CHK_BS_AST_MST(32, 31)
    CHK_BS_AST_MST(64, 24)
    CHK_BS_AST_MST(128, 42)
}

void calc_bs_limit_ws(double *wx1, double *wx2, double *wy1, double *wy2, int knum, bs_knm_map_int_params_t *p) {
    bs_knm_q_t *kq = p->knm_q;

    switch (knum) {
        case BS12:
            *wx1 = w_size(kq->qxt1_12, p->nr1);
            *wx2 = w_size(kq->qyt1_12, p->nr1);
            *wy1 = w_size(kq->qxt2_12, p->nr1);
            *wy2 = w_size(kq->qyt2_12, p->nr1);
            break;
        case BS21:
            *wx1 = w_size(kq->qxt1_21, p->nr1);
            *wx2 = w_size(kq->qyt1_21, p->nr1);
            *wy1 = w_size(kq->qxt2_21, p->nr1);
            *wy2 = w_size(kq->qyt2_21, p->nr1);
            break;
        case BS34:
            *wx1 = w_size(kq->qxt1_34, p->nr2);
            *wx2 = w_size(kq->qyt1_34, p->nr2);
            *wy1 = w_size(kq->qxt2_34, p->nr2);
            *wy2 = w_size(kq->qyt2_34, p->nr2);
            break;
        case BS43:
            *wx1 = w_size(kq->qxt1_43, p->nr2);
            *wx2 = w_size(kq->qyt1_43, p->nr2);
            *wy1 = w_size(kq->qxt2_43, p->nr2);
            *wy2 = w_size(kq->qyt2_43, p->nr2);
            break;
        case BS13:
            *wx1 = w_size(kq->qxt1_13, p->nr1);
            *wx2 = w_size(kq->qyt1_13, p->nr1);
            *wy1 = w_size(kq->qxt2_13, p->nr2);
            *wy2 = w_size(kq->qyt2_13, p->nr2);
            break;
        case BS31:
            *wx1 = w_size(kq->qxt1_31, p->nr2);
            *wx2 = w_size(kq->qyt1_31, p->nr2);
            *wy1 = w_size(kq->qxt2_31, p->nr1);
            *wy2 = w_size(kq->qyt2_31, p->nr1);
            break;
        case BS24:
            *wx1 = w_size(kq->qxt1_24, p->nr1);
            *wx2 = w_size(kq->qyt1_24, p->nr1);
            *wy1 = w_size(kq->qxt2_24, p->nr2);
            *wy2 = w_size(kq->qyt2_24, p->nr2);
            break;
        case BS42:
            *wx1 = w_size(kq->qxt1_42, p->nr2);
            *wx2 = w_size(kq->qyt1_42, p->nr2);
            *wy1 = w_size(kq->qxt2_42, p->nr1);
            *wy2 = w_size(kq->qyt2_42, p->nr1);
            break;
    }
}

        
/**
 * Returns true of false depending on if the aperture knm should be integrated or not
 * 
 * @param bs beamsplitter component
 * @param mismatch whether mismatch present
 * @param astigmatism whether beam is astigmatic
 * @return True or false depending on if the aperture should be integrated or analytically calculated
 */
bool should_integrate_bs_aperture_knm(beamsplitter_t *bs, int mismatch, int astigmatism) {
    
    //check if an aperture has even been defined
    if (bs->r_aperture > 0) {
        // check if we should integrate the aperture coupling coefficients rather
        // than computing them analytically
        if (bs->knm_flags & INT_APERTURE || astigmatism || mismatch) {

            // if using any of the Riemann methods, these currently do not support 
            // aperture integration without using a map
            if ((init.mapintmethod == RIEMANN_SUM_NEW) || (init.mapintmethod == NEWTON_COTES)) {
                
                if(bs->map_merged.usermessages & APERTURE_RIEMANN ) {
                    warn("Aperture calculation using Riemann integrators is not currently supported.\n"
                         "   Please use an aperture map or a Cuba integrating routine.\n");
                    bs->map_merged.usermessages |= APERTURE_RIEMANN;
                }              
                                
                // return false as with these methods no aperture coupling coefficient
                // can be integrated.
                return false;
            } else {
                return true;
            }

        } else {
            // otherwise we need to calculate the aperture coupling coefficient matrix here analytically
            if(!(bs->map_merged.usermessages & APERTURE_NO_ANALYTIC)){
                warn("Analytic aperture coupling coefficient calculation not completely implemented yet, becareful!\n");
                bs->map_merged.usermessages |= APERTURE_NO_ANALYTIC;
            }

            if (inter.debug && !options.quiet)
               message("* Calculating aperture Knm analytically\n");
            
            // return false to state that no aperture integration is needed
            // as we have done it analytically
            return false;
        }
    }

    return false;
}


void max_min_limit(double *wx1, double *wx2, double *wy1, double *wy2,
        double *_wx1, double *_wx2, double *_wy1, double *_wy2,
        KNM_BS_NODE_DIRECTION_t node, bs_knm_map_int_params_t *p) {

    calc_bs_limit_ws(_wx1, _wx2, _wy1, _wy2, node, p);
    *wx1 = max(*wx1, *_wx1);
    *wx2 = max(*wx2, *_wx2);
    *wy1 = max(*wy1, *_wy1);
    *wy2 = max(*wy2, *_wy2);
}

// Scale the integration limits slightly depending on the beam mode
// as the beam tends to get larger at higher modes. If a mirror aperture
// has been specified then polar coordinates are used for the integration
// so r and theta limits are set.
//
// If the Riemann integration method is being used then cartesian coords
// are used by default. Polar limits are only set when using the new integration
// methods Cuba parallel and serial.
//
// if the beam is much smaller than the aperture, cartesian coordinates are used.

void get_bs_int_limit(bs_knm_map_int_params_t *p, int knum) {
    if (p->aperture_radius < 0)
        bug_error("get_int_limit(*p): Beamsplitter aperture cannot be negative\n");

    double wx1 = 0, wx2 = 0, wy1 = 0, wy2 = 0;
    double _wx1, _wx2, _wy1, _wy2;

    switch (knum) {
        case 0:
            // used by cuba parallel, find the largest w values out of all the
            // different 
            if (CALC_BS_KNM(p, 12)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS12, p);
            if (CALC_BS_KNM(p, 21)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS21, p);
            if (CALC_BS_KNM(p, 34)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS34, p);
            if (CALC_BS_KNM(p, 43)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS43, p);
            if (CALC_BS_KNM(p, 13)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS13, p);
            if (CALC_BS_KNM(p, 31)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS31, p);
            if (CALC_BS_KNM(p, 24)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS24, p);
            if (CALC_BS_KNM(p, 42)) max_min_limit(&wx1, &wx2, &wy1, &wy2, &_wx1, &_wx2, &_wy1, &_wy2, BS42, p);

            break;
        case BS12:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS12, p);
            break;
        case BS21:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS21, p);
            break;
        case BS34:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS34, p);
            break;
        case BS43:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS43, p);
            break;
        case BS13:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS13, p);
            break;
        case BS31:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS31, p);
            break;
        case BS24:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS24, p);
            break;
        case BS42:
            calc_bs_limit_ws(&wx1, &wx2, &wy1, &wy2, BS42, p);
            break;
        default:
            bug_error("Cannot handle a knum input of %i\n", knum);
    }

    p->xmax[0] = 5 * max(sqrt(p->n1 + 0.5) * wx1, sqrt(p->n2 + 0.5) * wx2);
    p->xmin[0] = -p->xmax[0];
    p->xmax[1] = 5 * max(sqrt(p->m1 + 0.5) * wy1, sqrt(p->m2 + 0.5) * wy2);
    p->xmin[1] = -p->xmax[1];

    double r = p->aperture_radius;

    // if using the Riemann just stick to the Cartesian coordinates
    // if r == 0 then the mirror is infinitely big
    // if r > max(...) then the beam size is smaller than the aperture so Cartesian will do
    // also check if we should be integrating to calculate the aperture with inter.knm, if we're not
    // then there is no need to use polar coordinates as polar coordinates are only used for aperture
    // calculations.
    if ((p->merged_map->integration_method == NEWTON_COTES
            || p->merged_map->integration_method == RIEMANN_SUM_NEW)
            || !(p->bs->knm_flags & INT_APERTURE)
            || ((r == 0) || (r > min(p->xmax[0], p->xmax[1])))) {
        p->polar_limits_used = false;
    } else {
        //[0] is r, [1] is theta
        p->xmin[0] = 0;
        p->xmin[1] = 0;
        p->xmax[0] = r;
        p->xmax[1] = TWOPI;
        p->polar_limits_used = true;
    }

    unsigned int *usermessages = &(p->merged_map->usermessages);

    if (inter.debug && !options.quiet && !(*usermessages & USING_POLAR)) {
        if (p->polar_limits_used)
            message("* Polar coordinates used to integrate map %s\n", p->merged_map->name);
        else
            message("* Cartesian coordinates used to integrate map %s\n", p->merged_map->name);

        *usermessages = *usermessages | USING_POLAR;
    }
    
    double ca = 1./cos(p->bs->alpha_1 * RAD);
    
    if (p->usingMap && !(*usermessages & MAP_TOO_SMALL)) {
        surface_merged_map_t *map = p->merged_map;
        // here we check to see if the map size is smaller than the limits. if so
        // you could probably do with a bigger map
        if (p->polar_limits_used) {
            bool a = (map->cols - map->x0) * map->xstep * ca < p->xmax[0]*(1 - 1e-15);
            bool b = (map->rows - map->y0) * map->ystep < p->xmax[0]*(1 - 1e-15);
            bool c = (map->x0 - map->cols) * map->xstep * ca < -p->xmax[0];
            bool d = (map->y0 - map->rows) * map->ystep < -p->xmax[0];
            if ((a || b || c || d) && inter.debug && !options.quiet) {
                warn("The map %s is smaller than the dimensions of the integration limits\n"
                        "   You should use a bigger map or a smaller beam for more accurate results,\n"
                        "   and make sure that the map is correctly centred using the x0 and y0\n"
                        "   (aperture diameter:%e, map width:%e, map height: %e)\n"
                        , p->merged_map->filename
                        , (p->xmax[0] - p->xmin[0])*2
                        , (map->cols - 1) * map->xstep
                        , (map->rows - 1) * map->ystep);

                *usermessages = *usermessages | MAP_TOO_SMALL;
            }
        } else {
            if ((      ((map->cols - map->x0) * map->xstep * ca < p->xmax[0])
                    || ((map->x0 - map->cols) * map->xstep * ca < p->xmin[0])
                    || ((map->rows - map->y0) * map->ystep < p->xmax[1])
                    || ((map->y0 - map->rows) * map->ystep < p->xmin[1])
                    ) && inter.debug && !options.quiet) {
                warn("The map %s is smaller than the dimensions of the integration limits\n"
                        "   You should use a bigger map or a smaller beam for more accurate results,\n"
                        "   and make sure that the map is correctly centred using the x0 and y0.\n"
                        "   If an incident angle is applied a 1/cos(angle) factor is applied in the x-direction\n"
                        "   which needs to be taken into account.\n"
                        "   (x range=%e, y range=%e, map width=%e, map height=%e, incident angle=%e)\n"
                        , p->merged_map->filename
                        , p->xmax[0] - p->xmin[0]
                        , p->xmax[1] - p->xmin[1]
                        , (map->cols - 1) * map->xstep
                        , (map->rows - 1) * map->ystep
                        , p->bs->alpha_1);


                *usermessages = *usermessages | MAP_TOO_SMALL;
            }
        }
    }

    if (p->usingMap && !(*usermessages & MAP_RES_TOO_LOW)
            && p->merged_map->integration_method == RIEMANN_SUM_NEW) {
        if ((min(wx1, wx2) / p->merged_map->xstep < 5.0) || (min(wy1, wy2) / p->merged_map->ystep < 5.0)) {
            warn("The map %s might not have a high enough resolution. Currently\n"
                    "   the beam size samples less than 10 points on the map. This can\n"
                    "   cause large errors when using higher orders.\n", p->merged_map->name);
            *usermessages |= MAP_RES_TOO_LOW;
        }
    }
}

/** q values passed to this should have been turned depending on the direction of
 *  incoming and outgoing beam
 */
void calculate_bs_qt_qt2(KNM_BS_NODE_DIRECTION_t KNM,  bs_knm_q_t *knm_q, complex_t qx1,
        complex_t qy1, complex_t qx2, complex_t qy2, complex_t qx3, complex_t qy3, complex_t qx4, complex_t qy4,
        double nr1, double nr2, int n1idx, int n2idx, int n3idx, int n4idx,  int cidx) {
       
    ABCD_t trans1;

    switch (KNM) {
        case BS12:
            component_matrix(&trans1, cidx, n1idx, n2idx, TANGENTIAL);
            knm_q->qxt1_12 = q1_q2(trans1, qx1, nr1, nr1);
            component_matrix(&trans1, cidx, n1idx, n2idx, SAGITTAL);
            knm_q->qyt1_12 = q1_q2(trans1, qy1, nr1, nr1);
            knm_q->qxt2_12 = qx2;
            knm_q->qyt2_12 = qy2;
            break;
        case BS21:
            component_matrix(&trans1, cidx, n2idx, n1idx, TANGENTIAL);
            knm_q->qxt1_21 = q1_q2(trans1, cminus(cconj(qx2)), nr1, nr1);
            component_matrix(&trans1, cidx, n2idx, n1idx, SAGITTAL);
            knm_q->qyt1_21 = q1_q2(trans1, cminus(cconj(qy2)), nr1, nr1);
            knm_q->qxt2_21 = cminus(cconj(qx1));
            knm_q->qyt2_21 = cminus(cconj(qy1));
            break;
        case BS34:
            component_matrix(&trans1, cidx, n3idx, n4idx, TANGENTIAL);
            knm_q->qxt1_34 = q1_q2(trans1, qx3, nr2, nr2);
            component_matrix(&trans1, cidx, n3idx, n4idx, SAGITTAL);
            knm_q->qyt1_34 = q1_q2(trans1, qy3, nr2, nr2);
            knm_q->qxt2_34 = qx4;
            knm_q->qyt2_34 = qy4;
            break;
        case BS43:
            component_matrix(&trans1, cidx, n4idx, n3idx, TANGENTIAL);
            knm_q->qxt1_43 = q1_q2(trans1, cminus(cconj(qx4)), nr2, nr2);
            component_matrix(&trans1, cidx, n4idx, n3idx, SAGITTAL);
            knm_q->qyt1_43 = q1_q2(trans1, cminus(cconj(qy4)), nr2, nr2);
            knm_q->qxt2_43 = cminus(cconj(qx3));
            knm_q->qyt2_43 = cminus(cconj(qy3));
            break;
        case BS13:
            component_matrix(&trans1, cidx, n1idx, n3idx, TANGENTIAL);
            knm_q->qxt1_13 = q1_q2(trans1, qx1, nr1, nr2);
            component_matrix(&trans1, cidx, n1idx, n3idx, SAGITTAL);
            knm_q->qyt1_13 = q1_q2(trans1, qy1, nr1, nr2);
            knm_q->qxt2_13 = cminus(cconj(qx3));
            knm_q->qyt2_13 = cminus(cconj(qy3));
            break;
        case BS31:
            component_matrix(&trans1, cidx, n3idx, n1idx, TANGENTIAL);
            knm_q->qxt1_31 = q1_q2(trans1, qx3, nr2, nr1);
            component_matrix(&trans1, cidx, n3idx, n1idx, SAGITTAL);
            knm_q->qyt1_31 = q1_q2(trans1, qy3, nr2, nr1);
            knm_q->qxt2_31 = cminus(cconj(qx1));
            knm_q->qyt2_31 = cminus(cconj(qy1));
            break;
        case BS24:
            component_matrix(&trans1, cidx, n2idx, n4idx, TANGENTIAL);
            knm_q->qxt1_24 = q1_q2(trans1, cminus(cconj(qx2)), nr1, nr2);
            component_matrix(&trans1, cidx, n2idx, n4idx, SAGITTAL);
            knm_q->qyt1_24 = q1_q2(trans1, cminus(cconj(qy2)), nr1, nr2);
            knm_q->qxt2_24 = qx4;
            knm_q->qyt2_24 = qy4;
            break;
        case BS42:
            component_matrix(&trans1, cidx, n4idx, n2idx, TANGENTIAL);
            knm_q->qxt1_42 = q1_q2(trans1, cminus(cconj(qx4)), nr2, nr1);
            component_matrix(&trans1, cidx, n4idx, n2idx, SAGITTAL);
            knm_q->qyt1_42 = q1_q2(trans1, cminus(cconj(qy4)), nr2, nr1);
            knm_q->qxt2_42 = qx2;
            knm_q->qyt2_42 = qy2;
            break;
        default:
            bug_error("calculate_qt_qt2 cannot handle a KNM value of %i\n", KNM);
    }
}

void compute_bs_knm_integral(beamsplitter_t *bs, double nr1, double nr2, int integrating, int mismatch, int astigmatism) {

    surface_merged_map_t *map = &(bs->map_merged);
    bs_knm_t *knm = &(bs->knm_map);

    // A quick check is needed to determine if any maps have actually been applied.
    // if so and there is no other integrating needed we need not continue.
    // Otherwise we need to allocate some memory to store the results in.
    if (map->noMapPresent && !integrating) {
        return;
    } else
        if (!IS_BS_KNM_ALLOCD(knm))
            bug_error("beamsplitter map knm has not been allocated");

    // We now need to determine whether we actually need to compute the integral
    // again. As this method can be called multiple times if one of the parameters
    // is varied by the x-axis.
    bool recompute = false;

    recompute = recompute | (map->_nr1 != nr1);
    recompute = recompute | (map->_nr2 != nr2);

    recompute = recompute | (map->_angle != bs->map_rotation);
    recompute = recompute | (map->_AoI != bs->alpha_1);
    recompute = recompute | (map->_x_off != bs->x_off);
    recompute = recompute | (map->_y_off != bs->x_off);

    recompute = recompute | !ceq(bs->knm_q.qxt1_12, map->bs_knm_q.qxt1_12);
    recompute = recompute | !ceq(bs->knm_q.qyt1_12, map->bs_knm_q.qyt1_12);
    recompute = recompute | !ceq(bs->knm_q.qxt2_12, map->bs_knm_q.qxt2_12);
    recompute = recompute | !ceq(bs->knm_q.qyt2_12, map->bs_knm_q.qyt2_12);

    recompute = recompute | !ceq(bs->knm_q.qxt1_21, map->bs_knm_q.qxt1_21);
    recompute = recompute | !ceq(bs->knm_q.qyt1_21, map->bs_knm_q.qyt1_21);
    recompute = recompute | !ceq(bs->knm_q.qxt2_21, map->bs_knm_q.qxt2_21);
    recompute = recompute | !ceq(bs->knm_q.qyt2_21, map->bs_knm_q.qyt2_21);

    recompute = recompute | !ceq(bs->knm_q.qxt1_34, map->bs_knm_q.qxt1_34);
    recompute = recompute | !ceq(bs->knm_q.qyt1_34, map->bs_knm_q.qyt1_34);
    recompute = recompute | !ceq(bs->knm_q.qxt2_34, map->bs_knm_q.qxt2_34);
    recompute = recompute | !ceq(bs->knm_q.qyt2_34, map->bs_knm_q.qyt2_34);

    recompute = recompute | !ceq(bs->knm_q.qxt1_43, map->bs_knm_q.qxt1_43);
    recompute = recompute | !ceq(bs->knm_q.qyt1_43, map->bs_knm_q.qyt1_43);
    recompute = recompute | !ceq(bs->knm_q.qxt2_43, map->bs_knm_q.qxt2_43);
    recompute = recompute | !ceq(bs->knm_q.qyt2_43, map->bs_knm_q.qyt2_43);

    recompute = recompute | !ceq(bs->knm_q.qxt1_13, map->bs_knm_q.qxt1_13);
    recompute = recompute | !ceq(bs->knm_q.qyt1_13, map->bs_knm_q.qyt1_13);
    recompute = recompute | !ceq(bs->knm_q.qxt2_13, map->bs_knm_q.qxt2_13);
    recompute = recompute | !ceq(bs->knm_q.qyt2_13, map->bs_knm_q.qyt2_13);

    recompute = recompute | !ceq(bs->knm_q.qxt1_31, map->bs_knm_q.qxt1_31);
    recompute = recompute | !ceq(bs->knm_q.qyt1_31, map->bs_knm_q.qyt1_31);
    recompute = recompute | !ceq(bs->knm_q.qxt2_31, map->bs_knm_q.qxt2_31);
    recompute = recompute | !ceq(bs->knm_q.qyt2_31, map->bs_knm_q.qyt2_31);

    recompute = recompute | !ceq(bs->knm_q.qxt1_24, map->bs_knm_q.qxt1_24);
    recompute = recompute | !ceq(bs->knm_q.qyt1_24, map->bs_knm_q.qyt1_24);
    recompute = recompute | !ceq(bs->knm_q.qxt2_24, map->bs_knm_q.qxt2_24);
    recompute = recompute | !ceq(bs->knm_q.qyt2_24, map->bs_knm_q.qyt2_24);

    recompute = recompute | !ceq(bs->knm_q.qxt1_42, map->bs_knm_q.qxt1_42);
    recompute = recompute | !ceq(bs->knm_q.qyt1_42, map->bs_knm_q.qyt1_42);
    recompute = recompute | !ceq(bs->knm_q.qxt2_42, map->bs_knm_q.qxt2_42);
    recompute = recompute | !ceq(bs->knm_q.qyt2_42, map->bs_knm_q.qyt2_42);

    recompute = recompute | (init.abserr != map->_abserr);
    recompute = recompute | (init.relerr != map->_relerr);

    recompute = recompute | (init.lambda != map->_lambda);

    // check the current methods against the previously used methods
    recompute = recompute | (map->interpolation_method != map->_intermeth);
    recompute = recompute | (map->interpolation_size != map->_intersize);
    recompute = recompute | (map->integration_method != map->_intmeth);

    recompute = recompute | ((bs->knm_flags & INT_APERTURE) && (bs->r_aperture != map->_r_aperture));

    recompute = recompute | ((bs->knm_flags & INT_BAYER_HELMS) && (map->_xbeta != bs->beta_x));
    recompute = recompute | ((bs->knm_flags & INT_BAYER_HELMS) && (map->_ybeta != bs->beta_y));

    int start_tem = 0;

    if (!recompute) {
        // So far everything should be the same, the last comparison to make
        // is between the maximum number of calculated modes, if more are requested
        // start off where we were, if not we have the previous ones already
        if (map->_maxtem >= inter.tem) {
            if ((inter.debug & 128) && !options.quiet)
                message("* Did not need to integrate any knm's for beamsplitter %s\n", bs->name);
            
            return;
        } else {
            start_tem = map->_maxtem + 1; // start from the next maxtem

            // adf this would be useful to print always (unless quiet is set)
            if (!options.quiet)
                message(" * Start computing coefficients for beamsplitter %s from HG order %i\n", bs->name, start_tem);
        }
    }

    bool usingMap = !(map->noMapPresent);

    if (inter.debug && !options.quiet) {
        message("* Effects integrated for mirror %s:\n", bs->name);

        if (usingMap)message("    - Merged maps\n");
        if (integrating & 1)message("    - Bayer-Helms\n");
        if (integrating & 2)message("    - Aperture\n");
        if (integrating & 4)message("    - Curvature\n");

        message("* Coupling coefficients to compute for %s:", bs->name);
        if (CALC_BS_KNM(bs, 12))message(" K12");
        if (CALC_BS_KNM(bs, 21))message(" K21");
        if (CALC_BS_KNM(bs, 34))message(" K34");
        if (CALC_BS_KNM(bs, 43))message(" K43");
        if (CALC_BS_KNM(bs, 13))message(" K13");
        if (CALC_BS_KNM(bs, 31))message(" K31");
        if (CALC_BS_KNM(bs, 24))message(" K24");
        if (CALC_BS_KNM(bs, 42))message(" K42");
        message("\n");
    }

    // boolean value that states whether we should calculate knm using knm without
    // having to do the whole integral and just by applying some phase factor
    // 12/05/2012 disabling this for now, needs fixing
    int calc_knm_transpose = (init.calc_knm_transpose && (mismatch == 0) && (astigmatism == 0));

    if (inter.debug && calc_knm_transpose == 1 && !options.quiet && !(map->usermessages & KNM_TRANSPOSE)) {
        message("* Calculating knm matrix lower triangle using phase factor\n"
                "   and upper triangle. See kat.ini calc_knm_transpose variable.\n");
        map->usermessages |= KNM_TRANSPOSE;
    }

    int max_m, max_n;
    get_tem_modes_from_field_index(&max_n, &max_m, inter.num_fields - 1);
    max_m = max_n; // max m is no the m is also the max_n. e.g. we get n=1 m=2 for maxtem 2

    bs_knm_q_t *kq = &(bs->knm_q);
    // TODO This might not have to be called every time if the q's don't change

    // Initialise all the unm accelerators for calculations later
    if (CALC_BS_KNM(bs, 12)) {
        u_nm_accel_get(acc_12_nr1_1, max_n, max_m, kq->qxt1_12, kq->qyt1_12, nr1);
        u_nm_accel_get(acc_12_nr1_2, max_n, max_m, kq->qxt2_12, kq->qyt2_12, nr1);
    }
    if (CALC_BS_KNM(bs, 21)) {
        u_nm_accel_get(acc_21_nr1_1, max_n, max_m, kq->qxt1_21, kq->qyt1_21, nr1);
        u_nm_accel_get(acc_21_nr1_2, max_n, max_m, kq->qxt2_21, kq->qyt2_21, nr1);
    }

    if (CALC_BS_KNM(bs, 34)) {
        u_nm_accel_get(acc_34_nr2_1, max_n, max_m, kq->qxt1_34, kq->qyt1_34, nr2);
        u_nm_accel_get(acc_34_nr2_2, max_n, max_m, kq->qxt2_34, kq->qyt2_34, nr2);
    }
    if (CALC_BS_KNM(bs, 43)) {
        u_nm_accel_get(acc_43_nr2_1, max_n, max_m, kq->qxt1_43, kq->qyt1_43, nr2);
        u_nm_accel_get(acc_43_nr2_2, max_n, max_m, kq->qxt2_43, kq->qyt2_43, nr2);
    }

    if (CALC_BS_KNM(bs, 13)) {
        u_nm_accel_get(acc_13_nr1_1, max_n, max_m, kq->qxt1_13, kq->qyt1_13, nr2);
        u_nm_accel_get(acc_13_nr2_2, max_n, max_m, kq->qxt2_13, kq->qyt2_13, nr2);
    }
    if (CALC_BS_KNM(bs, 31)) {
        u_nm_accel_get(acc_31_nr2_1, max_n, max_m, kq->qxt1_31, kq->qyt1_31, nr1);
        u_nm_accel_get(acc_31_nr1_2, max_n, max_m, kq->qxt2_31, kq->qyt2_31, nr1);
    }

    if (CALC_BS_KNM(bs, 24)) {
        u_nm_accel_get(acc_24_nr1_1, max_n, max_m, kq->qxt1_24, kq->qyt1_24, nr2);
        u_nm_accel_get(acc_24_nr2_2, max_n, max_m, kq->qxt2_24, kq->qyt2_24, nr2);
    }
    if (CALC_BS_KNM(bs, 42)) {
        u_nm_accel_get(acc_42_nr2_1, max_n, max_m, kq->qxt1_42, kq->qyt1_42, nr1);
        u_nm_accel_get(acc_42_nr1_2, max_n, max_m, kq->qxt2_42, kq->qyt2_42, nr1);
    }

    bs_knm_map_int_params_t param;

    param.usingMap = usingMap;
    param.merged_map = map;
    param.bs = bs;

    param.nr1 = nr1;
    param.nr2 = nr2;

    param.knm_q = &(bs->knm_q);

    param.acc_12_nr1_1 = (CALC_BS_KNM(bs, 12)) ? acc_12_nr1_1 : NULL;
    param.acc_12_nr1_2 = (CALC_BS_KNM(bs, 12)) ? acc_12_nr1_2 : NULL;
    param.acc_21_nr1_1 = (CALC_BS_KNM(bs, 21)) ? acc_21_nr1_1 : NULL;
    param.acc_21_nr1_2 = (CALC_BS_KNM(bs, 21)) ? acc_21_nr1_2 : NULL;
    param.acc_34_nr2_1 = (CALC_BS_KNM(bs, 34)) ? acc_34_nr2_1 : NULL;
    param.acc_34_nr2_2 = (CALC_BS_KNM(bs, 34)) ? acc_34_nr2_2 : NULL;
    param.acc_43_nr2_1 = (CALC_BS_KNM(bs, 43)) ? acc_43_nr2_1 : NULL;
    param.acc_43_nr2_2 = (CALC_BS_KNM(bs, 43)) ? acc_43_nr2_2 : NULL;
    param.acc_13_nr1_1 = (CALC_BS_KNM(bs, 13)) ? acc_13_nr1_1 : NULL;
    param.acc_13_nr2_2 = (CALC_BS_KNM(bs, 13)) ? acc_13_nr2_2 : NULL;
    param.acc_31_nr2_1 = (CALC_BS_KNM(bs, 31)) ? acc_31_nr2_1 : NULL;
    param.acc_31_nr1_2 = (CALC_BS_KNM(bs, 31)) ? acc_31_nr1_2 : NULL;
    param.acc_24_nr1_1 = (CALC_BS_KNM(bs, 24)) ? acc_24_nr1_1 : NULL;
    param.acc_24_nr2_2 = (CALC_BS_KNM(bs, 24)) ? acc_24_nr2_2 : NULL;
    param.acc_42_nr2_1 = (CALC_BS_KNM(bs, 42)) ? acc_42_nr2_1 : NULL;
    param.acc_42_nr1_2 = (CALC_BS_KNM(bs, 42)) ? acc_42_nr1_2 : NULL;

    param.knm_calc_flags = bs->knm_calc_flags;
   
    param.aperture_radius = bs->r_aperture;

    param.betax = bs->beta_x;
    param.betay = bs->beta_y;

    param.integrating = integrating;

    // the ordering of these should not be changed! As they are declared in order
    // we can refer to them as an array without actually having to define one later
    complex_t results[8];

    results[0] = complex_0;
    results[1] = complex_0;
    results[2] = complex_0;
    results[3] = complex_0;
    results[4] = complex_0;
    results[5] = complex_0;
    results[6] = complex_0;
    results[7] = complex_0;

    //This sets the number of cores to use for cuba library
    if (bs->map_merged.integration_method == CUBA_CUHRE_SERIAL
            || bs->map_merged.integration_method == CUBA_CUHRE_PARA) {
        char buf[ERR_MSG_LEN];
        sprintf(buf, "CUBACORES=%d", init.cuba_numprocs);
        putenv(buf);
    }

    if (inter.debug && !options.quiet && !(map->usermessages & INT_METHODS)) {
        map->usermessages |= INT_METHODS;

        message("* Beamsplitter integration - %s\n", bs->name);
        message("   - Using ");

        switch (map->integration_method) {
            case RIEMANN_SUM_NEW:
                message("Riemann integrator\n");
                break;
            case NEWTON_COTES:
                message("old Riemann integrator\n");
                break;
#if INCLUDE_CUBA == 1
            case CUBA_CUHRE_PARA:
                message("parallel cubature integrator\n");
                break;
            case CUBA_CUHRE_SERIAL:
                message("serial cubature integrator\n");
                break;
#endif
        }

        message("   - Using %i core%s for integration\n", init.cuba_numprocs, ((init.cuba_numprocs == 1) ? "" : "s"));

        if (usingMap) {
            message("   - Using ");

            switch (map->interpolation_method) {
                case SPLINE:message("spline");
                    break;
                case LINEAR:message("linear");
                    break;
                case NEAREST:message("nearest-neighbour");
                    break;
            }

            message(" interpolation, kernel size %i\n", map->interpolation_size);
        }
    }

    int num_coeffs = inter.num_fields * inter.num_fields;
    int current_coeff = 1;
    //fprintf(stderr, " * Progress integrating map %s: \n", map->name);

    time_t starttime = time(NULL);

    int n, m, n1, m1, n2, m2;
    set_progress_action_text("Integrating map on %s", bs->name);
    print_progress_and_time(num_coeffs, current_coeff, starttime);

    // Iterate over each mode to calculate k_n1,m1,n2,m2
    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {

            // If we are using the knm to calc knm then we do not need to bother
            // with doing all the integrating for any of the lower triangle.
            if (!calc_knm_transpose || (n >= m)) {

                //Transform linear mode index system into actual TEM_NM mode
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);

                // There is no need to calculate knm's that have been loaded
                // or calculated before
                if (n1 + m1 >= start_tem || n2 + m2 >= start_tem) {
                    param.m1 = m1;
                    param.m2 = m2;
                    param.n1 = n1;
                    param.n2 = n2;

                    // compute some constants that are n1,n2,m1,m2 dependant
                    if (CALC_BS_KNM(&param, 12)) param.znm1c2 = z_by_zc(z_by_z(acc_12_nr1_1->acc_n->prefac[n1], acc_12_nr1_1->acc_m->prefac[m1]),
                            z_by_z(acc_12_nr1_2->acc_n->prefac[n2], acc_12_nr1_2->acc_m->prefac[m2]));
                    if (CALC_BS_KNM(&param, 21)) param.znm2c1 = z_by_zc(z_by_z(acc_21_nr1_1->acc_n->prefac[n1], acc_21_nr1_1->acc_m->prefac[m1]),
                            z_by_z(acc_21_nr1_2->acc_n->prefac[n2], acc_21_nr1_2->acc_m->prefac[m2]));
                    if (CALC_BS_KNM(&param, 34)) param.znm3c4 = z_by_zc(z_by_z(acc_34_nr2_1->acc_n->prefac[n1], acc_34_nr2_1->acc_m->prefac[m1]),
                            z_by_z(acc_34_nr2_2->acc_n->prefac[n2], acc_34_nr2_2->acc_m->prefac[m2]));
                    if (CALC_BS_KNM(&param, 43)) param.znm4c3 = z_by_zc(z_by_z(acc_43_nr2_1->acc_n->prefac[n1], acc_43_nr2_1->acc_m->prefac[m1]),
                            z_by_z(acc_43_nr2_2->acc_n->prefac[n2], acc_43_nr2_2->acc_m->prefac[m2]));
                    if (CALC_BS_KNM(&param, 13)) param.znm1c3 = z_by_zc(z_by_z(acc_13_nr1_1->acc_n->prefac[n1], acc_13_nr1_1->acc_m->prefac[m1]),
                            z_by_z(acc_13_nr2_2->acc_n->prefac[n2], acc_13_nr2_2->acc_m->prefac[m2]));
                    if (CALC_BS_KNM(&param, 31)) param.znm3c1 = z_by_zc(z_by_z(acc_31_nr2_1->acc_n->prefac[n1], acc_31_nr2_1->acc_m->prefac[m1]),
                            z_by_z(acc_31_nr1_2->acc_n->prefac[n2], acc_31_nr1_2->acc_m->prefac[m2]));
                    if (CALC_BS_KNM(&param, 24)) param.znm2c4 = z_by_zc(z_by_z(acc_24_nr1_1->acc_n->prefac[n1], acc_24_nr1_1->acc_m->prefac[m1]),
                            z_by_z(acc_24_nr2_2->acc_n->prefac[n2], acc_24_nr2_2->acc_m->prefac[m2]));
                    if (CALC_BS_KNM(&param, 42)) param.znm4c2 = z_by_zc(z_by_z(acc_42_nr2_1->acc_n->prefac[n1], acc_42_nr2_1->acc_m->prefac[m1]),
                            z_by_z(acc_42_nr1_2->acc_n->prefac[n2], acc_42_nr1_2->acc_m->prefac[m2]));

                    if (map->save_interp_file) {
                        if (inter.debug && !options.quiet && !(map->usermessages & INTERP_SAVE)) {
                            message("* Writing interpolated data for each knm for %s, switch off for many knm!!!\n", bs->name);
                            map->usermessages = map->usermessages | INTERP_SAVE;
                        }

                        char ipfn[FILENAME_MAX];
                        sprintf(ipfn, "%s_%i%i_%i%i.interp", bs->name, n1, m1, n2, m2);
                        open_file_to_write_ascii(ipfn, &ipfile);
                    }
                    
                    if (map->save_integration_points){
                        if (inter.debug && !options.quiet && !(map->usermessages & INTERP_SAVE)) {
                            message("* Writing integration sample points for each knm for %s, switch off for many knm!!!\n", bs->name);
                            map->usermessages = map->usermessages | INTERP_SAVE;
                        }
                        
                        char idfn[FILENAME_MAX];
                        sprintf(idfn, "%s_%i%i_%i%i.intdata", bs->name, n1, m1, n2, m2);
                        open_file_to_write_ascii(idfn, &idfile);
                    }

                    switch (map->integration_method) {
                        case RIEMANN_SUM_NEW:
                            do_riemann_sum_new_int((void*) &param, BEAMSPLITTER_CMP, results);
                            break;
                        case NEWTON_COTES:
                            bug_error("Old Riemann not available");
                            break;
#if INCLUDE_CUBA == 1
                        case CUBA_CUHRE_SERIAL:
                            do_serial_cuhre_map_int((void*) &param, BEAMSPLITTER_CMP, results, init.abserr, init.relerr);
                            break;
                        case CUBA_CUHRE_PARA:
                            do_para_cuhre_map_int((void*) &param, BEAMSPLITTER_CMP, results, init.abserr, init.relerr);
                            break;
#endif
                        default:
                            bug_error("Could not handle a map integration of type %d", map->integration_method);
                    }

                    if (map->save_integration_points) {
                        fflush(idfile);
                        fclose(idfile);
                        idfile = NULL;
                    }
                    
                    if (map->save_interp_file) {
                        fflush(ipfile);
                        fclose(ipfile);
                        ipfile = NULL;
                    }

                    knm->k12[n][m] = results[0];
                    knm->k21[n][m] = results[1];
                    knm->k34[n][m] = results[2];
                    knm->k43[n][m] = results[3];
                    knm->k13[n][m] = results[4];
                    knm->k31[n][m] = results[5];
                    knm->k24[n][m] = results[6];
                    knm->k42[n][m] = results[7];

                    // If we have a mode matched beam we are able to calculate the transposed
                    // elements by applying a simple phase factor
                    if (calc_knm_transpose && !(n1 == n2 && m1 == m2)) {
                        // TODO what qx/qy parameter to use here the input or output one?
                        knm->k12[m][n] = z_by_phr(results[0], 2.0 * gouy(bs->knm_q.qxt1_12) * (n2 - n1 + m2 - m1));
                        knm->k21[m][n] = z_by_phr(results[1], 2.0 * gouy(bs->knm_q.qxt1_21) * (n2 - n1 + m2 - m1));
                        knm->k34[m][n] = z_by_phr(results[2], 2.0 * gouy(bs->knm_q.qxt1_34) * (n2 - n1 + m2 - m1));
                        knm->k43[m][n] = z_by_phr(results[3], 2.0 * gouy(bs->knm_q.qxt1_43) * (n2 - n1 + m2 - m1));
                        knm->k13[m][n] = z_by_phr(results[4], 2.0 * gouy(bs->knm_q.qxt1_13) * (n2 - n1 + m2 - m1));
                        knm->k31[m][n] = z_by_phr(results[5], 2.0 * gouy(bs->knm_q.qxt1_31) * (n2 - n1 + m2 - m1));
                        knm->k24[m][n] = z_by_phr(results[6], 2.0 * gouy(bs->knm_q.qxt1_24) * (n2 - n1 + m2 - m1));
                        knm->k42[m][n] = z_by_phr(results[7], 2.0 * gouy(bs->knm_q.qxt1_42) * (n2 - n1 + m2 - m1));
                    }

                    if (inter.debug & 64) {
                        message("%s: (%d %d) -> (%d %d) %s %s %s %s %s %s %s %s\n",
                                map->name, n1, m1, n2, m2,
                                complex_form(knm->k12[n][m]), complex_form(knm->k21[n][m]),
                                complex_form(knm->k34[n][m]), complex_form(knm->k43[n][m]),
                                complex_form(knm->k13[n][m]), complex_form(knm->k31[n][m]),
                                complex_form(knm->k24[n][m]), complex_form(knm->k42[n][m]));
                    }
                }
            }

            current_coeff++;
            print_progress_and_time(num_coeffs, current_coeff, starttime);
        }
    }

    // we now store the values used to calculate the current knm so later we can 
    // determine whether they need recalculating
    map->knm_calculated = true;
    map->_maxtem = inter.tem;
    map->_lambda = init.lambda;
    map->_nr1 = nr1;
    map->_nr2 = nr2;

    map->bs_knm_q.qxt1_12 = bs->knm_q.qxt1_12;
    map->bs_knm_q.qxt2_12 = bs->knm_q.qxt2_12;
    map->bs_knm_q.qyt1_12 = bs->knm_q.qyt1_12;
    map->bs_knm_q.qyt2_12 = bs->knm_q.qyt2_12;
    map->bs_knm_q.qxt1_21 = bs->knm_q.qxt1_21;
    map->bs_knm_q.qxt2_21 = bs->knm_q.qxt2_21;
    map->bs_knm_q.qyt1_21 = bs->knm_q.qyt1_21;
    map->bs_knm_q.qyt2_21 = bs->knm_q.qyt2_21;
    map->bs_knm_q.qxt1_34 = bs->knm_q.qxt1_34;
    map->bs_knm_q.qxt2_34 = bs->knm_q.qxt2_34;
    map->bs_knm_q.qyt1_34 = bs->knm_q.qyt1_34;
    map->bs_knm_q.qyt2_34 = bs->knm_q.qyt2_34;
    map->bs_knm_q.qxt1_43 = bs->knm_q.qxt1_43;
    map->bs_knm_q.qxt2_43 = bs->knm_q.qxt2_43;
    map->bs_knm_q.qyt1_43 = bs->knm_q.qyt1_43;
    map->bs_knm_q.qyt2_43 = bs->knm_q.qyt2_43;

    map->bs_knm_q.qxt1_13 = bs->knm_q.qxt1_13;
    map->bs_knm_q.qxt2_13 = bs->knm_q.qxt2_13;
    map->bs_knm_q.qyt1_13 = bs->knm_q.qyt1_13;
    map->bs_knm_q.qyt2_13 = bs->knm_q.qyt2_13;
    map->bs_knm_q.qxt1_31 = bs->knm_q.qxt1_31;
    map->bs_knm_q.qxt2_31 = bs->knm_q.qxt2_31;
    map->bs_knm_q.qyt1_31 = bs->knm_q.qyt1_31;
    map->bs_knm_q.qyt2_31 = bs->knm_q.qyt2_31;
    map->bs_knm_q.qxt1_24 = bs->knm_q.qxt1_24;
    map->bs_knm_q.qxt2_24 = bs->knm_q.qxt2_24;
    map->bs_knm_q.qyt1_24 = bs->knm_q.qyt1_24;
    map->bs_knm_q.qyt2_24 = bs->knm_q.qyt2_24;
    map->bs_knm_q.qxt1_42 = bs->knm_q.qxt1_42;
    map->bs_knm_q.qxt2_42 = bs->knm_q.qxt2_42;
    map->bs_knm_q.qyt1_42 = bs->knm_q.qyt1_42;
    map->bs_knm_q.qyt2_42 = bs->knm_q.qyt2_42;
    
    map->_angle = bs->map_rotation;
    map->_AoI = bs->alpha_1;
    map->_r_aperture = bs->r_aperture;
    map->_xbeta = bs->beta_x;
    map->_ybeta = bs->beta_y;
    map->_abserr = init.abserr;
    map->_relerr = init.relerr;
    map->_intermeth = map->interpolation_method;
    map->_intersize = map->interpolation_size;
    map->_intmeth = map->integration_method;

    if (map->save_to_file)
        write_knm_file((void*) bs, false, BEAMSPLITTER_CMP);

    if ((!(map->noMapPresent) || integrating) && map->save_knm_matrices) {
        char buf[LINE_LEN];
        sprintf(buf, "%s_integrated", map->filename);
        write_bs_knm_to_matrix_file(buf, &(bs->knm_map));
    }
}

void get_bs_acc_zc_values(KNM_BS_NODE_DIRECTION_t knum, bs_knm_map_int_params_t *pbs, u_nm_accel_t **a1, u_nm_accel_t **a2, complex_t **zc) {
    switch (knum) {
        case BS12:
            *a1 = pbs->acc_12_nr1_1;
            *a2 = pbs->acc_12_nr1_2;
            *zc = &pbs->znm1c2;
            break;
        case BS21:
            *a1 = pbs->acc_21_nr1_1;
            *a2 = pbs->acc_21_nr1_2;
            *zc = &pbs->znm2c1;
            break;
        case BS34:
            *a1 = pbs->acc_34_nr2_1;
            *a2 = pbs->acc_34_nr2_2;
            *zc = &pbs->znm3c4;
            break;
        case BS43:
            *a1 = pbs->acc_43_nr2_1;
            *a2 = pbs->acc_43_nr2_2;
            *zc = &pbs->znm4c3;
            break;
        case BS13:
            *a1 = pbs->acc_13_nr1_1;
            *a2 = pbs->acc_13_nr2_2;
            *zc = &pbs->znm1c3;
            break;
        case BS31:
            *a1 = pbs->acc_31_nr2_1;
            *a2 = pbs->acc_31_nr1_2;
            *zc = &pbs->znm3c1;
            break;
        case BS24:
            *a1 = pbs->acc_24_nr1_1;
            *a2 = pbs->acc_24_nr2_2;
            *zc = &pbs->znm2c4;
            break;
        case BS42:
            *a1 = pbs->acc_42_nr2_1;
            *a2 = pbs->acc_42_nr1_2;
            *zc = &pbs->znm4c2;
            break;
        default:
            bug_error("Unacceptable knum input");
    }
}
