
/*
 * -- SuperLU routine (version 2.0) --
 * Lawrence Berkeley National Lab, Univ. of California Berkeley,
 * and Xerox Palo Alto Research Center.
 * September 10, 2007
 *
 */
#ifndef __SUPERLU_DCOMPLEX /* allow multiple inclusions */
#define __SUPERLU_DCOMPLEX

/* 
 * This header file is to be included in source files z*.c
 */
#ifndef DCOMPLEX_INCLUDE
#define DCOMPLEX_INCLUDE

typedef struct { double re, im; } complex_t;

typedef complex_t doublecomplex;

/* Macro definitions */

/* Complex Addition c = a + b */
#define z_add(c, a, b) { (c)->re = (a)->re + (b)->re; \
			 (c)->im = (a)->im + (b)->im; }

/* Complex Subtraction c = a - b */
#define z_sub(c, a, b) { (c)->re = (a)->re - (b)->re; \
			 (c)->im = (a)->im - (b)->im; }

/* Complex-Double Multiplication */
#define zd_mult(c, a, b) { (c)->re = (a)->re * (b); \
                           (c)->im = (a)->im * (b); }

/* Complex-Complex Multiplication */
#define zz_mult(c, a, b) { \
	double cr, ci; \
    	cr = (a)->re * (b)->re - (a)->im * (b)->im; \
    	ci = (a)->im * (b)->re + (a)->re * (b)->im; \
    	(c)->re = cr; \
    	(c)->im = ci; \
    }

#define zz_conj(a, b) { \
        (a)->re = (b)->re; \
        (a)->im = -((b)->im); \
    }

/* Complex equality testing */
#define z_eq(a, b)  ( (a)->re == (b)->re && (a)->im == (b)->im )


#ifdef __cplusplus
extern "C" {
#endif

/* Prototypes for functions in dcomplex.c */
void z_div(doublecomplex *, doublecomplex *, doublecomplex *);
double z_abs(doublecomplex *);     /* exact */
double z_abs1(doublecomplex *);    /* approximate */
void z_exp(doublecomplex *, doublecomplex *);
void d_cnjg(doublecomplex *r, doublecomplex *z);
double d_imag(doublecomplex *);


#ifdef __cplusplus
  }
#endif

#endif

#endif  /* __SUPERLU_DCOMPLEX */
