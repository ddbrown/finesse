
/*
 * -- SuperLU routine (version 2.0) --
 * Lawrence Berkeley National Lab, Univ. of California Berkeley,
 * and Xerox Palo Alto Research Center.
 * September 10, 2007
 *
 */
#ifndef __SUPERLU_SCOMPLEX /* allow multiple inclusions */
#define __SUPERLU_SCOMPLEX

/* 
 * This header file is to be included in source files c*.c
 */
#ifndef SCOMPLEX_INCLUDE
#define SCOMPLEX_INCLUDE

typedef struct { float re, im; } complex;


/* Macro definitions */

/* Complex Addition c = a + b */
#define c_add(c, a, b) { (c)->re = (a)->re + (b)->re; \
			 (c)->im = (a)->im + (b)->im; }

/* Complex Subtraction c = a - b */
#define c_sub(c, a, b) { (c)->re = (a)->re - (b)->re; \
			 (c)->im = (a)->im - (b)->im; }

/* Complex-Double Multiplication */
#define cs_mult(c, a, b) { (c)->re = (a)->re * (b); \
                           (c)->im = (a)->im * (b); }

/* Complex-Complex Multiplication */
#define cc_mult(c, a, b) { \
	float cr, ci; \
    	cr = (a)->re * (b)->re - (a)->im * (b)->im; \
    	ci = (a)->im * (b)->re + (a)->re * (b)->im; \
    	(c)->re = cr; \
    	(c)->im = ci; \
    }

#define cc_conj(a, b) { \
        (a)->re = (b)->re; \
        (a)->im = -((b)->im); \
    }

/* Complex equality testing */
#define c_eq(a, b)  ( (a)->re == (b)->re && (a)->im == (b)->im )


#ifdef __cplusplus
extern "C" {
#endif

/* Prototypes for functions in scomplex.c */
void c_div(complex *, complex *, complex *);
double c_abs(complex *);     /* exact */
double c_abs1(complex *);    /* approximate */
void c_exp(complex *, complex *);
void r_cnjg(complex *, complex *);
double r_imag(complex *);


#ifdef __cplusplus
  }
#endif

#endif

#endif  /* __SUPERLU_SCOMPLEX */
