
/*
 * -- SuperLU routine (version 2.0) --
 * Lawrence Berkeley National Lab, Univ. of California Berkeley,
 * and Xerox Palo Alto Research Center.
 * September 10, 2007
 *
 */
/*
 * This file defines common arithmetic operations for complex type.
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "slu_scomplex.h"


/* Complex Division c = a/b */
void c_div(complex *c, complex *a, complex *b)
{
    float ratio, den;
    float abr, abi, cr, ci;
  
    if( (abr = b->re) < 0.)
	abr = - abr;
    if( (abi = b->im) < 0.)
	abi = - abi;
    if( abr <= abi ) {
	if (abi == 0) {
	    fprintf(stderr, "c_div: division by zero\n");
            exit(-1);
	}	  
	ratio = b->re / b->im ;
	den = b->im * (1 + ratio*ratio);
	cr = (a->re*ratio + a->im) / den;
	ci = (a->im*ratio - a->re) / den;
    } else {
	ratio = b->im / b->re ;
	den = b->re * (1 + ratio*ratio);
	cr = (a->re + a->im*ratio) / den;
	ci = (a->im - a->re*ratio) / den;
    }
    c->re = cr;
    c->im = ci;
}


/* Returns sqrt(z.r^2 + z.i^2) */
double c_abs(complex *z)
{
    float temp;
    float real = z->re;
    float imag = z->im;

    if (real < 0) real = -real;
    if (imag < 0) imag = -imag;
    if (imag > real) {
	temp = real;
	real = imag;
	imag = temp;
    }
    if ((real+imag) == real) return(real);
  
    temp = imag/real;
    temp = real*sqrt(1.0 + temp*temp);  /*overflow!!*/
    return (temp);
}


/* Approximates the abs */
/* Returns abs(z.r) + abs(z.i) */
double c_abs1(complex *z)
{
    float real = z->re;
    float imag = z->im;
  
    if (real < 0) real = -real;
    if (imag < 0) imag = -imag;

    return (real + imag);
}

/* Return the exponentiation */
void c_exp(complex *r, complex *z)
{
    float expx;

    expx = exp(z->re);
    r->re = expx * cos(z->im);
    r->im = expx * sin(z->im);
}

/* Return the complex conjugate */
void r_cnjg(complex *r, complex *z)
{
    r->re = z->re;
    r->im = -z->im;
}

/* Return the imaginary part */
double r_imag(complex *z)
{
    return (z->im);
}


