#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "formulc.h"

int main (int argc, char *argv[])
{

  formu f,g;
  int length;
  int err;
  double x,y;
  double result;

  f = translate("(x+4)/y", "xy", &length, &err);
  
  x=3.0;
  y=2.0;

  result=fval(f,"xy",x,y);  /* 3.0, not 3 */

  printf(" x= %g, f= %g\n",x,result);


  return 0;
}
