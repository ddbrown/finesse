#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nicsluc.h"


int main(int argc, char *argv[])
{
    (void)argc;
	int ret;
	uint__t n, nnz;
	SNicsLUc *nicslu;
	complex__t *x;
	real__t err;
    char c;
    
    uint__t ni=0, np=0;
    uint__t *ai=NULL, *ap=NULL;
    complex__t *rhs=NULL, *ax=NULL;
    
	n = 0;
	nnz = 0;
	x = NULL;
    
    const char* file = "klu_full_matrix_car.dat";
    printf("Reading file...\n");
    FILE *f = fopen(file, "r");
    
    while ( (c=fgetc(f)) != EOF ) {
        if ( c == '\n' )
            nnz++;
    }
    
    ai = (uint__t*) malloc(nnz * sizeof(uint__t));
    ap = (uint__t*) malloc(nnz * sizeof(uint__t));
    ax = (complex__t*) malloc(nnz * sizeof(complex__t));

    char line[200] = {0};
    rewind(f);
    unsigned int prevcol = 0;
    while(fgets(line, 200, f) != NULL){
        uint__t row=0, col=0;
        double re, im;
        
        sscanf(line, "%u %u %lf %lf", &row, &col, &re, &im);
        
        // from matlab to c indices
        row--;
        col--;
        
        ai[ni] = row;
        
        ax[ni].real = re;
        ax[ni].imag = im;

        if(ni==0 || col != prevcol){
            ap[np] = ni;
            prevcol = col;
            np++;
        }

        ni++;
    }
    
    ap[np] = nnz;
    
    rhs = (complex__t*) malloc((np+1) * sizeof(complex__t));
    rhs[0].real = 1.0;
    n = np;
    
    printf("NNZ = %i\nRows = %i\nCols = %i\n", nnz, ni, np);
    
    printf("Done reading file...\n");
    
	nicslu = (SNicsLUc *)malloc(sizeof(SNicsLUc));
	NicsLUc_Initialize(nicslu);

	x = (complex__t *)malloc(sizeof(complex__t)*n);
	memcpy(x, rhs, sizeof(complex__t)*n);

	NicsLUc_CreateMatrix(nicslu, n, nnz, ax, ai, ap);
    // set to non-zero to signify we are using CSC format
    nicslu->cfgi[0] = 1;
    
	NicsLUc_Analyze(nicslu);
	printf("analysis time: %.8g\n", nicslu->stat[0]);
    
	ret = NicsLUc_CreateScheduler(nicslu);
	printf("time of creating scheduler: %.8g\n", nicslu->stat[4]);
	printf("suggestion: %s.\n", ret==0?"parallel":"sequential");

	NicsLUc_CreateThreads(nicslu, atoi(argv[1]), TRUE);
	printf("total cores: %d, threads created: %d\n", (int)(nicslu->stat[9]), (int)(nicslu->cfgi[5]));

	NicsLUc_BindThreads(nicslu, FALSE);

	NicsLUc_Factorize_MT(nicslu);
	printf("factorization time: %.8g\n", nicslu->stat[1]);

	NicsLUc_ReFactorize_MT(nicslu, ax);
	printf("re-factorization time: %.8g\n", nicslu->stat[2]);

	NicsLUc_SolveFast(nicslu, x);
	printf("substitution time: %.8g\n", nicslu->stat[3]);

	NicsLUc_Residual(n, ax, ai, ap, x, rhs, &err, 0);
	printf("Ax-b: %.8g\n", err);

	printf("NNZ(L+U-I): %ld\n", nicslu->lu_nnz);

	NicsLUc_Flops(nicslu, NULL);
	NicsLUc_Throughput(nicslu, NULL);
	printf("flops: %.8g\n", nicslu->stat[5]);
	printf("throughput (bytes): %.8g\n", nicslu->stat[12]);
	NicsLUc_MemoryUsage(nicslu, NULL);
	printf("memory (Mbytes): %.8g\n", nicslu->stat[21]/1024./1024.);
	
	NicsLUc_Destroy(nicslu);
	free(nicslu);
	free(x);
    free(ax);
    free(ai);
    free(ap);
    free(rhs);
	return 0;
}
