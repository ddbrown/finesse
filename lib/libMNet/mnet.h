/*
	Header file for libMNet
  
  version:
  release date:
  
  M Hewitson
	
	$Id: mnet.h 183 2006-03-31 08:52:15Z hewitson $
*/

#ifndef MNET_H
#define MNET_H

#include <unistd.h>       
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <dirent.h>
#include <fnmatch.h>
#include <math.h>


/********* Structures *************/


/*!
	structure for input/output stream
*/

//! definitions for last stream event
#define WRITE_EVENT 1
#define READ_EVENT 0

typedef struct mstream
{
	//! the input stream
	FILE *in;		
	//! the output stream
	FILE *out;		
	//! 0 => last = input/read, 1 => last = output/write
	int last;		
	
} mstream;


	
/*****************************************************/




/********* Function prototypes ****************/


/*--------------- from net.c ---------------*/

int netinit(mstream *s, int sd);
int sendString(int socketfd, char *s);
int readString(int socketfd, char *s, int len);			// read a string
int sendShort(int socketfd, short int si);				// send a short int
int readShort(int socketfd, short int *si);				// read a short int
int sendLong(int socketfd, long int si);				// send a long int
int readLong(int socketfd, long int *li);				// read long int
int freadDataF(mstream *s, float **data, int nsamples);
int fsendDataF(mstream *s, float *data, int nsamples);
int fsendDataD (mstream * s, double *data, int nsamples);
int freadDataD (mstream * s, double **data, int nsamples);

int fsendFloat(mstream *s, float f);
int freadFloat(mstream *s, float *f);
int fsendDouble (mstream * s, double d);
int freadDouble (mstream * s, double *d);

int freadLong(mstream *s, long int *li);					// read a long from stream
int fsendLong(mstream *s, long int li);					// send a long to stream
int freadShort(mstream *s, short int *si);					// read a short from stream
int fsendShort(mstream *s, short int si);					// send a short to stream
int freadString(mstream *s, char **buff, int *buff_size);	// read a string from stream
int fsendString(mstream *s, char *str);						// send a string to stream
int freadToFile(mstream *s, int fd, int nbytes);


int fsendData(mstream *s, char *data, int nbytes);		// send char data
int freadData(mstream *s, char **data, int nbytes);		// read char data
int freadDataS(mstream *s, short int **data, int nsamples);
int fsendDataS(mstream *s, short int *data, int nsamples);
int freadDataL(mstream *s, long int **data, int nsamples);
int fsendDataL(mstream *s, long int *data, int nsamples);

int readn(int fd, void *vptr, size_t n);
int writen(int fd, const void *vptr, size_t n);

double unpack_double(const char *p,  /* Where the high order byte is */
              int incr);       /* 1 for big-endian; -1 for little-endian */

int pack_double( double x, /* The number to pack */
                        char *p,  /* Where to pack the high order byte */
                        int incr); /* 1 for big-endian; -1 for little-endian */


/*------------------------------------------*/



#endif /* MNET_H */
