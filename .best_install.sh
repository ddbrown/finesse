#!/bin/bash

# get working directory
CWD=`pwd`

# Build finesse (or get a prebuilt one if we can find it)
COMMIT=`git rev-parse HEAD`
HOST=$BESTDATASERVER
PRE="/filestorage/"
URI="/commit/${COMMIT}"

echo "Searching database for file, using url: ${HOST}${PRE}list${URI}"

FLIST=`wget --no-check-certificate -O- "${HOST}${PRE}list${URI}"`
echo "Filelist found:"
echo $FLIST

if (echo $FLIST | grep -q "kat.o")
then
  echo "Precompiled binary found, downloading..."
  wget --no-check-certificate -O kat.ini "${HOST}${PRE}download${URI}:kat.ini"
  wget --no-check-certificate -O kat "${HOST}${PRE}download${URI}:kat.o"
  chmod +x kat
else
  echo "No precompiled finnesse binary found"
  ./finesse.sh --build #compile
  curl --insecure -F "file=@kat;filename=kat.o" "${HOST}${PRE}upload${URI}"
  curl --insecure -F "file=@kat.ini;filename=kat.ini" "${HOST}${PRE}upload${URI}"
  echo "New binary saved for re-use"
fi

# Install numpy (required for tests)
conda install numpy requests -y

# update finessetest repo
cd $FINESSE_TEST_DIR
git pull
cd $CWD
ln -s $FINESSE_TEST_DIR test

# check if we are in the extended tests enviroment
if [ "$FINESSE_EXTENDED" = true ]; then
  echo "We are in extended mode, lets get pykat"
  cd $PYKAT_DIR
  git pull
  echo 'and install'
  pip install -e .
  cd $CWD
  
  echo 'and any extra requirements'
  conda install --file test/extended_tests/requirements.txt -y
  echo 'Extended mode done'
fi

# Make mytest program
PYTEST="${CWD}/test/test.py"
MYTEST="${CWD}/mytest"

echo "#!/bin/bash" > $MYTEST
echo python $PYTEST '$1' "${HOST}${PRE}upload/test/"'$BEST_TEST_NUMBER'>> $MYTEST
chmod +x $MYTEST

# Make .best_env_var.sh on the fly as well
BESTENV=".best_env_var.sh"
echo "#!/bin/bash" > $BESTENV
echo "export FINESSE_DIR=${CWD}"/ >> $BESTENV
echo export KATINI='${FINESSE_DIR}kat.ini' >> $BESTENV
echo export PATH='${FINESSE_DIR}:$PATH' >> $BESTENV 
