#!/bin/bash

###########################################################################
#
#	Finesse Helper Script
#	Andreas Freise & Daniel Brown
#
#	This build helper script has been modified with the permission of 
#	Oliver Bock from the Einstein@Home Project to work with Finesse.
#
#	Usage can be found by typing ./finesse.sh at the terminal
#
###########################################################################

###########################################################################
#   Copyright (C) 2008-2009 by Oliver Bock                                #
#   oliver.bock[AT]aei.mpg.de                                             #
#                                                                         #
#   This file is part of Einstein@Home.                                   #
#                                                                         #
#   Einstein@Home is free software: you can redistribute it and/or modify #
#   it under the terms of the GNU General Public License as published     #
#   by the Free Software Foundation, version 2 of the License.            #
#                                                                         #
#   Einstein@Home is distributed in the hope that it will be useful,      #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
#   GNU General Public License for more details.                          #
#                                                                         #
#   You should have received a copy of the GNU General Public License     #
#   along with Einstein@Home. If not, see <http://www.gnu.org/licenses/>. #
#                                                                         #
###########################################################################

### globals ###############################################################

ROOT=`pwd`
PATH_ORG="$PATH"
PATH_MINGW="$PATH"
LOGFILE=$ROOT/build.log
CC=gcc

verbose=0

TARGET=0

TARGET_LINUX=1
TARGET_MAC=2
TARGET_WIN32=3
TARGET_WIN64=4
TARGET_CLEAN=6
TARGET_NATIVE=7

BRANCH_DEVELOP=1
BRANCH_STABLE=2

### functions (tools) #############################################################

failure()
{
    echo "************************************" 
    echo "Error detected! Stopping build!"
    echo "`date`" | tee -a $LOGFILE

    if [ -f "$LOGFILE" ]; then
        echo "------------------------------------"
        echo "Please check logfile: `basename $LOGFILE`"
        #echo "These are the final 20 lines:"
        echo "------------------------------------"
        #tail -n 14 $LOGFILE | head -n 20
    fi

    echo "************************************"

    exit 1
}


clean()
{
    cd $ROOT || failure

    echo "Cleaning Finesse files..." | tee -a $LOGFILE
    
    make realclean
    rm kat || failure
}

### functions (features) #############################################################

check_GSL_architecture()
{
		lib1=$gsl_libpath/libgsl.dylib
		lib2=$gsl_libpath/libgsl.a
		lib4=$gsl_libpath/libgslcblas.a
		lib3=$gsl_libpath/libgslcblas.dylib
        
		if [ -f $lib2 ]
		then
				echo "Found libgsl.a, use static linking" 
				export GSL_LIBS="$lib2 $lib4"
				
				echo $CPUARCH
				if [[ $platform == "mac" && -z $CPUARCH ]] 
				then
						libinfo=`file $lib2`
						if [[  "$libinfo" != *i386* && "$libinfo" != *i686* ]] 
						then
						    echo "Trying ar -x ..."
							 testfile=version.o
							 ar -x $lib2 $testfile
							 libinfo=`file $testfile`
							 echo $libinfo
							 rm $testfile
						fi
						if [[ "$libinfo" == *x86_64* ]] 
						then
						CPUARCH="x86_64"					       
						elif [[ "$libinfo" == *i386* || "$libinfo" == *i686* ]]
						then
								CPUARCH="i686"
						else 
						echo " ** Could not determne architecture from GSL libraries"
						exit 1

						fi
						echo "	- setting architecture based on GSL to $CPUARCH " | tee -a $LOGFILE
				fi
		elif [ -f $lib1 ]
		then
				echo "Found libgsl.dylib, use dynamic linking" 
				export GSL_LIBS="$lib1 $lib3"

				if [[ $platform == "mac"  && -z $CPUARCH  ]] 
				then
						libinfo=`file -L $lib1`
						if [[ "$libinfo" == *x86_64* ]] 
						then
								CPUARCH="x86_64"
						elif [[ "$libinfo" == *i386* || "$libinfo" == *i686* ]]
						then
								CPUARCH="i686"
						fi
						echo "	- setting architecture based on GSL to $CPUARCH " | tee -a $LOGFILE
				fi
		else
				echo"  - Could not determine architecture (x86_64 or i686) of GSL library" | tee -a $LOGFILE
		return 1
		fi
}

check_prerequisites()
{

  # setting default compiler 
  export CC
	# check if the src and lib directories are here
	echo "Required source and library folders exist?"
	if [ ! -d "src" ]; then
		echo "	- Source folder is not present, please run with --checkout before building"
		failure
	else
		echo "	- Source folder exists" | tee -a $LOGFILE
	fi
	
	if [ ! -d "lib" ]; then
		echo "	- Library folder is not present, please run with --checkout before building"
		failure
	else
		echo "	- Library folder exists" | tee -a $LOGFILE
	fi
	
    echo "Checking prerequisites..." | tee -a $LOGFILE
		
    # required toolchain
    if [[ $target_arch == "win" ]]; then
    	if [[ $platform == "win" ]]; then
		TOOLS="make gcc ar ranlib"
	else
	    echo "	- Compiling windows version on non-windows machine"	
            TOOLS="i586-mingw32msvc-gcc i586-mingw32msvc-ar i586-mingw32msvc-ranlib"
    	fi
    else
    	TOOLS="gcc ar ranlib"
    fi
		
    for tool in $TOOLS; do
		if which ls &> /dev/null; then
			echo "	- Found \"$tool\"..." | tee -a $LOGFILE
		else
			echo "	- Missing \"$tool\" which is required tool!" | tee -a $LOGFILE
            return 1
		fi
    done
	
    if [[ "$platform" == 'win32' ]]; then
        echo "	- GSL found!" | tee -a $LOGFILE
    elif [[ "$platform" == 'win64' ]]; then
        echo "	- GSL found!" | tee -a $LOGFILE
	elif ! (which gsl-config &> /dev/null); then
			echo "	- Missing GSL which is a required library" | tee -a $LOGFILE
			return 1
	else
            if [[ "$platform" == 'win' ]];
            then
		        echo "	- GSL found!" | tee -a $LOGFILE
		    else            
                gsl_libpath=`gsl-config --libs | cut -d " " -f1`
                gsl_libpath=${gsl_libpath#-L}
                gsl_libpath=${gsl_libpath#-l}
                echo "	- GSL found! ($gsl_libpath)" | tee -a $LOGFILE
                check_GSL_architecture
            fi
	fi
	
  echo "	- Required toolchain found!" | tee -a $LOGFILE
				
	return 0
}

build()
{
		
		pwdbefore=`pwd`
        
        if [[ $platform != "win32" && $platform != "win64" ]] ; then
            echo "Configuring CUBA" | tee -a $LOGFILE
            # configure cuba if needed
            if [ ! -f "./lib/Cuba-3.2/config.h" ] && [ ! -f "./lib/Cuba-3.2/makefile" ] ; then
                    cd ./lib/Cuba-3.2
                    chmod 700 ./configure

                    if [[ $platform == "mac" ]]
                    then
                            echo "Configuring Cuba with extra flags (CPUARCH=$CPUARCH)"
                            ./configure -q CFLAGS="-O3 -fomit-frame-pointer -ffast-math -Wall -Wextra -arch $CPUARCH"
                    else
                            ./configure -q CFLAGS="-O3 -fomit-frame-pointer -ffast-math -Wall -Wextra"
                    fi
                    rm libcuba.a # removing this to trigger rebuild
                    # These CFLAGS are those generated by the configure script on a clean system
            # We set them here explicitly in order to overwrite the previously exported
            # CFLAGS for the rest of the build (because they cause Cuba to crash with a 
            # segmentation fault).

                    cd $pwdbefore
            else
                    echo "	- Cuba is already configured"
            fi
        fi

		#------------ don't know why this is here ---------------------
		#gsl_libpath=`gsl-config --libs | cut -d " " -f1`
		#gsl_libpath=${gsl_libpath#-L}
		#gsl_libpath=${gsl_libpath#-l}
		# Setting GSL_LIBS flags to allow static linking
    #export GSL_LIBS="$gsl_libpath/libgslcblas.a $gsl_libpath/libgsl.a"

		echo "Calling make file, see make.log for more details..."
		echo ""

		if [ $verbose == 1 ]
		then
				make ARCH=$target_arch BUILD=$platform config 2>&1 | tee make.log || failure
				make ARCH=$target_arch BUILD=$platform kat 2>&1 | tee make.log || failure
		else
				make ARCH=$target_arch BUILD=$platform config 2>&1 1>make.log || failure
				make ARCH=$target_arch BUILD=$platform kat 2>&1 1>make.log || failure
		fi

		if grep " Error " make.log 1>/dev/null 2>&1
		then
				#cat make.err > $LOGFILE
				failure
		else
				return 0;
		fi
}

make-win-package()
{
	python getWinDLLs.py
	
    sys=`eval uname --machine`
    ver=`eval git describe --abbrev=0`
	echo "Making Windows $sys package..." | tee -a $LOGFILE
    rm -rf .windlls
    python getWinDLLs.py
    
    dir=FINESSE_"$ver"_WIN_"$sys"
    
    rm -rf $dir
	mkdir $dir
	cd $dir
	
	cp ../.windlls/*.dll .
	cp ../kat.exe .
    cp ../kat.ini .
	cp ../CHANGES .
	cp ../LICENSES .
	cp ../README .
	cp ../INSTALL .
	cp ../test.kat .
    cp ../test_plot.kat .
    cp ../install.bat .
    cp ../findGnuplot.bat .

    a=`which cygpath`    
    cp $a .
    
	echo "Created Windows distribution $dir" | tee -a $LOGFILE
	
	cd ..
}

print_usage()
{
    cd $ROOT

    echo "******************************************************************************"
    echo "Usage: `basename $0` <option> <--checkout: option>"
    echo
		echo "Run with the checkout command first, this fetches the latest stable"
		echo "version of Finesse source. You can then build it by using the system"
		echo "specific build argument to get the required version."
		echo
    echo "Available Arguments:"
		echo "  --build"
		echo "       Builds Finesse executable that is native to the system you are running"
		echo ""
		echo "  --build-linux"
		echo "  --build-linux32"
    echo "  --build-mac"
    echo "  --build-mac32"
    echo "  --build-win32"
    echo "  --build-win64"
		echo "       Choose the system which you would like to build an executable for."
		echo ""
    echo "  --clean"
		echo "       This cleans up all the build files. Should be used if you have"
		echo "       swapped branches."
		echo ""
		echo "  --make-win-package"
		echo "       This creates the binary distribution for win32. Only runs on Cygwin"
    echo "       capable Windows machines."
    echo "******************************************************************************"

    echo "Wrong usage. Stopping!" >> $LOGFILE

    return 0
}

### main control ##########################################################

# crude command line parsing :-)

if [ $# -ne 1 ]; then
	if [ $# -ne 2 ]; then
		print_usage
		exit 1
	fi
fi

echo "************************************" | tee -a $LOGFILE
echo "Starting new build!" | tee -a $LOGFILE
echo "`date`" | tee -a $LOGFILE
echo "************************************" | tee -a $LOGFILE


platform='unknown'
unamestr=`uname`
unamem=`uname -m`

if [[ "$unamestr" == 'Linux' ]]; then
   platform='linux'
elif [[ "$unamestr" == 'Darwin' ]]; then
   platform='mac'
elif [[ "$unamestr" == *CYGWIN* ]]; then
    if [[ "$unamem" == 'i686' ]]; then
        platform='cygwin32'
    else
        platform='cygwin64'
    fi
elif [[ "$unamestr" == *MINGW* ]]; then
    if [[ "$unamem" == 'i686' ]]; then
        platform='win32'
    else
        platform='win64'
    fi
else
   echo "Platform could not be determined ($unamestr)" | tee -a $LOGFILE
   failure
fi

echo 'Running on "'$platform'"'
		
case "$1" in
    "--build")
		    NATIVE="-march=native"
		    CPUARCH=""
		    target_arch=$platform
            
            if [[ "$platform" == 'linux' ]]; then
		       TARGET=$TARGET_LINUX
		    elif [[ "$platform" == 'mac' ]]; then
		       TARGET=$TARGET_MAC
            elif [[ "$platform" == 'cygwin32' ]]; then
		       TARGET=$TARGET_WIN32
                CPUARCH=
                target_arch=win32
            elif [[ "$platform" == 'cygwin64' ]]; then
		       TARGET=$TARGET_WIN64
                CPUARCH=
                target_arch=win64
		    elif [[ "$platform" == 'win32' ]]; then
		       TARGET=$TARGET_WIN32
                CPUARCH=
                target_arch=win64
            elif [[ "$platform" == 'win64' ]]; then
		       TARGET=$TARGET_WIN64
                CPUARCH=
                target_arch=win64
		    else
		       echo "Platform Target could not be determined" | tee -a $LOGFILE
		       failure
		    fi   
		
        echo "Building native Finesse version:" | tee -a $LOGFILE        
        ;;
    "--build-linux")
		    NATIVE=""
            TARGET=$TARGET_LINUX   
		        ="-m64"
		    target_arch="linux"
        echo "Building Linux version    :" | tee -a $LOGFILE        
        ;;
    "--build-linux32")
		    NATIVE=""
            TARGET=$TARGET_LINUX   
		    CPUARCH="-m32"
		    target_arch="linux"
        echo "Building Linux 32bit version:" | tee -a $LOGFILE        
        ;;
    "--build-mac32")
		    NATIVE=""
            TARGET=$TARGET_MAC        
		    CPUARCH="i386"
		    target_arch="mac"
		    echo "Building Mac (Intel) 32bit version:" | tee -a $LOGFILE        
        ;;
    "--build-mac")
		    NATIVE=""
            TARGET=$TARGET_MAC        
		    CPUARCH=""
		    target_arch="mac"
		    echo "Building Mac (Intel) version:" | tee -a $LOGFILE        
        ;;
    "--build-win32")
		    NATIVE=""
            TARGET=$TARGET_WIN32
		    CPUARCH="i686"
		    target_arch="win32"
        echo "Building Win32 version:" | tee -a $LOGFILE        
        ;;
    "--build-win64")
		    NATIVE=""
            TARGET=$TARGET_WIN64     
		    CPUARCH="x86_64"
		    target_arch="win64"
        echo "Building Win64 version:" | tee -a $LOGFILE        
        ;;
    "--checkout")
        echo ""
        echo "ERROR: --checkout is no longer used, use 'git checkout [branch name]' instead." | tee -a $LOGFILE
        echo ""
        ;;	
    "--remove")
		    echo "--remove is no longer used." | tee -a $LOGFILE
        ;;
	  "--clean")
		    TARGET=$TARGET_CLEAN
        ;;
	  "--make-win-package")
		    make-win-package
		    exit 0
        ;;
    *)
        print_usage
        exit 1
        ;;
esac

# here we go...

#export OPTIM_CFLAGS="-mfpmath=sse -O3 -ffast-math -Wall -fexpensive-optimizations -fomit-frame-pointer -funroll-loops -DNDEBUG "

if ( cc 2>&1 | grep clang >/dev/null ); then
		echo "Setting optimisation flags for clang compiler"
		export OPTIM_CFLAGS="-mfpmath=sse -O3 -fno-math-errno -ffinite-math-only -fno-trapping-math -fno-signed-zeros -Wall -Wextra -Wno-missing-field-initializers -fomit-frame-pointer -funroll-loops -DNDEBUG "
else
		echo "Setting optimisation flags for gcc compiler"
		export OPTIM_CFLAGS="-mfpmath=sse -O3 -fno-math-errno -ffinite-math-only -fno-signaling-nans -fno-trapping-math -fno-signed-zeros -Wall -Wextra -Wno-missing-field-initializers -fexpensive-optimizations -fomit-frame-pointer -funroll-loops -DNDEBUG "
fi

case $TARGET in
    $TARGET_LINUX)
        check_prerequisites || failure
				export CFLAGS="$OPTIM_CFLAGS $NATIVE $CPUARCH"
        build || failure
        ;;
    $TARGET_MAC)
        check_prerequisites || failure
				OSXminor=7
                
                #`sw_vers -productVersion | cut -d'.' -f2`
				if [ -z "$OSXminor" ]; then
						echo "Could not determine OSX version"
						failure
				fi
                
				echo "Building for OSX version: 10.$OSXminor"
				# probably should allow custom minor version for build-mac
				export MACOSX_DEPLOYMENT_TARGET="10.$OSXminor"
                
        if [ -d /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk/ ]; then
            echo "Preparing Mac OS X 10.13 SDK build environment..." | tee -a $LOGFILE
            export OSX_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk/"
            export OSX_CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot $OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=13"
            export OSX_LDFLAGS="-isysroot $OSX_SDK -Wl,-syslibroot,$OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS=$OSX_CFLAGS
            export CXXFLAGS=$OSX_CFLAGS
            export LDFLAGS=$OSX_LDFLAGS
		elif [ -d /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.12.sdk/ ]; then
            echo "Preparing Mac OS X 10.12 SDK build environment..." | tee -a $LOGFILE
            export OSX_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.12.sdk/"
            export OSX_CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot $OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=12"
            export OSX_LDFLAGS="-isysroot $OSX_SDK -Wl,-syslibroot,$OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS=$OSX_CFLAGS
            export CXXFLAGS=$OSX_CFLAGS
            export LDFLAGS=$OSX_LDFLAGS
        elif [ -d /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/ ]; then
            echo "Preparing Mac OS X 10.11 SDK build environment..." | tee -a $LOGFILE
            export OSX_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/"
            export OSX_CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot $OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=11"
            export OSX_LDFLAGS="-isysroot $OSX_SDK -Wl,-syslibroot,$OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS=$OSX_CFLAGS
            export CXXFLAGS=$OSX_CFLAGS
            export LDFLAGS=$OSX_LDFLAGS
       elif [ -d /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk/ ]; then
            echo "Preparing Mac OS X 10.10 SDK build environment..." | tee -a $LOGFILE
            export OSX_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk/"
            export OSX_CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot $OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=10"
            export OSX_LDFLAGS="-isysroot $OSX_SDK -Wl,-syslibroot,$OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS=$OSX_CFLAGS
            export CXXFLAGS=$OSX_CFLAGS
            export LDFLAGS=$OSX_LDFLAGS
      elif [ -d /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk/ ]; then
            echo "Preparing Mac OS X 10.9 SDK build environment..." | tee -a $LOGFILE
            export OSX_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk/"
            export OSX_CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot $OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=9"
            export OSX_LDFLAGS="-isysroot $OSX_SDK -Wl,-syslibroot,$OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS=$OSX_CFLAGS
            export CXXFLAGS=$OSX_CFLAGS
            export LDFLAGS=$OSX_LDFLAGS
	    elif [ -d /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/ ]; then
            echo "Preparing Mac OS X 10.8 SDK build environment..." | tee -a $LOGFILE
            export OSX_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/"
            export OSX_CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot $OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=8"
            export OSX_LDFLAGS=" -isysroot $OSX_SDK -Wl,-syslibroot,$OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS=$OSX_CFLAGS
            export CXXFLAGS=$OSX_CFLAGS
            export LDFLAGS=$OSX_LDFLAGS
        elif [ -d /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk/ ]; then
            echo "Preparing Mac OS X 10.7 SDK build environment..." | tee -a $LOGFILE
            export OSX_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk/"
            export OSX_CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot $OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=7"
            export OSX_LDFLAGS="-isysroot $OSX_SDK -Wl,-syslibroot,$OSX_SDK -arch $CPUARCH -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS=$OSX_CFLAGS
            export CXXFLAGS=$OSX_CFLAGS
            export LDFLAGS=$OSX_LDFLAGS
        elif [ -d /Developer/SDKs/MacOSX10.6.sdk ]; then
            echo "Preparing Mac OS X 10.6 x86_64 SDK build environment..." | tee -a $LOGFILE
            export LDFLAGS="-isysroot /Developer/SDKs/MacOSX10.6.sdk -Wl,-syslibroot,/Developer/SDKs/MacOSX10.6.sdk -arch $CPUARCH $LDFLAGS"
            export CPPFLAGS="-isysroot /Developer/SDKs/MacOSX10.6.sdk -arch $CPUARCH $CPPFLAGS -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot /Developer/SDKs/MacOSX10.6.sdk -arch $CPUARCH $CFLAGS -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET  -DMACOSX_SDK=6"
            export CXXFLAGS="-isysroot /Developer/SDKs/MacOSX10.6.sdk -arch $CPUARCH $CXXFLAGS"
            export SDKROOT="/Developer/SDKs/MacOSX10.6.sdk"
        elif [ -d /Developer/SDKs/MacOSX10.5u.sdk ]; then
            echo "Preparing Mac OS X 10.5 i386 SDK build environment..." | tee -a $LOGFILE
            export LDFLAGS="-isysroot /Developer/SDKs/MacOSX10.5.sdk -Wl,-syslibroot,/Developer/SDKs/MacOSX10.5.sdk -arch $CPUARCH $LDFLAGS"
            export CPPFLAGS="-isysroot /Developer/SDKs/MacOSX10.5.sdk -arch $CPUARCH $CPPFLAGS -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot /Developer/SDKs/MacOSX10.5.sdk -arch $CPUARCH $CFLAGS -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=5"
            export CXXFLAGS="-isysroot /Developer/SDKs/MacOSX10.5.sdk -arch $CPUARCH $CXXFLAGS"
            export SDKROOT="/Developer/SDKs/MacOSX10.5.sdk"
        elif [ -d /Developer/SDKs/MacOSX10.4u.sdk ]; then
            echo "Preparing Mac OS X 10.4 i386 SDK build environment..." | tee -a $LOGFILE
            export LDFLAGS="-isysroot /Developer/SDKs/MacOSX10.4u.sdk -Wl,-syslibroot,/Developer/SDKs/MacOSX10.4u.sdk -arch $CPUARCH $LDFLAGS"
            export CPPFLAGS="-isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch $CPUARCH $CPPFLAGS -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET"
            export CFLAGS="$NATIVE $OPTIM_CFLAGS -isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch $CPUARCH $CFLAGS -DOSX_BUILD_VER=$MACOSX_DEPLOYMENT_TARGET -DMACOSX_SDK=4"
            export CXXFLAGS="-isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch $CPUARCH $CXXFLAGS"
            export SDKROOT="/Developer/SDKs/MacOSX10.4u.sdk"
        else
            echo "Mac OS X 10.4 - 10.12 SDK required but missing!" | tee -a $LOGFILE
            failure
        fi
        #prepare_tree || failure
        build $TARGET_MAC || failure
        ;;
    $TARGET_WIN32)
        check_prerequisites || failure
				export CFLAGS="$CFLAGS -m32 $OPTIM_CFLAGS $NATIVE"
        build || failure
		
        ;;
    $TARGET_WIN64)
        check_prerequisites || failure
				export CFLAGS="$CFLAGS -m64 $OPTIM_CFLAGS $NATIVE"
        build || failure
		
        ;;
    $TARGET_CLEAN)
		    echo "Cleaning up build files, see clean.err and clean.log for more details..."
		    make ARCH=$target_arch BUILD=$platform realclean 2>&1 1>make.log 
        if grep " Error " make.log 1>/dev/null 2>&1
    		then
		  	    #cat make.err > $LOGFILE
			      failure
        fi
	 	    ;;
    *)
        # should be unreachable
        print_usage
        exit 1
        ;;
esac

echo
echo "************************************	" | tee -a $LOGFILE
echo " Finished with no errors              " | tee -a $LOGFILE
echo "`date`" | tee -a $LOGFILE
echo "************************************" | tee -a $LOGFILE

cd $ROOT

exit 0
